<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['namespace' => 'Api', 'middleware' => 'auth'], function () {
    Route::get('mails/{mail}/info', 'MailsController@info')->name('mails.info');
    Route::resource('mails', 'MailsController', ['only' => ['index', 'show', 'update', 'destroy', 'store']]);
});


<?php

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('imap:run', function () {
    $this->call('imap:import', ['--last' => true]);
    $this->call('imap:move-to-folder');
    $this->call('imap:delete-in-folder', ['folder' => 1]);
    $this->call('imap:truncate-deleted');
    $this->call('imap:set-imported-flag');
})->describe('Run all IMAP commands');

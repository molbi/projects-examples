<div class="col-2 bbg-faded sidebar h-100 border-right-gray">
    <ul class="nav nav-pils flex-column">
        <li class="nav-item">
            <a class="nav-link" :class="{ active: $store.state.folder === null }" @click="$store.dispatch('CHANGE_FOLDER', null)">Doručené</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" :class="{ active: $store.state.folder === 4 }" @click="$store.dispatch('CHANGE_FOLDER', 4)">GetSiteControll</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" :class="{ active: $store.state.folder === 2 }" @click="$store.dispatch('CHANGE_FOLDER', 2)">Vyřešené</a>
        </li>
    </ul>
</div>
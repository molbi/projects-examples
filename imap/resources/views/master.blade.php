<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
</head>
<body>
<div id="app">
    <nav class="navbar navbar-toggleable-md bg-gradient border-bottom-gray">
        <button class="navbar-toggler navbar-toggler-right hidden-lg-up" type="button" data-toggle="collapse"
                data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#">Hledamedodavatel.cz - INBOX</a>

        <ul class="nav navbar-nav ml-auto">
            <li class="nav-item">
                @if(auth()->user())
                    <strong>{{ auth()->user()->name }}</strong>
                @else
                    <strong>uživatel nepřihlášen</strong>
                @endif
            </li>
        </ul>

    </nav>

    <div class="container-fluid">
        <div class="row" style="height:calc(100vh - 39.5px)">
            @include('partials.sidebar')
            @yield('content')
        </div>
    </div>
</div>

<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>

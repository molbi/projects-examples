/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('@fancyapps/fancybox');

window.Vue = require('vue');

import store from './store';

import scrollmonitor from 'scrollmonitor';
Object.defineProperty(Vue.prototype, '$scrollmonitor', {
    get() {
        return this.$root.scrollmonitor
    }
});

Vue.component('mails-list', require('./components/mails-list.vue'));
Vue.component('mail-detail', require('./components/mail-detail.vue'));
Vue.component('action-box', require('./components/action-box.vue'));

const moment = require('moment');
require('moment/locale/cs');

Vue.use(require('vue-moment'), {
    moment
});

const app = new Vue({
    data() {
        return {scrollmonitor}
    },
    el: '#app',
    store
});

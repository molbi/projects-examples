import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        mails: [],
        folder: null,
        selected_mail: null,
        selected_mail_info: {},
        current_page: 0,
        total: 0,
        next_page: 0,
    },
    actions: {
        LOAD_MAILS: ({state, commit, dispatch}) => {
            axios.get('api/mails', {
                params: {
                    page: state.next_page,
                    folder: state.folder
                }
            }).then(
                response => {
                    commit('SET', {data: response.data});
                    if(state.selected_mail === null && state.mails.length) {
                        dispatch('SELECT_MAIL', state.mails[0])
                    }
                },
                err => console.log(err)
            )
        },
        ASSIGN_MAILS: ({state, commit, dispatch}) => {
            axios.post('api/mails', {folder: state.folder})
                .then(() => {
                    commit('CLEAR_STATE');
                    dispatch('LOAD_MAILS');
                });
        },
        SELECT_MAIL: ({commit, dispatch}, mail) => {
            commit('SET_MAIL', mail);
            dispatch('LOAD_MAIL_INFO');
        },
        CHANGE_FOLDER: ({commit, dispatch}, folder) => {
            commit('SET_FOLDER', folder);
            commit('CLEAR_STATE');
            dispatch('LOAD_MAILS');
        },
        LOAD_MAIL_INFO: ({state, commit}) => {
            axios.get(state.selected_mail.info_route)
                .then(
                    response => commit('SET_MAIL_INFO', response.data),
                    err => console.log(err)
                )
        },
        SOLVE_MAIL: ({state, commit, dispatch}) => {
            axios.put(state.selected_mail.update_route, {folder: 2})
                .then(() => {
                    commit('CLEAR_STATE');
                    dispatch('LOAD_MAILS');
                });
        },
        REMOVE_MAIL: ({state, commit, dispatch}) => {
            axios.delete(state.selected_mail.destroy_route)
                .then(() => {
                    commit('CLEAR_STATE');
                    dispatch('LOAD_MAILS');
                });
        }
    }


    ,
    mutations: {
        SET: (state, {data}) => {
            state.current_page = data.current_page;
            state.next_page = data.current_page + 1;
            state.total = data.total;

            state.mails = _.orderBy(_.unionWith(state.mails, data.data, _.isEqual), ['delivered_at'], ['asc']);
        },
        SET_MAIL: (state, mail) => {
            state.selected_mail = mail;
        },

        SET_MAIL_INFO: (state, info) => {
            state.selected_mail_info = info;
        },

        SET_FOLDER: (state, folder) => {
            state.folder = folder;
        },

        CLEAR_STATE: (state) => {
            state.mails = [];
            state.selected_mail = null;
            state.current_page = 0;
            state.total = 0;
            state.next_page = 0;
        }
    }
});

export default store
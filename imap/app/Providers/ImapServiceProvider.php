<?php

namespace App\Providers;

use Illuminate\Config\Repository;
use Illuminate\Support\ServiceProvider;
use PhpImap\Mailbox;

class ImapServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Mailbox::class, function ($app) {
            /** @var Repository $config */
            $config = $app['config'];

            return new Mailbox(
                $config->get('imap.path'),
                $config->get('imap.user'),
                $config->get('imap.password'),
                $config->get('imap.attachment_path')
            );
        });
    }
}

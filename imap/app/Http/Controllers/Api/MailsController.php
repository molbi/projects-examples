<?php

namespace App\Http\Controllers\Api;

use App\Models\Mail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * Class MailsController
 * @package App\Http\Controllers\Api
 */
class MailsController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $result = Mail::imported()->forUser(auth()->user())->orderBy('delivered_at');

        if (!is_null($folder = $request->get('folder', null))) {
            $result->where('folder', $folder);
        } else {
            $result->whereNull('folder');
        }

        return response()->json($result->paginate(30));
    }

    /**
     * @param Mail $mail
     * @return \Illuminate\Http\Response
     */
    public function show(Mail $mail)
    {
        return response()->view('mail.show', compact('mail'));
    }

    /**
     * @param Mail $mail
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Mail $mail, Request $request)
    {
        $this->validate($request, [
            'folder' => 'required'
        ]);

        $mail->folder = $request->get('folder');
        $mail->save();

        return response()->json([
            'moved' => true
        ]);
    }

    public function store(Request $request) {
        dd($request->input());
    }

    /**
     * @param Mail $mail
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Mail $mail)
    {
        $mail->delete();

        return response()->json(['deleted' => true]);
    }

    /**
     * @param Mail $mail
     * @return array
     */
    public function info(Mail $mail)
    {
        return response()->json([
            'email_from_text' => $email = $mail->mailFromText(),
            'logout'          => $mail->logoutInfo($email)
        ]);
    }
}

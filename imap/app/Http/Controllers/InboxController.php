<?php

namespace App\Http\Controllers;

/**
 * Class InboxController
 * @package App\Http\Controllers
 */
class InboxController extends Controller
{
    /**
     *
     */
    public function index()
    {
        return response()->view('inbox.index');
    }
}

<?php

namespace App\Console;

use App\Console\Commands\Imap\DeleteInFolder;
use App\Console\Commands\Imap\ImportMails;
use App\Console\Commands\Imap\MoveToFolder;
use App\Console\Commands\Imap\SetImportedFlag;
use App\Console\Commands\Imap\TruncateDeleted;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ImportMails::class,
        MoveToFolder::class,
        DeleteInFolder::class,
        TruncateDeleted::class,
        SetImportedFlag::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('imap:run')->everyTenMinutes()->withoutOverlapping();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}

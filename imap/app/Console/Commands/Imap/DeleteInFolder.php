<?php

namespace App\Console\Commands\Imap;

use App\Models\Mail;
use App\Models\MailError;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use PhpImap\Mailbox;

/**
 * Class DeleteInFolder
 * @package App\Console\Commands
 */
class DeleteInFolder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'imap:delete-in-folder {folder}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete emails in specific folder';
    /**
     * @var Mailbox
     */
    private $mailbox;


    /**
     * Create a new command instance.
     * @param Mailbox $mailbox
     */
    public function __construct(Mailbox $mailbox)
    {
        parent::__construct();

        $this->mailbox = $mailbox;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->output->title('Delete emails in folder "1"');

        /** @var Collection $mails */
        $mails = Mail::select(['id', 'uid'])->where('folder', $this->argument('folder'))->get();

        $bar = $this->output->createProgressBar($mails->count());

        /** @var Mail $mail */
        foreach ($mails as $mail) {
            $this->mailbox->deleteMail($mail->uid);
            $mail->delete();
            $bar->advance();
        }

        $bar->finish();
    }
}

<?php

namespace App\Console\Commands\Imap;

use App\Models\Mail;
use App\Models\MailError;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use PhpImap\Mailbox;

/**
 * Class DeleteInFolder
 * @package App\Console\Commands
 */
class TruncateDeleted extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'imap:truncate-deleted';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Truncate deleted mail older then 1 week';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->output->title('Permament remove deleted mails older then 1 week');

        $deleted = Mail::onlyTrashed()->where('folder', 1)->forceDelete();

        $this->info('The oldest mails was deleted: '. $deleted);

    }
}

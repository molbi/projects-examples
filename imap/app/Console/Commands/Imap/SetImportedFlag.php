<?php

namespace App\Console\Commands\Imap;

use App\Models\Mail;
use App\Models\MailError;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use PhpImap\Mailbox;

/**
 * Class DeleteInFolder
 * @package App\Console\Commands
 */
class SetImportedFlag extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'imap:set-imported-flag';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set flag on the newly imported mails';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->output->title('Set flag on the newly imported mails');

        $updated = Mail::where('imported', false)
            ->withTrashed()
            ->update(['imported' => true]);

        $this->info('Updated: ' . $updated);
    }
}

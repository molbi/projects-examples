<?php

namespace App\Console\Commands\Imap;

use App\Models\Mail;
use App\Models\MailError;
use ErrorException;
use Illuminate\Console\Command;
use PhpImap\Mailbox;

/**
 * Class ImportMails
 * @package App\Console\Commands
 */
class ImportMails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'imap:import {--last}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    /**
     * @var Mailbox
     */
    private $mailbox;

    /**
     * @var int
     */
    private $limit = 100;
    /**
     * @var \stdClass
     */
    private $info;
    /**
     * @var int
     */
    private $total;


    /**
     * Create a new command instance.
     * @param Mailbox $mailbox
     */
    public function __construct(Mailbox $mailbox)
    {
        parent::__construct();

        $this->mailbox = $mailbox;
        $this->info = $this->mailbox->checkMailbox();
        $this->total = $this->info->Nmsgs;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->output->title('Import new email from IMAP');

        $index = 0;
        $bar = $this->output->createProgressBar($this->total);

        if ($this->option('last')) {
            $last = Mail::orderBy('id', 'desc')->first();
            $info = collect($this->mailbox->getMailsInfo([$last->uid]))->first();
            $index = floor($info->msgno / $this->limit);
            $bar->advance($index * $this->limit);
        }

        do {
            $from = ($this->limit * $index) + 1;
            $to = $this->limit * ($index + 1);

            if ($this->total <= $to) {
                $to = $this->total;
            }

            $msgs = imap_fetch_overview($this->mailbox->getImapStream(), sprintf('%s:%s', $from, $to));

            foreach ($msgs as $msg) {
                try {
                    $record = $this->mailRecord($msg);
                    if (!is_null($record)) {
                        Mail::create($record);
                    }
                } catch (\Exception $e) {
                    MailError::create([
                        'uid'     => $msg->uid,
                        'content' => $e->getMessage()
                    ]);
                }

                $bar->advance();
            }

            $index++;
        } while ($this->total > $this->limit * $index);

        $bar->finish();
    }

    /**
     * @param $msg
     * @return array
     */
    private function mailRecord($msg)
    {
        try {
            $info = $this->mailbox->getMail($msg->uid, false);
        } catch (ErrorException $e) {
            return null;
        }

        return [
            'uid'          => $msg->uid,
            'delivered_at' => $info->date,
            'subject'      => $info->subject,
            'from_name'    => $info->fromName,
            'from_email'   => $info->fromAddress,
            'to'           => $info->to,
            'cc'           => $info->cc,
            'reply'        => $info->replyTo,
            'message_id'   => $info->messageId,
            'text_plain'   => base64_encode(gzcompress($info->textPlain, 9)),
            'text_html'    => base64_encode(gzcompress($info->textHtml, 9))
        ];
    }
}

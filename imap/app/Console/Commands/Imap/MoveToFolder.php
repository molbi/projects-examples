<?php

namespace App\Console\Commands\Imap;

use App\Models\Mail;
use Illuminate\Console\Command;

/**
 * Class MoveToFolder
 * @package App\Console\Commands
 */
class MoveToFolder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'imap:move-to-folder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->output->title('Analyze emails and move them to appropriate folder');

        $mails = Mail::select([
            'id',
            'subject',
            'from_email',
            'from_name'
        ])->whereNull('folder')->get();

        $bar = $this->output->createProgressBar($mails->count());

        /** @var Mail $mail */
        foreach ($mails as $mail) {
            $folder = $this->performCheck($mail);

            if (!is_null($folder)) {
                $mail->setAttribute('folder', $folder);
                $mail->save();
            }

            $bar->advance();
        }

        $bar->finish();
    }

    /**
     * @param $mail
     * @return int|null
     */
    private function performCheck($mail)
    {
        if ($this->isUndelivered($mail)) {

            return Mail::FOLDER_UNDELIVERED;
        }

        if ($this->isGsc($mail)) {
            return Mail::FOLDER_GST;
        }

        return null;
    }

    /**
     * @param $mail
     * @return bool
     */
    private function isUndelivered($mail)
    {
        $patterns = [
            'Undelivered',
            'Rejected:',
            'Delivery has failed',
            'Undeliverable',
            'Nedoručitel',
            'No entregable',
            'failure',
            'Returned',
            'Unzustellbar',
            '배달되지 않음',
            'Zprávu nelze doručit',
            'Mail Delivery Subsystem',
            'Mail delivery failed',
            'Doručení bylo zpožděno',
            'NEDORUČITEL',
            'Automatically rejected',
            'Automatická odpověď',
            'Non delivery',
            'Automaticka odpoved',
            'Não entregue',
            'Olevererbart',
            'NEDORUČENO',
            'Onbestelbaar',
            'Non remis',
            'Kan ikke leveres',
            'Nedoručiteľné:',
            'Automatic reply',
            'Auto:',
            'Vacation',
            'Ei voitu toimittaa:',
            'Chybna adresa',
            'Out of office',
            'Auto Response',
            'No se puede entregar',
            'This is an autoreply',
            'no-reply',
            'noreply',
            'Delayed',
            'automatická zpráva',
            'Automatisch',
            'Autoreply',
            'Mimo kancelář',
            'Automatická odpoveď',
            'Automatická odpověď'
        ];
        $regex = '/(' . implode('|', $patterns) . ')/i';

        preg_match($regex, $mail->subject, $matches);

        if (!empty($matches)) {
            return true;
        }

        $patterns = [
            'Mail Delivery Subsystem',
            'Mail Delivery System',
            'Mail Sieve Subsystem'
        ];

        $regex = '/(' . implode('|', $patterns) . ')/i';

        preg_match($regex, $mail->from_name, $matches2);

        return !empty($matches2);
    }

    /**
     * @param $mail
     * @return bool
     */
    private function isGsc($mail)
    {
        $patterns = [
            'GetSiteControl'
        ];
        $regex = '/(' . implode('|', $patterns) . ')/i';

        preg_match($regex, $mail->from_name, $matches);

        return !empty($matches);
    }
}

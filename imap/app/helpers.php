<?php

if (!function_exists('get_sql')) {
    /**
     * @param $builder
     *
     * @return mixed
     */
    function get_sql($builder)
    {
        $sql = $builder->toSql();
        foreach ($builder->getBindings() as $binding) {
            $value = is_numeric($binding) ? $binding : "'" . $binding . "'";
            $sql = preg_replace('/\?/', $value, $sql, 1);
        }

        return $sql;
    }
}
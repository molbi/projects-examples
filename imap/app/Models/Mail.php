<?php

namespace App\Models;

use App\Models\Logout\Record;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Mail
 * @package App\Models
 */
class Mail extends Model
{
    use SoftDeletes;

    /**
     *
     */
    const FOLDER_UNDELIVERED = 1;
    /**
     *
     */
    const FOLDER_SOLVED = 2;
    /**
     *
     */
    const FOLDER_DELETED = 3;
    /**
     *
     */
    const FOLDER_GST = 4;

    /**
     * @var array
     */
    protected $dates = [
        'delivered_at'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'to'    => 'array',
        'cc'    => 'array',
        'reply' => 'array'
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'uid',
        'delivered_at',
        'subject',
        'from_name',
        'from_email',
        'to',
        'cc',
        'reply',
        'message_id',
        'text_plain',
        'text_html'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'text_html',
        'text_plain'
    ];

    /**
     * @var array
     */
    protected $appends = [
        'content_route',
        'info_route',
        'update_route',
        'destroy_route'
    ];

    /**
     * @return mixed
     */
    public function getTextHtmlAttribute()
    {
        return gzuncompress(base64_decode($this->attributes['text_html']));
    }

    /**
     * @return mixed
     */
    public function getTextPlainAttribute()
    {
        return gzuncompress(base64_decode($this->attributes['text_plain']));
    }

    /**
     * @return string
     */
    public function getContentRouteAttribute()
    {
        return route('mails.show', $this);
    }

    /**
     * @return string
     */
    public function getInfoRouteAttribute()
    {
        return route('mails.info', $this);
    }

    /**
     * @return string
     */
    public function getUpdateRouteAttribute()
    {
        return route('mails.update', $this);
    }

    /**
     * @return string
     */
    public function getDestroyRouteAttribute()
    {
        return route('mails.destroy', $this);
    }

    /**
     * @param Builder $builder
     */
    public function scopeImported(Builder $builder)
    {
        $builder->where('imported', true);
    }

    /**
     * @param Builder $builder
     * @param User|null $user
     * @return Builder
     */
    public function scopeForUser(Builder $builder, $user)
    {
        if(is_null($user)) {
            return $builder;
        }

        return $builder->where('user_id', $user->id);
    }

    /**
     *
     */
    public function mailFromText()
    {
        $text = empty($this->text_html) ? $this->text_plain : $this->text_html;
        $text = trim(preg_replace('/\s+/', ' ', $text));

        $patterns = [
            '/Hledat\.cz <br> \(.+- ([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)\)/',
            '/Tento e-mail Vám \((.+)\) byl zaslán na základě zadání poptávky/',
            '/Hledat\.cz \(#(\d+)\)/',
            '/Hledat\.cz ".+- ([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)"/',
            '/Hledat.cz ".* - <a.*>([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)<\/a>/',
            '/ePoptávka.cz \(.+- ([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)\)/',
            '/<br>\(.+- ([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)\)/',
            '/\(.+- ([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)\)/',
            '/\(.*- <a.*">([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)<\/a>\)/'
        ];

        foreach ($patterns as $pattern) {
            preg_match($pattern, $text, $matches);

            if (!empty($matches)) {
                return $matches[1];
            }
        }

        return $this->from_email;

    }

    /**
     * @param $email
     * @return array
     */
    public function logoutInfo($email)
    {
        $email = is_null($email) ? $this->from_email : $email;

        if ($records = Record::wherePattern($email)->with('projects', 'requests.projects')->first()) {
            return $records->toArray();
        }

        return null;
    }
}

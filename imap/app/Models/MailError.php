<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MailError extends Model
{
    protected $fillable = [
        'uid',
        'content'
    ];
}

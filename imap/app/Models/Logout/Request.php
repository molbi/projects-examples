<?php

namespace App\Models\Logout;

use Illuminate\Database\Eloquent\Model;

/**
 * Logout\Models\Request
 *
 * @property integer $id
 * @property integer $record_id
 * @property string $hash
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Logout\Project[] $projects
 * @property-read \App\Models\Logout\Record $record
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Logout\Request whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Logout\Request whereRecordId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Logout\Request whereHash($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Logout\Request whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Logout\Request whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Request extends Model
{
    /**
     * @var string
     */
    protected $connection = 'mysql2';

    /**
     * @var array
     */
    protected $fillable = ['hash'];

    /**
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function projects()
    {
        return $this->belongsToMany(Project::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function record()
    {
        return $this->belongsTo(Record::class);
    }

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'hash';
    }
}

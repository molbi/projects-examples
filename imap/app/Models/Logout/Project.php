<?php

namespace App\Models\Logout;

use Illuminate\Database\Eloquent\Model;

/**
 * Logout\Models\Project
 *
 * @property integer $id
 * @property string $slug
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Logout\Models\Record[] $records
 * @property-read \Illuminate\Database\Eloquent\Collection|\Logout\Models\Request[] $requests
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Logout\Project whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Logout\Project whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Logout\Project whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Logout\Project whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Logout\Project whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Project extends Model
{
    protected $connection = 'mysql2';

    protected $fillable = ['slug', 'name'];

    public function records() {
        return $this->belongsToMany(Record::class)->withTimestamps();
    }

    public function requests() {
        return $this->belongsToMany(Request::class);
    }
}

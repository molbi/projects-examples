<?php

namespace App\Models\Logout;

use Illuminate\Database\Eloquent\Model;

/**
 * Logout\Models\Record
 *
 * @property integer $id
 * @property string $pattern
 * @property boolean $generic
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Logout\Project[] $projects
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Logout\Request[] $requests
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Logout\Record whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Logout\Record wherePattern($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Logout\Record whereGeneric($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Logout\Record whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Logout\Record whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Record extends Model
{
    /**
     * @var string
     */
    protected $connection = 'mysql2';

    /**
     * @var array
     */
    protected $fillable = ['pattern', 'generic'];

    /**
     * @var array
     */
    protected $appends = [
        'send_requests'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'requests'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function projects()
    {
        return $this->belongsToMany(Project::class)->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function requests()
    {
        return $this->hasMany(Request::class);
    }

    /**
     *
     */
    public function getSendRequestsAttribute()
    {
        $projects = collect([]);
        foreach ($this->requests as $request) {
            foreach ($request->projects as $project) {
                $projects[$project->name] = $request->created_at->format('Y-m-d H:i:s');
            }
        }

        return $projects->map(function ($created_at, $name) {
            return [
                'name'       => $name,
                'created_at' => $created_at
            ];
        })->values();
    }

}

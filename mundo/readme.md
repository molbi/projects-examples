## Telemarketing module

This project is our main application for our sales team. There is a lot of controllers and backend solutions and few components in Vue.js.
I have done a lots work on this project. In the app folder is almost all codes.   

Here is few screen shot of Vue.js components and some screens from app itself.

#### Operators view

![Operators view](mundo-operator-view.png "Operators view")

#### Statistics example

![Statistics example](mundo-statistics.png "Statistics example")

#### Move records from user to another

![Move records](mundo-move-tm-records.png "Move records")

#### Leads forwarding

![Leads forwarding](mundo-leads-forwarding.png "Leads forwarfing")

#### Fill campaigns

![Fill campaigns](mundo-fill-campaign.png "Fill campaigns")
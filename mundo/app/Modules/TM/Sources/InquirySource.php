<?php
namespace Mundo\Modules\TM\Sources;

use Illuminate\Database\Query\Builder;
use Mundo\Modules\TM\Sources\Support\DBSource;

class InquirySource extends DBSource
{
    /**
     * Return select for remote IDs
     *
     * @param array|\IteratorAggregate $records
     *
     * @return Builder
     */
    public function getData(array $records)
    {
        $connection = $this->getConnection();
        $ids = array_pluck($records, 'remote_id');
        $data = array_assoc($connection->table('business.Inquiry AS I')
            ->select([
                'I.inquiry_id as _id',
                'I.userAccount_id',
                'I.lang as lang',
                'IL.title',
                'C.contactPersonName as contact_person',
                'C.firmTitle as company',
                'C.email as email',
                'C.phoneNumber as phone_number',
                'C.description as description',
                'C.idNumber as id_number',
                'C.vatNumber as vat_number',
                'A.street as street',
                'A.city as city',
                'A.postalCode as postal_code'
            ])
            ->join('business.InquiryLang AS IL', 'IL.inquiry_id', '=', 'I.inquiry_id')
            ->join('business.Contact AS C', 'C.contact_id', '=', 'I.contact_id')
            ->join('business.Address AS A', 'A.address_id', '=', 'C.address_id')
            ->whereIn('I.inquiry_id', $ids)
            ->get(), '_id');

        return $data;
    }

    public function getName()
    {
        return 'inquiry';
    }
}

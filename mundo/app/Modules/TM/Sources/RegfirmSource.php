<?php
namespace Mundo\Modules\TM\Sources;

use Illuminate\Database\Query\Builder;
use Mundo\Modules\TM\Sources\Support\DBSource;

class RegfirmSource extends DBSource
{

    /**
     * Return select for remote IDs
     * @param  \IteratorAggregate $records
     * @return Builder
     */
    public function getData(array $records)
    {
        $connection = $this->getConnection();
        $ids = array_pluck($records, 'remote_id');
        $data = array_assoc($connection->table('log.RegFirm')
            ->select([
                'RegFirm.id as _id',
                'RegFirm.contact_person as contact_person',
                'RegFirm.title as company',
                'RegFirm.phone_number as phone_number',
                'RegFirm.id_number as id_number',
                'RegFirm.vat_number as vat_number',
                'RegFirm.email as email',
                'RegFirm.lang as lang',
                'RegFirm.street as street',
                'RegFirm.city as city',
                'RegFirm.postal_code as postal_code',
                'RegFirm.web',
            ])
            ->whereIn('RegFirm.id', $ids)
            ->get(), '_id');

        $categories = $this->loadCategories($ids);
        foreach ($data as $id => $item) {
            $item->categories = isset($categories[$id]) ? $categories[$id] : [];
        }

        return $data;
    }

    protected function loadCategories(array $ids)
    {
        $connection = $this->getConnection();

        return array_assoc($connection->table('log.RegFirm_Category')
            ->select(['regFirm_id', 'category_id'])
            ->whereIn('regFirm_id', $ids)
            ->get(), 'regFirm_id[]=category_id');
    }

    public function getName()
    {
        return 'reg-firm';
    }
}

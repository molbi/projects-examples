<?php

namespace Mundo\Modules\TM\Sources\Index;

use Illuminate\Database\Connection;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Mundo\Models\TM\IndexEntry;
use Mundo\Models\TM\Record;

class B2MChangesResolver implements Resolver
{
    /** @var  Connection */
    protected $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function resolve(IndexEntry $entry)
    {
        if (in_array($entry->table, ['business.Firm', 'business.Firm_Category'])) {
            return $this->recordsByRemoteIds($entry->key);
        } elseif ($entry->table === 'business.Address') {
            $id = Arr::first($this->connection->table('business.Firm')
                ->select(['firm_id'])
                ->where('Firm.address_id', $entry->key)
                ->limit(1)
                ->pluck('firm_id'));
            if ($id) {
                return $this->recordsByRemoteIds($id);
            }
        }

        return Collection::make();
    }

    protected function recordsByRemoteIds($remoteId)
    {
        return Record::where('remote_id', $remoteId)
            ->sourceIs('b2m')
            ->get();
    }
}

<?php

namespace Mundo\Modules\TM\Sources\Index;

use Illuminate\Database\Connection;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Mundo\Models\TM\IndexEntry;
use Mundo\Models\TM\Record;

class TmChangesResolver implements Resolver
{
    /** @var  Connection */
    protected $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function resolve(IndexEntry $entry)
    {
        return $this->recordsByRemoteIds($entry->key);
    }

    protected function recordsByRemoteIds($remoteId)
    {
        return Record::where('remote_id', $remoteId)
            ->sourceIs('tm')
            ->get();
    }
}

<?php

namespace Mundo\Modules\TM\Sources\Index;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Mundo\Models\TM\IndexEntry;

/**
 * Class ChangesResolver
 *
 * Watches the DB changes and reindex them based on resolvers for each source.
 * If you want to observe changes you have to change configuration in config/mysql-watcher.json file
 *
 * @package Mundo\Modules\TM\Sources\Index
 */
class ChangesResolver
{
    /** @var Resolver[] */
    protected $resolvers = [];

    /**
     * Load all changes since last time, resolve them to Records instances and reindex them
     */
    public function resolve()
    {
        foreach (IndexEntry::all() as $entry) {
            if ($resolver = $this->getResolver($entry)) {
                $items = $resolver->resolve($entry);
                if (!$items->isEmpty()) {
                    $items->searchable();
                }
                $entry->delete();
            }
        }
    }

    /**
     * Add new resolver
     *
     * @param  String $source
     * @param  \Mundo\Modules\TM\Sources\Index\Resolver $resolver
     * @return $this
     */
    public function addResolver($source, Resolver $resolver)
    {
        $this->resolvers[$source] = $resolver;

        return $this;
    }

    /**
     * Get resolver for given entry (source)
     *
     * @param  \Mundo\Models\TM\IndexEntry $entry
     * @return mixed
     */
    protected function getResolver(IndexEntry $entry)
    {
        return Arr::get($this->resolvers, $entry->source, null);
    }

    /**
     * Reindex collection of records
     *
     * @param array|Collection $records
     */
    protected function reindexRecords($records)
    {
        $records = is_null($records) ? [] : $records;

        foreach ($records as $record) {
            $record->addToIndex();
        }
    }
}

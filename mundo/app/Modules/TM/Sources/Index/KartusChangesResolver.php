<?php

namespace Mundo\Modules\TM\Sources\Index;

use Illuminate\Database\Connection;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Mundo\Models\TM\IndexEntry;
use Mundo\Models\TM\Record;

class KartusChangesResolver implements Resolver
{
    /** @var  Connection */
    protected $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function resolve(IndexEntry $entry)
    {
        if ($entry->table === 'cs_contact.users') {
            return $this->recordsByRemoteIds($entry->key);
        } elseif ($entry->table === 'cs_contact.contacts') {
            $id = Arr::first($this->connection->table('cs_contact.contacts')
                ->select(['contactable_id'])
                ->where('contacts.id', $entry->key)
                ->where('contacts.contactable_type', 'Kartus\Model\User')
                ->limit(1)
                ->pluck('contactable_id'));
            if ($id) {
                return $this->recordsByRemoteIds($id);
            }
        }

        return Collection::make();
    }

    protected function recordsByRemoteIds($remoteId)
    {
        return Record::where('remote_id', $remoteId)
            ->sourceIs('kartus')
            ->get();
    }
}

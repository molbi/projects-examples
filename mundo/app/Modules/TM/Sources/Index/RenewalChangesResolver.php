<?php

namespace Mundo\Modules\TM\Sources\Index;

use Illuminate\Database\Connection;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Mundo\Models\RIS\RenewalFirm;
use Mundo\Models\TM\IndexEntry;
use Mundo\Models\TM\Record;

class RenewalChangesResolver implements Resolver
{
    /** @var  Connection */
    protected $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function resolve(IndexEntry $entry)
    {
        if (in_array($entry->table, ['business.RenewalFirm'])) {
            return $this->recordsByRenewalFirm($entry->key);
        }

        return Collection::make();
    }

    protected function recordsByRenewalFirm($renewalFirm_id)
    {
        return RenewalFirm::where('renewalFirm_id', $renewalFirm_id)->get();
    }
}

<?php

namespace Mundo\Modules\TM\Sources\Index;

use Illuminate\Console\Command as BaseCommand;

/**
 * Index all source changes
 *
 * @package Mundo\Kernel\Synchro
 */
class SyncIndexCommand extends BaseCommand
{
    protected $signature = 'mundo:search:import-changes';

    protected $description = 'Sync index with changes';

    /** @var  ChangesResolver */
    protected $resolver;

    /**
     * Command constructor.
     * @param \Mundo\Modules\TM\Sources\Index\ChangesResolver $resolver
     */
    public function __construct(ChangesResolver $resolver)
    {
        $this->resolver = $resolver;
        parent::__construct();
    }

    /**
     * Execute the command
     */
    public function handle()
    {
        $this->output->title('Indexing DB changes');
        $this->resolver->resolve();
    }
}

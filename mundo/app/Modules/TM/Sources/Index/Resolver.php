<?php

namespace Mundo\Modules\TM\Sources\Index;

use Mundo\Models\TM\IndexEntry;

interface Resolver {
    public function resolve(IndexEntry $entry);
}
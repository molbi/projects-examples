<?php

namespace Mundo\Modules\TM\Sources;

use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\JoinClause;
use Mundo\Modules\TM\Sources\Support\DBSource;

/**
 * Class KartusSource
 *
 * Source data are loaded from: B2M.cs_contact
 *
 * @package Mundo\Modules\TM\Sources
 */
class KartusSource extends DBSource
{
    /**
     * Name of the source
     * @return string
     */
    public function getName()
    {
        return 'kartus';
    }

    /**
     * Return select for remote IDs
     * @param  $records
     * @return Builder
     */
    public function getData(array $records)
    {
        $connection = $this->getConnection();
        $ids = array_pluck($records, 'remote_id');
        $data = array_assoc($connection->table('cs_contact.users')
            ->join('cs_contact.contacts', function (JoinClause $join) {
                $join->on('contacts.contactable_id', '=', 'users.id')
                    ->where('contacts.contactable_type', '=', 'Kartus\\Model\\User');
            })->select([
                'users.id as _id',
                'users.firm_id as firm_id',
                'users.lang as lang',
                'contacts.title as company',
                'contacts.name as contact_person',
                'contacts.email',
                'contacts.phone as phone_number',
                'contacts.id_number',
                'contacts.vat_number',
                'contacts.street',
                'contacts.city',
                'contacts.postal_code',
            ])
            ->whereIn('users.id', $ids)
            ->get(), '_id');

        $paymentMorals = $this->loadPaymentMorals($ids);
        foreach ($data as $id => $item) {
            $item->pm_info = isset($paymentMorals[$id]) ? $paymentMorals[$id] : null;
        }

        return $data;
    }

    protected function loadPaymentMorals(array $ids)
    {
        $connection  = $this->getConnection();

        $innerQuery = $connection->table('obis.Order AS O')
            ->select(['U.id','O.*'])
            ->join('cs_contact.users AS U', function(JoinClause $j) {
                $j->on('U.firm_id', '=', 'O.firm_id')
                    ->where('U.active', '=', 1);
            })
            ->join('obis.Order_Service AS OS', 'OS.order_id', '=', 'O.order_id')
            ->join('obis.Service AS S', 'S.service_id', '=', 'OS.service_id')
            ->join('obis.Service_Item AS SI', 'SI.service_id', '=', 'OS.service_id')
            ->whereNotIn('SI.type', ['cskontakt-micro'])
            ->where('S.price', '>', 0)
            ->whereIn('U.id', $ids)
            ->groupBy('order_id');

        return array_assoc($connection->table($connection->raw(sprintf('(%s) AS O', get_sql($innerQuery))))
            ->select([
                'id',
                $connection->raw('SUM(IF(state = "cancelled", 1, 0)) / COUNT(*) AS payment_morals'),
                $connection->raw('SUM(IF(state = "cancelled", 1, 0)) AS cancelled'),
                $connection->raw('COUNT(*) AS all_orders'),
            ])
            ->groupBy('id')
            ->get(), 'id');
    }
}

<?php

namespace Mundo\Modules\TM\Sources;

use Illuminate\Database\Eloquent\Collection;
use Mundo\Models\TM\Record;
use Mundo\Modules\TM\Sources\Support\ISource;

class InternalSource implements ISource
{

    public function getName()
    {
        return 'internal';
    }

    public function fill(Record $record)
    {
        $data = [];
        foreach ($record->params as $param) {
            $data[$param->key] = $param->value;
        }
        $record->setRemoteData((object)$data);
    }

    public function fillArray($records)
    {
        foreach ($records as $record) {
            $this->fill($record);
        }
    }
}

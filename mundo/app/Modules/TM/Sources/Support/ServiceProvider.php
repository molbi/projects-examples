<?php

namespace Mundo\Modules\TM\Sources\Support;

use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider as BaseProvider;
use Mundo\Modules\TM\Sources\AattroxSource;
use Mundo\Modules\TM\Sources\B2MSource;
use Mundo\Modules\TM\Sources\InquirySource;
use Mundo\Modules\TM\Sources\InternalSource;
use Mundo\Modules\TM\Sources\KartusSource;
use Mundo\Modules\TM\Sources\RegfirmSource;
use Mundo\Modules\TM\Sources\TmSource;
use Mundo\Modules\TM\Sources\UserAccountSource;
use Mundo\Modules\TM\Sources\UseSources;


class ServiceProvider extends BaseProvider
{
    /** @var array */
    protected $sources = [
        'basic' => [
            [InternalSource::class],
        ],
        'db'    => [
            [KartusSource::class, 'b2m'],
            [B2MSource::class, 'b2m'],
            [RegfirmSource::class, 'b2m'],
            [TmSource::class, 'b2m'],
            [InquirySource::class, 'b2m'],
            [UserAccountSource::class, 'b2m'],
            [AattroxSource::class, 'b2m']
        ],
    ];

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(SourceManager::class, function (Application $app) {
            $manager = new SourceManager();
            foreach ($this->sources as $type => $sources) {
                $method = 'create' . ucfirst($type) . 'Source';
                foreach ($sources as $source) {
                    $manager->add(call_user_func_array([$this, $method], $source));
                }
            }

            return $manager;
        });

        $this->app->alias(SourceManager::class, 'tm.sources');
    }

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        DBSource::setConnectionResolver($this->app['db']);
        UseSources::setSourceManger($this->app['tm.sources']);
    }

    /**
     * Create instance of DB source
     * @param $source
     * @return ISource
     */
    protected function createDbSource($source, $connection)
    {
        return $this->createBasicSource($source)->setConnection($connection);
    }

    /**
     * Create instance of basic source
     * @param $source
     * @return ISource
     */
    protected function createBasicSource($source)
    {
        return $this->app->make($source);
    }
}

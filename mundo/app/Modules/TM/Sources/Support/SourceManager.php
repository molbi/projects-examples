<?php

namespace Mundo\Modules\TM\Sources\Support;

/**
 * Class SourceManager
 * @package Mundo\Modules\TM\Sources\Support
 */
class SourceManager
{
    /** @var array */
    protected $sources = [];

    /**
     * Add source to manager
     * @param ISource $source
     */
    public function add(ISource $source)
    {
        $this->sources[$source->getName()] = $source;
    }

    /**
     * Get source
     * @param  string $name
     * @return ISource
     */
    public function get($name)
    {
        if (!$this->has($name)) {
            throw new \InvalidArgumentException("Sourece name [$name] is not registered.");
        }

        return $this->sources[$name];
    }

    /**
     * Is registered source?
     * @param  string $name
     * @return bool
     */
    public function has($name)
    {
        return isset($this->sources[$name]);
    }
}

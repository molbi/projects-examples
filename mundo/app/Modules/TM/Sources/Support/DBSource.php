<?php

namespace Mundo\Modules\TM\Sources\Support;

use Illuminate\Database\Connection;
use Illuminate\Database\ConnectionResolver;
use Illuminate\Database\ConnectionResolverInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\Builder;
use Mundo\Models\TM\Record;

abstract class DBSource implements ISource
{
    /** @var  ConnectionResolverInterface */
    protected static $resolver;

    /** @var  string */
    protected $connection;

    /**
     * Resolve a connection instance.
     *
     * @param  string|null $connection
     * @return \Illuminate\Database\Connection
     */
    public static function resolveConnection($connection = null)
    {
        return static::$resolver->connection($connection);
    }

    /**
     * Get the connection resolver instance.
     *
     * @return \Illuminate\Database\ConnectionResolverInterface
     */
    public static function getConnectionResolver()
    {
        return static::$resolver;
    }

    /**
     * Set the connection resolver instance.
     *
     * @param  \Illuminate\Database\ConnectionResolverInterface $resolver
     * @return void
     */
    public static function setConnectionResolver(ConnectionResolverInterface $resolver)
    {
        static::$resolver = $resolver;
    }

    /**
     * Get the current connection name for the source.
     *
     * @return string
     */
    public function getConnectionName()
    {
        return $this->connection;
    }

    /**
     * Set the connection associated with the model.
     *
     * @param  string $name
     * @return $this
     */
    public function setConnection($name)
    {
        $this->connection = $name;

        return $this;
    }

    /**
     * Get the database connection for the model.
     *
     * @return \Illuminate\Database\Connection
     */
    public function getConnection()
    {
        return static::resolveConnection($this->getConnectionName());
    }

    /**
     * Load external data for record
     *
     * @param \Mundo\Models\TM\Record $record
     */
    public function fill(Record $record)
    {
        if ($record->campaign->source !== $this->getName()) {
            throw new \LogicException('Record is not this type of source');
        }
        $record->setRemoteData(array_first($this->getData([$record])));
    }

    /**
     * Load external data for collection of records
     *
     * @param $records
     */
    public function fillArray($records)
    {
        if (!is_array($records) && !$records instanceof \Traversable) {
            throw new \InvalidArgumentException('Records have to be traversable.');
        }
        foreach ($records as $record) {
            if ($record->campaign->source !== $this->getName()) {
                throw new \LogicException('Records ate not only this type of source');
            }
        }

        $data = array_assoc($this->getData($records), '_id');

        foreach ($records as $record) {
            $record->setRemoteData(isset($data[$record->remote_id]) ? $data[$record->remote_id] : null);
        }
    }

    /**
     * Return select for remote IDs
     * @param  array $records
     * @return Builder
     */
    abstract public function getData(array $records);
}

<?php

namespace Mundo\Modules\TM\Sources\Support;

use Mundo\Models\TM\Record;

interface ISource
{
    public function getName();

    public function fill(Record $record);

    public function fillArray($records);
}

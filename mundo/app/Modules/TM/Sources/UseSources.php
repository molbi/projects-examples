<?php

namespace Mundo\Modules\TM\Sources;

use Mundo\Modules\TM\Sources\Support\SourceManager;

trait UseSources
{
    /** @var  SourceManager */
    protected static $sources;

    /**
     * Set instance for source manager for all instances of model
     * @param \Mundo\Modules\TM\Sources\Support\SourceManager $sources
     */
    public static function setSourceManger(SourceManager $sources)
    {
        static::$sources = $sources;
    }

    /**
     * Instance of source manager
     *
     * @return \Mundo\Modules\TM\Sources\Support\SourceManager
     */
    public static function getSourceManager()
    {
        return static::$sources;
    }
}

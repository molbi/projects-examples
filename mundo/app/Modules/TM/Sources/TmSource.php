<?php
namespace Mundo\Modules\TM\Sources;

use Illuminate\Database\Query\Builder;
use Mundo\Modules\TM\Sources\Support\DBSource;

class TmSource extends DBSource
{

    /**
     * Return select for remote IDs
     * @param  \IteratorAggregate $records
     * @return Builder
     */
    public function getData(array $records)
    {
        $connection = $this->getConnection();
        $ids = array_pluck($records, 'remote_id');
        $data = array_assoc($connection->table('tm.Firm')
            ->select([
                'Firm.firm_id as _id',
                'Firm_ContactPerson.contactPerson as contact_person',
                'Firm.title as company',
                'Firm.lang as lang',
                'Firm.street as street',
                'Firm.city as city',
                'Firm.postalCode as postal_code',
                'Firm.description as description',
                'Firm.web',
                'Firm_Email.email as email',
                $connection->raw('group_concat(`Firm_PhoneNumber`.`phoneNumber` SEPARATOR ", ") as phone_number')
            ])
            ->leftJoin('tm.Firm_PhoneNumber', 'Firm_PhoneNumber.firm_id', '=', 'Firm.firm_id')
            ->leftJoin('tm.Firm_ContactPerson', 'Firm_ContactPerson.firm_id', '=', 'Firm.firm_id')
            ->leftJoin('tm.Firm_Email', 'Firm_Email.firm_id', '=', 'Firm.firm_id')
            ->whereIn('Firm.firm_id', $ids)
            ->groupBy('Firm.firm_id')
            ->get(), '_id');

        $categories = $this->loadCategories($ids);
        foreach ($data as $id => $item) {
            $item->categories = isset($categories[$id]) ? $categories[$id] : [];
        }

        return $data;
    }

    protected function loadCategories(array $ids)
    {
        $connection = $this->getConnection();

        return array_assoc($connection->table('tm.Firm_Category')
            ->select(['firm_id', 'category_id'])
            ->whereIn('firm_id', $ids)
            ->get(), 'firm_id[]=category_id');
    }

    public function getName()
    {
        return 'tm';
    }


}

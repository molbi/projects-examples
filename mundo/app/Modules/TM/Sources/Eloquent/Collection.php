<?php

namespace Mundo\Modules\TM\Sources\Eloquent;

use Illuminate\Database\Eloquent\Collection as BaseCollection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Mundo\Models\TM\Record;

/**
 * Class Collection
 *
 * Collection with remote loading support
 *
 * @package Mundo\Modules\TM\Sources\Eloquent
 */
class Collection extends BaseCollection
{
    use RemoteLoadable;

    /**
     * Paginate the given query.
     *
     * @param  int      $perPage
     * @param  string   $pageName
     * @param  int|null $page
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     *
     * @throws \InvalidArgumentException
     */
    public function paginate($perPage = null, $pageName = 'page', $page = null)
    {
        $total = count($this->items);
        $items = $this->forPage($page = $page ?: Paginator::resolveCurrentPage($pageName), $perPage);

        return new LengthAwarePaginator($items, $total, $perPage, $page, [
            'path'     => Paginator::resolveCurrentPath(),
            'pageName' => $pageName,
        ]);
    }

    public function loadServices()
    {
        $servicesManager = Record::getServicesManager();
        $servicesManager->fillArray($this, 'b2m');

        return $this;
    }
}

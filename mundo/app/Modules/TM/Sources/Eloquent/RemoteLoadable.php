<?php

namespace Mundo\Modules\TM\Sources\Eloquent;

use Mundo\Models\TM\Record;

trait RemoteLoadable
{
    /**
     * Loads effectively data from remote sources
     * @return RemoteLoadable
     */
    public function withRemote()
    {
        $sourceManager = Record::getSourceManager();

        foreach ($this->splitBySources() as $source => $records) {
            $sourceManager->get($source)->fillArray($records);
        }

        return $this;
    }

    /**
     * Split collection to arrays by
     * @return array
     */
    protected function splitBySources()
    {
        $result = [];
        foreach ($this as $record) {
            $result[$record->campaign->source][] = $record;
        }

        return $result;
    }
}

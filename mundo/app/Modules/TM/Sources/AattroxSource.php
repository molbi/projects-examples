<?php
namespace Mundo\Modules\TM\Sources;

use Illuminate\Database\Query\Builder;
use Mundo\Modules\TM\Sources\Support\DBSource;

class AattroxSource extends DBSource
{
    /**
     * Return select for remote IDs
     *
     * @param array|\IteratorAggregate $records
     *
     * @return Builder
     */
    public function getData(array $records)
    {
        $connection = $this->getConnection();
        $ids = array_pluck($records, 'remote_id');
        $data = array_assoc($connection->table('aattrox.leads AS L')
            ->select([
                'L.id as _id',
                'L.lang as lang',
                'L.email as email',
                'L.phone as phone_number',
                'L.contact_person as contact_person'
            ])
            ->whereIn('L.id', $ids)
            ->get(), '_id');

        return $data;
    }

    public function getName()
    {
        return 'aattrox';
    }
}

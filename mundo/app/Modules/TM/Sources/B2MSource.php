<?php
namespace Mundo\Modules\TM\Sources;

use Illuminate\Database\Query\Builder;
use Mundo\Modules\TM\Sources\Support\DBSource;

class B2MSource extends DBSource
{

    /**
     * Return select for remote IDs
     * @param array|\IteratorAggregate $records
     * @return Builder
     */
    public function getData(array $records)
    {
        $connection = $this->getConnection();
        $ids = array_pluck($records, 'remote_id');
        $data = array_assoc($connection->table('business.Firm')
            ->select([
                'Firm.firm_id as _id',
                'Firm.contactPerson as contact_person',
                'Firm.title as company',
                'RegionLang.title as region',
                'Firm.phoneNumber as phone_number',
                'Firm.description as description',
                'Firm.idNumber as id_number',
                'Firm.vatNumber as vat_number',
                'Firm.email as email',
                'Firm.lang as lang',
                'Address.street as street',
                'Address.city as city',
                'Address.postalCode as postal_code',
                'Firm.description as description',
                'Firm.web',
                'Firm.faxNumber as fax',
                'Firm.ID_BCFirm'
            ])
            ->leftJoin('business.Address', 'Address.address_id', '=', 'Firm.address_id')
            ->leftJoin('business.RegionLang', 'Firm.region_id', '=', 'RegionLang.region_id')
            ->whereIn('firm_id', $ids)
            ->get(), '_id');

        $categories = $this->loadCategories($ids);
        foreach ($data as $id => $item) {
            $item->categories = isset($categories[$id]) ? $categories[$id] : [];
        }

        $paymentMorals = $this->loadPaymentMorals($ids);
        foreach ($data as $id => $item) {
            $item->pm_info = isset($paymentMorals[$id]) ? $paymentMorals[$id] : null;
        }

        return $data;
    }

    protected function loadCategories(array $ids)
    {
        $connection = $this->getConnection();

        return array_assoc($connection->table('business.Firm_Category')
            ->select(['firm_id', 'ID_BCFirmRegion as category_id'])
            ->whereIn('firm_id', $ids)
            ->get(), 'firm_id[]=category_id');
    }

    protected function loadPaymentMorals(array $ids)
    {
        $connection  = $this->getConnection();

        $innerQuery = $connection->table('obis.Order AS O')
            ->select(['O.*'])
            ->join('obis.Order_Service AS OS', 'OS.order_id', '=', 'O.order_id')
            ->join('obis.Service AS S', 'S.service_id', '=', 'OS.service_id')
            ->join('obis.Service_Item AS SI', 'SI.service_id', '=', 'OS.service_id')
            ->whereNotIn('SI.type', ['cskontakt-micro'])
            ->where('S.price', '>', 0)
            ->whereIn('firm_id', $ids)
            ->groupBy('order_id');

        return array_assoc($connection->table($connection->raw(sprintf('(%s) AS O', get_sql($innerQuery))))
            ->select([
                'firm_id',
                $connection->raw('SUM(IF(state = "cancelled", 1, 0)) / COUNT(*) AS payment_morals'),
                $connection->raw('SUM(IF(state = "cancelled", 1, 0)) AS cancelled'),
                $connection->raw('COUNT(*) AS all_orders'),
            ])
            ->groupBy('firm_id')
            ->get(), 'firm_id');
    }

    public function getName()
    {
        return 'b2m';
    }
}

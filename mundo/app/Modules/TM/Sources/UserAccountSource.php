<?php
namespace Mundo\Modules\TM\Sources;

use Illuminate\Database\Query\Builder;
use Mundo\Modules\TM\Sources\Support\DBSource;

class UserAccountSource extends DBSource
{
    /**
     * Return select for remote IDs
     *
     * @param array|\IteratorAggregate $records
     *
     * @return Builder
     */
    public function getData(array $records)
    {
        $connection = $this->getConnection();
        $ids = array_pluck($records, 'remote_id');
        $data = array_assoc($connection->table('business.UserAccount AS UA')
            ->select([
                'UA.userAccount_id as _id',
                'C.contactPersonName as contact_person',
                'C.firmTitle as company',
                'C.email as email',
                'C.phoneNumber as phone_number',
                'C.description as description',
                'C.idNumber as id_number',
                'C.vatNumber as vat_number',
                'A.street as street',
                'A.city as city',
                'A.postalCode as postal_code',
                $connection->raw('(select lang from business.Inquiry AS I where I.userAccount_id = UA.userAccount_id order by createdOn desc limit 1) as lang')
            ])
            ->leftJoin('business.Contact AS C', 'C.contact_id', '=', 'UA.contact_id')
            ->leftJoin('business.Address AS A', 'A.address_id', '=', 'C.address_id')
            ->whereIn('UA.userAccount_id', $ids)
            ->get(), '_id');

        return $data;
    }

    public function getName()
    {
        return 'user-account';
    }
}

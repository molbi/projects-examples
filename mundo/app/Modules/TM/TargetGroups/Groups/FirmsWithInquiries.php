<?php
/**
 * Created by PhpStorm.
 * User: kubahavle
 * Date: 15.03.17
 * Time: 10:31
 */

namespace Mundo\Modules\TM\TargetGroups\Groups;


use Illuminate\Database\Query\JoinClause;
use Mundo\Models\Business\ItParam;
use Mundo\Modules\TM\TargetGroups\TargetGroup;

/**
 * Class FirmsWithInquiries
 * @package Mundo\Modules\TM\TargetGroups\Groups
 */
class FirmsWithInquiries extends TargetGroup
{
    /**
     * @var string
     */
    protected $tableName = 'business.Firm';
    /**
     * @var string
     */
    protected $identificator = 'firm_id';

    /**
     * @return void
     */
    protected function getBasicQuery()
    {
        parent::getBasicQuery();

        $this->builder->whereRaw('UNST.firm_id NOT IN (SELECT firm_id FROM telemarketing.Logout)');
    }

    /**
     * @return void
     */
    protected function getStacksQuery()
    {
        $this->conditionFirmWithService();
        $this->conditionFirmWithoutService();
        $this->conditionStackDuplicity();
        $this->conditionTisCampaign();
        $this->conditionOapCategory();
        $this->conditionTelemarketingLogout();
        $this->conditionCrossCampaign();

        $this->conditionBeforeRis();
        $this->conditionAfterRis();
        $this->conditionBlockedOrders();

        $this->builder->whereNotNull('UNST.phoneNumber');
        $this->builder->where('UNST.phoneNumber', '!=', '');

        $this->orderBy();


    }

    /**
     * získání jazyku pro kampan (nové tm jazyk neřeší, ale bohužel z historických důvodů stále rozdělujeme kampaně na
     * CZ/SK -> jazyk se bere v potaz v pripade, ze je nastaveno sync_id u kampane
     *
     * @return void
     */
    protected function applyLang()
    {
        $this->builder->where('UNST.lang', $this->getCampaignLang());
    }

    private function conditionFirmWithoutService()
    {
        $firmNoServiceSelect = $this->db()->table('business.Firm_Service AS FS')
            ->select(['FS.firm_id'])
            ->whereRaw('FS.firm_id = UNST.firm_id')
            ->whereIn('FS.service', ['activeselling']);

        $this->builder->whereRaw('UNST.firm_id NOT IN ('. get_sql($firmNoServiceSelect) .')');
    }

    private function conditionFirmWithService()
    {
        $firmServiceSelect = $this->db()->table('business.Firm_Service AS FS')
            ->select(['FS.firm_id'])
            ->whereRaw('FS.firm_id = UNST.firm_id')
            ->whereRaw('FS.lang = UNST.lang')
            ->whereIn('FS.service', ['inquiries', 'inquiries-keywords', 'bold', 'prepaid', 'psinquiries', 'prepaid-offer'])
            ->whereRaw('FS.beginsOn < NOW() - INTERVAL 2 MONTH');

        $this->builder->whereRaw('UNST.firm_id IN ('. get_sql($firmServiceSelect) .')');
    }

    private function orderBy()
    {
        $history2 = $this->db()->table('telemarketing.Stack AS Stack')
            ->select([
                'History.createdAt AS creatingDate'
            ])
            ->join('telemarketing.Campaign AS Campaign', 'Campaign.campaign_id', '=', 'Stack.campaign_id')
            ->join('telemarketing.History AS History', 'History.stack_id', '=', 'Stack.stack_id')
            ->join('telemarketing.TargetGroup AS TargetGroup', function(JoinClause $j) {
                $j->on('TargetGroup.targetGroup_id', '=', 'Campaign.targetGroup_id')
                    ->where('TargetGroup.targetType_id', '=', 4);
            })
            ->whereRaw('Stack.targetSubject_id = UNST.firm_id')
            ->orderBy('creatingDate', 'DESC')
            ->limit(1);

        $this->builder->orderByRaw('(' . get_sql($history2) . ') ASC');
    }

    private function conditionStackDuplicity()
    {
        $this->builder->leftJoin('telemarketing.Stack AS SIn', function(JoinClause $j) {
            $j->on('SIn.targetSubject_id', '=', 'UNST.firm_id')
                ->where(function($q) {
                    $q->where('SIn.campaign_id', '=', $this->getCampaignId())
                        ->on('SIn.createdAt', '>=', $this->raw('NOW() - INTERVAL 60 WEEK'))
                        ->on('SIn.createdAt', '<=', $this->raw('NOW()'));
                });
        });
        $this->builder->whereNull('SIn.stack_id');
    }

    private function conditionTisCampaign()
    {
        $this->builder->leftJoin('tis.Stack AS TS', function(JoinClause $j) {
            $j->on('TS.firm_id', '=', 'UNST.firm_id')
                ->where('TS.campaign_id', '=', 185);
        });
        $this->builder->whereNull('TS.stack_id');
    }

    private function conditionOapCategory()
    {
        $this->builder->leftJoin('business.Firm_OapCategory AS FOP', 'FOP.firm_id', '=', 'UNST.firm_id');
        $this->builder->whereNotNull('FOP.firm_id');
        $this->builder->where('FOP.oapCategory_id', '!=' ,0);
    }

    private function conditionTelemarketingLogout()
    {
        $this->builder->leftJoin('telemarketing.Logout AS L', 'L.firm_id', '=', 'UNST.firm_id');
        $this->builder->whereNull('L.firm_id');
    }

    private function conditionCrossCampaign()
    {
        $this->builder->leftJoin('telemarketing.Stack AS SCS', function(JoinClause $j) {
            $j->on('SCS.targetSubject_id', '=', 'UNST.firm_id')
                ->whereIn('SCS.campaign_id', [429, 431, 441, 443]);
        });
        $this->builder->whereNull('SCS.stack_id');
    }

    private function conditionBeforeRis()
    {
        $beforeRisQuery = $this->db()->table('obis.Order AS OX2')
            ->select(['OX2.firm_id'])
            ->join('obis.Order_Service AS OSX2', 'OSX2.order_id', '=', 'OX2.order_id')
            ->join('obis.Service AS SX2', 'SX2.service_id', '=', 'OSX2.service_id')
            ->where('OX2.lang', $this->getCampaignLang())
            ->whereRaw('(((((OX2.beginsAt + INTERVAL SX2.duration MONTH) + INTERVAL 36 DAY) > NOW()) AND ((OX2.beginsAt + INTERVAL SX2.duration MONTH) < NOW()))')
            ->orWhereRaw('(((OX2.beginsAt + INTERVAL SX2.duration MONTH)  > NOW()) AND ((OX2.beginsAt + INTERVAL SX2.duration MONTH)  < (NOW() + INTERVAL 36 DAY))))');

        $this->builder->whereRaw('UNST.firm_id NOT IN ('.get_sql($beforeRisQuery).')');
    }

    private function conditionAfterRis()
    {
        $afterRisQuery = $this->db()->table('obis.Order AS OXA')
            ->select(['OXA.firm_id'])
            ->where('OXA.lang', $this->getCampaignLang())
            ->whereIn('OXA.source', ['ris-renewal', 'ris-manual'])
            ->whereRaw('OXA.createdOn > NOW() - INTERVAL 36 DAY');

        $this->builder->whereRaw('UNST.firm_id NOT IN ('.get_sql($afterRisQuery).')');
    }

    private function conditionBlockedOrders()
    {
        $blockerOrdersQuery = $this->db()->table('obis.Order AS OX')
            ->select(['OX.firm_id'])
            ->whereRaw('OX.firm_id = UNST.firm_id')
            ->whereIn('OX.state', ['blocked', 'processed']);

        $this->builder->whereRaw('UNST.firm_id NOT IN ('.get_sql($blockerOrdersQuery).')');
    }
}
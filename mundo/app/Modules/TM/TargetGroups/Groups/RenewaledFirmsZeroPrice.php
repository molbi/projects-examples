<?php
/**
 * Created by PhpStorm.
 * User: Mike
 * Date: 17.08.17
 * Time: 13:34
 */

namespace Mundo\Modules\TM\TargetGroups\Groups;

use Illuminate\Database\Query\JoinClause;
use Mundo\Models\Business\ItParam;
use Mundo\Modules\TM\TargetGroups\TargetGroup;

/**
 * Class UpdatedFirmsWithoutService
 * @package Mundo\Modules\TM\TargetGroups\Groups
 */
class RenewaledFirmsZeroPrice extends TargetGroup
{
    /**
     * @var string
     */
    protected $tableName = 'business.Firm';
    /**
     * @var string
     */
    protected $identificator = 'firm_id';

    /**
     * @var bool
     */
    protected $distinct = true;

    /**
     * @return void
     */
    protected function getBasicQuery()
    {
        parent::getBasicQuery();

        $this->builder->whereRaw('UNST.firm_id NOT IN (SELECT firm_id FROM telemarketing.Logout)');
    }

    /**
     * @return void
     */
    protected function getStacksQuery()
    {
        $this->builder->join('obis.Order AS O', 'O.firm_id', '=', 'UNST.firm_id')
            ->join('obis.Order_Service AS OS', 'OS.order_id', '=', 'O.order_id')
            ->join('obis.Service AS S', function (JoinClause $join) {
                $join->on('S.service_id', '=', 'OS.service_id')
                    ->where('S.price', '=', 0);
            })
            ->whereIn('O.source', ['ris-renewal', 'ris-manual'])
            ->where('UNST.state', 'visible')
            ->whereNotNull('UNST.phoneNumber')
            ->where('UNST.phoneNumber', '!=', '');

        if (in_array($this->getCampaignId(), [825, 827])) {
            $this->builder->whereRaw('DATE(O.createdOn) BETWEEN "2017-01-01" AND "2017-05-31"');
        }

        $this->conditionRound();
        $this->conditionRis();
//        $this->conditionOrdersInTwoMonths();
        $this->conditionProcessedBlockedOrders();
        $this->conditionCrossCampaigns();
        $this->setOrderBy();
    }

    /**
     * získání jazyku pro kampan (nové tm jazyk neřeší, ale bohužel z historických důvodů stále rozdělujeme kampaně na
     * CZ/SK -> jazyk se bere v potaz v pripade, ze je nastaveno sync_id u kampane
     *
     * @return void
     */
    protected function applyLang()
    {
        $this->builder->where('UNST.lang', $this->getCampaignLang());
    }


    private function conditionRound()
    {
        $this->builder
            ->leftJoin('telemarketing.Stack AS S', function (JoinClause $j) {
                $j->on('S.targetSubject_id', '=', 'UNST.firm_id')
                    ->where('S.campaign_id', '=', $this->getCampaignId())
                    ->on($this->raw('DATE(S.createdAt)'), '>=', $this->raw('DATE(NOW() - INTERVAL 60 DAY)'));
            })->whereNull('S.stack_id');
    }

    private function setOrderBy()
    {
        $history = $this->db()
            ->table('telemarketing.Stack AS Stack')
            ->select([
                'History.createdAt AS creatingDate'
            ])
            ->join('telemarketing.Campaign AS Campaign', 'Campaign.campaign_id', '=', 'Stack.campaign_id')
            ->join('telemarketing.History AS History', 'History.stack_id', '=', 'Stack.stack_id')
            ->join('telemarketing.TargetGroup AS TargetGroup', function (JoinClause $j) {
                $j->on('TargetGroup.targetGroup_id', '=', 'Campaign.targetGroup_id')
                    ->where('TargetGroup.targetType_id', '=', 4);
            })
            ->whereRaw('Stack.targetSubject_id = UNST.firm_id')
            ->orderBy('creatingDate', 'DESC')
            ->limit(1);

        $this->builder->orderByRaw(sprintf('(%s) ASC', get_sql($history)));
    }

    private function conditionRis()
    {
        $this->builder
            ->leftJoin('obis.RenewalFirm AS RF', function (JoinClause $j) {
                $j->on('RF.firm_id', '=', 'UNST.firm_id')
                    ->on($this->raw('DATE(RF.createdAt)'), '>=', $this->raw('DATE(NOW() - INTERVAL 90 DAY)'));
            })
            ->leftJoin('obis.RenewalFirmHistory AS RFH', function (JoinClause $j) {
                $j->on('RFH.renewalFirm_id', '=','RF.renewalFirm_id')
                    ->on($this->raw('DATE(RFH.createdAt)'), '>=', $this->raw('DATE(NOW() - INTERVAL 25 DAY)'));
            })
            ->whereNull('RFH.renewalFirmHistory_id');
    }

    private function conditionOrdersInTwoMonths()
    {
        $this->builder
            ->leftJoin('obis.Order AS O2', function (JoinClause $j) {
                $j->on('O2.firm_id', '=', 'UNST.firm_id')->on($this->raw('DATE(O2.createdOn)'), '>=', $this->raw('DATE(NOW() - INTERVAL 2 MONTH)'));
            })->whereNull('O2.firm_id');
    }

    private function conditionProcessedBlockedOrders()
    {
        $this->builder
            ->leftJoin('obis.Order AS O3', function (JoinClause $j) {
                $j->on('O3.firm_id', '=', 'UNST.firm_id')->whereIn('O3.state', ['processed', 'blocked']);
            })->whereNull('O3.order_id');
    }

    private function conditionCrossCampaigns()
    {
        $ids = ItParam::getCrossCampaignIds([$this->getCampaignId()]);

        $this->builder->leftJoin('telemarketing.Stack AS TMS_L', function (JoinClause $j) use ($ids) {
            $j->on('TMS_L.targetSubject_id', '=', 'UNST.firm_id')
                ->whereIn('TMS_L.campaign_id', $ids)
                ->on($this->raw('DATE(TMS_L.createdAt)'), '>=', $this->raw('DATE(NOW() - INTERVAL 20 DAY)'));
        })->whereNull('TMS_L.stack_id');
    }
}
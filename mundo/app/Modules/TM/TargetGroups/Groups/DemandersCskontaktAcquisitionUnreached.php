<?php
/**
 * Created by PhpStorm.
 * User: kubahavle
 * Date: 15.03.17
 * Time: 10:31
 */

namespace Mundo\Modules\TM\TargetGroups\Groups;


use Illuminate\Database\Query\JoinClause;
use Mundo\Models\Business\ItParam;
use Mundo\Modules\TM\TargetGroups\TargetGroup;

/**
 * Class DemandersCskontaktAcquisitionUnreached
 * @package Mundo\Modules\TM\TargetGroups\Groups
 */
class DemandersCskontaktAcquisitionUnreached extends TargetGroup
{
    /**
     * @var string
     */
    protected $tableName = 'telemarketing.Stack';
    /**
     * @var string
     */
    protected $identificator = 'targetSubject_id';

    /**
     * @var bool
     */
    protected $distinct = true;

    /**
     * @return void
     */
    protected function getBasicQuery()
    {
        parent::getBasicQuery();
    }

    /**
     * @return void
     */
    protected function getStacksQuery()
    {
        $this->builder->where('UNST.campaign_id', 667);
        $this->builder->where('UNST.state', 'declined');
        $this->builder->groupBy('UNST.stack_id');
        $this->builder->havingRaw('sum(if(H.action = "unreached", 1, 0)) = 2');
        $this->builder->havingRaw('sum(if(H.action = "reached", 1, 0)) = 0');

        $this->builder->leftJoin('telemarketing.History AS H', function (JoinClause $j) {
            $j->on('H.stack_id', '=', 'UNST.stack_id')
                ->whereIn('H.action', ['reached', 'unreached', 'declined']);
        });

        $this->conditionStackDuplicity();
    }

    /**
     *
     */
    protected function applyLang()
    {
    }

    /**
     *
     */
    private function conditionStackDuplicity()
    {
        $this->builder->leftJoin('telemarketing.Stack AS TMS', function (JoinClause $j) {
            $j->on('TMS.targetSubject_id', '=', 'UNST.targetSubject_id')
                ->whereIn('TMS.campaign_id', [$this->getCampaignId()]);
        })->whereNull('TMS.stack_id');
    }
}
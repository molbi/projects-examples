<?php
/**
 * Created by PhpStorm.
 * User: kubahavle
 * Date: 15.03.17
 * Time: 10:31
 */

namespace Mundo\Modules\TM\TargetGroups\Groups;


use Illuminate\Database\Query\JoinClause;
use Mundo\Models\Business\ItParam;
use Mundo\Modules\TM\TargetGroups\TargetGroup;

/**
 * Class FirmsWithAnyService
 * @package Mundo\Modules\TM\TargetGroups\Groups
 */
class DeclinedRenewalServices extends TargetGroup
{
    /**
     * @var string
     */
    protected $tableName = 'obis.RenewalFirm';
    /**
     * @var string
     */
    protected $identificator = 'firm_id';
    /**
     * @var bool
     */
    protected $distinct = false;

    /**
     * @return void
     */
    protected function getBasicQuery()
    {
        $this->builder->from(sprintf('%s AS UNST', $this->tableName));
        $this->builder->addSelect(sprintf('UNST.%s AS targetSubject_identificator', $this->identificator));
    }

    /**
     * @return void
     */
    protected function getStacksQuery()
    {
        $this->conditionRis();
        $this->conditionTelemarketingLogout();
        $this->conditionStackDuplicity();

        $this->conditionProcessedBlockedOrders();
        $this->conditionOrdersInTwoMonths();
        $this->conditionOrderExactor();

        $this->conditionWithoutFirms();

        $this->conditionCrossCampaigns();

        $this->conditionUNST();

        $this->setOrderBy();
    }

    protected function applyLang()
    {
        $this->builder->join('business.Firm AS F', 'F.firm_id', '=', 'UNST.firm_id');
        $this->builder->where('F.lang', $this->getCampaignLang());
    }

    /**
     *
     */
    private function setOrderBy()
    {
        $history = $this->db()->table('obis.RenewalFirm AS RenewalFirm')
            ->select([
                'History.createdAt AS creatingDate'
            ])
            ->join('obis.RenewalFirmHistory AS History', 'History.renewalFirm_id', '=', 'RenewalFirm.renewalFirm_id')
            ->whereRaw('RenewalFirm.firm_id = UNST.firm_id')
            ->orderBy('creatingDate', 'DESC')
            ->limit(1);

        $this->builder->orderByRaw(sprintf('(%s) ASC', get_sql($history)));
    }

    private function conditionRis()
    {
        $this->builder->leftJoin('obis.RenewalFirm AS RF2', function(JoinClause $j) {
            $j->on('RF2.firm_id', '=', 'UNST.firm_id')
                ->where($this->raw('DATE(RF2.createdAt)'), '>', date('Y-m-t', strtotime('-1 month')));
        })->whereNull('RF2.renewalFirm_id');
    }

    private function conditionOrdersInTwoMonths()
    {
        $this->builder
            ->leftJoin('obis.Order AS O2', function (JoinClause $j) {
                $j->on('O2.firm_id', '=', 'UNST.firm_id')->on($this->raw('DATE(O2.createdOn)'), '>=', $this->raw('DATE(NOW() - INTERVAL 2 MONTH)'));
            })->whereNull('O2.firm_id');
    }

    private function conditionProcessedBlockedOrders()
    {
        $this->builder
            ->leftJoin('obis.Order AS O', function (JoinClause $j) {
                $j->on('O.firm_id', '=', 'UNST.firm_id')
                    ->whereIn('O.state', ['processed', 'blocked']);
            })->whereNull('O.order_id');
    }

    private function conditionCrossCampaigns()
    {
        $withoutSources = ItParam::getInhouseLeadsCampaignIds();
        array_push($withoutSources,$this->getCampaignId());
        $ids = ItParam::getCrossCampaignIds($withoutSources);

        $this->builder->leftJoin('telemarketing.Stack AS TMS_L', function (JoinClause $j) use ($ids) {
            $j->on('TMS_L.targetSubject_id', '=', 'UNST.firm_id')
                ->whereIn('TMS_L.campaign_id', $ids)
                ->on($this->raw('DATE(TMS_L.createdAt)'), '>=', $this->raw('DATE(NOW() - INTERVAL 25 DAY)'));
        })->whereNull('TMS_L.stack_id');

        $this->builder->leftJoin('telemarketing.Stack AS TMS_LL', function(JoinClause $j) use ($withoutSources) {
            $j->on('TMS_LL.targetSubject_id', '=', 'UNST.firm_id')
                ->whereIn('TMS_LL.campaign_id', $withoutSources)
                ->on($this->raw('DATE(TMS_LL.createdAt)'), '>=', $this->raw('DATE(NOW() - INTERVAL 25 DAY)'))
                ->whereNotIn('TMS_LL.state', ['accepted', 'declined', 'removed']);
        })->whereNull('TMS_LL.stack_id');
    }

    private function conditionTelemarketingLogout()
    {
        $this->builder->leftJoin('telemarketing.Logout AS L', 'L.firm_id', '=', 'UNST.firm_id')
            ->whereNull('L.firm_id');
    }

    private function conditionStackDuplicity()
    {
        $this->builder->leftJoin('telemarketing.Stack AS S', function(JoinClause $j) {
            $j->on('S.targetSubject_id', '=', 'UNST.firm_id')
                ->where('S.campaign_id', '=', $this->getCampaignId())
                ->on($this->raw('DATE(S.createdAt)'), '>=', $this->raw('DATE(NOW() - INTERVAL 30 DAY)'));
        })->whereNull('S.stack_id');
    }

    private function conditionOrderExactor()
    {
        $this->builder->leftJoin('obis.Order AS O3', 'O3.firm_id', '=', 'UNST.firm_id')
            ->leftJoin('obis.OrderExactor AS OE', 'OE.order_id', '=', 'O3.order_id')
            ->whereNull('OE.order_id');
    }

    private function conditionUNST()
    {
        $this->builder->whereDate('UNST.createdAt', '>=', '2017-01-01');
        $this->builder->whereDate('UNST.createdAt', '<', date('Y-m-t', strtotime('-1 month')));
        $this->builder->whereNotIn('UNST.state', ['renewal', 'pending']);
        $this->builder->whereNotIn('UNST.service', ['referencehodnoceni-without-ppc']);
        $this->builder->groupBy('UNST.firm_id');
    }

    private function conditionWithoutFirms()
    {
        //Aktivni firmy s koncici sluzbou
        $activeFirms = collect(
            $this->db()->table('business.Firm_Service AS FS')
            ->whereRaw('DATE(endsOn) <= DATE(NOW() + INTERVAL 35 DAY)')
            ->groupBy('FS.firm_id')
            ->pluck('firm_id')
        );


        //firmy s neukoncenou garanci
        $gisFirms = collect(
            $this->db()->table('obis.GarantionRequest AS GR')
            ->select(['GR.firm_id'])
            ->join('business.Firm_Service AS FS', 'FS.firm_id', '=', 'GR.firm_id')
            ->whereRaw('DATE(FS.endsOn) <= DATE(NOW() + INTERVAL 3 MONTH)')
            ->whereNotIn('GR.state', ['refunded', 'declined', 'in-progress'])
            ->groupBy('GR.firm_id')
            ->pluck('firm_id')
        );

        //firmy se stornovanymy vsemi objednavkami
        $autoCancelledFirms = collect(
            $this->db()->table('obis.Order AS O')
            ->select([
                'O.firm_id',
                $this->raw('COUNT(*) AS count'),
                $this->raw('SUM(IF(O.state = "cancelled", 1, 0)) AS cancelled')
            ])
            ->whereYear('O.createdOn', '>=', 2014)
            ->havingRaw('count = cancelled')
            ->groupBy('O.firm_id')
            ->pluck('firm_id')
        );

        $withoutFirms = $activeFirms->merge($gisFirms)->merge($autoCancelledFirms)->unique();

        $this->builder->whereNotIn('UNST.firm_id', $withoutFirms->toArray());
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Mike
 * Date: 17.08.17
 * Time: 13:34
 */

namespace Mundo\Modules\TM\TargetGroups\Groups;

use Illuminate\Database\Query\JoinClause;
use Mundo\Models\Business\ItParam;
use Mundo\Modules\TM\TargetGroups\TargetGroup;

/**
 * Class UpdatedFirmsWithoutService
 * @package Mundo\Modules\TM\TargetGroups\Groups
 */
class FirmsWithoutService extends TargetGroup
{
    /**
     * @var string
     */
    protected $tableName = 'business.Firm';
    /**
     * @var string
     */
    protected $identificator = 'firm_id';

    /**
     * @return void
     */
    protected function getBasicQuery()
    {
        parent::getBasicQuery();

        $this->builder->whereRaw('UNST.firm_id NOT IN (SELECT firm_id FROM telemarketing.Logout)');
    }

    /**
     * @return void
     */
    protected function getStacksQuery()
    {
        $this->builder->leftJoin('business.Firm_Service AS FS', 'FS.firm_id', '=', 'UNST.firm_id')
             ->leftJoin('business.Firm_CreditService AS FCS', 'FCS.firm_id', '=', 'UNST.firm_id')
             ->where('UNST.state', 'visible')
             ->whereNull('FS.firm_id')
             ->whereNull('FCS.firm_id')
             ->where('UNST.ipAddress', 'NOT LIKE', '%217.169.182.206%')
             ->whereNotNull('UNST.phoneNumber')
             ->where('UNST.phoneNumber', '!=', '');

        $this->conditionTis();
        $this->conditionWithoutActiveFirms();
        $this->conditionRound();
        if (in_array($this->getCampaignId(), [791])) {
            $this->conditionCategoryFilter();
        }

        $this->conditionCrossCampaigns();
        $this->setOrderBy();
    }

    /**
     * získání jazyku pro kampan (nové tm jazyk neřeší, ale bohužel z historických důvodů stále rozdělujeme kampaně na
     * CZ/SK -> jazyk se bere v potaz v pripade, ze je nastaveno sync_id u kampane
     *
     * @return void
     */
    protected function applyLang()
    {
        $this->builder->where('UNST.lang', $this->getCampaignLang());
    }


    private function conditionRound()
    {
        $this->builder
             ->leftJoin('telemarketing.Stack AS S', function (JoinClause $j) {
                 $j->on('S.targetSubject_id', '=', 'UNST.firm_id')
                   ->where('S.campaign_id', '=', $this->getCampaignId())
                   ->on($this->raw('DATE(S.createdAt)'), '>=', $this->raw('DATE(NOW() - INTERVAL 60 DAY)'));
             })->whereNull('S.stack_id');
    }

    private function conditionWithoutActiveFirms()
    {
        /*
        * Vylouceni firem se sluzbami
        */
        $firmService = $this->db()->table('business.Firm_Service AS FS')
            ->distinct()
            ->select(['F.idNumber'])
            ->join('business.Firm AS F', 'F.firm_id', '=', 'FS.firm_id')
            ->whereNotNull('F.idNumber')
            ->where('F.idNumber', '!=', '')
            ->pluck('idNumber');

        $firmCreditService = $this->db()->table('business.Firm_CreditService AS FCS')
            ->distinct()
            ->select(['F.idNumber'])
            ->join('business.Firm AS F', 'F.firm_id', '=', 'FCS.firm_id')
            ->whereNotNull('F.idNumber')
            ->where('F.idNumber', '!=', '')
            ->pluck('idNumber');

        $this->builder->whereNotIn('UNST.idNumber', $firmService);
        $this->builder->whereNotIn('UNST.idNumber', $firmCreditService);
    }

    private function conditionCategoryFilter()
    {
        $this->builder->join('business.Firm_Category AS FC', 'FC.firm_id', '=', 'UNST.firm_id');

        switch ($this->getCampaignId()) {
            case 791:
                $this->builder->whereNotIn('FC.ID_BCFirmRegion', [
                    1, 2007, 2010, 2009, 2011, 2021, 1055, 1052, 200011, 1054, 2020, 2018, 2019, 9, 10, 12, 14,
                    1073, 2001, 17, 2000, 241, 2042, 233, 2054, 2053, 28, 31, 243, 39, 200022, 200024, 45, 47,
                    3004, 100039, 100040, 100041, 100043, 200020, 49, 50, 100054, 2085, 2084, 2083, 240, 52, 53,
                    2093, 2091, 2092, 60, 61, 1071, 100046, 62, 1065, 1066, 1067, 1068, 1069, 1070, 2099, 65, 67,
                    2273, 68, 70, 74, 80, 236, 86, 87, 88, 89, 91, 100050, 93, 94, 108, 234, 109, 110, 200048, 113,
                    114, 115, 116, 2134, 2135, 2140, 120, 100051, 100048, 126, 127, 1072, 130, 231, 137, 2155, 140,
                    2156, 141, 3006, 1084, 144, 1083, 151, 235, 2178, 152, 2179, 2043, 2094, 157, 156, 159, 200062,
                    1087, 1089, 169, 1090, 1097, 200096, 172, 175, 176, 181, 136, 182, 183, 238, 195, 2231, 239,
                    200068, 200077, 2253, 2254, 199, 2252, 2246, 2251, 2249, 2248, 2247, 2245, 190, 100047, 200,
                    2256, 2255, 203, 207, 237, 200060, 210, 213, 200008, 2271, 3013, 200087, 218, 219, 200090, 73,
                    220, 200089, 2274, 242, 300197, 2104
                ]);
                break;
        }
    }

    private function setOrderBy()
    {
        $history = $this->db()
            ->table('telemarketing.Stack AS Stack')
            ->select([
                'History.createdAt AS creatingDate'
            ])
            ->join('telemarketing.Campaign AS Campaign', 'Campaign.campaign_id', '=', 'Stack.campaign_id')
            ->join('telemarketing.History AS History', 'History.stack_id', '=', 'Stack.stack_id')
            ->join('telemarketing.TargetGroup AS TargetGroup', function (JoinClause $j) {
                $j->on('TargetGroup.targetGroup_id', '=', 'Campaign.targetGroup_id')
                    ->where('TargetGroup.targetType_id', '=', 4);
            })
            ->whereRaw('Stack.targetSubject_id = UNST.firm_id')
            ->orderBy('creatingDate', 'DESC')
            ->limit(1);

        $this->builder->orderByRaw(sprintf('(%s) ASC', get_sql($history)));
    }

    private function conditionCrossCampaigns()
    {
        $ids = ItParam::getCrossCampaignIds([$this->getCampaignId()]);

        $this->builder->leftJoin('telemarketing.Stack AS TMS_L', function (JoinClause $j) use ($ids) {
            $j->on('TMS_L.targetSubject_id', '=', 'UNST.firm_id')
                ->whereIn('TMS_L.campaign_id', $ids)
                ->on($this->raw('DATE(TMS_L.createdAt)'), '>=', $this->raw('DATE(NOW() - INTERVAL 20 DAY)'));
        })->whereNull('TMS_L.stack_id');
    }

    private function conditionTis()
    {
        $this->builder->leftJoin('tis.Stack AS T_S', function (JoinClause $join) {
            $join->on('T_S.firm_id', '=', 'UNST.firm_id')
                ->whereIn('T_S.campaign_id', [255])
                ->on($this->raw('DATE(T_S.createdAt)'), '>=', $this->raw('DATE(NOW() - INTERVAL 90 DAY)'));
        });
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: kubahavle
 * Date: 15.03.17
 * Time: 10:31
 */

namespace Mundo\Modules\TM\TargetGroups\Groups;


use Illuminate\Database\Query\JoinClause;
use Mundo\Models\Business\ItParam;
use Mundo\Modules\TM\TargetGroups\TargetGroup;

/**
 * Class DemandersCskontaktAcquisition
 * @package Mundo\Modules\TM\TargetGroups\Groups
 */
class DemandersCskontaktAcquisitionPrivate extends TargetGroup
{
    /**
     * @var string
     */
    protected $tableName = 'business.UserAccount';
    /**
     * @var string
     */
    protected $identificator = 'userAccount_id';

    /**
     * @var bool
     */
    protected $distinct = true;

    /**
     * @return void
     */
    protected function getBasicQuery()
    {
        parent::getBasicQuery();
    }

    /**
     * @return void
     */
    protected function getStacksQuery()
    {
        $this->conditionPrivateDemander();
        $this->conditionHasInquiryInPastTwoYears();
        $this->conditionAltavidaPrevent();
        $this->conditionStackDuplicity();

        $this->builder->where('I.state', 'published');
        $this->builder->where('I.lang', $this->getCampaignLang());
        $this->builder->groupBy('UNST.userAccount_id');

        $this->conditionNonHaveInquiryInLastSixMonth();
    }

    /**
     *
     */
    protected function applyLang()
    {
    }

    /**
     *
     */
    private function conditionStackDuplicity()
    {
        $this->builder->leftJoin('telemarketing.Stack AS TMS', function (JoinClause $j) {
            $j->on('TMS.targetSubject_id', '=', 'UNST.userAccount_id')
                ->whereIn('TMS.campaign_id', [$this->getCampaignId()]);
        })->whereNull('TMS.stack_id');
    }

    private function conditionPrivateDemander()
    {
        $this->builder->join('business.Contact AS C', function (JoinClause $j) {
            $j->on('C.contact_id', '=', 'UNST.contact_id')
                ->where('C.type', '=', 'private');
        });
    }

    private function conditionHasInquiryInPastTwoYears()
    {
        $this->builder->join('business.Inquiry AS I', function (JoinClause $j) {
            $j->on('I.userAccount_id', '=', 'UNST.userAccount_id')
                ->where($this->raw('DATE(I.createdOn)'), '>', '2014-12-31');
        });
    }

    private function conditionAltavidaPrevent()
    {
        $this->builder->leftJoin('tm.Filter_Params AS FP', function (JoinClause $j) {
            $j->on('FP.param_id', '=', 'UNST.userAccount_id')
                ->where('FP.filter_id', '=', 149);
        })->whereNull('FP.param_id');
    }

    private function conditionNonHaveInquiryInLastSixMonth()
    {
        $having = $this->db()
            ->table('business.Inquiry AS II')
            ->select([
                $this->raw('COUNT(*)')
            ])
            ->whereRaw('II.userAccount_id = UNST.userAccount_id')
            ->whereRaw('DATE(II.createdOn) >= DATE(NOW() - INTERVAL 6 MONTH)')
            ->whereRaw('DATE(II.createdOn) <= DATE(NOW())');

        $this->builder->havingRaw('(' . get_sql($having) . ') = 0');
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: kubahavle
 * Date: 15.03.17
 * Time: 10:31
 */

namespace Mundo\Modules\TM\TargetGroups\Groups;


use Illuminate\Database\Query\JoinClause;
use Mundo\Models\Business\ItParam;
use Mundo\Modules\TM\TargetGroups\TargetGroup;

/**
 * Class CskontaktOnpSixMonth
 * @package Mundo\Modules\TM\TargetGroups\Groups
 */
class CskontaktOnpSixMonth extends TargetGroup
{
    /**
     * @var string
     */
    protected $tableName = 'cs_contact.users';
    /**
     * @var string
     */
    protected $identificator = 'id';

    /**
     * @var bool
     */
    protected $distinct = true;

    /**
     * @return void
     */
    protected function getBasicQuery()
    {
        parent::getBasicQuery();
    }

    /**
     * @return void
     */
    protected function getStacksQuery()
    {
        $this->conditionHasInquiryInPast();
        $this->conditionNonHaveInquiryInLastSixMonth();
        $this->conditionStackDuplicity();

        $this->builder->whereNotNull('I.sent_at');
        $this->builder->where('I.locale', $this->getCampaignLang() == 'cs' ? 'cs_CZ' : 'sk_SK');
        $this->builder->groupBy('UNST.id');
    }

    /**
     *
     */
    protected function applyLang()
    {
    }

    /**
     *
     */
    private function conditionStackDuplicity()
    {
        $this->builder->leftJoin('telemarketing.Stack AS TMS', function (JoinClause $j) {
            $j->on('TMS.targetSubject_id', '=', 'UNST.id')
                ->whereIn('TMS.campaign_id', [$this->getCampaignId()]);
        })->whereNull('TMS.stack_id');
    }

    private function conditionHasInquiryInPast()
    {
        $this->builder->join('cs_contact.inquiries AS I', function (JoinClause $j) {
            $j->on('I.demander_id', '=', 'UNST.id');
        });
    }

    private function conditionNonHaveInquiryInLastSixMonth()
    {
        $having = $this->db()
            ->table('cs_contact.inquiries AS II')
            ->select([
                $this->raw('COUNT(*)')
            ])
            ->whereRaw('II.demander_id = UNST.id')
            ->whereRaw('DATE(II.created_at) >= DATE(NOW() - INTERVAL 6 MONTH)')
            ->whereRaw('DATE(II.created_at) <= DATE(NOW())');

        $this->builder->havingRaw('(' . get_sql($having) . ') = 0');
    }
}
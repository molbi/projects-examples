<?php
/**
 * Created by PhpStorm.
 * User: kubahavle
 * Date: 15.03.17
 * Time: 10:31
 */

namespace Mundo\Modules\TM\TargetGroups\Groups;


use Illuminate\Database\Query\JoinClause;
use Mundo\Models\Business\ItParam;
use Mundo\Modules\TM\TargetGroups\TargetGroup;

/**
 * Class FirmsWithAcquisitionSentWithoutService
 * @package Mundo\Modules\TM\TargetGroups\Groups
 */
class FirmsWithAcquisitionSentWithoutService extends TargetGroup
{
    /**
     * @var string
     */
    protected $tableName = 'log.acquisition_sent_log';
    /**
     * @var string
     */
    protected $identificator = 'firm_id';
    /**
     * @var bool
     */
    protected $distinct = true;

    /**
     * @return void
     */
    protected function getBasicQuery()
    {
        $this->builder->from(sprintf('%s AS ASL', $this->tableName));
        $this->builder->addSelect(sprintf('UNST.%s AS targetSubject_identificator', $this->identificator));
    }

    /**
     * @return void
     */
    protected function getStacksQuery()
    {
        $this->joinFirm();
        $this->conditionActiveCreditService();
        $this->conditionActiveService();
        $this->conditionStackDuplicity();
        $this->conditionCsContactUsers();
        $this->conditionPhoneNumber();
        $this->conditionOtherStacks();
        $this->conditionCrossCampaigns();

        $this->builder->orderByRaw('RAND()');
    }

    protected function applyLang()
    {
        $this->builder->where('UNST.lang', $this->getCampaignLang());
    }

    private function conditionCrossCampaigns()
    {
        $ids = ItParam::getCrossCampaignIds([$this->getCampaignId()]);

        $this->builder->leftJoin('telemarketing.Stack AS TMS_L', function (JoinClause $j) use ($ids) {
            $j->on('TMS_L.targetSubject_id', '=', 'UNST.firm_id')
                ->whereIn('TMS_L.campaign_id', $ids)
                ->on($this->raw('DATE(TMS_L.createdAt)'), '>=', $this->raw('DATE(NOW() - INTERVAL 20 DAY)'));
        })->whereNull('TMS_L.stack_id');
    }

    private function conditionStackDuplicity()
    {
        $this->builder->leftJoin('telemarketing.Stack AS S', function (JoinClause $j) {
            $j->on('S.targetSubject_id', '=', 'UNST.firm_id')
                ->where('S.campaign_id', '=', $this->getCampaignId());
        })->whereNull('S.stack_id');
    }

    private function conditionActiveService()
    {
        $this->builder->leftJoin('business.Firm_Service AS FS', 'FS.firm_id', '=', 'UNST.firm_id')
            ->whereNull('FS.firm_id');
    }

    private function conditionActiveCreditService()
    {
        $this->builder->leftJoin('business.Firm_CreditService AS FCS', 'FCS.firm_id', '=', 'UNST.firm_id')
            ->whereNull('FCS.firm_id');
    }

    private function joinFirm()
    {
        $this->builder->join('business.Firm AS UNST', function (JoinClause $j) {
            $j->on('UNST.firm_id', '=', 'ASL.initiator_id')
                ->where('UNST.state', '=', 'visible');
        });
    }

    private function conditionCsContactUsers()
    {
        $idNumbers = $this->db()->table('cs_contact.contacts AS C')
            ->select(['C.id_number'])
            ->where('C.contactable_type', 'Kartus\\Model\\User')
            ->whereNotNull('C.id_number')
            ->where('C.id_number', '!=', '')
            ->groupBy('C.id_number')
            ->pluck('id_number');

        $this->builder->whereNotIn('UNST.idNumber', $idNumbers);

    }

    private function conditionPhoneNumber()
    {
        $this->builder->where('UNST.phoneNumber', '!=', '')
            ->whereNotNull('UNST.phoneNumber');
    }

    private function conditionOtherStacks()
    {
        $allLeadsCampaigns = array(
            533, 535, 541, 543, 513, 515, 471, 473, 469, 471, 429, 431, 337, 339, 277, 279, 107, 109
        );

        $this->builder->leftJoin('telemarketing.Stack AS SS', function (JoinClause $j) use ($allLeadsCampaigns) {
            $j->on('SS.targetSubject_id', '=', 'UNST.firm_id')
                ->whereIn('SS.campaign_id', $allLeadsCampaigns);
        })
            ->whereNull('SS.stack_id')
            ->where('ASL.subject_type', 'cs_contact')
            ->whereRaw('DATE(ASL.created_at) >= DATE(NOW() - INTERVAL 2 MONTH)');
    }
}
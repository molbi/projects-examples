<?php
/**
 * Created by PhpStorm.
 * User: kubahavle
 * Date: 15.03.17
 * Time: 10:31
 */

namespace Mundo\Modules\TM\TargetGroups\Groups;


use Illuminate\Database\Query\JoinClause;
use Mundo\Modules\TM\TargetGroups\TargetGroup;

/**
 * Class DemandersCskontaktAcquisition
 * @package Mundo\Modules\TM\TargetGroups\Groups
 */
class InquiriesSpecification extends TargetGroup
{
    /**
     * @var string
     */
    protected $tableName = 'business.InquiryLog';
    /**
     * @var string
     */
    protected $identificator = 'inquiry_id';

    /**
     * @var bool
     */
    protected $distinct = false;

    /**
     * @return void
     */
    protected function getBasicQuery()
    {
        parent::getBasicQuery();
    }

    /**
     * @return void
     */
    protected function getStacksQuery()
    {

        $this->builder->leftJoin('business.InquiryLog AS IL2', function (JoinClause $j) {
            $j->on('IL2.inquiry_id', '=', 'UNST.inquiry_id')
                ->on($this->raw('DATE(IL2.createdOn)'), '>', $this->raw('DATE(NOW() - INTERVAL 4 DAY)'));
        })->whereNull('IL2.inquiry_id');

        $this->builder->leftJoin('telemarketing.Stack AS TMS', function (JoinClause $j) {
            $j->on('TMS.targetSubject_id', '=', 'UNST.inquiry_id')
                ->whereIn('TMS.campaign_id', [$this->getCampaignId()]);
        })->whereNull('TMS.stack_id');

        $this->builder->join('business.Inquiry AS I', 'I.inquiry_id', '=', 'UNST.inquiry_id');

        $this->builder->whereRaw('DATE(UNST.createdOn) <= DATE(NOW() - INTERVAL 4 DAY)');
        $this->builder->whereRaw('DATE(UNST.createdOn) >= DATE(NOW() - INTERVAL 8 DAY)');
        $this->builder->whereIn('I.state', ['toAnswer', 'toSpecify', 'toSpecify_lite']);
        $this->builder->groupBy('UNST.inquiry_id');
        $this->builder->orderBy('I.createdOn');

    }

    /**
     *
     */
    protected function applyLang()
    {
        $this->builder->where('I.lang', $this->getCampaignLang());
    }
}
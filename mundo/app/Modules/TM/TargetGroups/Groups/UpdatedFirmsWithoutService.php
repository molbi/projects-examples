<?php
/**
 * Created by PhpStorm.
 * User: kubahavle
 * Date: 15.03.17
 * Time: 10:31
 */

namespace Mundo\Modules\TM\TargetGroups\Groups;


use Illuminate\Database\Query\JoinClause;
use Mundo\Models\Business\ItParam;
use Mundo\Modules\TM\TargetGroups\TargetGroup;

/**
 * Class UpdatedFirmsWithoutService
 * @package Mundo\Modules\TM\TargetGroups\Groups
 */
class UpdatedFirmsWithoutService extends TargetGroup
{
    /**
     * @var string
     */
    protected $tableName = 'business.Firm';
    /**
     * @var string
     */
    protected $identificator = 'firm_id';

    /**
     * @return void
     */
    protected function getBasicQuery()
    {
        parent::getBasicQuery();

        $this->builder->whereRaw('UNST.firm_id NOT IN (SELECT firm_id FROM telemarketing.Logout)');
    }

    /**
     * @return void
     */
    protected function getStacksQuery()
    {
        $this->builder->leftJoin('business.Firm_Service AS FS', 'FS.firm_id', '=', 'UNST.firm_id')
            ->leftJoin('business.Firm_CreditService AS FCS', 'FCS.firm_id', '=', 'UNST.firm_id')
            ->leftJoin('telemarketing.Stack AS S', function(JoinClause $j) {
                $j->on('S.targetSubject_id', '=', 'UNST.firm_id')
                    ->where('S.campaign_id', '=', $this->getCampaignId());
            })
            ->leftJoin('tis.Stack AS TS', function(JoinClause $j) {
                $j->on('TS.firm_id', '=', 'UNST.firm_id')
                    ->whereIn('TS.campaign_id', [213]);
            })
            ->leftJoin('obis.Order AS O', function(JoinClause $j) {
                $j->on('O.firm_id', '=', 'UNST.firm_id')
                    ->on($this->raw('YEAR(O.createdOn)'), '>=', $this->raw('YEAR(NOW() - INTERVAL 1 YEAR)'));
            })
            ->where('UNST.state', 'visible')
            ->where('UNST.ipAddress', 'NOT LIKE', '%217.169.182.206%')
            ->whereRaw('DATE(UNST.createdOn) != DATE(UNST.updatedOn)')
            ->whereNotNull('UNST.updatedOn')
            ->whereRaw('DATE(UNST.createdOn) <= DATE(NOW() - INTERVAL 1 MONTH)')
            ->whereRaw('YEAR(UNST.updatedOn) >= 2016')
            ->whereNull('FS.firm_id')
            ->whereNull('FCS.firm_id')
            ->whereNull('S.stack_id')
            ->whereNull('TS.stack_id')
            ->whereNull('O.order_id')
            ->whereNotNull('UNST.phoneNumber')
            ->where('UNST.phoneNumber', '!=', '')
            ->orderBy('UNST.updatedOn','DESC');

        //Firmy s aktivni sluzbou
        $this->conditionActiveFirms();
        //cross-campaigns
        $this->conditionCrossCampaigns();
    }

    /**
     * získání jazyku pro kampan (nové tm jazyk neřeší, ale bohužel z historických důvodů stále rozdělujeme kampaně na
     * CZ/SK -> jazyk se bere v potaz v pripade, ze je nastaveno sync_id u kampane
     *
     * @return void
     */
    protected function applyLang()
    {
        $this->builder->where('UNST.lang', $this->getCampaignLang());
    }

    private function conditionActiveFirms()
    {
        /*
        * Vylouceni firem se sluzbami
        */
        $firmService = $this->db()->table('business.Firm_Service AS FS')
            ->distinct()
            ->select(['F.idNumber'])
            ->join('business.Firm AS F', 'F.firm_id', '=', 'FS.firm_id')
            ->whereNotNull('F.idNumber')
            ->where('F.idNumber', '!=', '')
            ->pluck('idNumber');

        $firmCreditService = $this->db()->table('business.Firm_CreditService AS FCS')
            ->distinct()
            ->select(['F.idNumber'])
            ->join('business.Firm AS F', 'F.firm_id', '=', 'FCS.firm_id')
            ->whereNotNull('F.idNumber')
            ->where('F.idNumber', '!=', '')
            ->pluck('idNumber');

        $this->builder->whereNotIn('UNST.idNumber', $firmService);
        $this->builder->whereNotIn('UNST.idNumber', $firmCreditService);
    }

    private function conditionCrossCampaigns()
    {
        $ids = ItParam::getCrossCampaignIds([$this->getCampaignId()]);

        $this->builder->leftJoin('telemarketing.Stack AS TMS_L', function (JoinClause $j) use ($ids) {
            $j->on('TMS_L.targetSubject_id', '=', 'UNST.firm_id')
                ->whereIn('TMS_L.campaign_id', $ids)
                ->on($this->raw('DATE(TMS_L.createdAt)'), '>=', $this->raw('DATE(NOW() - INTERVAL 20 DAY)'));
        })->whereNull('TMS_L.stack_id');
    }
}
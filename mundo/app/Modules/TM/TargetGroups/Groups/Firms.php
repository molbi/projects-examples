<?php
/**
 * Created by PhpStorm.
 * User: kubahavle
 * Date: 15.03.17
 * Time: 10:31
 */

namespace Mundo\Modules\TM\TargetGroups\Groups;


use Illuminate\Database\Query\JoinClause;
use Mundo\Models\Business\ItParam;
use Mundo\Modules\TM\TargetGroups\TargetGroup;

/**
 * Class Firms
 * @package Mundo\Modules\TM\TargetGroups\Groups
 */
class Firms extends TargetGroup
{
    /**
     * @var string
     */
    protected $tableName = 'business.Firm';
    /**
     * @var string
     */
    protected $identificator = 'firm_id';

    /**
     * @var bool
     */
    protected $distinct = true;

    /**
     * @return void
     */
    protected function getBasicQuery()
    {
        parent::getBasicQuery();
    }

    /**
     * @return void
     */
    protected function getStacksQuery()
    {
        $this->conditionTelemarketingLogout();
        $this->conditionStackDuplicity();
        $this->conditionPhoneNumber();
        $this->conditionBeforeRis();
        $this->conditionAfterRis();
        $this->conditionBlockedOrders();

    }

    /**
     *
     */
    protected function applyLang()
    {
        $this->builder->where('UNST.lang', $this->getCampaignLang());
    }

    /**
     *
     */
    private function conditionTelemarketingLogout()
    {
        $this->builder->leftJoin('telemarketing.Logout AS L', 'L.firm_id', '=', 'UNST.firm_id')
            ->whereNull('L.firm_id');
    }

    /**
     *
     */
    private function conditionStackDuplicity()
    {
        $this->builder->leftJoin('telemarketing.Stack AS TMS', function (JoinClause $j) {
            $j->on('TMS.targetSubject_id', '=', 'UNST.firm_id')
                ->whereIn('TMS.campaign_id', [$this->getCampaignId()]);
        })->whereNull('TMS.stack_id');
    }

    /**
     *
     */
    private function conditionBeforeRis()
    {
        $beforeRisQuery = $this->db()->table('obis.Order AS OX2')
            ->select(['OX2.firm_id'])
            ->join('obis.Order_Service AS OSX2', 'OSX2.order_id', '=', 'OX2.order_id')
            ->join('obis.Service AS SX2', 'SX2.service_id', '=', 'OSX2.service_id')
            ->where('OX2.lang', $this->getCampaignLang())
            ->whereRaw('(((((OX2.beginsAt + INTERVAL SX2.duration MONTH) + INTERVAL 36 DAY) > NOW()) AND ((OX2.beginsAt + INTERVAL SX2.duration MONTH) < NOW()))')
            ->orWhereRaw('(((OX2.beginsAt + INTERVAL SX2.duration MONTH)  > NOW()) AND ((OX2.beginsAt + INTERVAL SX2.duration MONTH)  < (NOW() + INTERVAL 36 DAY))))');

        $this->builder->whereRaw('UNST.firm_id NOT IN (' . get_sql($beforeRisQuery) . ')');
    }

    /**
     *
     */
    private function conditionAfterRis()
    {
        $afterRisQuery = $this->db()->table('obis.Order AS OXA')
            ->select(['OXA.firm_id'])
            ->where('OXA.lang', $this->getCampaignLang())
            ->whereIn('OXA.source', ['ris-renewal', 'ris-manual'])
            ->whereRaw('OXA.createdOn > NOW() - INTERVAL 36 DAY');

        $this->builder->whereRaw('UNST.firm_id NOT IN (' . get_sql($afterRisQuery) . ')');
    }

    /**
     *
     */
    private function conditionBlockedOrders()
    {
        $blockerOrdersQuery = $this->db()->table('obis.Order AS OX')
            ->select(['OX.firm_id'])
            ->whereRaw('OX.firm_id = UNST.firm_id')
            ->whereIn('OX.state', ['blocked', 'processed']);

        $this->builder->whereRaw('UNST.firm_id NOT IN (' . get_sql($blockerOrdersQuery) . ')');
    }

    /**
     *
     */
    private function conditionPhoneNumber()
    {
        $this->builder->where('UNST.phoneNumber', '!=', '')
            ->whereNotNull('UNST.phoneNumber');
    }
}
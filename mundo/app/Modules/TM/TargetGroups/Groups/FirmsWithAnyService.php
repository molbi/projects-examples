<?php
/**
 * Created by PhpStorm.
 * User: kubahavle
 * Date: 15.03.17
 * Time: 10:31
 */

namespace Mundo\Modules\TM\TargetGroups\Groups;


use Illuminate\Database\Query\JoinClause;
use Mundo\Models\Business\ItParam;
use Mundo\Modules\TM\TargetGroups\TargetGroup;

/**
 * Class FirmsWithAnyService
 * @package Mundo\Modules\TM\TargetGroups\Groups
 */
class FirmsWithAnyService extends TargetGroup
{
    /**
     * @var string
     */
    protected $tableName = 'business.Firm';
    /**
     * @var string
     */
    protected $identificator = 'firm_id';

    /**
     * @return void
     */
    protected function getBasicQuery()
    {
        parent::getBasicQuery();

        $this->builder->whereRaw('UNST.firm_id NOT IN (SELECT firm_id FROM telemarketing.Logout)');
    }

    /**
     * @return void
     */
    protected function getStacksQuery()
    {
        //nemá vystavenou ci blokovanou boejdnavku
        $this->conditionProcessedBlockedOrders();
        //nemá za poslendí 2 měsíce objednávku
        $this->conditionOrdersInTwoMonths();
        //nemá uzavrený záznam v risu
        $this->conditionRis();
        //vyradíme firmy, ktere v kamapni jsou a nejsou stare vic než 50 dni
        $this->conditionRound();
        //Firmy s aktivni sluzbou
        $this->conditionActiveFirms();
        //firmy v garancnim systemu
        $this->conditionGis();
        //cross-campaigns
        $this->conditionCrossCampaigns();
        //ordering
        $this->setOrderBy();
    }

    /**
     * získání jazyku pro kampan (nové tm jazyk neřeší, ale bohužel z historických důvodů stále rozdělujeme kampaně na
     * CZ/SK -> jazyk se bere v potaz v pripade, ze je nastaveno sync_id u kampane
     *
     * @return void
     */
    protected function applyLang()
    {
        $this->builder->where('UNST.lang', $this->getCampaignLang());
    }

    /**
     * @return array
     */
    private function firmWithActiveService()
    {
        $firmService = $this->db()
            ->table('business.Firm_Service AS FS')
            ->select(['FS.firm_id', 'FS.endsOn'])
            ->leftJoin('business.Firm_Service AS FS2', function (JoinClause $join) {
                $join->on('FS2.firm_id', '=', 'FS.firm_id')
                     ->on($this->raw('DATE(FS2.endsOn)'), '<', $this->raw('DATE(NOW() + INTERVAL 40 DAY)'));
            })
            ->whereNull('FS2.firm_id')
            ->whereNotIn('FS.service', ['cskontakt-golden']);

        $firmCreditService = $this->db()
            ->table('business.Firm_CreditService AS FCS')
            ->select([
                'firm_id',
                $this->raw('DATE(NOW() + INTERVAL 1 YEAR) AS endsOn')
            ])
            ->whereNotIn('FCS.service', ['cskontakt-golden']);

        if (in_array($this->getCampaignId(), [609, 789, 831])) {
            $firmCreditService->whereRaw('YEAR(FCS.beginsOn) >= 2016');
        }

        return $this->db()
            ->table($this->raw(sprintf('(%s) AS results', get_sql($firmService->unionAll($firmCreditService)))))
            ->whereRaw('DATE(endsOn) >= DATE(NOW() + INTERVAL 40 DAY)')
            ->groupBy('firm_id')
            ->pluck('firm_id');
    }

    /**
     * @return array
     */
    private function firmWithUnfinishedGarantion()
    {
        return $this->db()->table('obis.GarantionRequest AS GR')
            ->select(['GR.firm_id'])
            ->join('business.Firm_Service AS FS', 'FS.firm_id', '=', 'GR.firm_id')
            ->whereRaw('DATE(FS.endsOn) <= DATE(NOW() + INTERVAL 3 MONTH)')
            ->whereNotIn('GR.state', ['refunded', 'declined'])
            ->groupBy('GR.firm_id')
            ->pluck('firm_id');
    }

    /**
     *
     */
    private function setOrderBy()
    {
        $history = $this->db()
            ->table('telemarketing.Stack AS Stack')
            ->select([
                'History.createdAt AS creatingDate'
            ])
            ->join('telemarketing.Campaign AS Campaign', 'Campaign.campaign_id', '=', 'Stack.campaign_id')
            ->join('telemarketing.History AS History', 'History.stack_id', '=', 'Stack.stack_id')
            ->join('telemarketing.TargetGroup AS TargetGroup', function (JoinClause $j) {
                $j->on('TargetGroup.targetGroup_id', '=', 'Campaign.targetGroup_id')
                    ->where('TargetGroup.targetType_id', '=', 4);
            })
            ->whereRaw('Stack.targetSubject_id = UNST.firm_id')
            ->orderBy('creatingDate', 'DESC')
            ->limit(1);

        $this->builder->orderByRaw(sprintf('(%s) ASC', get_sql($history)));
    }

    private function conditionGis()
    {
        $this->builder->whereNotIn('UNST.firm_id', $this->firmWithUnfinishedGarantion());
    }

    private function conditionActiveFirms()
    {
        $this->builder->whereIn('UNST.firm_id', $this->firmWithActiveService());
    }

    private function conditionRound()
    {
        $this->builder
            ->leftJoin('telemarketing.Stack AS S', function (JoinClause $j) {
                //Pro vice cross kampani se musi kontrolovat vsechny
                $campaigns = [$this->getCampaignId()];
                if (in_array($this->getCampaignId(), [607, 609, 789, 829, 831])) {
                    $campaigns = [607, 609, 789, 829, 831];
                }

                $j->on('S.targetSubject_id', '=', 'UNST.firm_id')
                    ->whereIn('S.campaign_id', $campaigns)
                    ->on($this->raw('DATE(S.createdAt)'), '>=', $this->raw('DATE(NOW() - INTERVAL 60 DAY)'));
            })->whereNull('S.stack_id');
    }

    private function conditionRis35Days()
    {
        $this->builder
            ->leftJoin('obis.RenewalFirm AS RF2', function (JoinClause $j) {
                $j->on('RF2.firm_id', '=', 'UNST.firm_id')
                    ->on($this->raw('DATE(RF2.createdAt)'), '>=', $this->raw('DATE(NOW() - INTERVAL 35 DAY)'));
            })->whereNull('RF2.renewalFirm_id');
    }

    private function conditionRis()
    {
        $this->builder
            ->leftJoin('obis.RenewalFirm AS RF', function (JoinClause $j) {
                $j->on('RF.firm_id', '=', 'UNST.firm_id')
                  ->on($this->raw('DATE(RF.createdAt)'), '>=', $this->raw('DATE(NOW() - INTERVAL 90 DAY)'));
            })
            ->leftJoin('obis.RenewalFirmHistory AS RFH', function (JoinClause $j) {
                $j->on('RFH.renewalFirm_id', '=','RF.renewalFirm_id')
                    ->on($this->raw('DATE(RFH.createdAt)'), '>=', $this->raw('DATE(NOW() - INTERVAL 25 DAY)'));
            })
            ->leftJoin('obis.RenewalFirm AS RF2', function (JoinClause $j) {
                $j->on('RF2.firm_id', '=', 'UNST.firm_id')
                    ->whereNotIn('RF2.state', ['declined', 'renewal', 'removed']);
            })
            ->whereNull('RFH.renewalFirmHistory_id')
            ->whereNull('RF2.renewalFirm_id');
    }

    private function conditionOrdersInTwoMonths()
    {
        $this->builder
            ->leftJoin('obis.Order AS O2', function (JoinClause $j) {
                $j->on('O2.firm_id', '=', 'UNST.firm_id')->on($this->raw('DATE(O2.createdOn)'), '>=', $this->raw('DATE(NOW() - INTERVAL 2 MONTH)'));
            })->whereNull('O2.firm_id');
    }

    private function conditionProcessedBlockedOrders()
    {
        $this->builder
            ->leftJoin('obis.Order AS O', function (JoinClause $j) {
                $j->on('O.firm_id', '=', 'UNST.firm_id')->whereIn('O.state', ['processed', 'blocked']);
            })->whereNull('O.order_id');
    }

    private function conditionCrossCampaigns()
    {
        $ids = ItParam::getCrossCampaignIds([$this->getCampaignId()]);

        $this->builder->leftJoin('telemarketing.Stack AS TMS_L', function (JoinClause $j) use ($ids) {
            $j->on('TMS_L.targetSubject_id', '=', 'UNST.firm_id')
                ->whereIn('TMS_L.campaign_id', $ids)
                ->on($this->raw('DATE(TMS_L.createdAt)'), '>=', $this->raw('DATE(NOW() - INTERVAL 30 DAY)'));
        })->whereNull('TMS_L.stack_id');
    }
}
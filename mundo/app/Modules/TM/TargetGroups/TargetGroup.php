<?php

namespace Mundo\Modules\TM\TargetGroups;


use Illuminate\Database\Connection;
use Illuminate\Database\Query\Builder;
use Mundo\Models\TM\Campaign;
use Mundo\Modules\TM\Sources\Support\SourceManager;

/**
 * Class TargetGroup
 * @package Mundo\Modules\TM\TargetGroups
 */
abstract class TargetGroup implements ITargetGroup
{
    /**
     * @var Campaign
     */
    private $campaign;
    /**
     * @var SourceManager
     */
    private $source;
    /**
     * @var Builder
     */
    protected $builder = null;
    /**
     * @var null
     */
    protected $tableName = null;
    /**
     * @var null
     */
    protected $identificator = null;
    /**
     * @var bool
     */
    protected $distinct = true;

    /**
     * TargetGroup constructor.
     *
     * @param Campaign      $campaign
     * @param SourceManager $source
     */
    public function __construct(Campaign $campaign, SourceManager $source)
    {
        $this->campaign = $campaign;
        $this->source = $source;

        $this->prepareQuery();
    }

    /**
     * @param null $limit
     *
     * @return array|static[]
     */
    public function get($limit = null)
    {
        if ($this->builder === null) {
            $this->prepareQuery();
        }

        if (!is_null($limit)) {
            $this->builder->limit($limit);
        }
        return $this->builder->get();
    }

    /**
     * @return int
     */
    public function count()
    {
        if ($this->builder === null) {
            $this->prepareQuery();
        }

        return count($this->builder->get());
    }

    /**
     * @return string
     */
    public function sql()
    {
        if ($this->builder === null) {
            $this->prepareQuery();
        }

        return get_sql($this->builder);
    }

    /**
     * @return Connection
     */
    protected function db()
    {
        return $this->source->get('b2m')->getConnection();
    }

    protected function getCampaignId()
    {
        return $this->campaign->sync_id === null ? $this->campaign->id : $this->campaign->sync_id;
    }

    protected function getCampaignLang()
    {
        if ($this->campaign->sync_id === null) return false;

        if ($campaign = $this->db()->table('telemarketing.Campaign')->where('campaign_id', $this->campaign->sync_id)->first()) {
            return $campaign->lang;
        }

        return false;
    }

    /**
     * @param $value
     *
     * @return \Illuminate\Database\Query\Expression
     *
     */
    protected function raw($value)
    {
        return $this->db()->raw($value);
    }

    /**
     *
     *
     * @return void
     */
    protected function getBasicQuery()
    {
        $this->builder->from(sprintf('%s AS UNST', $this->tableName));
        $this->builder->addSelect(sprintf('UNST.%s AS targetSubject_identificator', $this->identificator));
    }

    /**
     * @return void
     */
    protected abstract function getStacksQuery();

    /**
     * získání jazyku pro kampan (nové tm jazyk neřeší, ale bohužel z historických důvodů stále rozdělujeme kampaně na
     * CZ/SK -> jazyk se bere v potaz v pripade, ze je nastaveno sync_id u kampane
     *
     * @return void
     */
    protected abstract function applyLang();

    /**
     *
     */
    private function prepareQuery()
    {
        $this->builder = new Builder($this->db());

        if($this->distinct) {
            $this->builder->distinct();
        }

        $this->getBasicQuery();
        $this->getStacksQuery();
        $this->applyLang();
    }
}
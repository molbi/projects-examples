<?php

namespace Mundo\Modules\TM\TargetGroups;

interface ITargetGroup
{
    public function get($limit = null);
    public function count();
    public function sql();
}
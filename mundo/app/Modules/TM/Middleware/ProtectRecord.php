<?php

namespace Mundo\Modules\TM\Middleware;

use Closure;
use Mundo\Models\TM\Record;

class ProtectRecord
{
    public function handle($request, Closure $next)
    {
        if ($request->user()->isAdmin()) {
            return $next($request);
        }

        /** @var Record $record */
        $record = $request->route('records');

        if ($record->isTerminal() || !$record->isOwnedBy($request->user())) {
            return redirect()->route('records.index');
        }

        return $next($request);
    }
}
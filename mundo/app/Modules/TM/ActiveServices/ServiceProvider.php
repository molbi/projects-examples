<?php

namespace Mundo\Modules\TM\ActiveServices;

use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider as BaseProvider;

class ServiceProvider extends BaseProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Manager::class, function (Application $app) {
            return new Manager($app['tm.sources']);
        });

        $this->app->alias(Manager::class, 'tm.active-services');
    }

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        UseActiveServices::setServicesManager($this->app['tm.active-services']);
    }
}

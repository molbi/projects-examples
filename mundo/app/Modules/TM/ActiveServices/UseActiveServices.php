<?php

namespace Mundo\Modules\TM\ActiveServices;

/**
 * Class UseActiveServices
 * @package Mundo\Modules\TM\ActiveServices
 */
trait UseActiveServices
{
    /** @var Manager */
    protected static $manager;

    /** @var null array */
    public $activeServices = null;

    /**
     * Set instance for source manager for all instances of model
     *
     * @param Manager $manager
     */
    public static function setServicesManager(Manager $manager)
    {
        static::$manager = $manager;
    }

    /**
     * @return Manager
     */
    public static function getServicesManager()
    {
        return static::$manager;
    }

    /**
     * @param $value
     *
     */
    public function setActiveServicesAttribute($value)
    {
        $this->attributes['activeServices'] = $value;
    }

    /**
     * @param null $service
     *
     * @return bool
     */
    public function hasService($service = null)
    {
        return in_array($service, $this->activeServices());
    }

    /**
     * @param array $services
     *
     * @return bool
     */
    public function hasAnyServices(array $services = [])
    {
        return !empty(array_intersect($this->activeServices(), $services));
    }

    /**
     * @param array $excludedServices
     *
     * @return bool
     */
    public function hasActiveServicesWithout(array $excludedServices = [])
    {
        return !empty(array_diff($this->activeServices(), $excludedServices));
    }

    /**
     *  Return collection of services
     */
    public function activeServices()
    {
        if (is_null($this->activeServices)) {
            $this->activeServices = $this->getServicesManager()->get($this);
        }

        return $this->activeServices;
    }
}

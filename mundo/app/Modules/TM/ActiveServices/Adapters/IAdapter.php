<?php

namespace Mundo\Modules\TM\ActiveServices\Adapters;

interface IAdapter
{
    public function get();
}

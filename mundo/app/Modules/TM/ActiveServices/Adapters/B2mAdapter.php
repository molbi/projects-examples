<?php

namespace Mundo\Modules\TM\ActiveServices\Adapters;

class B2mAdapter extends BaseAdapter implements IAdapter
{

    public function get()
    {
        return array_unique(array_merge_recursive($this->feeServices(), $this->creditServices()));
    }

    private function feeServices()
    {
        return $this->b2mDb()
            ->table('business.Firm_Service')
            ->where('firm_id', $this->record->remote_id)
            ->pluck('service');

    }

    private function creditServices()
    {
        return $this->b2mDb()
            ->table('business.Firm_CreditService')
            ->where('firm_id', $this->record->remote_id)
            ->pluck('service');
    }

    public function getForRecords($ids)
    {
        $data = [];
        foreach(array_assoc(array_merge($this->feeServicesForRecords($ids), $this->creditServicesForRecords($ids)), 'firm_id|service') as $firm => $services)  {
            $data[$firm] = array_keys($services);
        }

        return $data;
    }

    private function feeServicesForRecords($ids)
    {
        return $this->b2mDb()
            ->table('business.Firm_Service')
            ->whereIn('firm_id', $ids)
            ->get();

    }

    private function creditServicesForRecords($ids)
    {
        return $this->b2mDb()
            ->table('business.Firm_CreditService')
            ->whereIn('firm_id', $ids)
            ->get();
    }
}

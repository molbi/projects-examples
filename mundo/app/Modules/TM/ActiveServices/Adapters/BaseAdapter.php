<?php
/**
 * Created by PhpStorm.
 * User: kubahavle
 * Date: 21.11.16
 * Time: 15:27
 */

namespace Mundo\Modules\TM\ActiveServices\Adapters;


use Mundo\Models\TM\Record;
use Mundo\Modules\TM\Sources\Support\SourceManager;

class BaseAdapter
{
    /**
     * @var SourceManager
     */
    protected $sources;
    /**
     * @var Record
     */
    protected $record;

    public function __construct(SourceManager $sources, Record $record = null)
    {
        $this->sources = $sources;
        $this->record = $record;
    }

    protected function b2mDb()
    {
        return $this->sources->get('b2m')->getConnection();
    }

}
<?php

namespace Mundo\Modules\TM\ActiveServices;

use Mundo\Models\TM\Record;
use Mundo\Modules\TM\ActiveServices\Adapters\B2mAdapter;
use Mundo\Modules\TM\ActiveServices\Adapters\IAdapter;
use Mundo\Modules\TM\Sources\Support\SourceManager;

/**
 * Class Manager
 * @package Mundo\Modules\TM\ActiveServices
 */
class Manager
{
    /**
     * @var SourceManager
     */
    private $sources;
    /**
     * @var IAdapter
     */
    private $adapter;

    /**
     * Manager constructor.
     *
     * @param SourceManager $sources
     */
    public function __construct(SourceManager $sources)
    {
        $this->sources = $sources;
    }


    /**
     *
     * @param Record $record
     *
     * @return array
     */
    public function get(Record $record)
    {
        if (class_exists($klassName = $this->getAdapterName($record->campaign->source))) {
            /** @var IAdapter $klass */
            $klass = new $klassName($this->sources, $record);

            return $klass->get();
        }

        return [];
    }

    /**
     * @param $source
     *
     * @return string
     */
    private function getAdapterName($source)
    {
        return sprintf('%s\Adapters\%sAdapter', __NAMESPACE__, ucfirst($source));
    }

    public function fillArray($records, $source)
    {
        if (!is_array($records) && !$records instanceof \Traversable) {
            throw new \InvalidArgumentException('Records have to be traversable.');
        }

        // TODO - Nutné předělat na více univerzální řešení. Ověřit zda jsou všechny záznamy stejného typu, pak je 100 % možné určit jaký adaptér je potřeba
        /** @var B2mAdapter $adapter */
        $adapter = $this->getAdapter($source);
        $data = $adapter->getForRecords($records->pluck('remote_id'));

        foreach ($records as $record) {
            $record->activeServices = isset($data[$record->getRemoteId()]) ? $data[$record->getRemoteId()] : [];
        }
    }

    private function getAdapter($source)
    {
        if ($this->adapter !== null) {
            return $this->adapter;
        }

        if (class_exists($klassName = $this->getAdapterName($source))) {
            /** @var IAdapter $klass */
            $klass = new $klassName($this->sources);
            $this->adapter = $klass;
        }

        return $this->adapter;
    }
}

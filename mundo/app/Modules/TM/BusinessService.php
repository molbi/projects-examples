<?php

namespace Mundo\Modules\TM;

use Illuminate\Database\Connection;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Database\QueryException;
use Mundo\Models\TM\Record;
use Mundo\Models\User;
use Notify;

class BusinessService
{
    /** @var  Connection */
    protected $connection;

    /**
     * BusinessService constructor.
     *
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        ini_set("memory_limit", "2048M");
        $this->connection = $connection;
    }

    public function loadServiceTitles(array $services)
    {

        $ids = $this->extractServiceIds($services);

        return array_assoc($this->connection->table('obis.Service')
            ->select([
                'service_id',
                'title',
                'lang',
                'price',
                'duration',
            ])
            //->where('Service.active', 't')
            ->whereIn('Service.service_id', $ids)
            ->get(), 'service_id');
    }

    public function loadCategories($lang)
    {
        return $this->connection->table('business.Category')
            ->select(['Category.category_id', 'CategoryLang.title'])
            ->join('business.CategoryLang', function (JoinClause $join) use ($lang) {
                $join->on('Category.category_id', '=', 'CategoryLang.category_id')
                    ->where('CategoryLang.lang', '=', $lang);
            })
            ->where('Category.type', 'firm')
            ->orderBy('CategoryLang.title')
            ->pluck('title', 'category_id');
    }

    protected function extractServiceIds($def)
    {
        $res = [];
        if (is_array($def) && isset($def['items'])) {
            return array_pluck($def['items'], 'id');
        } elseif (is_array($def)) {
            foreach ($def as $next) {
                $res[] = $this->extractServiceIds($next);
            }
        }

        return array_unique(array_flatten($res));
    }

    public function isFromBusiness(Record $record)
    {
        return $record->campaign->source === 'b2m';
    }

    protected function getAddressData(array $data)
    {
        return [
            'title'          => $data['company'],
            'street'         => $data['street'],
            'city'           => $data['city'],
            'postalCode'     => $data['postal_code'],
            'cityPostalCode' => sprintf('%s %s', $data['postal_code'], $data['city']),
        ];
    }

    protected function getFirmData(array $data)
    {
        return [
            'title'          => $data['company'],
            'street'         => $data['street'],
            'postalCode'     => $data['postal_code'],
            'cityPostalCode' => sprintf('%s %s', $data['postal_code'], $data['city']),
            'email'          => $data['email'],
            'web'            => empty($data['web']) ? null : $data['web'],
            'phoneNumber'    => empty($data['phone_number']) ? null : $data['phone_number'],
            'faxNumber'      => empty($data['fax']) ? null : $data['fax'],
            'description'    => $data['description'],
            'idNumber'       => empty($data['id_number']) ? null : $data['id_number'],
            'vatNumber'      => empty($data['vat_number']) ? null : $data['vat_number'],
            'contactPerson'  => $data['contact_person'],
            'ipAddress'      => $_SERVER['REMOTE_ADDR'],
        ];
    }

    protected function getFirm($firm_id)
    {
        return $this->connection->table('business.Firm')
            ->select('*')
            ->where('firm_id', $firm_id)
            ->first();
    }

    public function insertFirm(array $data)
    {
        $addressData = array_merge($this->getAddressData($data), [
            'country' => $this->langToRegion($data['lang']),
        ]);

        $addressId = $this->connection->table('business.Address')
            ->insertGetId($addressData);

        $firmData = array_merge($this->getFirmData($data), [
            'address_id' => $addressId,
            'state'      => 'temporary',
            'lang'       => $data['lang'],
            'country'    => $this->langToRegion($data['lang']),
            'ExtraType'  => $this->langToRegion($data['lang']),
        ]);

        $firmId = $this->connection->table('business.Firm')
            ->insertGetId($firmData);

        $firm = $this->getFirm($firmId);
        $this->updateFirmCategories($firm, array_unique($data['categories']));

        return $firm;
    }

    public function updateFirm(array $data)
    {
        $firm = $this->getFirm($data['firm_id']);

        $this->connection->table('business.Address')
            ->where('address_id', $firm->address_id)
            ->update($this->getAddressData($data));

        $this->connection->table('business.Firm')
            ->where('firm_id', $firm->firm_id)
            ->update($this->getFirmData($data));

        $firm = $this->getFirm($firm->firm_id);

        $oldFirmData = $this->getOldFirmData((array)$firm);
        $db = ($firm->lang == 'cs') ? 'businesscontact' : 'businesscontact_eu';

        if (empty($firm->ID_BCFirm)) {
            try {
                $id = $this->connection->table("$db.BCFirm")
                    ->insertGetId($oldFirmData);
            } catch (QueryException $e) {
                $id = $this->connection->table("$db.BCFirm")
                    ->where('firm_id', $firm->firm_id)
                    ->first(['ID_BCFirm'])->ID_BCFirm;
            }

            $this->connection->table('business.Firm')
                ->where('firm_id', $firm->firm_id)
                ->update(['ID_BCFirm' => $id]);
        } else {
            $this->connection->table("$db.BCFirm")
                ->where('ID_BCFirm', $firm->ID_BCFirm)
                ->update($oldFirmData);
        }

        $this->updateFirmCategories($firm, array_unique($data['categories']));

        return $firm;
    }

    public function updateFirmCategories($firm, array $categories)
    {
        $this->connection->table('business.Firm_Category')
            ->where('firm_id', $firm->firm_id)
            ->delete();
        foreach ($categories as $category) {
            if ($category > 0) {
                $this->connection->table('business.Firm_Category')
                    ->insert(['firm_id' => $firm->firm_id, 'ID_BCFirmRegion' => $category]);
            }
        }

        if (isset($firm->ID_BCFirm)) {
            $db = ($firm->lang == 'cs') ? 'businesscontact' : 'businesscontact_eu';
            $this->connection->table("$db.BCRegRelFirm")
                ->where('ID_BCFirm', $firm->ID_BCFirm)
                ->delete();
            foreach ($categories as $category) {
                if ($category > 0) {
                    $this->connection->table("$db.BCRegRelFirm")
                        ->insert(['ID_BCFirm' => $firm->ID_BCFirm, 'ID_BCFirmRegion' => $category]);
                }
            }
        }
    }

    public function createOrder($firmId, $source, $userId, array $serviceIds, $note = null, $garantion = false, $garantionType = null)
    {
        $firm = $this->getFirm($firmId);
        $orderData = [
            'firm_id'            => $firm->firm_id,
            'lang'               => $firm->lang,
            'source'             => $source,
            'employee_id'        => $userId,
            'contactPerson'      => $firm->contactPerson,
            'contactPersonEmail' => $firm->email,
            'ipAddress'          => $_SERVER['REMOTE_ADDR'],
            'garantion'          => $garantion,
            'garantionType'      => $garantionType
        ];

        $orderId = $this->connection->table('obis.Order')->insertGetId($orderData);
        foreach ($serviceIds as $id) {
            $this->connection->table('obis.Order_Service')
                ->insert(['order_id' => $orderId, 'service_id' => $id]);
        }
        if ($note) {
            $this->connection->table('obis.OrderNote')
                ->insert(['order_id' => $orderId, 'type' => 'admin', 'content' => $note, 'employee_id' => $userId]);
        }

        return $orderId;
    }

    /**
     * @param array $firmData
     *
     * @return array
     */
    protected function getOldFirmData(array $firmData)
    {
        $oldFirmData = [];
        $map = [
            'title'          => 'FirmName',
            'street'         => 'FirmAdrress',
            'email'          => 'FirmEmail',
            'phoneNumber'    => 'FirmPhone',
            'contactPerson'  => 'FirmContact',
            'idNumber'       => 'FirmIDOne',
            'vatNumber'      => 'FirmIDTwo',
            'description'    => 'FirmMemo',
            'web'            => 'FirmUrl',
            'faxNumber'      => 'FirmFax',
            'cityPostalCode' => 'FirmCity',
            'firm_id'        => 'firm_id',
        ];

        foreach ($firmData as $key => $value) {
            if (isset($map[$key])) {
                $oldFirmData[$map[$key]] = $value;
            }
        }

        $oldFirmData['LastRevizion'] = Date('Y-m-d H:i:s');
        $oldFirmData['OwnDateTime'] = Date('Y-m-d H:i:s');
        $oldFirmData['OwnIP'] = $_SERVER['REMOTE_ADDR'];
        if ($firmData['lang'] != 'cs') {
            $oldFirmData['ExtraType'] = strtolower($firmData['lang']);
        }

        return $oldFirmData;
    }


    /**
     * @param $lang
     *
     * @return string
     * @internal param array $data
     *
     */
    protected function langToRegion($lang)
    {
        return $lang == 'cs' ? 'CZ' : strtoupper($lang);
    }

    public function createNote($firmId, $note, User $user = null)
    {
        $this->connection->table('obis.FirmNote')
            ->insert([
                'firm_id'     => $firmId,
                'title'       => 'Remingering',
                'note'        => $note,
                'employee_id' => is_null($user) ? null : $user->id,
                'type'        => 'global'
            ]);
    }

    public function insertToStack($targetSubject_id, $campaign_id)
    {
        $this->connection->table('telemarketing.Stack')
            ->insert([
                'targetSubject_id' => $targetSubject_id,
                'state'            => 'pending',
                'campaign_id'      => $campaign_id,
            ]);
    }
}

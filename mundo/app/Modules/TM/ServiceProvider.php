<?php

namespace Mundo\Modules\TM;

use Illuminate\Support\ServiceProvider as BaseProvider;
use Mundo\Models\TM\Reminder;
use Mundo\Modules\TM\Index\CampaignType;
use Mundo\Modules\TM\Index\RecordType;
use Mundo\Modules\TM\Sources\Index\B2MChangesResolver;
use Mundo\Modules\TM\Sources\Index\ChangesResolver;
use Mundo\Modules\TM\Sources\Index\KartusChangesResolver;
use Mundo\Modules\TM\Sources\Index\RegFirmChangesResolver;
use Mundo\Modules\TM\Sources\Index\RenewalChangesResolver;
use Mundo\Modules\TM\Sources\Index\SyncIndexCommand;
use Mundo\Modules\TM\Sources\Index\TmChangesResolver;

class ServiceProvider extends BaseProvider
{
    public function boot()
    {
        Reminder::setConnection($this->app->make('db')->connection('b2m'));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerIndexChangesResolver();
        $this->app->register(Sources\Support\ServiceProvider::class);
        $this->app->register(ActiveServices\ServiceProvider::class);
        $this->app->singleton(BusinessService::class, function($app) {
            return new BusinessService($app['db']->connection('b2m'));
        });
    }
    
    protected function registerIndexChangesResolver()
    {
        $this->app->singleton(ChangesResolver::class, function ($app) {
            $resolver = new ChangesResolver();

            $resolver->addResolver('b2m', new B2MChangesResolver($app['db']->connection('b2m')));
            $resolver->addResolver('kartus', new KartusChangesResolver($app['db']->connection('b2m')));
            $resolver->addResolver('reg-firm', new RegFirmChangesResolver($app['db']->connection('b2m')));
            $resolver->addResolver('tm', new TmChangesResolver($app['db']->connection('b2m')));
            $resolver->addResolver('renewal', new RenewalChangesResolver($app['db']->connection('b2m')));

            return $resolver;
        });

        $this->commands([
            SyncIndexCommand::class,
        ]);
    }
}

<div class="tiles tiles-purple">
    <div class="tiles-body">
        <div class="tiles-title">Zaplacený obrat</div>
        <div class="heading small">{{ number_format($data->paid_orders, 0, ',', ' ') }} Kč</div>
        <div class="tiles-title">Provize</div>
        <div class="heading small">{{ number_format($data->commissions, 0, ',', ' ') }} Kč</div>
    </div>
</div>
<?php

namespace Mundo\Widgets\Commissions;

use InvalidArgumentException;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\User;
use Mundo\Widgets\Commissions\Type\ICommissions;


/**
 * Class Commissions
 * @package Mundo\Widgets\Commissions
 */
class Commissions extends BaseWidget
{
    /** @var  User */
    protected $user;

    protected $type = null;


    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function render()
    {
        return $this->renderView(__DIR__ . '/views/panel.blade.php', [
            'data' => $this->get(),
        ]);
    }

    /**
     * @return object
     */
    public function get()
    {
        return (object)[
            'commissions' => $this->getType()->getCommissions(),
            'paid_orders' => $this->getType()->getPaidOrders(),
        ];
    }


    /**
     * @return ICommissions
     * @throws \Exception
     */
    protected function getType()
    {
        if (is_null($type = $this->user->param('tm_commissions'))) {
            throw new InvalidArgumentException("User has not set commissions class!");
        }

        if (!class_exists($object = sprintf('%s\Type\%s', __NAMESPACE__, $type))) {
            throw new \Exception(sprintf('Class %s does not exists!', $object));
        };

        if(!is_null($this->type)) {
            return $this->type;
        }

        return $this->type = $this->getRoot()->getApplication()->make($object, [$this->user]);
    }

}
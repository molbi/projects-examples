<?php

namespace Mundo\Widgets\Commissions\Type;

use Carbon\Carbon;
use Mundo\Kernel\Cache\Cache;
use Mundo\Models\User;


/**
 * Class Business
 * @package Mundo\Widgets\Commissions\Type
 */
class Business implements ICommissions
{
    /**
     *
     */
    private $bonusPaidOrders = [
        0      => 0,
        60000  => 1,
        70000  => 2,
        80000  => 2.5,
        90000  => 3,
        100000 => 3.5,
        120000 => 4,
        140000 => 4.5,
        160000 => 5,
        180000 => 5.5,
        200000 => 6,
        220000 => 6.5,
        250000 => 7,
        300000 => 8,
        350000 => 9,
        400000 => 10,
    ];


    /**
     * @var array
     */
    private $bonusPaymentMorals = [
        72 => 1000,
        73 => 1500,
        76 => 2000,
        78 => 3000,
    ];

    /**
     * @var User
     */
    private $user;
    /**
     * @var Cache
     */
    private $cache;

    /**
     * @var int
     */
    private $paid_orders = null;

    /**
     * @var int
     */
    private $commissions = null;

    /**
     * Business constructor.
     *
     * @param User  $user
     * @param Cache $cache
     */
    public function __construct(User $user, Cache $cache)
    {
        $this->user = $user;
        $this->cache = $cache;

        $this->paidOrders();
        $this->commissionsFromPaidOrders();
        $this->commissionsFromPaymentMorals();

    }

    /**
     *
     */
    private function commissionsFromPaidOrders()
    {
        foreach ($this->bonusPaidOrders as $bonus => $bonusPercentage) {
            if ($this->paid_orders >= $bonus) {
                $this->commissions = (($this->paid_orders / 100) * $bonusPercentage);
            }
        }
    }

    /**
     *
     */
    private function commissionsFromPaymentMorals()
    {
        $data = $this->cache->getforUser($this->user, Cache::PAYMENTS_MORALS);
        $month = Carbon::now()->month - 2;

        if (!isset($data[$month])) {
            return;
        }

        $pm = $data[$month];
        $percent = ($pm->paid / $pm->total * 100);

        if ($percent >= array_first($this->bonusPaymentMorals)) {
            $final_bonus = 0;
            foreach ($this->bonusPaymentMorals as $per => $bonus) {
                $final_bonus = $bonus;

                if ($percent < $per) {
                    break;
                }
            }

            $this->commissions += $final_bonus;
        }
    }

    /**
     *
     */
    private function paidOrders()
    {
        $this->paid_orders = array_sum(array_map(function ($item) {
            return $item->price;
        }, $this->cache->getForUser($this->user, Cache::PAID_ORDERS)));
    }

    /**
     * @return mixed
     */
    public function getCommissions()
    {
        return $this->commissions;
    }

    /**
     * @return mixed
     */
    public function getPaidOrders()
    {
        return $this->paid_orders;
    }
}

<?php

namespace Mundo\Widgets\Commissions\Type;

/**
 * Interface ICommissions
 * @package Mundo\Widgets\CommissionsPerformance\Commissions
 */
interface ICommissions
{
    /**
     * @return mixed
     */
    public function getCommissions();
    public function getPaidOrders();
}
<?php

namespace Mundo\Widgets\Commissions\Type;

use Carbon\Carbon;
use Mundo\Kernel\Cache\Cache;
use Mundo\Models\User;

/**
 * Class Contact
 * @package Mundo\Widgets\Commissions\Type
 */
class Contact implements ICommissions
{

    /** @var array */
    private $order_price_bonus = [
        3000 => 200,
        4000 => 250,
        5000 => 300,
        6000 => 330,
        7000 => 350,
        8000 => 380,
    ];
    /** @var int */
    private $limitPaymentMorals = 80;
    /** @var User */
    private $user;
    /** @var Cache */
    private $cache;
    /** @var null */
    private $paid_orders = 0;
    /** @var int */
    private $commissions = 0;


    /**
     * Business constructor.
     *
     * @param User  $user
     * @param Cache $cache
     *
     */
    public function __construct(User $user, Cache $cache)
    {
        $this->user = $user;
        $this->cache = $cache;

        $this->paidOrders();
        $this->commissionsFromPaidOrders();
        $this->commissionsFromPaymentMorals();
    }

    /**
     * Commissions from paid orders, in case that the price of order is not in array $order_price_bonus,
     * we round down the price of order to thousands and find bonus in array.
     */
    private function commissionsFromPaidOrders()
    {
        foreach ($this->cache->getForUser($this->user, Cache::PAID_ORDERS) as $order) {
            if (isset($this->order_price_bonus[$order->price])) {
                $this->commissions += $this->order_price_bonus[$order->price];
                continue;
            }

            if (isset($this->order_price_bonus[$price = (int)round($order->price, -3)])) {
                $this->commissions += $this->order_price_bonus[$price];
                continue;
            }
        }
    }

    /**
     *
     */
    private function commissionsFromPaymentMorals()
    {
        $data = $this->cache->getForUser($this->user, Cache::PAYMENTS_MORALS);

        if (isset($data[Carbon::now()->month - 1])) {
            $pm = $data[Carbon::now()->month - 1];
            $diff = ($percent = number_format(($pm->paid / $pm->total * 100), 2)) - $this->limitPaymentMorals;

            if ($diff != 0) {
                $this->commissions = $this->commissions * ((100 + $diff) / 100);
            }
        }
    }

    /**
     * Sum price from paid orders
     */
    private function paidOrders()
    {
        $this->paid_orders = array_sum(array_map(function ($item) {
            return $item->price;
        }, $this->cache->getForUser($this->user, Cache::PAID_ORDERS)));
    }

    /**
     * @return mixed
     */
    public function getCommissions()
    {
        return $this->commissions;
    }

    /**
     * @return int
     */
    public function getPaidOrders()
    {
        return $this->paid_orders;
    }
}

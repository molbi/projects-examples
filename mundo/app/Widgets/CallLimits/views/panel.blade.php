<div class="split">
    <div class="split-half">
        <div class="tiles tiles-default">
            <div class="tiles-body">
                <div class="tiles-title">Provolaný čas</div>
                <div class="heading small">{{ gmdate("H:i:s", $callTime->value) }}</div>
                <div class="progress transparent progress-small no-radius">
                    <div class="progress-bar progress-bar-default animate-progress-bar"
                         style="width: {{ min($callTime->percent * 100, 100) }}%;"></div>
                </div>
                <div class="description">
                    váš limit <strong>{{ gmdate("H:i:s", $callTime->target) }}</strong>.
                </div>
            </div>
        </div>
    </div>
    <div class="split-half">
        <div class="tiles tiles-blue">
            <div class="tiles-body">
                <div class="tiles-title">Spojené hovory</div>
                <div class="heading small">{{ number_format($answered->value, 0, ',', ' ')  }}</div>
                <div class="progress transparent progress-small no-radius">
                    <div class="progress-bar progress-bar-white animate-progress-bar"
                         style="width: {{ min($answered->percent * 100, 100) }}%;"></div>
                </div>
                <div class="description">
                    váš limit <strong>{{ number_format($answered->target, 0, ',', ' ') }}</strong>.
                </div>
            </div>
        </div>
    </div>
</div>

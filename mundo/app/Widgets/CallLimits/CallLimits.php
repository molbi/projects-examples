<?php

namespace Mundo\Widgets\CallLimits;

use Carbon\Carbon;
use Illuminate\Database\Connection;
use Illuminate\Database\DatabaseManager;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\User;

class CallLimits extends BaseWidget
{
    /** @var Cache */
    protected $cache;
    /** @var  User */
    protected $user;

    /**
     * TalkTime constructor.
     * @param \Mundo\Models\User $user
     * @param \Mundo\Widgets\CallLimits\Cache $cache
     */
    public function __construct(User $user, Cache $cache)
    {
        $this->cache = $cache;
        $this->user = $user;
    }

    protected function getCallTime()
    {
        return (object)[
            'value'   => $value = $this->getUserStats()->call_time,
            'percent' => $value / ($target = $this->user->param('tm_call_time')),
            'target'  => $target,
        ];
    }

    protected function getAnswered()
    {
        return (object)[
            'value'   => $value = $this->getUserStats()->answered,
            'percent' => $value / ($target = $this->user->param('tm_call_answered')),
            'target'  => $target,
        ];
    }


    public function render()
    {
        return $this->renderView(__DIR__ . '/views/panel.blade.php', [
            'callTime' => $this->getCallTime(),
            'answered' => $this->getAnswered(),
        ]);
    }

    protected function getUserStats()
    {
        if ($result = $this->cache->get($this->user)) {
            return $result;
        }

        return (object)[
            'call_time' => 0,
            'answered'  => 0,
        ];
    }
}

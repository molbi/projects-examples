<?php

namespace Mundo\Widgets\CallLimits;

use Carbon\Carbon;
use Illuminate\Cache\Repository;
use Illuminate\Database\Connection;
use Mundo\Kernel\Cron\Cronable;
use Mundo\Models\User;

class Cache implements Cronable
{
    const KEY = 'widget:talk-time';

    /** @var \Illuminate\Cache\Repository */
    protected $cache;

    public function __construct(Repository $cache)
    {
        $this->cache = $cache;
    }

    public function get(User $user, Carbon $date = null)
    {
        $date = $this->defaultDate($date);
        $data = $this->cache->get($this->getKey($date), []);
        if (!isset($data[$user->id])) {
            return null;
        }

        return $data[$user->id];
    }

    public function run(Connection $db = null, Carbon $date = null)
    {
        $date = $this->defaultDate($date);
        $data = $db->query()
            ->from('phony.user_out_stats as t')
            ->whereIn('t.user_id', User::all()->pluck('id'))
            ->where('t.date', '=', $date->toDateString())
            ->get();
        $data = array_assoc($data, 'user_id');
        $this->cache->forever($this->getKey($date), $data);
    }

    /**
     * @param  Carbon $date
     * @return string
     */
    protected function getKey(Carbon $date)
    {
        return self::KEY . '-' . $date->toDateString();
    }

    /**
     * @param $date
     * @return Carbon
     */
    protected function defaultDate($date)
    {
        if ($date === null) {
            return Carbon::now();
        }

        return $date;
    }
}

<?php

namespace Mundo\Widgets\EuroCompetition;

use Carbon\Carbon;
use Illuminate\Cache\Repository;
use Illuminate\Database\Connection;
use Illuminate\Database\Query\JoinClause;
use Mundo\Kernel\Cron\Cronable;
use Mundo\Models\User;

/**
 * Class Cache
 * @package Mundo\Widgets\EuroCompetition
 */
class Cache implements Cronable
{
    /**
     *
     */
    const KEY = 'widget:euro-competition';

    /** @var \Illuminate\Cache\Repository */
    protected $cache;

    /**
     * Cache constructor.
     * @param Repository $cache
     */
    public function __construct(Repository $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @param User $user
     * @return array
     */
    public function get(User $user)
    {
        $data = $this->cache->get(self::KEY, []);
        if (!isset($data[$user->id])) {
            return [];
        }

        return $data[$user->id];
    }

    /**
     * @param Connection|null $db
     */
    public function run(Connection $db = null)
    {
        $this->cache->forever(self::KEY, $this->getOrders($db));
    }

    /**
     * @param Connection $db
     * @return array|static[]
     */
    protected function getData(Connection $db)
    {
        return $db->query()
            ->select([
                $db->raw('SUM(I.price) AS price'),
                $db->raw('MONTH(OH.changedAt) AS month'),
                'O.lang',
                'O.employee_id'
            ])
            ->from('obis.Order AS O')
            ->join('obis.OrderHistory AS OH', function (JoinClause $j) {
                $j->on('OH.order_id', '=', 'O.order_id')
                    ->where('OH.action', '=', 'paid');
            })
            ->join('obis.Invoice AS I', function (JoinClause $j) {
                $j->on('I.order_id', '=', 'O.order_id')
                    ->where('I.price', '>', 0);
            })
            ->whereBetween($db->raw('DATE(OH.changedAt)'), [EuroCompetition::DATE_FROM, EuroCompetition::DATE_TO])
            ->whereIn('O.employee_id', array_keys(EuroCompetition::EMPLOYEES))
            ->groupBy($db->raw('MONTH(OH.changedAt)'), 'O.employee_id', 'O.lang')
            ->get();
    }

    /**
     * @param Connection $db
     * @return null
     */
    protected function getOrders(Connection $db)
    {
        return array_assoc(
            $this->getData($db),
            'employee_id|month[]'
        );
    }

}

@if(!isset($disabled))
<div class="tiles tiles-purple">
    <div class="tiles-body">
        <div class="tiles-title">Eurovíkendy - plnění v měsíci</div>

        <div class="heading">{{ round($currentPercent * 100)}} %
        </div>
        <div class="progress transparent progress-small no-radius">
            <div class="progress-bar progress-bar-white animate-progress-bar"
                 style="width: {{ min($currentPercent * 100, 100) }}%;"></div>
        </div>
        <div class="description">
            &nbsp; splněné měsíce: {{ $completedMonths }}
        </div>
    </div>
</div>
@endif
<?php

namespace Mundo\Widgets\EuroCompetition;

use Carbon\Carbon;
use Mundo\Kernel\Constants;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\User;

/**
 * Class EuroCompetition
 * @package Mundo\Widgets\EuroCompetition
 */
class EuroCompetition extends BaseWidget
{
    /** When competition starts */
    const DATE_FROM = '2017-04-01';
    /** When competition ends */
    const DATE_TO = '2017-06-30';
    /** List of employees with their limit */
    const EMPLOYEES = [
        1765 => 204000,
        1065 => 276000,
        1225 => 288000,
        2011 => 156000,
        1849 => 288000,
        1543 => 252000,
        1521 => 288000,
        1923 => 225000,
        1647 => 240000
    ];

    /**
     * @var array
     */
    protected $_holidays = [
        '1-1',
        '6-4',
        '1-5',
        '8-5',
        '5-7',
        '6-7',
        '28-9',
        '28-10',
        '17-11',
        '24-12',
        '25-12',
        '26-12',
    ];
    /** @var array */
    protected $_weekends = [
        'Saturday',
        'Sunday',
    ];

    /** @var  Cache */
    protected $cache;
    /** @var User */
    protected $user;
    /**
     * @var null
     */
    private $target;


    /**
     * EuroCompetition constructor.
     * @param User $user
     * @param Cache $cache
     * @param null $target
     */
    public function __construct(User $user, Cache $cache, $target = null)
    {
        $this->user = $user;
        $this->cache = $cache;
        $this->target = $target;
    }

    /**
     * @return string
     */
    public function render()
    {
        $sums = $this->getSums();

        if (!isset(self::EMPLOYEES[$this->user->id])) {
            return $this->renderView(__DIR__ . '/views/panel.blade.php', [
                'disabled' => true
            ]);
        }

        return $this->renderView(__DIR__ . '/views/panel.blade.php', [
            'completedMonths' => $this->completedMonths($sums),
            'value'           => $value = (isset($sums[Carbon::now()->month]) ? $sums[Carbon::now()->month] : 0),
            'currentPercent'  => $value / self::EMPLOYEES[$this->user->id],
        ]);
    }

    /**
     * @param $sums
     * @return bool
     */
    protected function completedMonths($sums)
    {
        $now = Carbon::now()->month;
        $completed = 0;
        foreach ($sums as $month => $sum) {
            if ($month < $now && $sum > self::EMPLOYEES[$this->user->id]) {
                $completed++;
            }
        }

        return $completed;
    }

    /**
     * @return array
     */
    protected function getSums()
    {
        return array_map(function ($orders) {
            return array_sum(array_map([$this, 'convert'], $orders));
        }, $this->cache->get($this->user));
    }

    /**
     * @param $item
     * @return mixed
     */
    protected function convert($item)
    {
        return $item->price * Constants::RATES[$item->lang];
    }
}
<?php

namespace Mundo\Widgets\UserWidgets;

use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\User;

class UserWidgets extends BaseWidget
{

    /** @var  User */
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function render()
    {
        $widgets = ['top' => [], 'bottom' => []];
        foreach ($this->user->widgets() as $position => $items) {
            if (count($items) > 0) {
                $widgets[$position] = range(0, count($items) - 1);
            } else {
                $widgets[$position] = [];
            }
        }

        return $this->renderView(__DIR__ . '/views/aside.blade.php', compact('widgets'));
    }

    protected function createWidget($name)
    {
        $widgets = $this->user->widgets();
        list($position, $index) = explode('_', $name);
        $def = array_get($widgets->$position, $index);

        return $this->getRoot()->getApplication()->make(
            $def->type,
            array_merge((array)$def->params, ['user' => $this->user])
        );
    }
}

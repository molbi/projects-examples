@unless(empty(array_flatten($widgets)))
    <aside>
        @foreach($widgets as $position => $widgets)
            <div class="{{ $position }}">
                @foreach($widgets as $index)
                    <?php
                    try {
                        echo $widget->getWidget($position . '_' . $index)->render();
                    } catch (Exception $e) {
                        echo "<div class=\"alert alert-danger\"><strong>{$e->getMessage()}</strong><pre>{$e->getTraceAsString()}</pre></div>";
                    }
                    ?>
                @endforeach
            </div>
        @endforeach
    </aside>
@endunless
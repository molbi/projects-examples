<?php

namespace Mundo\Widgets\RM\Contact;

use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\RM\Record;

class Contact extends BaseWidget
{
    /** @var Record */
    protected $record;

    public function __construct(Record $record)
    {
        $this->record = $record;
    }

    public function render()
    {
        return $this->renderView(__DIR__ . '/views/widget.blade.php', [
            'contact' => $this->contact(),
            'record'  => $this->record,
            'view'    => 'b2m',
        ]);
    }

    protected function contact()
    {
        return $this->record->firm;
    }
}

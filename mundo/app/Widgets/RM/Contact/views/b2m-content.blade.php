<table class="table table-bordered">
    <tr>
        <th style="width: 150px; background-color: #fafafa;">Kontaktní osoba</th>
        <td style="width: 350px;">{{ $contact->contactPerson }} (<a target="_blank"
                                                                     href="http://{{ $record->firm->lang == 'cs' ? 'hledat.cz/editace-udaju' : '123dodavatel.sk/editacia-udajov' }}/{{ md5($record->firm_id) }}">editace</a>)
        </td>
        <th style="width: 150px; background-color: #fafafa;">Telefon</th>
        <td>
            {{ phone_number($contact->phoneNumber, lang_to_country($contact->lang)) }}
        </td>
    </tr>
    <tr>
        <th style="background-color: #fafafa;">E-mail</th>
        <td><a href="mailto:{{ $contact->email }}">{{ $contact->email }}</a></td>
        <th style="background-color: #fafafa;">Adresa</th>
        <td>{{ $contact->street }}, {{ $contact->cityPostalCode }}</td>
    </tr>
    <tr>
        <th style="background-color: #fafafa;">IČ</th>
        <td>
            @if($contact->lang === 'cs')
                <a target="_blank"
                   href="http://wwwinfo.mfcr.cz/cgi-bin/ares/ares_es.cgi?jazyk=cz&obch_jm=&ico={{$contact->idNumber}}&cestina=cestina&obec=&k_fu=&maxpoc=200&ulice=&cis_or=&cis_po=&setrid=ZADNE&pr_for=&nace=&xml=1&filtr=1">{{$contact->idNumber}}</a>
            @else
                {{$contact->idNumber}}
            @endif
        </td>
        <th style="background-color: #fafafa;">DIČ</th>
        <td>
            {{$contact->vatNumber}}
        </td>
    </tr>

    @unless($contact->web == null)
        <tr>
            <th style="background-color: #fafafa;">Web</th>
            <td colspan="3">
                <a href="{{ company_url($contact->web) }}" target="_blank">{{$contact->web}}</a>
            </td>
        </tr>
    @endunless

    <tr>
        <th style="background-color: #fafafa;">Popis činnosti</th>
        <td colspan="3">
            {{$contact->description}}
        </td>
    </tr>
</table>
<?php

namespace Mundo\Widgets\RM\History;

use Illuminate\Database\Eloquent\Model;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\EmailLog;
use Mundo\Models\RM\Record;
use Mundo\Models\RM\RecordHistory;

class History extends BaseWidget
{

    /** @var Record */
    protected $record;

    /**
     * History constructor.
     *
     * @param Record $record
     */
    public function __construct(Record $record)
    {
        $this->record = $record;
    }

    public function render()
    {
        return $this->renderView(__DIR__ . '/views/widget.blade.php', [
            'records' => $this->records(),
        ]);
    }

    protected function records()
    {
        return $this->histories()
            ->merge($this->emails())
            ->sortByDesc(function ($history, $key) {
                switch (get_class($history)) {
                    case RecordHistory::class:
                        return strtotime($history->created_at);
                        break;
                    case EmailLog::class:
                        return strtotime($history->created_at);
                        break;
                }
            });
    }

    protected function histories()
    {
        return $this->record->histories;
    }

    protected function emails()
    {
        return $this->record->emailLogs()->with('user')->get();
    }

    public function toViewName(Model $record)
    {
        return 'records/' . $record->getTable();
    }
}

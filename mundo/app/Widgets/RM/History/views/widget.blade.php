{? $modalId = uuid() ?}
<div class="tiles tiles-framed tiles-default widget widget-action-history">
    <div class="tiles-title">
        Historie

        <div class="btn-group">
            <a onclick="$('#widget-history-table').toggle()" class="btn btn-primary btn-xs">zobrazit/skrýt</a>
        </div>

    </div>
    <div class="tiles-body tiles-body-table"
         style="overflow-y: hidden"
    >
        <table class="table table-bordered" id="widget-history-table">
            @foreach($records as $record)
                @partial($widget->toViewName($record), ['record' => $record])
            @endforeach
        </table>
    </div>
</div>
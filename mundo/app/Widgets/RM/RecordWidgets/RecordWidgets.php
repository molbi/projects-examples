<?php

namespace Mundo\Widgets\RM\RecordWidgets;

use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\Module;
use Mundo\Models\RM\Record as RMRecord;

class RecordWidgets extends BaseWidget
{

    /** @var RMRecord */
    protected $record;
    /**
     * @var Module
     */
    private $module;

    public function __construct(RMRecord $record, Module $module = null)
    {
        $this->record = $record;
        $this->module = $module;
    }

    public function render()
    {
        $widgets = $this->getRecordWidgets();

        return $this->renderView(__DIR__ . '/views/widgets.blade.php', compact('widgets'));
    }

    protected function createWidget($name)
    {

        $def = (object)array_get($this->getRecordWidgets(), str_replace('_', '.', $name));

        return $this->getRoot()->getApplication()->make(
            $def->type,
            array_merge((array)$def->params, ['record' => $this->record])
        );
    }

    /**
     * @return array();
     */
    protected function getRecordWidgets()
    {
        return $this->module->getWidgets();
    }
}

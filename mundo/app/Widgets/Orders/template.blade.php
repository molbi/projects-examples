<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Objednávky</h3>
    </div>
    <table class="table">
        <tr>
            <th>ID</th>
        </tr>
        @foreach($orders as $order)
            <tr>
                <td>{{ $order->order_id }}</td>
            </tr>
        @endforeach
    </table>
</div>
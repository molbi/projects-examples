<?php

namespace Mundo\Widgets\Orders;

use Illuminate\Database\Connection;
use Illuminate\Database\DatabaseManager;
use Mundo\Kernel\Widget\Widget as BaseWidget;

class Widget extends BaseWidget
{
    /**
     * The connection instance
     *
     * @var Connection
     */
    protected $connection;

    /**
     * The identification of business.Firm record
     *
     * @var integer
     */
    protected $id;

    /**
     * Widget constructor.
     *
     * @param \Illuminate\Database\DatabaseManager $manager
     * @param array                                $params
     */
    public function __construct(DatabaseManager $manager, array $params)
    {
        $this->connection = $manager->connection($params['connection']);
        $this->id = $params['id'];
    }

    public static function validationRules()
    {
        return [
            'id'         => 'required|integer',
            'connection' => 'required',
        ];
    }

    /**
     * Get the evaluated contents of the object.
     *
     * @return string
     */
    public function render()
    {
        return $this->view('template', ['orders' => $this->getOrders()])->render();
    }

    protected function getOrders()
    {
        return $this->connection->query()->select('order_id')
            ->from('obis.Order')
            ->where('firm_id', '=', $this->id)->get();
    }
}

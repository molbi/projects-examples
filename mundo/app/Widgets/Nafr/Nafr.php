<?php

namespace Mundo\Widgets\Nafr;

use Mundo\Kernel\Widget\Illuminate\BaseWidget;

class Nafr extends BaseWidget
{
    /** @var Cache */
    protected $cache;

    public function __construct(Cache $cache)
    {
        $this->cache = $cache;
    }

    public function render()
    {
        return $this->renderView(__DIR__ . '/views/panel.blade.php', [
            'max_firms_in_month' => $this->getMaxFirmsInMonth()
        ]);
    }

    private function getMaxFirmsInMonth()
    {
        $data = $this->cache->get(Cache::KEY);

        if (empty($data)) {
            return 0;
        }

        return $data['max_firms_in_month'];
    }
}

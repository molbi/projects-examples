<?php

namespace Mundo\Widgets\Nafr;

use Exception;
use GuzzleHttp\Client;
use Illuminate\Cache\Repository;
use Illuminate\Database\Connection;
use Mundo\Kernel\Cron\Cronable;

class Cache implements Cronable
{
    const KEY = 'widget:nafr';
    const TOKEN = '9ucY3w9hZ8oHa28QUW08bC9K4b8c2n9n';
    const API_URL = 'http://www.nafr.cz/api/settings?_token=' . self::TOKEN;

    /** @var \Illuminate\Cache\Repository */
    protected $cache;

    public function __construct(Repository $cache)
    {
        $this->cache = $cache;
    }

    public function get()
    {
        return $this->cache->get(self::KEY, []);
    }

    public function run(Connection $db = null)
    {
        $this->cache->forever(self::KEY, $this->getNafrSettings());
    }

    public function getNafrSettings()
    {
        $client = new Client();
        try {
            $res = $client->get(self::API_URL);

            return (array)json_decode($res->getBody()->getContents());
        } catch (Exception $e) {
            return [];
        }
    }

}

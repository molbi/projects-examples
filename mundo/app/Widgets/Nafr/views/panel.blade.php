<div class="tiles tiles-purple">
    <div class="tiles-body">
        <div class="tiles-title">Národní registr faktur</div>
        <div style="display:flex">
            <div style="flex:1" class="heading small">Zbývající počet firem</div>
            <div style="text-align: right;" class="heading small">{{ $max_firms_in_month }}</div>
        </div>
    </div>
</div>

<?php

namespace Mundo\Widgets\Ris\B2M\ActiveServices;

use Illuminate\Database\Connection;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\RIS\RenewalFirm;
use Mundo\Models\TM\Record;
use Mundo\Modules\TM\Sources\Support\SourceManager;
use Mundo\Widgets\TM\B2M\Constants;

class ActiveServices extends BaseWidget
{
    /** @var RenewalFirm  */
    protected $record;
    /** @var  SourceManager */
    protected $sources;

    public function __construct(RenewalFirm $record, SourceManager $sources)
    {
        $this->record = $record;
        $this->sources = $sources;
    }

    public function render()
    {
        return $this->renderView(__DIR__ . '/views/widget.blade.php', [
            'record'         => $this->record,
            'services'       => $this->services(),
            'creditServices' => $this->creditServices(),
        ]);
    }

    protected function services()
    {
        $connection = $this->db();

        return $connection->table('business.Firm_Service')
            ->select([
                'Firm_Service.service',
                'Firm_Service.lang',
                'Firm_Service.beginsOn as begins_at',
                'Firm_Service.endsOn as ends_at',
                $connection->raw('IF (DATE(Firm_Service.endsOn) <= CURDATE(), 1, 0) as expired'),
            ])
            ->where('Firm_Service.firm_id', $this->record->firm_id)
            ->get();
    }

    protected function creditServices()
    {
        $connection = $this->db();

        return $connection->table('business.Firm_CreditService')
            ->select([
                'Firm_CreditService.service',
                'Firm_CreditService.lang',
                'Firm_CreditService.beginsOn as begins_at',
            ])
            ->where('Firm_CreditService.firm_id', $this->record->firm_id)
            ->get();
    }

    public function serviceName($service)
    {
        $services = Constants::SERVICE_TITLES;
        return isset($services[$service]) ? $services[$service] : $service;
    }

    /**
     * @return Connection
     */
    protected function db()
    {
        return $this->sources->get('b2m')->getConnection();
    }
}

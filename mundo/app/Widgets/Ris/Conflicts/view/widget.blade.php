@unless($conflicts->isEmpty())
    <div class="tiles tiles-framed tiles-red widget-confilcts">
        <div class="tiles-title">
            Záznamy ze stejného měsíce
        </div>

        <div class="tiles-body tiles-body-table" style="overflow-y: hidden">
            <table class="table table-bordered">
                @foreach ($conflicts as $conflict)
                    <tr>
                        <td style="width: 1px; white-space: nowrap; text-align: left;">{!! state_label($conflict->state, true) !!}</td>
                        <td>
                            {{ $conflict->firm->title }}<br/>
                            <small>{{ \Mundo\Widgets\TM\B2M\Constants::SERVICE_TITLES[$conflict->service] }}</small>
                        </td>
                        <td style="width: 1px; white-space: nowrap; text-align: right;">
                            <small>{{ datetime($conflict->createdAt) }} <i class="fa fa-fw fa-clock-o"></i></small>
                            <br>
                            <small>  {!! $conflict->user ?  $conflict->user->name : '<em>nepřiřazeno</em>' !!} <i
                                        class="fa fa-fw fa-user"></i></small>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endunless


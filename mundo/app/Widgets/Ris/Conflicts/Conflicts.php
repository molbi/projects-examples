<?php

namespace Mundo\Widgets\Ris\Conflicts;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\JoinClause;
use Mundo\Kernel\Constants;
use Mundo\Kernel\Source\B2M\Modifiers\Renewal;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\RIS\RenewalFirm;
use Mundo\Models\TM\Record;
use Mundo\Models\TM\RecordHistory;

class Conflicts extends BaseWidget
{
    /** @var RenewalFirm */
    protected $record;

    public function __construct(RenewalFirm $record)
    {
        $this->record = $record;
    }

    protected function conflicts()
    {
        return RenewalFirm::whereYear('createdAt', '=', $this->record->createdAt->format('Y'))
            ->whereMonth('createdAt', '=', $this->record->createdAt->format('m'))
            ->where('firm_id', $this->record->firm_id)
            ->where('renewalFirm_id', '!=', $this->record->renewalFirm_id)
            ->get();

    }

    public function render()
    {
        return $this->renderView(__DIR__ . '/view/widget.blade.php', [
            'conflicts' => $this->conflicts(),
        ]);
    }

    public function label(Record $record)
    {
        $label = Constants::LABELS[$record->state];

        return sprintf("<span title=\"%s\" class=\"label label-%s\">%s</span>", $label[2], $label[1], $label[0]);
    }

    public function toViewName(Model $record)
    {
        if (is_a($record, RecordHistory::class)) {
            return '../../History/views/records/' . $record->getTable() . '_' . $record->column;
        } else {
            return '../../History/views/records/' . $record->getTable();
        }
    }
}

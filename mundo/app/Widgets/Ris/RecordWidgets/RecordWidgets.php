<?php

namespace Mundo\Widgets\Ris\RecordWidgets;

use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\RIS\RenewalFirm;
use Mundo\Models\TM\Record;

class RecordWidgets extends BaseWidget
{

    /** @var RenewalFirm */
    protected $record;

    public function __construct(RenewalFirm $record)
    {
        $this->record = $record;
    }

    public function render()
    {
        $widgets = $this->getRecordWidgets();

        return $this->renderView(__DIR__ . '/views/widgets.blade.php', compact('widgets'));
    }

    protected function createWidget($name)
    {

        $def = (object)array_get($this->getRecordWidgets(), str_replace('_', '.', $name));

        return $this->getRoot()->getApplication()->make(
            $def->type,
            array_merge((array)$def->params, ['record' => $this->record])
        );
    }

    /**
     * @return array();
     */
    protected function getRecordWidgets()
    {
        return [
            [
                'type'   => 'Mundo\Widgets\Ris\Conflicts\Conflicts',
                'params' => []
            ],
            [
                'type'   => 'Mundo\Widgets\Ris\Contact\Contact',
                'params' => []
            ],
            [
                'type'   => 'Mundo\Widgets\Ris\B2M\ActiveServices\ActiveServices',
                'params' => []
            ],
            [
                'type'   => 'Mundo\Widgets\TM\B2M\Services\Services',
                'params' => [
                    'services'=> ['activeselling']
                ]
            ],
            [
                'type'   => 'Mundo\Widgets\TM\B2M\ServicesHistory\ServicesHistory',
                'params' => []
            ],
            [
                'type'   => 'Mundo\Widgets\Ris\History\History',
                'params' => []
            ],
            [
                'type'   => 'Mundo\Widgets\TM\B2M\History\History',
                'params' => [
                    'group' => true
                ]
            ],
            [
                'type'   => 'Mundo\Widgets\TM\B2M\Orders\Orders',
                'params' => []
            ]
        ];
    }
}

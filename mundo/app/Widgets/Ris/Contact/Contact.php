<?php

namespace Mundo\Widgets\Ris\Contact;

use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\RIS\RenewalFirm;
use Mundo\Models\TM\Record;
use Mundo\Modules\TM\Sources\Support\SourceManager;

class Contact extends BaseWidget
{
    /** @var \Mundo\Models\TM\Record */
    protected $record;
    /** @var  SourceManager */
    protected $sources;

    public function __construct(RenewalFirm $record, SourceManager $sources)
    {
        $this->record = $record;
        $this->sources = $sources;
    }

    public function render()
    {
        return $this->renderView(__DIR__ . '/views/widget.blade.php', [
            'contact' => $this->contact(),
            'record'  => $this->record,
            'view'    => 'b2m',
        ]);
    }

    protected function contact()
    {
        return $this->record->firm;
    }
}

<tr>
    <td class="table-rest" style="vertical-align: top;">
        <span class="label label-default">email</span>
    </td>
    <td>
        <span class="text-muted">Předmět:</span> {{ $record->subject }}<br>
        <span class="text-muted">Komu:</span> {{$record->to}}
    </td>
    <td style="width: 1px; white-space: nowrap; text-align: right;">
        <small>{{ datetime($record->created_at) }} <i class="fa fa-fw fa-clock-o"></i></small>
        <br>
        <small> {{ $record->user->name }} <i class="fa fa-fw fa-user"></i></small>
    </td>
    <td class="table-rest">
        <a href="@action('EmailLogsController@show', [$record])" data-remote="true" class="action">
            <i class="fa fa-eye"></i>
        </a>
    </td>
</tr>
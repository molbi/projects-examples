<?php

namespace Mundo\Widgets\Ris\History;

use Illuminate\Database\Eloquent\Model;
use Mundo\Kernel\Source\B2M\Modifiers\Renewal;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\EmailLog;
use Mundo\Models\RIS\RenewalFirm;
use Mundo\Models\RIS\RenewalFirmHistory;
use Mundo\Models\TM\Record;
use Mundo\Widgets\TM\B2M\Constants;

class History extends BaseWidget
{

    /** @var RenewalFirm  */
    protected $record;

    /**
     * History constructor.
     *
     * @param RenewalFirm $record
     */
    public function __construct(RenewalFirm $record)
    {
        $this->record = $record;
    }

    public function render()
    {
        return $this->renderView(__DIR__ . '/views/widget.blade.php', [
            'records' => $this->records(),
        ]);
    }

    protected function records()
    {
        return $this->histories()
            ->merge($this->emails())
            ->sortByDesc(function($history, $key) {
                switch(get_class($history)) {
                    case RenewalFirmHistory::class:
                        return strtotime($history->createdAt);
                        break;
                    case EmailLog::class:
                        return strtotime($history->created_at);
                        break;
                }
            });
    }

    protected function histories()
    {
        return $this->record->histories;
    }

    protected function emails()
    {
        return $this->record->emailLogs()->with('user')->get();
    }

    public function toViewName(Model $record)
    {
        if (is_a($record, RenewalFirmHistory::class)) {
            return 'records/renewal_firm_history';
        } else {
            return 'records/' . $record->getTable();
        }
    }

    public function serviceName($service)
    {
        if (is_array($services = explode('|', $service))) {
            return implode(', ', array_map(function ($service) {
                return Constants::SERVICE_TITLES[$service];
            }, $services));
        }

        return Constants::SERVICE_TITLES[$service];
    }
}

<tr>
    <td class="table-rest" style="vertical-align: top;">{{ state_label($record->next, true) }}</td>
    <td>
        {!! !empty($record->note) ? $record->note : '<em>bez poznánky</em>' !!}
    </td>
    <td colspan="2" style="width: 1px; white-space: nowrap; text-align: right;">
        <small>{{ datetime($record->created_at) }} <i class="fa fa-fw fa-clock-o"></i></small><br>
        <small> {{ $record->user->name or '' }} <i class="fa fa-fw fa-user"></i></small>
    </td>
</tr>
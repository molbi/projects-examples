<tr>
    <td class="table-rest" style="vertical-align: top;">
        <span class="label label-success">nabízená služba</span>
    </td>
    <td>
        <span class="text-muted">Služba:</span> {{ $widget->serviceName($record->service) }}<br>
    </td>
    <td style="width: 1px; white-space: nowrap; text-align: right;">
        <small>{{ datetime($record->created_at) }} <i class="fa fa-fw fa-clock-o"></i></small>
        <br>
        <small> {{ $record->user->name }} <i class="fa fa-fw fa-user"></i></small>
    </td>
</tr>
<tr>
    <td class="table-rest" style="vertical-align: top;">
        <span class="label label-default">předáno</span>
    </td>
    <td>
        @if(!is_null($record->next))
            Záznam byl předán uživateli: <br> <i class="fa fa-user"></i> {{ Mundo\Models\User::withoutGlobalScopes()->find($record->next)->name }}
        @else
            bez poznámky
        @endif
    </td>
    <td colspan="2" style="width: 1px; white-space: nowrap; text-align: right;">
        <small>{{ datetime($record->created_at) }} <i class="fa fa-fw fa-clock-o"></i></small><br>
        <small> {{ $record->user->name or '' }} <i class="fa fa-fw fa-user"></i></small>
    </td>
</tr>
<?php

namespace Mundo\Widgets\TM\History;

use Illuminate\Database\Eloquent\Model;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\TM\Record;
use Mundo\Models\TM\RecordHistory;
use Mundo\Widgets\TM\B2M\Constants;

/**
 * Class History
 * @package Mundo\Widgets\TM\History
 */
class History extends BaseWidget
{

    /** @var \Mundo\Models\TM\Record */
    protected $record;

    /**
     * History constructor.
     */
    public function __construct(Record $record)
    {
        $this->record = $record;
    }

    /**
     * @return string
     */
    public function render()
    {
        return $this->renderView(__DIR__ . '/views/widget.blade.php', [
            'records' => $this->records(),
        ]);
    }

    /**
     * @return mixed
     */
    protected function records()
    {
        return $this->histories()
            ->merge($this->emails())
            ->merge($this->services())
            ->sortByDesc('created_at');
    }

    /**
     * @return mixed
     */
    protected function histories()
    {
        return $this->record->displayableHistories()->get();
    }

    /**
     * @return mixed
     */
    protected function emails()
    {
        return $this->record->emailLogs()->with(['user' => function ($builder) {
            $builder->withoutGlobalScopes();
        }])->get();
    }

    /**
     * @return mixed
     */
    protected function services()
    {
        return $this->record->serviceLogs()->with(['user' => function ($builder) {
            $builder->withoutGlobalScopes();
        }])->get();
    }

    /**
     * @param Model $record
     * @return string
     */
    public function toViewName(Model $record)
    {
        if (is_a($record, RecordHistory::class)) {
            return 'records/' . $record->getTable() . '_' . $record->column;
        } else {
            return 'records/' . $record->getTable();
        }
    }

    /**
     * @param $service
     * @return string
     */
    public function serviceName($service)
    {
        if (is_array($services = explode('|', $service))) {
            return implode(', ', array_map(function ($service) {
                return Constants::SERVICE_TITLES[$service];
            }, $services));
        }

        return Constants::SERVICE_TITLES[$service];
    }
}

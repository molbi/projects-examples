<?php

namespace Mundo\Widgets\TM\LeadInfo;

use Illuminate\Http\Request;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\TM\Record;
use Mundo\Models\User;

class LeadInfo extends BaseWidget
{
    /** @var array */
    protected $info;
    /** @var \Mundo\Models\TM\Record */
    protected $record;
    /** @var \Mundo\Models\User */
    protected $user;

    public function __construct(array $info, Record $record, Request $request)
    {
        $this->info = $info;
        $this->record = $record;
        $this->user = $request->user();
    }

    public function render()
    {
        return $this->renderView(__DIR__ . '/views/widget.blade.php', [
            'info'   => $this->info,
            'record' => $this->record,
            'user'   => $this->user,
        ]);
    }

    public function parseFirstNote()
    {
        $note = $this->record->histories->last() ? (string)$this->record->histories->last()->note : '';
        $note = trim(str_replace(array('Poptávám:', 'Poptávka:', 'Dopyt na:'), '', $note));
        if (preg_match('/^Údaje z formuláře:.*$/', $note)) {
            if (preg_match('/^.*(<.*<\/a>).*$/', $note, $match) && isset($match[1])) {
                return trim($match[1]);
            }
        }

        return $note;
    }
}

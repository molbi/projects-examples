<div class="modal fade" id="lead-info-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="fa fa-info-circle"></i> Info o leadu</h4>
            </div>
            <div class="modal-body">

                <h4>Vstup do hovoru</h4>
                <p class="alert alert-warning">
                    {!! sprintf($info['callIntro'], $user->name, $record->remoteData->contact_person, $widget->parseFirstNote()) !!}
                </p>

                <h4>Vznik leadu</h4>
                <p>
                    {!! $info['leadOrigin'] !!}
                </p>
                @if(isset($info['images']))
                    <div class="images text-center">
                        <hr>
                        @foreach($info['images'] as $image)
                            <img style="width: 250px;" src="{{ $image }}" class="img-thumbnail">
                        @endforeach
                    </div>
                @endif

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Zavřít</button>
            </div>
        </div>
    </div>
</div>

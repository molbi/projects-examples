@unless($conflicts->isEmpty())
    <div class="tiles tiles-framed tiles-red widget-confilcts">
        <div class="tiles-title">
            Záznam v jiných kampaních
        </div>

        <div class="tiles-body tiles-body-table" style="overflow-y: hidden">
            <table class="table table-bordered">
                @foreach ($conflicts as $conflict)
                    <tr>
                        <td style="width: 1px; white-space: nowrap; text-align: left;">{!! state_label($conflict->state, true) !!}</td>
                        <td>{{ $conflict->campaign->name }}</td>
                        <td style="width: 1px; white-space: nowrap; text-align: right;">
                            <small>{{ datetime($conflict->created_at) }} <i class="fa fa-fw fa-clock-o"></i></small>
                            <br>
                            <small>  {!! $conflict->user ?  $conflict->user->name : '<em>nepřiřazeno</em>' !!} <i
                                        class="fa fa-fw fa-user"></i></small>
                        </td>
                        <td class="table-rest">
                            <a data-toggle="modal" data-target="#history-modal-{{ $conflict->id }}" class="action">
                                <i class="fa fa-search"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>


    @foreach ($conflicts as $conflict)
        <div id="history-modal-{{ $conflict->id }}" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Historie akcí</h4>
                    </div>
                    <div class="modal-body modal-body-table">
                        <table class="table table-bordered">
                            @foreach($conflict->displayableHistories()->get() as $record)
                                @partial($widget->toViewName($record), ['record' => $record])
                            @endforeach
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Zavřít</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endunless


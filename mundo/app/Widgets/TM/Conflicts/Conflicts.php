<?php

namespace Mundo\Widgets\TM\Conflicts;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\JoinClause;
use Mundo\Kernel\Constants;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\TM\Record;
use Mundo\Models\TM\RecordHistory;

class Conflicts extends BaseWidget
{
    /** @var \Mundo\Models\TM\Record */
    protected $record;

    public function __construct(Record $record)
    {
        $this->record = $record;
    }

    protected function conflicts()
    {
        return Record::select([
            'tm_records.*',
        ])
            ->join('tm_campaigns', function (JoinClause $clause) {
                $clause->on('tm_campaigns.id', '=', 'tm_records.campaign_id')
                    ->where('tm_campaigns.source', '=', $this->record->campaign->source);
            })
            ->where('tm_records.remote_id', '=', $this->record->remote_id)
            ->where('tm_records.id', '!=', $this->record->id)
            ->with('user', 'campaign', 'histories', 'histories.user')
            ->get()
            ->withRemote();
    }

    public function render()
    {
        return $this->renderView(__DIR__ . '/view/widget.blade.php', [
            'conflicts' => $this->conflicts(),
        ]);
    }

    public function label(Record $record)
    {
        $label = Constants::LABELS[$record->state];

        return sprintf("<span title=\"%s\" class=\"label label-%s\">%s</span>", $label[2], $label[1], $label[0]);
    }

    public function toViewName(Model $record)
    {
        if (is_a($record, RecordHistory::class)) {
            return '../../History/views/records/' . $record->getTable() . '_' . $record->column;
        } else {
            return '../../History/views/records/' . $record->getTable();
        }
    }
}

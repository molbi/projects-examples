<?php

namespace Mundo\Widgets\TM\Logout;

use Illuminate\Database\Connection;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\TM\Record;
use Mundo\Modules\TM\Sources\Support\SourceManager;

class Logout extends BaseWidget
{
    /** @var \Mundo\Models\TM\Record */
    protected $record;
    /** @var  SourceManager */
    protected $sources;

    public function __construct(Record $record, SourceManager $sources)
    {
        $this->record = $record;
        $this->sources = $sources;
    }

    public function render()
    {
        return $this->renderView(__DIR__ . '/views/widget.blade.php', [
            'isLogouted' => $this->isLogouted(),
        ]);
    }

    protected function isLogouted()
    {
        if ($this->record->campaign->source !== 'b2m') {
            return false;
        }

        return $this->db()->table('telemarketing.Logout')
            ->where('firm_id', $this->record->remote_id)
            ->count() > 0;
    }

    /**
     * @return Connection
     */
    protected function db()
    {
        return $this->sources->get('b2m')->getConnection();
    }
}

@if($isLogouted)
    <div class="tiles tiles-framed tiles-red widget">
        <div class="tiles-title text-center">
            Upozornění
        </div>
        <div class="tiles-body text-center">
            <h3 style="margin: 0;" class="text-danger">TATO FIRMA SI DÁLE NEPŘEJE BÝT KONTAKTOVÁNA! OZNAČTE PROSÍM FIRMU JAKO ODSTRANĚNOU A NEVOLEJTE JÍ!</h3>
        </div>
    </div>
@endif
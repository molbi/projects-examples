<?php

namespace Mundo\Widgets\TM\B2M\Inquiry;

use Illuminate\Database\Connection;
use Illuminate\Database\Eloquent\Model;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\RIS\RenewalFirm;
use Mundo\Models\TM\Record;
use Mundo\Modules\TM\Sources\Support\SourceManager;

/**
 * Class Inquiries
 * @package Mundo\Widgets\TM\B2M\Inquiries
 */
class Inquiry extends BaseWidget
{
    /** @var Record|RenewalFirm */
    protected $record;
    /** @var  SourceManager */
    protected $sources;

    /**
     * Inquiries constructor.
     *
     * @param Model         $record
     * @param SourceManager $sources
     *
     * @internal param $inquiryRecord
     */
    public function __construct(Model $record, SourceManager $sources)
    {
        $this->record = $record;
        $this->sources = $sources;
    }

    /**
     * @return string
     */
    public function render()
    {
        return $this->renderView(__DIR__ . '/views/widget.blade.php', [
            'inquiry' => $this->inquiry(),
            'logs'    => $this->inquiryLogs(),
            'notes'   => $this->inquiryNotes()
        ]);
    }


    /**
     * @return \Illuminate\Support\Collection
     */
    protected function inquiries()
    {
        return collect();
    }

    /**
     * @return mixed|null
     */
    private function inquiry()
    {
        return $this->db()->table('business.Inquiry AS I')
            ->join('business.InquiryLang AS IL', 'IL.inquiry_id', '=', 'I.inquiry_id')
            ->leftJoin('zis.Employee AS E', 'E.employee_id', '=', 'I.operator_id')
            ->where('I.inquiry_id', $this->record->getRemoteId())
            ->first();
    }

    /**
     * @return Connection
     */
    protected function db()
    {
        return $this->sources->get('b2m')->getConnection();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    private function inquiryLogs()
    {
        return collect(
            $this->db()->table('business.InquiryLog AS IL')
                ->leftJoin('zis.Employee AS E', 'E.employee_id', '=', 'IL.operator_id')
                ->where('IL.inquiry_id', $this->record->getRemoteId())
                ->orderBy('IL.createdOn', 'desc')
                ->get()
        );
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    private function inquiryNotes()
    {
        return collect(
            $this->db()->table('business.InquiryNote AS IN')
                ->leftJoin('zis.Employee AS E', 'E.employee_id', '=', 'IN.operator_id')
                ->where('IN.inquiry_id', $this->record->getRemoteId())
                ->orderBy('IN.createdOn', 'desc')
                ->get()
        );
    }
}

<div class="tiles tiles-framed tiles-blue widget">
    <div class="tiles-title">
        Informace o poptávce
    </div>
    <div class="tiles-body tiles-body-table">
        <table class="table table-bordered">
            <tr>
                <th style="width: 150px; background-color: #fafafa;">Název</th>

                <td style="width:500px">
                    {{ $inquiry->title }}
                </td>

                <th style="width: 150px; background-color: #fafafa;">PIS</th>
                <td>
                    <a class="btn btn-primary" target="_blank"
                       href="http://admin.b2m.cz/pis/search#from=&to=&inquiry={{ $inquiry->inquiry_id }}&contact=&string=&category=&states=&operator=&source=">
                        Zobrazit v PIS
                    </a>
                </td>
            </tr>
            <tr>
                <th style="width: 150px; background-color: #fafafa;">Datum</th>
                <td>{{ datetime($inquiry->createdOn) }}</td>
                <th style="width: 150px; background-color: #fafafa;">Název</th>
                <td>{{ $inquiry->realname }}</td>
            </tr>
            <tr>
                <th style="background-color: #fafafa;">Obash poptávky</th>
                <td colspan="3">
                    {!! nl2br($inquiry->body) !!}
                </td>
            </tr>
        </table>
    </div>
</div>

@unless($logs->isEmpty())
    <div class="tiles tiles-framed tiles-blue widget">
        <div class="tiles-title">
            Historie poptávky
        </div>
        <div class="tiles-body tiles-body-table">
            <table class="table table-bordered">
                @foreach($logs as $log)
                    <tr>
                        <td>
                            {{ $log->action }}
                        </td>
                        <td style="width: 1px; white-space: nowrap; text-align: right;">
                            <small>{{ datetime($log->createdOn) }} <i class="fa fa-fw fa-clock-o"></i></small>
                            <br>
                            <small> {{ $log->realname }} <i class="fa fa-fw fa-user"></i></small>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endunless

@unless($notes->isEmpty())
    <div class="tiles tiles-framed tiles-blue widget">
        <div class="tiles-title">
            Poznámky k poptávce
        </div>
        <div class="tiles-body tiles-body-table">
            <table class="table table-bordered">
                <tr>
                    <th></th>
                    <th></th>
                </tr>
                @foreach($notes as $note)
                    <tr>
                        <td>
                            {{ $note->body }}
                        </td>
                        <td style="width: 1px; white-space: nowrap; text-align: right;">
                            <small>{{ datetime($note->createdOn) }} <i class="fa fa-fw fa-clock-o"></i></small>
                            <br>
                            <small> {{ $note->realname }} <i class="fa fa-fw fa-user"></i></small>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endunless
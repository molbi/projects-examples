<?php

namespace Mundo\Widgets\TM\B2M\CSkontaktInquiryLink;

use Illuminate\Database\Connection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\RIS\RenewalFirm;
use Mundo\Models\TM\Record;
use Mundo\Modules\TM\Sources\Support\SourceManager;

/**
 * Class Inquiries
 * @package Mundo\Widgets\TM\B2M\Inquiries
 */
class CSkontaktInquiryLink extends BaseWidget
{
    /** @var Record|RenewalFirm */
    protected $record;
    /** @var  SourceManager */
    protected $sources;
    /**
     * @var Request
     */
    private $request;

    /**
     * Inquiries constructor.
     *
     * @param Model $record
     * @param SourceManager $sources
     * @param Request $request
     */
    public function __construct(Model $record, SourceManager $sources, Request $request)
    {
        $this->record = $record;
        $this->sources = $sources;
        $this->request = $request;
    }

    /**
     * @return string
     */
    public function render()
    {
        return $this->renderView(__DIR__ . '/views/widget.blade.php', [
            'record' => $this->record,
            'hash'   => $this->hash()
        ]);
    }

    /**
     * @return Connection
     */
    protected function db()
    {
        return $this->sources->get('b2m')->getConnection();
    }

    /**
     *
     * @param null $append
     * @return string
     */
    public function hash($append = null)
    {
        return strtr(base64_encode(gzencode(json_encode(array(
                'contact' => array(
                    'title'      => $this->record->remoteData->company,
                    'name'       => $this->record->remoteData->contact_person,
                    'email'      => $this->record->remoteData->email,
                    'phone'      => $this->record->remoteData->phone_number,
                    'id_number'  => $this->record->remoteData->id_number,
                    'vat_number' => $this->record->remoteData->vat_number,
                ),
            )))), '+/=', '-_~') . sprintf('?utm_source=%s', $this->inquirySource($append));
    }

    /**
     * @param null $append
     * @return string
     */
    private function inquirySource($append = null)
    {
        if ($append !== null) {
            return sprintf('tm_inquiry_%s_%s_%s', $this->record->campaign_id, $this->request->user()->id, $append);
        }

        return sprintf('tm_inquiry_%s_%s', $this->record->campaign_id, $this->request->user()->id);
    }

    /**
     * @param $record
     *
     * @return mixed|static
     */
    public function isAccountExists($record)
    {
        return $this->db()->table('cs_contact.users')->where('email', $record->remoteData->email)->first();
    }
}

<div class="tiles tiles-framed tiles-blue widget">
    <div class="tiles-title">
        Zadání poptávky
    </div>
    <div class="tiles-body">
        <p>
            Účet na CSkontaktu:

            @if($widget->isAccountExists($record))
                <strong class="text-success">existuje</strong>
            @else
                <strong class="text-danger">neexistuje</strong>
            @endif
        </p>
        <hr />
        <div class="row">
            <div class="col-md-12">
                <div class="btn-group">
                    @if($record->getRemoteLang() == 'cs')
                        <a href="http://www.cskontakt.cz/nova-poptavka-powerbank/{{ $widget->hash() }}" class="btn btn-primary" target="_blank">Zadat poptávku s powerbankou</a>
                        <a href="http://www.cskontakt.cz/nova-poptavka/{{ $widget->hash('_bez_darku') }}" class="btn btn-success" target="_blank">Zadat poptávku</a>
                    @else
                        <a href="http://www.cskontakt.sk/novy-dopyt-powerbank/{{ $widget->hash() }}" class="btn btn-primary" target="_blank">Zadat poptávku s powerbankou</a>
                        <a href="http://www.cskontakt.sk/novy-dopyt/{{ $widget->hash('_bez_darku') }}" class="btn btn-success" target="_blank">Zadat poptávku</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
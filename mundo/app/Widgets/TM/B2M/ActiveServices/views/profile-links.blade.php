@if($service->service == 'inquiries')
    @if($service->lang == 'cs')
        <a href="http://www.epoptavka.cz/edit_vip.phtml?firm_id={{ $record->remote_id }}&m={{ $record->remoteData->email }}"
           class="btn btn-primary btn-xs"
           target="_blank">Profil</a>
    @else
        <a href="http://www.123dopyt.sk/edit_vip.phtml?firm_id={{ $record->remote_id }}&m={{ $record->remoteData->email }}"
           class="btn btn-primary btn-xs"
           target="_blank">Profil</a>
    @endif
@endif

@if($service->service == 'inquiries-keywords')
    @if($service->lang == 'cs')
        <a href="http://www.epoptavka.cz/prihlaseni?formSent=1&email={{ $record->remoteData->email }}&password={{ $record->remoteData->ID_BCFirm }}&redirect=http%3A%2F%2Fwww.epoptavka.cz%2Fmuj-ucet%2Fnastaveni-zasilani-poptavek-cileni-podle-klicovych-slov"
           class="btn btn-primary btn-xs"
           target="_blank">Profil</a>
    @else
        <a href="http://www.123dopyt.sk/prihlasenie?formSent=1&email={{ $record->remoteData->email }}&password={{ $record->remoteData->ID_BCFirm }}&redirect=http%3A%2F%2Fwww.123dopyt.sk%2Fmoj-ucet%2Fnastavenie-zasielanie-dopytov-cilenie-podla-klucovych-slov"
           class="btn btn-primary btn-xs"
           target="_blank">Profil</a>
    @endif
@endif

@if($service->service == 'activeselling')
    @if($service->lang == 'cs')
        <a href="http://www.epoptavka.cz/prihlaseni?formSent=1&email={{ $record->remoteData->email }}&password={{ $record->remoteData->ID_BCFirm }}&redirect=http%3A%2F%2Fwww.epoptavka.cz%2Fmuj-ucet%2Fnabidky%2Fnastaveni-aktivniho-prodeje"
           class="btn btn-primary btn-xs"
           target="_blank">Nastavení</a>
    @else
        <a href="http://www.123dopyt.sk/prihlasenie?formSent=1&email={{ $record->remoteData->email }}&password={{ $record->remoteData->ID_BCFirm }}&redirect=http%3A%2F%2Fwww.123dopyt.sk%2Fmoj-ucet%2Fponuky%2Fnastavenie-aktivneho-predaja"
           class="btn btn-primary btn-xs"
           target="_blank">Nastavení</a>
    @endif
@endif

@if($service->service == 'prepaid-offer')
    @if($service->lang == 'cs')
        <a href="http://www.epoptavka.cz/prihlaseni?formSent=1&email={{ $record->remoteData->email }}&password={{ $record->remoteData->ID_BCFirm }}&redirect=http%3A%2F%2Fwww.epoptavka.cz%2Fmuj-ucet%2Fnastaveni-prednostni-nabidky"
           class="btn btn-primary btn-xs"
           target="_blank">Nastavení</a>
    @else
        <a href="http://www.123dopyt.sk/prihlasenie?formSent=1&email={{ $record->remoteData->email }}&password={{ $record->remoteData->ID_BCFirm }}&redirect=http%3A%2F%2Fwww.123dopyt.sk%2Fmoj-ucet%2Fnastavenie-prednostnej-ponuky"
           class="btn btn-primary btn-xs"
           target="_blank">Nastavení</a>
    @endif
@endif

@if($service->service == 'official-board-keywords')
    @if($service->lang == 'cs')
        <a href="http://www.uredninastenka.cz/~/api_session/{{ $record->remote_id }}/cs"
           class="btn btn-primary btn-xs"
           target="_blank">Profil</a>
    @else
        <a href="http://www.uradnanastenka.sk/~/api_session/{{ $record->remote_id }}/sk"
           class="btn btn-primary btn-xs"
           target="_blank">Profil</a>
    @endif
@endif

@if($service->service == 'psinquiries')
    @if($service->lang == 'cs')
        <a href="http://www.epoptavka.cz/edit_pvs.phtml?c={{ $record->remoteData->ID_BCFirm }}&m={{ $record->remoteData->email }}"
           class="btn btn-primary btn-xs"
           target="_blank">Profil</a>
    @else
        <a href="http://www.123dopyt.sk/prihlasenie?formSent=1&email={{ $record->remoteData->email }}&password={{ $record->remoteData->ID_BCFirm }}&redirect=http%3A%2F%2Fwww.123dopyt.sk%2Fmoj-ucet%2Fnastavenie-zasielanie-dopytov-cilenie-podla-klucovych-slov"
           class="btn btn-primary btn-xs"
           target="_blank">Profil</a>
    @endif
@endif
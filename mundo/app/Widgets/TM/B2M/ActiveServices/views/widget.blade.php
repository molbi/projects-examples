@unless(empty($services))
<div class="tiles tiles-framed tiles-default widget">
    <div class="tiles-title">
        Aktivní paušální služby
    </div>
    <div class="tiles-body tiles-body-table">
        <table class="table table-bordered">
            @foreach($services as $service)
                <tr>
                    <td style="width: 1px;">
                        {{ flag($service->lang) }}
                    </td>
                    <td>
                        {{ $widget->serviceName($service->service) }}
                    </td>
                    <td style="width: 1px;">
                        <span class="label label-{{$service->expired ? 'danger' : 'success'}}">
                            {{$service->expired ? 'vypršelá' : 'aktivní'}}
                        </span>
                    </td>
                    <td>
                       {{ datetime($service->begins_at, 'd. m. Y') }} &rarr; {{ datetime($service->ends_at, 'd. m. Y') }}
                    </td>
                    <td>
                        @if ($service->expired)
                            zbývá 0 dní
                        @else
                            zbývá {{ Carbon\Carbon::parse($service->ends_at)->diffInDays() }} dní
                        @endif
                    </td>
                    <td>
                        @partial('profile-links')
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
@endunless

@unless(empty($creditServices))
    <div class="tiles tiles-framed tiles-default widget">
        <div class="tiles-title">
            Aktivní kreditové služby
        </div>
        <div class="tiles-body tiles-body-table">
            <table class="table table-bordered">
                @foreach($creditServices as $service)
                    <tr>
                        <td style="width: 1px;">
                            {{ flag($service->lang) }}
                        </td>
                        <td>
                            {{ $widget->serviceName($service->service) }}
                        </td>
                        <td>
                            {{ datetime($service->begins_at, 'd. m. Y') }}
                        </td>
                        <td>
                                aktivní {{ Carbon\Carbon::parse($service->begins_at)->diffInDays() }} dní
                        </td>
                        <td>
                            @partial('profile-links')
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endunless
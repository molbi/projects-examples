<?php

namespace Mundo\Widgets\TM\B2M\ActiveServices;

use Illuminate\Cache\CacheManager;
use Illuminate\Database\Connection;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\TM\Record;
use Mundo\Modules\TM\Sources\Support\SourceManager;
use Mundo\Widgets\TM\B2M\Constants;

class ActiveServices extends BaseWidget
{
    /** @var \Mundo\Models\TM\Record */
    protected $record;
    /** @var  SourceManager */
    protected $sources;
    /**
     * @var CacheManager
     */
    private $cache;

    public function __construct(Record $record, SourceManager $sources, CacheManager $cache)
    {
        $this->record = $record;
        $this->sources = $sources;
        $this->cache = $cache;
    }

    public function render()
    {
        return $this->renderView(__DIR__ . '/views/widget.blade.php', [
            'record'         => $this->record,
            'services'       => $this->services(),
            'creditServices' => $this->creditServices(),
        ]);
    }

    protected function services()
    {
//        return $this->cache->store('file')->remember($this->cacheKey('services'), 1, function () {
            return $this->db()->table('business.Firm_Service')
                ->select([
                    'Firm_Service.service',
                    'Firm_Service.lang',
                    'Firm_Service.beginsOn as begins_at',
                    'Firm_Service.endsOn as ends_at',
                    $this->db()->raw('IF (DATE(Firm_Service.endsOn) <= CURDATE(), 1, 0) as expired'),
                ])
                ->where('Firm_Service.firm_id', $this->record->getRemoteId())
                ->get();
//        });
    }

    protected function creditServices()
    {
//        return $this->cache->store('file')->remember($this->cacheKey('credit_services'), 1, function () {
            return $this->db()->table('business.Firm_CreditService')
                ->select([
                    'Firm_CreditService.service',
                    'Firm_CreditService.lang',
                    'Firm_CreditService.beginsOn as begins_at',
                ])
                ->where('Firm_CreditService.firm_id', $this->record->getRemoteId())
                ->get();
//        });
    }

    public function serviceName($service)
    {
        $services = Constants::SERVICE_TITLES;

        return isset($services[$service]) ? $services[$service] : $service;
    }

    /**
     * @return Connection
     */
    protected function db()
    {
        return $this->sources->get('b2m')->getConnection();
    }

    private function cacheKey($type)
    {
        return sprintf('widget_%s_%s', $type, $this->record->getRemoteId());
    }
}

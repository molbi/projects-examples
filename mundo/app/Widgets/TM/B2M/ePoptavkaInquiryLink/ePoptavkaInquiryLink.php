<?php

namespace Mundo\Widgets\TM\B2M\ePoptavkaInquiryLink;

use Illuminate\Database\Connection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\RIS\RenewalFirm;
use Mundo\Models\TM\Record;
use Mundo\Modules\TM\Sources\Support\SourceManager;

/**
 * Class Inquiries
 * @package Mundo\Widgets\TM\B2M\Inquiries
 */
class ePoptavkaInquiryLink extends BaseWidget
{
    /** @var Record|RenewalFirm */
    protected $record;
    /** @var  SourceManager */
    protected $sources;
    /**
     * @var Request
     */
    private $request;

    /**
     * Inquiries constructor.
     *
     * @param Model $record
     * @param SourceManager $sources
     * @param Request $request
     */
    public function __construct(Model $record, SourceManager $sources, Request $request)
    {
        $this->record = $record;
        $this->sources = $sources;
        $this->request = $request;
    }

    /**
     * @return string
     */
    public function render()
    {
        return $this->renderView(__DIR__ . '/views/widget.blade.php', [
            'record' => $this->record
        ]);
    }

    /**
     * @return Connection
     */
    protected function db()
    {
        return $this->sources->get('b2m')->getConnection();
    }

    /**
     *
     * @param null $append
     * @return string
     */
    public function hash($append = null)
    {
        if($this->record->campaign->source == 'inquiry') {
            $remote_id = $this->record->remoteData->userAccount_id;
        } else {
            $remote_id = $this->record->remote_id;
        }

        return md5($remote_id) . sprintf('?source=%s', $this->inquirySource($append));
    }

    /**
     * @param null $append
     * @return string
     */
    private function inquirySource($append = null)
    {
        if ($append !== null) {
            return sprintf('tm_inquiry_%s_%s_%s', $this->record->campaign_id, $this->request->user()->id, $append);
        }

        return sprintf('tm_inquiry_%s_%s', $this->record->campaign_id, $this->request->user()->id);
    }
}

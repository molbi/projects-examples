<div class="tiles tiles-framed tiles-blue widget">
    <div class="tiles-title">
        Zadání poptávky
    </div>
    <div class="tiles-body">
        <div class="row">
            <div class="col-md-12">
                <div class="btn-group">
                    @if($record->getRemoteLang() == 'cs')
                        {{--<a href="http://www.epoptavka.cz/zadat-poptavku/poptavajici/{{ $widget->hash() }}" class="btn btn-primary" target="_blank">Zadat poptávku s powerbankou</a>--}}
                        <a href="http://www.epoptavka.cz/zadat-poptavku/poptavajici/{{ $widget->hash('_bez_darku') }}" class="btn btn-success" target="_blank">Zadat poptávku</a>
                    @else
                        {{--<a href="http://www.123dopyt.sk/zadat-dopyt/dopytujuci/{{ $widget->hash() }}" class="btn btn-primary" target="_blank">Zadat poptávku s powerbankou</a>--}}
                        <a href="http://www.123dopyt.sk/zadat-dopyt/dopytujuci/{{ $widget->hash('_bez_darku') }}" class="btn btn-success" target="_blank">Zadat poptávku</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
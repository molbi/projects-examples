<?php

namespace Mundo\Widgets\TM\B2M\Orders;

use Carbon\Carbon;
use Illuminate\Database\Connection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\JoinClause;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\RIS\RenewalFirm;
use Mundo\Models\TM\Record;
use Mundo\Modules\TM\Sources\Support\SourceManager;
use Mundo\Widgets\TM\B2M\Constants;

class Orders extends BaseWidget
{
    /** @var Record|RenewalFirm */
    protected $record;
    /** @var  SourceManager */
    protected $sources;
    /**
     * @var string
     */
    private $view;

    public function __construct(Model $record, SourceManager $sources, $view = 'default')
    {
        $this->record = $record;
        $this->sources = $sources;
        $this->view = $view;
    }

    public function render()
    {
        return $this->renderView(__DIR__ . "/views/{$this->view}.blade.php", [
            'orders' => $this->orders(),
        ]);
    }

    protected function orders()
    {
        if ($this->getRemoteId() === null) {
            return [];
        }

        return $this->db()->table('obis.Order')
            ->select([
                'Order.order_id',
                'Order.lang',
                'Order.createdOn as created_at',
                'Order.state',
                $this->db()->raw("GROUP_CONCAT(Service.title SEPARATOR '-|-') as title"),
                'Invoice.number as invoice_number',
                'Invoice.price',
                'Invoice.currency',
                'Invoice.maturity',
                $this->db()->raw('DATE(Order.createdOn) + INTERVAL Invoice.maturity DAY AS maturity_at'),
                'OH.changedAt AS paid_at',
                'E.realname',
                'Order.garantion',
                'Order.garantionType',
                'OE.order_id AS lgc'
            ])
            ->join('obis.Order_Service', 'Order_Service.order_id', '=', 'Order.order_id')
            ->leftJoin('zis.Employee AS E', 'E.employee_id', '=', 'Order.employee_id')
            ->leftJoin('obis.OrderExactor AS OE', 'OE.order_id', '=', 'Order.order_id')
            ->join('obis.Service', 'Order_Service.service_id', '=', 'Service.service_id')
            ->join('obis.Invoice', function (JoinClause $clause) {
                $clause->on('Invoice.order_id', '=', 'Order.order_id')
                    ->where('Invoice.price', '>=', 0);
            })
            ->leftJoin('obis.OrderHistory AS OH', function (JoinClause $j) {
                $j->on('OH.order_id', '=', 'Order.order_id')
                    ->where('OH.action', '=', 'paid');
            })
            ->where('Order.firm_id', $this->getRemoteId())
            ->orderBy('Order.createdOn', 'DESC')
            ->groupBy('Order.order_id')
            ->get();
    }

    public function getStateTitleAndLabelClass($state)
    {
        return (object)[
            'title' => Constants::ORDER_STATE_TITLES[$state],
            'class' => Constants::ORDER_STATE_CLASSES[$state],
        ];
    }

    /**
     * @return Connection
     */
    protected function db()
    {
        return $this->sources->get('b2m')->getConnection();
    }

    public function paymentMorals($order)
    {
        if ($order->state != Constants::ORDER_STATE_PAID) {
            return null;
        }
        return Carbon::createFromTimestamp(strtotime($order->maturity_at))->diffInDays(Carbon::createFromTimestamp(strtotime($order->paid_at)), false);
    }

    private function getRemoteId()
    {
        if (is_a($this->record, RenewalFirm::class)) {
            return $this->record->getRemoteId();
        }

        if (is_a($this->record, Record::class)) {
            switch ($this->record->campaign->source) {
                case 'kartus':
                    return $this->record->remoteData->firm_id;
                    break;
            }
        }

        return $this->record->getRemoteId();
    }
}

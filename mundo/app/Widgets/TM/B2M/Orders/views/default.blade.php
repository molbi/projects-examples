@unless(empty($orders))
    <div class="tiles tiles-framed tiles-default widget">
        <div class="tiles-title">
            Objednávky
        </div>
        <div class="tiles-body tiles-body-table">
            <table class="table table-bordered">
                <tr>
                    <th>ID</th>
                    <th>Datum</th>
                    <th>Stav</th>
                    <th>PM</th>
                    <th colspan="2">Služby</th>
                    <th>Faktura</th>
                    <th>Cena</th>
                </tr>
                @foreach($orders as $order)
                    <tr>
                        <td style="width: 1px;">
                            <code>{{ $order->order_id }}</code>
                        </td>
                        <td style="width: 1px; white-space: nowrap;">
                            {{ datetime($order->created_at, 'd. m. Y') }}
                        </td>
                        <td style="width: 1px; white-space: nowrap;">
                            <span class="label label-{{ $widget->getStateTitleAndLabelClass($order->state)->class }}">
                                {{ strtolower($widget->getStateTitleAndLabelClass($order->state)->title) }}
                            </span>
                        </td>
                        <td>
                            @if(($diff = $widget->paymentMorals($order)) !== null)
                                @if($diff >= 0)
                                    <i class="fa fa-remove text-danger"></i> <small><abbr title="počet dnů po splatnosti">({{ $diff }})</abbr></small>
                                @else
                                    <i class="fa fa-check text-success"></i>
                                @endif
                            @endif
                        </td>
                        <td  style="width: 1px; white-space: nowrap;">
                            {{ flag($order->lang) }}&nbsp;
                        </td>
                        <td>
                            {!! implode('<br>', explode('-|-', $order->title)) !!}
                        </td>
                        <td>
                            {{ $order->invoice_number }}
                        </td>
                        <td class="text-right">
                            {{ number_format($order->price, 2, ',', ' ') }} {{ $order->currency }}
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endunless
@unless(empty($orders))
    <div class="tiles tiles-framed tiles-default widget">
        <div class="tiles-title">
            Objednávky
        </div>
        <div class="tiles-body tiles-body-table">
            <table class="table table-bordered">
                <tr>
                    <th>ID</th>
                    <th>Garance</th>
                    <th>LGC</th>
                    <th>Datum</th>
                    <th>Datum splatnosti</th>
                    <th>Stav</th>
                    <th colspan="2">Služby</th>
                    <th>Vystavil</th>
                    <th>Faktura</th>
                    <th>Cena</th>
                </tr>
                @foreach($orders as $order)
                    <tr>
                        <td style="width: 1px;">
                            <code>{{ $order->order_id }}</code>
                        </td>
                        <td style="width: 1px;">
                            @if($order->garantion)
                                <i class="fa fa-fw fa-puzzle-piece text-success"></i>
                            @endif
                        </td>
                        <td style="width:1px">
                            @if($order->lgc)
                                Ano
                            @else
                                Ne
                            @endif
                        </td>
                        <td style="width: 1px; white-space: nowrap;">
                            {{ datetime($order->created_at, 'd. m. Y') }}
                            @if(in_array($order->state, ['processed', 'blocked']))
                                <small><abbr title="Počet dní od vystavení">({{ \Carbon\Carbon::parse($order->created_at)->diffInDays(\Carbon\Carbon::now()) }})</abbr></small>
                            @endif
                        </td>
                        <td style="width: 1px; white-space: nowrap;">
                            {{ datetime($order->maturity_at, 'd. m. Y') }}
                        </td>
                        <td style="width: 1px; white-space: nowrap;">
                            <span class="label label-{{ $widget->getStateTitleAndLabelClass($order->state)->class }}">
                                {{ strtolower($widget->getStateTitleAndLabelClass($order->state)->title) }}
                            </span>
                        </td>
                        <td  style="width: 1px; white-space: nowrap;">
                            {{ flag($order->lang) }}&nbsp;
                        </td>
                        <td>
                            {!! implode('<br>', explode('-|-', $order->title)) !!}
                        </td>
                        <td>
                            {{ $order->realname }}
                        </td>
                        <td>
                            {{ $order->invoice_number }}
                        </td>
                        <td class="text-right">
                            {{ number_format($order->price, 2, ',', ' ') }} {{ $order->currency }}
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endunless
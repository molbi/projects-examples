<?php

namespace Mundo\Widgets\TM\B2M\KartusAcquisitionInquiries;

use Illuminate\Database\Connection;
use Illuminate\Database\Query\JoinClause;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\TM\Record;
use Mundo\Modules\TM\Sources\Support\SourceManager;
use Mundo\Widgets\TM\B2M\Constants;

class KartusAcquisitionInquiries extends BaseWidget
{
    /** @var \Mundo\Models\TM\Record */
    protected $record;
    /** @var  SourceManager */
    protected $sources;

    public function __construct(Record $record, SourceManager $sources)
    {
        $this->record = $record;
        $this->sources = $sources;
    }

    public function render()
    {
        return $this->renderView(__DIR__ . '/views/widget.blade.php', [
            'inquiries' => $this->inquiries(),
        ]);
    }

    protected function inquiries()
    {
        return $this->db()->table('log.acquisition_sent_log')
            ->select([
                'acquisition_sent_log.created_at',
                'inquiries.title',
                'inquiries.body_html'
            ])
            ->join('cs_contact.inquiries', 'inquiries.id', '=', 'acquisition_sent_log.subject_id')
            ->where('acquisition_sent_log.initiator_id', $this->record->remote_id)
            ->where('acquisition_sent_log.subject_type', 'cs_contact')
            ->orderBy('acquisition_sent_log.created_at', 'DESC')
            ->limit(5)
            ->get();
    }

    /**
     * @return Connection
     */
    protected function db()
    {
        return $this->sources->get('b2m')->getConnection();
    }
}

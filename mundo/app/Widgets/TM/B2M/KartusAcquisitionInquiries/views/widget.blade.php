@if($inquiries)
    <div class="tiles tiles-framed tiles-default widget widget-last-kartus-inquiry">
        <div class="tiles-title">
            Poslední zaslané poptávky v rámci akvizice
        </div>
        <div class="tiles-body tiles-body-table">
            <table class="table table-bordered">
                <tr>
                    <th>Datum</th>
                    <th>Název poptávky</th>
                </tr>
                @foreach($inquiries as $inquiry)
                    <tr>
                        <td>
                            {{ datetime($inquiry->created_at, 'd. m. Y') }}
                        </td>
                        <td>
                            {{ $inquiry->title }}
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endif
@if(!$profiles->isEmpty())
    <div class="tiles tiles-framed tiles-green widget-cskontakt">
        <div class="tiles-title">
            Záznamy v CSkontaktu
        </div>

        <div class="tiles-body tiles-body-table" style="overflow-y: hidden">
            <table class="table table-bordered">
                <tr>
                    <th>Email</th>
                    <th>Kredit</th>
                    <th>Zbylý kredit</th>
                    <th>AO</th>
                    <th>Otevřené zakázky</th>
                    <th>Transakce</th>
                    <th>Zaslané zakázky</th>
                    <th></th>
                </tr>
                @foreach($profiles as $profile)
                    <tr>
                        <td>{{ $profile->email }}</td>
                        <td>{{ sprintf('%s %s', $profile->chargedCredit, $profile->lang == 'cs' ? 'Kč' : '€') }}</td>
                        <td>{{ sprintf('%s %s', $profile->credits, $profile->lang == 'cs' ? 'Kč' : '€') }}</td>

                        <td>
                            {{ sprintf('%s %s', ($ao_trans = $profile->transactions->where('type', 'auto-trading'))->sum('price'), $profile->lang == 'cs' ? 'Kč' : '€') }}
                            <small class="pull-right">({{ $ao_trans->count() }})</small>
                        </td>
                        <td>
                            {{ sprintf('%s %s', ($credit_trans = $profile->transactions->where('type', 'credit'))->sum('price'), $profile->lang == 'cs' ? 'Kč' : '€') }}
                            <small class="pull-right">({{ $credit_trans->count() }})</small>
                        </td>
                        <td>
                            {{ $profile->transactions->count() }}
                            <small>
                                <a data-toggle="modal" data-target="#cskontakt-transaction-modal-{{ $profile->id }}"
                                   class="action pull-right">
                                    <i class="fa fa-search"></i>
                                </a>
                            </small>
                        </td>
                        <td>
                            {{ $profile->sendInquiries->count() }}
                            <small>
                                <a data-toggle="modal" data-target="#cskontakt-inquiries-modal-{{ $profile->id }}"
                                   class="action pull-right">
                                    <i class="fa fa-search"></i>
                                </a>
                            </small>
                        </td>
                        <td style="width:1px;white-space: nowrap;">
                            <a href="http://admin.b2m.cz/cs-contact/detail/index/id/{{ $profile->id }}" target="_blank" class="btn btn-xs btn-primary">Profil</a>
                        </td>
                    </tr>
                @endforeach

            </table>
        </div>

    </div>


    @foreach ($profiles as $profile)
        @if($profile->transactions->count())
            <div id="cskontakt-transaction-modal-{{ $profile->id }}" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Seznam transakcí</h4>
                        </div>
                        <div class="modal-body modal-body-table">
                            <table class="table table-bordered">
                                @foreach($profile->transactions as $trans)
                                    <tr>
                                        <td style="width: 1px; white-space: nowrap;">{{ datetime($trans->created_at) }}</td>
                                        <td style="width: 1px; white-space: nowrap;">{{ $trans->type }}</td>
                                        <td>{{ $trans->title }}</td>
                                        <td style="width: 1px; white-space: nowrap;">{{ sprintf('%s %s', $trans->price, $profile->lang == 'cs' ? 'Kč' : '€') }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Zavřít</button>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if($profile->sendInquiries->count())
            <div id="cskontakt-inquiries-modal-{{ $profile->id }}" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Seznam zalsaných poptávek</h4>
                        </div>
                        <div class="modal-body modal-body-table">
                            <table class="table table-bordered">
                                @foreach($profile->sendInquiries as $inquiry)
                                    <tr>
                                        <td style="width: 1px; white-space: nowrap;">{{ datetime($inquiry->created_at) }}</td>
                                        <td>{{ $inquiry->title }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Zavřít</button>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endforeach
@endif
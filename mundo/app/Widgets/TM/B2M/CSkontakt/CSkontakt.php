<?php

namespace Mundo\Widgets\TM\B2M\CSkontakt;

use Carbon\Carbon;
use Illuminate\Database\Connection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Collection;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\RIS\RenewalFirm;
use Mundo\Models\TM\Record;
use Mundo\Modules\TM\Sources\Support\SourceManager;
use Mundo\Widgets\TM\B2M\Constants;
use Mundo\Widgets\TM\Contact\Contact;

/**
 * Class CSkontakt
 * @package Mundo\Widgets\TM\B2M\CSkontakt
 */
class CSkontakt extends BaseWidget
{
    /** @var Record|RenewalFirm */
    protected $record;
    /** @var  SourceManager */
    protected $sources;

    /**
     * CSkontakt constructor.
     *
     * @param Model         $record
     * @param SourceManager $sources
     */
    public function __construct(Model $record, SourceManager $sources)
    {
        $this->record = $record;
        $this->sources = $sources;
    }

    /**
     * @return string
     */
    public function render()
    {
        return $this->renderView(__DIR__ . '/views/widget.blade.php', [
            'profiles' => $this->profiles(),
        ]);
    }


    /**
     * @return Collection
     */
    protected function profiles()
    {
        return $this->users()->map(function ($firm) {
            $firm->transactions = $this->transactionForFirm($firm);
            $firm->sendInquiries = $this->sendInquiries($firm);
            $firm->chargedCredit = $this->chargedCredit($firm);

            return $firm;
        });
    }

    /**
     * @return Connection
     */
    protected function db()
    {
        return $this->sources->get('b2m')->getConnection();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    private function users()
    {
        return collect($this->db()->table('cs_contact.users AS U')
            ->select(['U.*'])
            ->where('U.firm_id', $this->record->getRemoteId())
            ->where('U.active', 1)
            ->get());
    }

    /**
     * @param $firm
     *
     * @return \Illuminate\Support\Collection
     */
    private function transactionForFirm($firm)
    {
        return collect($this->db()->table('cs_contact.inquiry_user AS IU')
            ->select(['IU.*', 'I.title'])
            ->join('cs_contact.inquiries AS I', 'I.id', '=', 'IU.inquiry_id')
            ->where('IU.user_id', $firm->id)
            ->orderBy('IU.created_at', 'desc')
            ->get());
    }

    /**
     * @param $firm
     *
     * @return \Illuminate\Support\Collection
     */
    private function sendInquiries($firm)
    {
        return collect($this->db()->table('cs_contact.notifications AS N')
            ->select([
                'N.*',
                'I.title'
            ])
            ->join('cs_contact.inquiries AS I', 'I.id', '=', 'N.notificable_id')
            ->where('N.user_id', $firm->id)
            ->where('N.notificable_type', 'Kartus\Model\Inquiry')
            ->where('N.type', 'new')
            ->orderBy('N.created_at', 'desc')
            ->get());
    }

    private function chargedCredit($firm)
    {
        return $this->db()->table('obis.Order AS O')
            ->select([
                $this->db()->raw('SUM(S.price + SIS.value) AS credit')
            ])
            ->join('obis.Order_Service AS OS', 'OS.order_id', '=', 'O.order_id')
            ->join('obis.Service AS S', 'S.service_id', '=', 'OS.service_id')
            ->join('obis.Service_Item AS SI', function (JoinClause $j) {
                $j->on('SI.service_id', '=', 'OS.service_id')
                    ->where('SI.type', '=', 'cskontakt');
            })
            ->join('obis.ServiceItem_Settings AS SIS', 'SIS.serviceItem_id', '=', 'SI.serviceItem_id')
            ->where('O.firm_id', $firm->firm_id)
            ->whereNotIn('O.state', ['cancelled'])
            ->first()
            ->credit;
    }
}

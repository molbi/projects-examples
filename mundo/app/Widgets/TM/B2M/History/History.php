<?php

namespace Mundo\Widgets\TM\B2M\History;

use Illuminate\Cache\CacheManager;
use Illuminate\Database\Connection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\RIS\RenewalFirm;
use Mundo\Models\RM\Record as RMRecord;
use Mundo\Models\TM\Record;
use Mundo\Modules\TM\Sources\Support\SourceManager;

class History extends BaseWidget
{
    const TYPE_FIRM = 'firm';
    const TYPE_ORDER = 'order';
    const TYPE_RIS = 'ris';
    const TYPE_TM = 'tm';

    /** @var Record|RenewalFirm|RMRecord */
    protected $record;

    /** @var SourceManager */
    private $sources;

    /** @var array */
    private $types = [
        self::TYPE_FIRM  => 'Firemní',
        self::TYPE_ORDER => 'Objednávkové',
        self::TYPE_RIS   => 'Retenční',
        self::TYPE_TM    => 'Telemarketing'
    ];

    private $notes = [];
    /**
     * @var bool
     */
    private $group;
    /**
     * @var CacheManager
     */
    private $cache;

    /**
     * History constructor.
     *
     * @param Model         $record
     * @param SourceManager $sources
     * @param CacheManager  $cache
     * @param bool          $group
     */
    public function __construct(Model $record, SourceManager $sources, CacheManager $cache, $group = false)
    {
        $this->record = $record;
        $this->sources = $sources;
        $this->group = $group;
        $this->cache = $cache;
    }

    public function render()
    {
        return $this->renderView(__DIR__ . '/views/widget.blade.php', [
            'notes' => $this->notes(),
            'group' => $this->group
        ]);
    }

    public function toViewName($note)
    {
        return 'types/' . $note->type;
    }

    /**
     * @return Connection
     */
    protected function db()
    {
        return $this->sources->get('b2m')->getConnection();
    }

    private function notes()
    {
        if ($collection = $this->getFromCache()) {
            return $collection;
        }

        foreach ($this->types as $type => $title) {
            $method = sprintf('get%s', ucfirst($type));
            if (method_exists($this, $method)) {
                call_user_func_array([$this, $method], []);
            }
        }

        $collection = collect(array_map(function ($row) {
            $row->addition = json_decode($row->addition);

            return $row;
        }, $this->mergeAll()));

        $this->storeToCache($collection);

        return $collection;
    }

    private function getFirm()
    {
        $this->notes[] = $this->db()->table('obis.FirmNote AS FN')
            ->select([
                'FN.firm_id AS note_id',
                'FN.createdOn AS created_at',
                'FN.note',
                'E.realname',
                DB::raw('"firm" AS type'),
                DB::raw('"" AS addition')
            ])
            ->join('zis.Employee AS E', 'E.employee_id', '=', 'FN.employee_id')
            ->whereRaw('DATE(FN.createdOn) > DATE(NOW() - INTERVAL 1 YEAR)')
            ->where('FN.firm_id', $this->record->getRemoteId())
            ->get();
    }

    private function getOrder()
    {
        $this->notes[] = $this->db()->table('obis.Order AS O')
            ->select([
                'ON.order_id AS note_id',
                'ON.createdOn AS created_at',
                'ON.content AS note',
                'E.realname',
                DB::raw('"order" AS type'),
                DB::raw('"" AS addition')
            ])
            ->join('obis.OrderNote AS ON', 'ON.order_id', '=', 'O.order_id')
            ->join('zis.Employee AS E', 'E.employee_id', '=', 'ON.employee_id')
            ->whereRaw('DATE(ON.createdOn) > DATE(NOW() - INTERVAL 1 YEAR)')
            ->where('O.firm_id', $this->record->getRemoteId())
            ->get();
    }

    private function getRis()
    {
        $this->notes[] = $this->db()->table('obis.RenewalFirmHistory AS RFH')
            ->select([
                'RFH.renewalFirmHistory_id AS note_id',
                'RFH.createdAt AS created_at',
                DB::raw('RFH.note COLLATE utf8_general_ci AS note'),
                'E.realname',
                DB::raw('"ris" AS type'),
                DB::raw('concat(\'{"service":"\',RF.service,\'"}\') AS addition')
            ])
            ->join('obis.RenewalFirm AS RF', 'RF.renewalFirm_id', '=', 'RFH.renewalFirm_id')
            ->join('zis.Employee AS E', 'E.employee_id', '=', 'RFH.employee_id')
            ->whereRaw('DATE(RFH.createdAt) > DATE(NOW() - INTERVAL 1 YEAR)')
            ->where('RF.firm_id', $this->record->getRemoteId())
            ->get();
    }

    private function getTm()
    {
        $query = $this->db()->table('telemarketing.History AS H')
            ->select([
                'H.history_id AS note_id',
                'H.createdAt AS created_at',
                'H.note',
                'E.realname',
                DB::raw('"tm" AS type'),
                DB::raw('concat(\'{"title":"\',C.title,\'","id":"\',C.campaign_id,\'"}\') AS addition')
            ])
            ->join('telemarketing.Stack AS S', 'S.stack_id', '=', 'H.stack_id')
            ->join('telemarketing.Campaign AS C', 'C.campaign_id', '=', 'S.campaign_id')
            ->join('zis.Employee AS E', 'E.employee_id', '=', 'H.employee_id')
            ->whereRaw('DATE(H.createdAt) > DATE(NOW() - INTERVAL 1 YEAR)')
            ->where('S.targetSubject_id', $this->record->getRemoteId());

        if (is_a($this->record, Record::class)) {
            $query->where('C.campaign_id', '!=', $this->record->campaign->sync_id);
        }

        $this->notes[] = $query->get();
    }

    private function mergeAll()
    {
        return collect(array_flatten($this->notes))->sortByDesc('created_at')->toArray();
    }

    public function getTabPanelName($type)
    {
        return $this->types[$type];
    }

    private function getFromCache()
    {
        return $this->cache->store('file')->get($this->cacheyKey());
    }

    private function storeToCache($collection)
    {
        $this->cache->store('file')->put($this->cacheyKey(), $collection, 60);
    }

    private function cacheyKey()
    {
        return 'widget_b2m_history_' . $this->record->getRemoteId();
    }
}

<div class="tiles tiles-framed tiles-default widget widget-action-history">
    <div class="tiles-title">
        Historie z B2M ({{ count($notes) }})

        <div class="btn-group">
            <a onclick="$('#widget-b2m-history-table').toggle()" class="btn btn-primary btn-xs">zobrazit/skrýt</a>
        </div>

    </div>
    @if($group)
        {? $groups = $notes->groupBy('type') ?}

        <div class="tiles-body tiles-body-table" style="display:none" id="widget-b2m-history-table">
            <ul class="nav nav-tabs" style="margin-bottom: 0;">
                {? $iterator = 0; ?}
                @foreach($groups as $key => $items)
                    <li @if($iterator == 0)class="active"@endif>
                        <a href="#widget_history_{{ $key }}"
                           data-toggle="tab">
                            {{ $widget->getTabPanelName($key) }} ({{ $items->count() }})
                        </a>
                    </li>
                    {? $iterator++; ?}
                @endforeach
                <li>
                    <a href="#widget_history_all" data-toggle="tab">Vše ({{ $notes->count() }})</a>
                </li>
            </ul>
            <div id="myTabContent" class="tab-content">
                {? $iterator = 0; ?}
                @foreach($groups as $key => $_notes)
                    <div class="tab-pane fade @if($iterator == 0) active in @endif" id="widget_history_{{ $key }}">
                        <table class="table table-bordered">
                            @foreach($_notes as $note)
                                @partial($widget->toViewName($note))
                            @endforeach
                        </table>
                    </div>
                    {? $iterator++; ?}
                @endforeach

                <div class="tab-pane fade" id="widget_history_all">
                    <table class="table table-bordered">
                        @foreach($notes as $note)
                            @partial($widget->toViewName($note))
                        @endforeach
                    </table>
                </div>
            </div>

        </div>
    @else
        <div class="tiles-body tiles-body-table" style="overflow-y: hidden">
            <table class="table table-bordered" style="display:none" id="widget-b2m-history-table">
                @foreach($notes as $note)
                    @partial($widget->toViewName($note))
                @endforeach
            </table>
        </div>
    @endif


</div>
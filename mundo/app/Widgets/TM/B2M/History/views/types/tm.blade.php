<tr>
    <td class="table-rest" style="vertical-align: top;">
        <span class="label label-success">TM</span>
    </td>
    <td>
        @if(!is_null($note->addition))<small>{{ $note->addition->title }}</small><br />@endif
        {{ $note->note }}
    </td>
    <td colspan="2" style="width: 1px; white-space: nowrap; text-align: right;">
        <small>{{ datetime($note->created_at) }} <i class="fa fa-fw fa-clock-o"></i></small>
        <br>
        <small> {{ $note->realname or '' }} <i class="fa fa-fw fa-user"></i></small>
    </td>
</tr>
<?php

namespace Mundo\Widgets\TM\B2M\ServicesHistory;

use Illuminate\Database\Connection;
use Illuminate\Database\Eloquent\Model;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\TM\Record;
use Mundo\Modules\TM\Sources\Support\SourceManager;
use Mundo\Widgets\TM\B2M\Constants;
use Mundo\Widgets\TM\B2M\ServicesHistory\Groups\IService;

/**
 * Class ActiveSelling
 * @package Mundo\Widgets\TM\B2M\ActiveSelling
 */
class ServicesHistory extends BaseWidget
{
    const CACHE_LIFETIME = 44640;

    const GROUP_INQUIRIES = 'inquiries';
    const GROUP_BOARD = 'official-board';
    const GROUP_RH = 'rh';
    const GROUP_CATALOG = 'catalog';
    const GROUP_PREMIUM = 'premium';
    const GROUP_PREPAID_OFFER = 'prepaid-offer';

    /**
     * @var array
     */
    private $serviceGroups = [
        'inquiries'                      => self::GROUP_INQUIRIES,
        'inquiries-keywords'             => self::GROUP_INQUIRIES,
        'official-board'                 => self::GROUP_BOARD,
        'official-board-keywords'        => self::GROUP_BOARD,
        'official-board-bold'            => self::GROUP_BOARD,
        'official-board-prepaid'         => self::GROUP_BOARD,
        'referencehodnoceni'             => self::GROUP_RH,
        'referencehodnoceni-without-ppc' => self::GROUP_RH,
        'bold'                           => self::GROUP_CATALOG,
        'prepaid'                        => self::GROUP_CATALOG,
        'premium'                        => self::GROUP_PREMIUM,
        'prepaid-offer'                  => self::GROUP_PREPAID_OFFER
    ];

    private $serviceGroupsName = [
        self::GROUP_INQUIRIES     => 'Zasílání poptávek',
        self::GROUP_BOARD         => 'Úřední nástěnka',
        self::GROUP_RH            => 'Reference hodnocení',
        self::GROUP_CATALOG       => 'Katalogové služby',
        self::GROUP_PREMIUM       => 'Premium',
        self::GROUP_PREPAID_OFFER => 'Přeplacené nabídky',
    ];


    /** @var Model */
    protected $record;
    /** @var  SourceManager */
    protected $sources;

    /**
     * ActiveSelling constructor.
     *
     * @param Model  $record
     * @param SourceManager $sources
     */
    public function __construct(Model $record, SourceManager $sources)
    {
        $this->record = $record;
        $this->sources = $sources;
    }

    /**
     * @return string
     */
    public function render()
    {
        return $this->renderView(__DIR__ . '/views/widget.blade.php', [
            'services' => $this->services()
        ]);
    }

    /**
     * @return Connection
     */
    protected function db()
    {
        return $this->sources->get('b2m')->getConnection();
    }

    /**
     * @return array|static[]
     */
    private function services()
    {
        return array_unique(array_map(function ($item) {
            return isset($this->serviceGroups[$item->service]) ? $this->serviceGroups[$item->service] : false;
        }, $this->db()
            ->table('obis.RenewalFirm AS RF')
            ->where('RF.firm_id', $this->record->getRemoteId())
            ->groupBy('RF.service')
            ->get()
        ));
    }


    /**
     * Ověří zda existuje třída pro danou skupinu služeb.
     * Pokud je parametr $instantiate = true, vrátí instanci třídy
     *
     * @param      $service
     *
     * @param bool $instantiate
     *
     * @return bool|IService
     *
     */
    public function has($service, $instantiate = false)
    {
        if (class_exists($klass = sprintf('%s\Groups\%s', __NAMESPACE__, studly_case($service)))) {
            return !$instantiate ? true : new $klass($this->record, $this->db());
        }

        return false;
    }

    /**
     * Vřátí data pro požadovanou službu, pokud tato služba (skupina služeb) má třídu, která data vrátí.
     *
     * @param $service
     *
     * @return null|object
     */
    public function get($service)
    {
        if ($class = $this->has($service, true)) {
            return $class->get();
        }

        return null;
    }

    /**
     * @param $service
     *
     * @return mixed
     */
    public function groupName($service)
    {
        return $this->serviceGroupsName[$service];
    }
}

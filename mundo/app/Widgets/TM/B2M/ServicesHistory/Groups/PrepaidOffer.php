<?php namespace Mundo\Widgets\TM\B2M\ServicesHistory\Groups;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Mundo\Models\RIS\RenewalFirm;
use Mundo\Models\TM\Record;
use Illuminate\Database\Connection;
use Mundo\Widgets\TM\B2M\ServicesHistory\ServicesHistory;

/**
 * Class PrepaidOffer
 * @package Mundo\Widgets\TM\B2M\ServicesHistory\Groups
 */
class PrepaidOffer implements IService
{
    /**
     * @var Record|RenewalFirm
     */
    private $record;
    /**
     * @var Connection
     */
    private $db;

    /**
     * IService constructor.
     *
     * @param Model $record
     * @param Connection   $db
     */
    public function __construct(Model $record, Connection $db)
    {
        $this->record = $record;
        $this->db = $db;
    }

    /**
     * @return mixed
     */
    public function get()
    {
        $cacheKey = sprintf('widget_services_history_prepaid_offer_%s', $this->record->getRemoteId());

        return (object)[
            'offers' => $this->offersWithStats()
        ];
    }

    /**
     * @return array
     */
    private function offersWithStats()
    {
        return array_map(function ($offer) {
            return (object)[
                'title' => $offer->title,
                'stats' => $this->offerStats($offer)
            ];
        }, $this->offers());
    }

    private function offers()
    {
        return $this->db->table('business.Offer AS O')
            ->join('business.OfferLang AS OL', 'OL.offer_id', '=', 'O.offer_id')
            ->where('O.state', 'active')
            ->where('O.firm_id', $this->record->getRemoteId())
            ->orderBy('O.createdAt', 'DESC')
            ->orderBy('O.offer_id', 'DESC')
            ->get();
    }

    private function offerStats($offer)
    {
        return array_assoc($this->db->table('business.OfferCatalogStatistic AS OCS')
            ->select([
                DB::raw('DATE(OCS.createdAt) AS date'),
                'count'
            ])
            ->where('OCS.offer_id', $offer->offer_id)
            ->where('OCS.type', 'prepaid-offer')
            ->orderBy('OCS.createdAt', 'DESC')
            ->limit(12)
            ->get()
            , 'date=count');
    }
}
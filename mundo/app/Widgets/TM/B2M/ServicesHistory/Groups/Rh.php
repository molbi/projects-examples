<?php namespace Mundo\Widgets\TM\B2M\ServicesHistory\Groups;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\Cache;
use Mundo\Models\RIS\RenewalFirm;
use Mundo\Models\TM\Record;
use Illuminate\Database\Connection;
use Mundo\Widgets\TM\B2M\ServicesHistory\ServicesHistory;

/**
 * Class Rh
 * @package Mundo\Widgets\TM\B2M\ServicesHistory\Groups
 */
class Rh implements IService
{
    /**
     * @var Record|RenewalFirm
     */
    private $record;
    /**
     * @var Connection
     */
    private $db;

    /**
     * Inquiries constructor.
     *
     * @param Model $record
     * @param Connection   $db
     */
    public function __construct(Model $record, Connection $db)
    {
        $this->record = $record;
        $this->db = $db;
    }

    /**
     *
     * @return object
     */
    public function get()
    {
        return (object)[
            'info'           => $this->firmInfo(),
            'notes'          => $this->firmNotes(),
            'additionalInfo' => $this->additionalInfo()
        ];
    }

    /**
     * @return mixed
     */
    private function firmInfo()
    {
        return $this->db->table('soraka.firms AS F')
            ->select([
                'F.lang',
                'F.id',
                'F.slug'
            ])
            ->where('F.firm_id', $this->record->getRemoteId())
            ->first();
    }

    /**
     * @return array|static[]
     */
    private function firmNotes()
    {
        return $this->db->table('soraka.firms AS F')
            ->select([
                'F.*',
                'N.subject',
                'N.body',
                'N.created_at',
                'U.name'
            ])
            ->join('soraka.notes AS N', 'N.firm_id', '=', 'F.id')
            ->join('soraka.users AS U', 'U.id', '=', 'N.user_id')
            ->where('F.firm_id', $this->record->getRemoteId())
            ->get();
    }

    /**
     * @return object
     */
    private function additionalInfo()
    {
        $output = array();

        $output['ratings'] = $this->db->table('soraka.firms AS F')
            ->select([
                DB::raw('COUNT(DISTINCT R.id) AS count'),
                DB::raw('AVG(R.rating) AS average'),
            ])
            ->join('soraka.ratings AS R', 'R.firm_id', '=', 'F.id')
            ->where('F.firm_id', $this->record->getRemoteId())
            ->first();

        $output['references'] = $this->db->table('soraka.firms AS F')
            ->select([
                DB::raw('COUNT(DISTINCT R.id) AS count')
            ])
            ->join('soraka.references AS R', 'R.firm_id', '=', 'F.id')
            ->where('F.firm_id', $this->record->getRemoteId())
            ->first();


        $output['ppc'] = $this->db->table('soraka.firms AS F')
            ->select([
                DB::raw('CASE WHEN T.closed_at IS NULL THEN 0 ELSE 1 END AS active'),
                DB::raw('SUM(S.summary_ppc_show_all) AS total'),
                DB::raw('SUM(S.summary_profile_visited) AS conversion')
            ])
            ->leftJoin('soraka.tickets AS T', function (JoinClause $j) {
                $j->on('T.firm_id', '=', 'F.id')
                    ->where('T.state_name', '=', 'run_ppc');
            })
            ->leftJoin('soraka.firm_statistics AS S', 'S.firm_id', '=', 'F.id')
            ->where('F.firm_id', $this->record->getRemoteId())
            ->first();

        return (object)$output;
    }

}
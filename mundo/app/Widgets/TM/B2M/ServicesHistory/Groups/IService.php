<?php namespace Mundo\Widgets\TM\B2M\ServicesHistory\Groups;

use Illuminate\Database\Connection;
use Illuminate\Database\Eloquent\Model;
use Mundo\Models\TM\Record;

/**
 * Interface IService
 * @package Mundo\Widgets\TM\B2M\ServicesHistory\Services
 */
interface IService
{
    /**
     * IService constructor.
     *
     * @param Model $record
     * @param Connection   $db
     */
    public function __construct(Model $record, Connection $db);

    /**
     * @return mixed
     */
    public function get();
}
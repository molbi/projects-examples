<?php namespace Mundo\Widgets\TM\B2M\ServicesHistory\Groups;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Mundo\Models\RIS\RenewalFirm;
use Mundo\Models\TM\Record;
use Illuminate\Database\Connection;
use Mundo\Widgets\TM\B2M\ServicesHistory\ServicesHistory;

/**
 * Class Inquiries
 * @package Mundo\Widgets\TM\B2M\ServicesHistory\Services
 */
class Catalog implements IService
{
    /**
     * @var Record|RenewalFirm
     */
    private $record;
    /**
     * @var Connection
     */
    private $db;

    /**
     * Inquiries constructor.
     *
     * @param Model $record
     * @param Connection   $db
     */
    public function __construct(Model $record, Connection $db)
    {
        $this->record = $record;
        $this->db = $db;
    }

    /**
     *
     * @return object
     */
    public function get()
    {
        $cacheKey = sprintf('widget_services_history_catalog_%s', $this->record->getRemoteId());

        return (object)[
            'countViews'  => $this->countStatistics('list'),
            'countClicks' => $this->countStatistics('detail')
        ];
    }

    public function countStatistics($type)
    {
        return array_assoc($this->db->table('business.FirmCatalogStatistic AS FCS')
            ->select([
                'createdAt',
                'count'
            ])
            ->where('FCS.firm_id', $this->record->getRemoteId())
            ->where('FCS.type', $type)
            ->groupBy(DB::raw('YEAR(FCS.createdAt)'))
            ->groupBy(DB::raw('MONTH(FCS.createdAt)'))
            ->orderBy('FCS.createdAt', 'DESC')
            ->get(), 'createdAt=count');
    }
}
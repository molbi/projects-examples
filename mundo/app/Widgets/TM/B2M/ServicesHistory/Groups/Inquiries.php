<?php namespace Mundo\Widgets\TM\B2M\ServicesHistory\Groups;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\Cache;
use Mundo\Models\RIS\RenewalFirm;
use Mundo\Models\TM\Record;
use Illuminate\Database\Connection;
use Mundo\Widgets\TM\B2M\ServicesHistory\ServicesHistory;

/**
 * Class Inquiries
 * @package Mundo\Widgets\TM\B2M\ServicesHistory\Services
 */
class Inquiries implements IService
{
    /**
     * @var Record|RenewalFirm
     */
    private $record;
    /**
     * @var Connection
     */
    private $db;

    /**
     * Inquiries constructor.
     *
     * @param Model $record
     * @param Connection $db
     */
    public function __construct(Model $record, Connection $db)
    {
        $this->record = $record;
        $this->db = $db;
    }

    /**
     *
     * @return object
     */
    public function get()
    {
        $cacheKey = sprintf('widget_services_history_inquiries_%s', $this->record->getRemoteId());

        return (object)[
            'countInquiries' => $this->countInquiries(),
            'countLogin'     => $this->countLogin()
        ];
    }

    /**
     * @return integer
     */
    private function countInquiries()
    {
        /** Pro rychlost hlavního dotazu je nutné najít poptávku, od které se začne počítat */
        $lastInquiry = $this->db->table('business.Inquiry AS I')
            ->whereRaw('I.publishedOn > NOW() - INTERVAL 1 YEAR')
            ->where('I.published', true)
            ->orderBy('I.publishedOn')
            ->first();

        $res = $this->db->table('business.Firm_Inquiry AS FI')
            ->select([
                DB::raw('COUNT(*) AS count')
            ])
            ->join('business.Inquiry AS I', function (JoinClause $j) {
                $j->on('I.inquiry_id', '=', 'FI.inquiry_id');
            })
            ->where('FI.firm_id', $this->record->getRemoteId())
            ->where('FI.inquiry_id', '>', $lastInquiry->inquiry_id)
            ->first();

        return $res->count;
    }

    /**
     * @return integer
     */
    private function countLogin()
    {
        $res = $this->db->table('business.Login AS L')
            ->select([
                DB::raw('SUM(L.displayedContacts) AS count')
            ])
            ->where('L.firm_id', $this->record->getRemoteId())
            ->whereRaw('DATE(L.createdOn) >= NOW() - INTERVAL 365 DAY')
            ->whereRaw('ipAddress NOT LIKE "%217.169%"')
            ->first();

        return $res->count;
    }
}
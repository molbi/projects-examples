<?php namespace Mundo\Widgets\TM\B2M\ServicesHistory\Groups;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Mundo\Kernel\Source\B2M\Modifiers\Renewal;
use Mundo\Models\RIS\RenewalFirm;
use Mundo\Models\TM\Record;
use Illuminate\Database\Connection;

class Premium implements IService
{
    /**
     * @var Record|RenewalFirm
     */
    private $record;
    /**
     * @var Connection
     */
    private $db;

    /**
     * IService constructor.
     *
     * @param Model $record
     * @param Connection   $db
     */
    public function __construct(Model $record, Connection $db)
    {
        $this->record = $record;
        $this->db = $db;
    }

    /**
     * @return mixed
     */
    public function get()
    {
        return (object)[
            'catalogViews' => $this->catalogViews()
        ];
    }

    private function catalogViews()
    {
        return collect(array_assoc($this->db->table('business.FirmCatalogStatistic AS FCS')
            ->select([
                'FCS.createdAt',
                'FCS.count'
            ])
            ->where('FCS.firm_id', $this->record->getRemoteId())
            ->where('FCS.type', 'premium-category')
            ->groupBy(DB::raw('YEAR(FCS.createdAt)'))
            ->groupBy(DB::raw('MONTH(FCS.createdAt)'))
            ->orderBy('FCS.createdAt', 'DESC')
            ->limit(14)
            ->get(), 'createdAt=count'))->chunk(2)->toArray();
    }
}

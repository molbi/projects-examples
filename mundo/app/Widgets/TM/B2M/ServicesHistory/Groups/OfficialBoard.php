<?php namespace Mundo\Widgets\TM\B2M\ServicesHistory\Groups;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Mundo\Models\RIS\RenewalFirm;
use Mundo\Models\TM\Record;
use Illuminate\Database\Connection;
use Mundo\Widgets\TM\B2M\ServicesHistory\ServicesHistory;

class OfficialBoard implements IService
{
    /**
     * @var Record|RenewalFirm
     */
    private $record;
    /**
     * @var Connection
     */
    private $db;

    /**
     * Inquiries constructor.
     *
     * @param Model $record
     * @param Connection   $db
     */
    public function __construct(Model $record, Connection $db)
    {
        $this->record = $record;
        $this->db = $db;
    }

    /**
     *
     * @return object
     */
    public function get()
    {
        $cacheKey = sprintf('widget_services_history_board_%s', $this->record->getRemoteId());

        return (object)[
            'countPsInquiries' => $this->countPsInquiries(),
        ];
    }

    private function countPsInquiries()
    {
        $lastInquiry = $this->db->table('pvis.PsInquiry AS I')
            ->whereRaw('I.publishedOn > NOW() - INTERVAL 1 YEAR')
            ->orderBy('I.publishedOn')
            ->first();

        $res = $this->db->table('business.Firm_PsInquiry AS FI')
            ->select([
                DB::raw('COUNT(*) AS count')
            ])
            ->where('FI.firm_id', $this->record->getRemoteId())
            ->where('FI.psInquiry_id', '>', $lastInquiry->psInquiry_id)
            ->first();

        return $res->count;
    }
}
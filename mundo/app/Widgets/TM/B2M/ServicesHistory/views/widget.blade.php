<div class="tiles tiles-framed tiles-default widget">
    <div class="tiles-title">
        Historie služeb
    </div>
    <div class="tiles-body tiles-body-table">

        <ul class="nav nav-tabs" style="margin-bottom: 0;">
            {? $i = 0; ?}
            @foreach($services as $service)
                @if($widget->has($service))
                    <li @if($i == 0)class="active"@endif>
                        <a href="#service-history-{{$service}}"
                           data-toggle="tab">
                            {{ $widget->groupName($service) }}
                        </a>
                    </li>
                    {? $i++ ?}
                @endif
            @endforeach
        </ul>

        <div id="myTabContent" class="tab-content">
            {? $i = 0; ?}
            @foreach($services  as $key => $service)
                @if($widget->has($service))
                    <div class="tab-pane fade @if($i == 0)in active @endif" id="service-history-{{ $service }}">
                        @partial("groups/{$service}", ['data' => $widget->get($service)])
                    </div>
                    {? $i++ ?}
                @endif
            @endforeach
        </div>
    </div>
</div>
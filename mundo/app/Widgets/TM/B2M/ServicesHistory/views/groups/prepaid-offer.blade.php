<h4>Přednostní produkt</h4>

<table style="width:100%;">
    @foreach($data->offers as $offer)
        <tr>
            <td>{{ $offer->title }}</td>
            <td>
                <table style="width:100%;">
                    <tr>
                        @foreach($offer->stats as $date => $count)
                            <td>
                                <abbr title="{{ date('n. Y',strtotime($date)) }}">{{ $count }}</abbr>
                            </td>
                        @endforeach
                    </tr>
                </table>
            </td>
        </tr>
    @endforeach
</table>
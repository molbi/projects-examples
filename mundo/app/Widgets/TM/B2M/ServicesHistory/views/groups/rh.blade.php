@if(!is_null($data->info))
    <table class="table table-bordered">
        <tbody>
        <tr>
            <th style="width: 150px; background-color: #fafafa;">Odkaz na profil</th>
            <td colspan="3">
                <a href="http://{{ $data->info->slug }}.referencehodnoceni.cz/">
                    http://{{ $data->info->slug }}.referencehodnoceni.cz/
                </a>
            </td>
        </tr>
        <tr>
            <th style="width: 150px; background-color: #fafafa;">Počet referencí</th>
            <td>{{ $data->additionalInfo->references->count }}</td>
            <th style="width: 150px; background-color: #fafafa;">Počet hodnocení</th>
            <td>{{ $data->additionalInfo->ratings->count }}</td>
        </tr>
        <tr>
            <th style="width: 150px; background-color: #fafafa;">Průměr hodnocení</th>
            <td>{{ $data->additionalInfo->ratings->average / 2 }}</td>
            <th style="width: 150px; background-color: #fafafa;">Aktivní PPC</th>
            <td>{{ $data->additionalInfo->ppc->active ? 'Ano' : 'Ne' }}</td>
        </tr>
        <tr>
            <th style="width: 150px; background-color: #fafafa;"><abbr title="za celé období">Zobrazení PPC</abbr></th>
            <td>{{ $data->additionalInfo->ppc->total }}</td>
            <th style="width: 150px; background-color: #fafafa;"><abbr title="za celé období">Návštěvy profilu z
                    PPC</abbr></th>
            <td>{{ $data->additionalInfo->ppc->conversion }}</td>
        </tr>
        </tbody>
    </table>
@else
    <table class="table table-bordered">
        <tbody>
        <tr>
            <td><p>Firmě byl na RH smazán profil.</p></td>
        </tr>
    </table>
@endif
<table class="table table-bordered">
    <tbody>
    <tr>
        <th style="width: 150px; background-color: #fafafa;">Počet zobrazení:</th>
        <td>
            <table style="width:100%;">
                @foreach(collect($data->countViews)->take(12) as $date => $count)
                    <tr>
                        <td style="width:50%">{{ \Carbon\Carbon::createFromTimestamp(strtotime($date))->format('n. Y') }}</td>
                        <td><strong>{{ $count }}</strong></td>
                    </tr>
                @endforeach
            </table>
        </td>
        <th style="width: 150px; background-color: #fafafa;">Počet návštěv detailu:</th>
        <td>
            <table style="width:100%;">
                @foreach(collect($data->countClicks)->take(12) as $date => $count)
                    <tr>
                        <td style="width:50%">{{ \Carbon\Carbon::createFromTimestamp(strtotime($date))->format('n. Y') }}</td>
                        <td><strong>{{ $count }}</strong></td>
                    </tr>
                @endforeach
            </table>
        </td>
    </tr>
    </tbody>
</table>

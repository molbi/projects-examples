<table class="table table-bordered">
    <tbody>
    @foreach($data->catalogViews as $row)
        <tr>
            @foreach($row as $date => $count)
                <th style="width: 150px; background-color: #fafafa;">{{ \Carbon\Carbon::createFromTimestamp(strtotime($date))->format('n. Y') }}</th>
                <td>{{ $count }}</td>
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>
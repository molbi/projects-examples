<table class="table table-bordered">
    <tbody>
    <tr>
        <th style="width: 150px; background-color: #fafafa;">Zaslaných poptávek</th>
        <td>{{ $data->countInquiries ? $data->countInquiries : 0 }}</td>
        <th style="width: 150px; background-color: #fafafa;">Zobrazených kontaktů:</th>
        <td>{{ $data->countLogin ? $data->countLogin : 0 }}</td>
    </tr>
    </tbody>
</table>
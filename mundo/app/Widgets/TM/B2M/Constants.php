<?php

namespace Mundo\Widgets\TM\B2M;

class Constants
{
    /**
     * Typ poptávky
     * @var string
     */
    const SERVICE_INQUIRIES = 'inquiries';
    /**
     * Typ předplacená pozice
     * @var string
     */
    const SERVICE_PREPAID = 'prepaid';

    /**
     * Typ zvýrazněná pozice
     * @var string
     */
    const SERVICE_BOLD = 'bold';

    /**
     * Typ premium pozice (katalog dodavatelů)
     * @var string
     */
    const SERVICE_PREMIUM = 'premium';

    /**
     * Typ pro poptávky od veřejných subjektů & veřejných zakázek
     * @var unknown_type
     */
    const SERVICE_PSINQUIRIES = 'psinquiries';

    /**
     *
     * @var unknown_type
     */
    const SERVICE_PROFILE_PLUS = 'profile-plus';

    /**
     *
     * @var unknown_type
     */
    const SERVICE_PROFILE_MAX = 'profile-max';

    const SERVICE_CZECHCONTACT = 'czechcontact';

    /**
     * Aktivní prodej
     *
     * @var string
     */
    const SERVICE_ACTIVE_SELLING = 'activeselling';

    /**
     * Zasílání poptávek z Profita
     *
     * @var string
     */
    const SERVICE_PROFITO_INQUIRIES = 'profito-inquiries';

    /**
     * Google PPC
     *
     * @var string
     */
    const SERVICE_GOOGLE_PPC = 'google-ppc';

    /**
     * Úřední nástěnka
     *
     * @var string
     */
    const SERVICE_OFFICIAL_BOARD = 'official-board';

    /**
     * Úřední nástěnka na klíčová slova
     */
    const SERVICE_OFFICIAL_BOARD_KEYWORDS = 'official-board-keywords';


    /**
     * Úřední nástěnka - Centrum Pomoci
     */
    const SERVICE_OFFICIAL_BOARD_SUPPORT = 'official-board-support';

    /**
     * Přednostní nabídka
     *
     * @var string
     */
    const SERVICE_PREPAID_OFFER = 'prepaid-offer';

    /**
     * Zasílání poptávek - cílení podle klíčových slov
     *
     * @var string
     */
    const SERVICE_INQUIRIES_KEYWORDS = 'inquiries-keywords';

    /**
     * Kredit zdarma
     *
     * @var string
     */
    const SERVICE_FREE_CREDIT = 'free-credit';

    /**
     * Premium poptávky
     *
     * @var string
     */
    const SERVICE_PREMIUM_INQUIRIES = 'premium-inquiries';

    /**
     * Prepaid - úřednínástěnka
     *
     * @var string
     */
    const SERVICE_OFFICIAL_BOARD_PREPAID = 'official-board-prepaid';

    /**
     * Bold - uřednínástěnka
     *
     * @var string
     */
    const SERVICE_OFFICIAL_BOARD_BOLD = 'official-board-bold';

    /**
     * Stavebníservis24.cz
     *
     * @var string
     */
    const SERVICE_STAVEBNI_SERVIS = 'stavebniservis';

    /**
     * ReklamníZakázky.cz
     *
     * @var string
     */
    const SERVICE_REKLAMNI_ZAKAZKY = 'reklamnizakazky';

    /**
     * ItZakázky24.cz
     *
     * @var string
     */
    const SERVICE_IT_ZAKAZKY = 'itzakazky';

    /**
     * DopravníZakázky.cz
     *
     * @var string
     */
    const SERVICE_DOPRAVNI_ZAKAZKY = 'dopravnizakazky';

    /**
     * StrojníZakázky.cz
     *
     * @var string
     */
    const SERVICE_STROJNI_ZAKAZKY = 'strojnizakazky';

    /**
     * InterierovéZakázky.cz
     *
     * @var string
     */
    const SERVICE_INTERIEROVE_ZAKAZKY = 'interierovezakazky';

    /**
     * FiremníZakázky.cz
     *
     * @var string
     */
    const SERVICE_FIREMNI_ZAKAZKY = 'firemnizakazky';

    /**
     * ElektroZakázky.cz
     *
     * @var string
     */
    const SERVICE_ELEKTRO_ZAKAZKY = 'elektrozakazky';

    /**
     * KovoZakázky.cz
     *
     * @var string
     */
    const SERVICE_KOVO_ZAKAZKY = 'kovozakazky';

    /**
     * ZakázkyNaPlasty.cz
     *
     * @var string
     */
    const SERVICE_ZAKAZKY_NA_PLASTY = 'zakazkynaplasty';

    /**
     * Prepaid - Stavebníservis24.cz
     *
     * @var string
     */
    const SERVICE_STAVEBNI_SERVIS_PREPAID = 'stavebniservis-prepaid';

    /**
     * Bold - Stavebníservis24.cz
     *
     * @var string
     */
    const SERVICE_STAVEBNI_SERVIS_BOLD = 'stavebniservis-bold';

    /**
     * Prepaid - ReklamníZakázky.cz
     *
     * @var string
     */
    const SERVICE_REKLAMNI_ZAKAZKY_PREPAID = 'reklamnizakazky-prepaid';

    /**
     * Bold - ReklamníZakázky.cz
     *
     * @var string
     */
    const SERVICE_REKLAMNI_ZAKAZKY_BOLD = 'reklamnizakazky-bold';

    /**
     * Prepaid - DopravníZakázky.cz
     *
     * @var string
     */
    const SERVICE_DOPRAVNI_ZAKAZKY_PREPAID = 'dopravnizakazky-prepaid';

    /**
     * Bold - DopravníZakázky.cz
     *
     * @var string
     */
    const SERVICE_DOPRAVNI_ZAKAZKY_BOLD = 'dopravnizakazky-bold';

    /**
     * Prepaid - ItZakázky.cz
     *
     * @var string
     */
    const SERVICE_IT_ZAKAZKY_PREPAID = 'itzakazky-prepaid';

    /**
     * Bold - ItZakázky.cz
     *
     * @var string
     */
    const SERVICE_IT_ZAKAZKY_BOLD = 'itzakazky-bold';

    /** Prepaid - StrojníZakázky.cz
     * @var string
     */
    const SERVICE_STROJNI_ZAKAZKY_PREPAID = 'strojnizakazky-prepaid';

    /** Bold - StrojníZakázky.cz
     * @var string
     */
    const SERVICE_STROJNI_ZAKAZKY_BOLD = 'strojnizakazky-bold';

    /**
     * Prepaid - InteriérovéZakázky.cz
     *
     * @var string
     */
    const SERVICE_INTERIEROVE_ZAKAZKY_PREPAID = 'interierovezakazky-prepaid';

    /**
     * Bold - InteriérovéZakázky.cz
     *
     * @var string
     */
    const SERVICE_INTERIEROVE_ZAKAZKY_BOLD = 'interierovezakazky-bold';

    /**
     * Prepaid - KovoZakázky.cz
     *
     * @var string
     */
    const SERVICE_KOVO_ZAKAZKY_PREPAID = 'kovozakazky-prepaid';

    /**
     * Bold - KovoZakázky.cz
     *
     * @var string
     */
    const SERVICE_KOVO_ZAKAZKY_BOLD = 'kovozakazky-bold';

    /**
     * Prepaid - ElektroZakázky.cz
     *
     * @var string
     */
    const SERVICE_ELEKTRO_ZAKAZKY_PREPAID = 'elektrozakazky-prepaid';

    /**
     * Bold - ElektroZakázky.cz
     *
     * @var string
     */
    const SERVICE_ELEKTRO_ZAKAZKY_BOLD = 'elektroezakazky-bold';

    /**
     * Prepaid - FiremníZakázky.cz
     *
     * @var string
     */
    const SERVICE_FIREMNI_ZAKAZKY_PREPAID = 'firemnizakazky-prepaid';

    /**
     * Bold - FiremníZakázky.cz
     *
     * @var string
     */
    const SERVICE_FIREMNI_ZAKAZKY_BOLD = 'firemnizakazky-bold';

    /**
     * Prepaid - ZakazkyNaPlasty.cz
     *
     * @var string
     */
    const SERVICE_ZAKAZKY_NA_PLASTY_PREPAID = 'zakazkynaplasty-prepaid';

    /**
     * Bold - ZakázkyNaPlasty.cz
     *
     * @var string
     */
    const SERVICE_ZAKAZKY_NA_PLASTY_BOLD = 'zakazkynaplasty-bold';


    /**
     * ReferenceHodnocení.cz
     *
     * @var string
     */
    const SERVICE_REFERENCE_HODNOCENI = 'referencehodnoceni';

    /**
     * ReferenceHodnocení.cz - varianta bez PPC
     *
     * @var string
     */
    const SERVICE_REFERENCE_HODNOCENI_WITHOUT_PPC = 'referencehodnoceni-without-ppc';

    /**
     * ReferenceHodnocení.cz - varianta bez PPC
     *
     * @var string
     */
    const SERVICE_REFERENCE_HODNOCENI_CERTIFICATE = 'referencehodnoceni-certificate';

    /**
     * ReferenceHodnocení.cz - VIP Promo
     *
     * @var string
     */
    const SERVICE_REFERENCE_HODNOCENI_VIP_PROMO = 'referencehodnoceni-vip-promo';

    /**
     * CSkontakt
     *
     * @var string
     */
    const SERVICE_CSKONTAKT = 'cskontakt';

    /**
     * CSkontakt - Zlatý dodavatel
     *
     * @var string
     */
    const SERVICE_CSKONTAKT_GOLDEN = 'cskontakt-golden';

    /**
     * CSkontakt - Zlatý dodavatel
     *
     * @var string
     */
    const SERVICE_CSKONTAKT_FEE = 'cskontakt-fee';

    /**
     * Epoptavka - Video Course
     *
     */
    const SERVICE_VIDEO_COURSE = 'video-course';

    const SERVICE_NAFR = 'nafr';

    const SERVICE_ETRZISTE = 'etrziste';

    const SERVICE_TITLES = [
        self::SERVICE_INQUIRIES                       => 'Zasílání poptávek',
        self::SERVICE_BOLD                            => 'Zvýrazněná pozice',
        self::SERVICE_PREPAID                         => 'Zvýhodněná pozice',
        self::SERVICE_PREMIUM                         => 'Prémiová nabídka',
        self::SERVICE_PSINQUIRIES                     => 'Poptávky od veřejných subjektů',
        self::SERVICE_PROFILE_PLUS                    => 'Profil plus',
        self::SERVICE_PROFILE_MAX                     => 'Profil max',
        self::SERVICE_CZECHCONTACT                    => 'Zápis v Czech(Slovak)Contact.com',
        self::SERVICE_ACTIVE_SELLING                  => 'Aktivní prodej',
        self::SERVICE_PROFITO_INQUIRIES               => 'Profito - zasílání poptávek',
        self::SERVICE_GOOGLE_PPC                      => 'Google - PPC',
        self::SERVICE_OFFICIAL_BOARD                  => 'Úřednínástěnka - katalog zakázek',
        self::SERVICE_PREPAID_OFFER                   => 'Přednostní produkt',
        self::SERVICE_INQUIRIES_KEYWORDS              => 'Zasílání poptávek - klíčová slova',
        self::SERVICE_FREE_CREDIT                     => 'Kredit zdarma',
        self::SERVICE_OFFICIAL_BOARD_KEYWORDS         => 'Úřednínástěnka - klíčová slova',
        self::SERVICE_OFFICIAL_BOARD_SUPPORT          => 'Úřednínástěnka - Centrum pomoci',
        self::SERVICE_PREMIUM_INQUIRIES               => 'Premiové zasílání poptávek',
        self::SERVICE_OFFICIAL_BOARD_PREPAID          => 'Úřednínástěnka - zvýhodněná pozice',
        self::SERVICE_OFFICIAL_BOARD_BOLD             => 'Úřednínástěnka - zvýrazněná pozice',
        self::SERVICE_STAVEBNI_SERVIS                 => 'Stavebníservis24.cz',
        self::SERVICE_REKLAMNI_ZAKAZKY                => 'ReklamníZakázky.cz',
        self::SERVICE_IT_ZAKAZKY                      => 'ItZakázky24.cz',
        self::SERVICE_DOPRAVNI_ZAKAZKY                => 'DopravníZakázky.cz',
        self::SERVICE_STROJNI_ZAKAZKY                 => 'StrojníZakázky.cz',
        self::SERVICE_INTERIEROVE_ZAKAZKY             => 'InterierovéZakázky.cz',
        self::SERVICE_KOVO_ZAKAZKY                    => 'KovoZakázky.cz',
        self::SERVICE_ELEKTRO_ZAKAZKY                 => 'ElektroZakázky.cz',
        self::SERVICE_FIREMNI_ZAKAZKY                 => 'FiremníZakázky.cz',
        self::SERVICE_ZAKAZKY_NA_PLASTY               => 'ZakázkyNaPlasty.cz',
        self::SERVICE_STAVEBNI_SERVIS_PREPAID         => 'Stavebníservis24.cz - zvýhodněná pozice',
        self::SERVICE_STAVEBNI_SERVIS_BOLD            => 'Stavebníservis24.cz - zvýrazněná pozice',
        self::SERVICE_REKLAMNI_ZAKAZKY_PREPAID        => 'ReklamníZakázky.cz - zvýhodněná pozice',
        self::SERVICE_REKLAMNI_ZAKAZKY_BOLD           => 'ReklamníZakázky.cz - zvýrazněná pozice',
        self::SERVICE_IT_ZAKAZKY_PREPAID              => 'ItZakázky.cz - zvýhodněná pozice',
        self::SERVICE_IT_ZAKAZKY_BOLD                 => 'ItZakázky.cz - zvýrazněná pozice',
        self::SERVICE_DOPRAVNI_ZAKAZKY_PREPAID        => 'DopravníZakázky.cz - zvýhodněná pozice',
        self::SERVICE_DOPRAVNI_ZAKAZKY_BOLD           => 'DopravníZakázky.cz - zvýrazněná pozice',
        self::SERVICE_STROJNI_ZAKAZKY_PREPAID         => 'StrojníZakázky.cz - zvýhodněná pozice',
        self::SERVICE_STROJNI_ZAKAZKY_BOLD            => 'StrojníZakázky.cz - zvýrazněná pozice',
        self::SERVICE_INTERIEROVE_ZAKAZKY_PREPAID     => 'InterierovéZakázky.cz - zvýhodněná pozice',
        self::SERVICE_INTERIEROVE_ZAKAZKY_BOLD        => 'InterierovéZakázky.cz - zvýrazněná pozice',
        self::SERVICE_KOVO_ZAKAZKY_PREPAID            => 'KovoZakázky.cz - zvýhodněná pozice',
        self::SERVICE_KOVO_ZAKAZKY_BOLD               => 'KovoZakázky.cz - zvýrazněná pozice',
        self::SERVICE_ELEKTRO_ZAKAZKY_PREPAID         => 'ElektroZakázky.cz - zvýhodněná pozice',
        self::SERVICE_ELEKTRO_ZAKAZKY_BOLD            => 'ElektroZakázky.cz - zvýrazněná pozice',
        self::SERVICE_FIREMNI_ZAKAZKY_PREPAID         => 'FiremníZakázky.cz - zvýhodněná pozice',
        self::SERVICE_FIREMNI_ZAKAZKY_BOLD            => 'FiremníZakázky.cz - zvýrazněná pozice',
        self::SERVICE_REFERENCE_HODNOCENI             => 'ReferenceHodnocení.cz',
        self::SERVICE_REFERENCE_HODNOCENI_WITHOUT_PPC => 'ReferenceHodnocení.cz - bez PPC',
        self::SERVICE_REFERENCE_HODNOCENI_CERTIFICATE => 'ReferenceHodnocení.cz - certifikát',
        self::SERVICE_REFERENCE_HODNOCENI_VIP_PROMO   => 'ReferenceHodnocení.cz - VIP Promo',
        self::SERVICE_CSKONTAKT                       => 'CSkontakt.cz',
        self::SERVICE_CSKONTAKT_GOLDEN                => 'CSkontakt.cz - Zlatý dodavatel',
        self::SERVICE_CSKONTAKT_FEE                   => 'CSkontakt.cz - Paušál',
        self::SERVICE_VIDEO_COURSE                    => 'Epoptavka - 15 tipů',
        self::SERVICE_NAFR                            => 'Nafr',
        self::SERVICE_ETRZISTE                        => 'Tržiště',
    ];

    const ORDER_STATE_RECEIVED = 'received';
    const ORDER_STATE_TO_SPECIFY = 'toSpecify';
    const ORDER_STATE_PROCESSED = 'processed';
    const ORDER_STATE_PAID = 'paid';
    const ORDER_STATE_CANCELLED = 'cancelled';
    const ORDER_STATE_BLOCKED = 'blocked';

    const ORDER_STATE_TITLES = [
        self::ORDER_STATE_RECEIVED   => 'Přijatá',
        self::ORDER_STATE_TO_SPECIFY => 'Ke specifikaci',
        self::ORDER_STATE_PROCESSED  => 'Vystavená',
        self::ORDER_STATE_PAID       => 'Zaplacená',
        self::ORDER_STATE_BLOCKED    => 'Zablokovaná',
        self::ORDER_STATE_CANCELLED  => 'Zrušená',
    ];

    const ORDER_STATE_CLASSES = [
        self::ORDER_STATE_RECEIVED   => 'default',
        self::ORDER_STATE_TO_SPECIFY => 'warning',
        self::ORDER_STATE_PROCESSED  => 'primary',
        self::ORDER_STATE_PAID       => 'success',
        self::ORDER_STATE_BLOCKED    => 'danger',
        self::ORDER_STATE_CANCELLED  => 'warning',
    ];
}

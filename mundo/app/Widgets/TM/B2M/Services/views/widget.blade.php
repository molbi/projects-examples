<div class="tiles tiles-framed tiles-default widget">
    <div class="tiles-title">
        Obchodní informace pro prodej
    </div>
    <div class="tiles-body tiles-body-table">

        <ul class="nav nav-tabs" style="margin-bottom: 0;">
            @foreach($services as $key => $service)
                <li @if($key == 0)class="active"@endif>
                    <a href="#{{ $service->getKey() }}"
                       data-toggle="tab">
                        {{ $widget->serviceName($service->getKey()) }}
                    </a>
                </li>
            @endforeach
        </ul>
        <div id="myTabContent" class="tab-content">
            @foreach($services  as $service)
                <div class="tab-pane fade active in" id="{{ $service->getKey() }}">
                    @partial($service->toViewName(), $service->getData())
                </div>
            @endforeach
        </div>

    </div>
</div>
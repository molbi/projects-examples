<table class="table table-bordered">
    @if(is_null($oap))
        <tbody>
        <tr>
            <td>
                <p>Firma nemá nastevenou OAP kategorii. Není možné zobrazit informace o službě.</p>
                <p><a href="http://logicall.b2m.cz/apis/acquisition?q={{ $record->getRemoteId() }}" title="Nastavení OAP kategorie" target="_blank">Nastavení OAP kategorie</a></p>
            </td>
        </tr>
        </tbody>
    @else
        <tbody>
        <tr>
            <th style="width: 250px; background-color: #fafafa;">OAP kategorie</th>
            <td>
                {{ (!empty($oap->title)) ? $oap->title : '-' }}
                (<a href="http://logicall.b2m.cz/apis/acquisition?q={{ $record->getRemoteId() }}"
                    target="_blank">editace</a>)
            </td>
            <th style="width: 250px; background-color: #fafafa;">Nabídky dodavatele</th>
            <td>
                {{ (!empty($oap->title)) ? $oap->title : '-' }}
                (<a href="http://{{ ($record->getRemoteLang() == 'cs') ? 'dodavatele.epoptavka.cz' : 'dodavatelia.123dopyt.sk' }}/~/user-backend/login/id/{{ $oap->ID_BCFirm }}/email/{{ $record->getRemote()->email }}/forward/offers"
                    target="_blank">seznam nabídek</a>)
            </td>
        </tr>
        <tr>
            <th style="background-color: #fafafa;">
                Počet potenciálních zájemců<br/>(1 E-mailing)
            </th>
            <td>
                {{ number_format($customerCounts, 0, ',', ' ') }}
            </td>
            <th style="background-color: #fafafa;">Noví zákazníci (Aktivní nabídka)</th>
            <td>
                {{ number_format($inquiryCounts, 0, ',', ' ') }}
            </td>
        </tr>
        <tr>
            <th style="background-color: #fafafa;">Doba spojitého trvání služby zasílání poptávek</th>
            <td>
                @if(!is_null($inquiryServiceActive))
                    <span>{{ time_since($inquiryServiceActive) }}</span>
                @else
                    <span style="color: red">Nemá službu zasílání poptávek</span>
                @endif
            </td>
            <th style="background-color: #fafafa;">Doporučená cena</th>
            <td>
                {{ $recPrice }} {{ ($record->getRemoteLang() == 'cs') ? ' Kč' : ' EUR' }}
            </td>
        </tr>
        <tr>
            <th style="background-color: #fafafa;"><abbr title="za celý rok">Počet potenciálních zájemců o
                    nabídku</abbr>
            </th>
            <td>
                {{ number_format($customerYearCounts, 0, ',', ' ')  }}
            </td>
            <th style="background-color: #fafafa;">Cena za oslovení zákazníka</th>
            <td>
                {{ $oapPricePerCustomer }} {{ ($record->getRemoteLang() == 'cs') ? ' Kč' : ' EUR' }}
            </td>
        </tr>
        <tr>
            <th style="background-color: #fafafa;">PDF nabídka (pro B2M)</th>
            <td colspan="3">
                <a target="_blank"
                   href="http://logicall.b2m.cz/fobis/pdf-campaign/index/withoutlink/1/firm_id/{{ $record->getRemoteId() }}/lang/{{ $record->getRemoteLang() }}"
                >
                    stáhnout
                </a>
            </td>
        </tr>
        </tbody>
    @endif

</table>
<?php

namespace Mundo\Widgets\TM\B2M\Services;

use Illuminate\Cache\CacheManager;
use Illuminate\Database\Eloquent\Model;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\TM\Record;
use Mundo\Modules\TM\Sources\Support\SourceManager;
use Mundo\Widgets\TM\B2M\Constants;

/**
 * Class Services
 * @package Mundo\Widgets\TM\B2M\Services
 */
class Services extends BaseWidget
{
    /** @var Record|RenewalFirm */
    protected $record;
    /** @var  SourceManager */
    protected $sources;
    /** @var array */
    private $services;
    /**
     * @var CacheManager
     */
    private $cache;

    /**
     * ActiveSelling constructor.
     *
     * @param Model         $record
     * @param SourceManager $sources
     * @param CacheManager  $cache
     * @param array         $services
     */
    public function __construct(Model $record, SourceManager $sources, CacheManager $cache, array $services = null)
    {
        $this->record = $record;
        $this->sources = $sources;
        $this->services = $services;
        $this->cache = $cache;
    }

    /**
     * @return string
     */
    public function render()
    {
        return $this->renderView(__DIR__ . '/views/widget.blade.php', [
            'services' => $this->services()
        ]);
    }

    /**
     * @return array
     */
    private function services()
    {
        return array_map(function ($service) {
            if ($klass = $this->get($service)) {
                return $klass;
            }

            return false;
        }, $this->services);
    }

    /**
     * @param $service
     *
     * @return bool
     */
    private function get($service)
    {
        $className = sprintf('%s\Types\%s', __NAMESPACE__, studly_case($service));
        if (class_exists($className)) {
            return app($className, [$this->record]);
        };

        return false;
    }

    /**
     * @param $service
     *
     * @return mixed
     */
    public function serviceName($service)
    {
        return Constants::SERVICE_TITLES[$service];
    }

}

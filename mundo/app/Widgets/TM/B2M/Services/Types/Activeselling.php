<?php

namespace Mundo\Widgets\TM\B2M\Services\Types;

use Carbon\Carbon;
use DB;
use Illuminate\Cache\CacheManager;
use Illuminate\Database\Connection;
use Illuminate\Database\Eloquent\Model;
use Mundo\Models\TM\Record;
use Mundo\Modules\TM\Sources\Support\SourceManager;

/**
 * Class Activeselling
 * @package Mundo\Widgets\TM\B2M\Services\Types
 */
class Activeselling
{
    /** @var Record|RenewalFirm */
    protected $record;
    /** @var  SourceManager */
    protected $sources;

    /**
     * @var array
     */
    private $price = [
        'cs' => 5990,
        'sk' => 219
    ];

    /** @var
     * \Illuminate\Database\Query\Builder
     */
    private $oapCategory;
    /**
     * @var ActivesellingCache
     */
    private $cache;
    /**
     * @var CacheManager
     */
    private $manager;

    /**
     * ActiveSelling constructor.
     *
     * @param Model              $record
     * @param SourceManager      $sources
     * @param CacheManager       $manager
     * @param ActivesellingCache $cache
     *
     */
    public function __construct(Model $record, SourceManager $sources, CacheManager $manager, ActivesellingCache $cache)
    {
        $this->record = $record;
        $this->sources = $sources;
        $this->manager = $manager;
        $this->cache = $cache;

        $this->oapCategory = $this->oapCategory();
    }

    /**
     * @return string
     */
    public function getData()
    {
        list($inquiryServiceActive, $recPrice) = $this->inquiryService();

        return [
            'record'               => $this->record,
            'inquiryCounts'        => $oapInquiryCounts = $this->cache->get($this->oapCategory, $this->record->getRemoteLang(), 'inquiries'),
            'customerCounts'       => $oapCustomerCounts = $this->cache->get($this->oapCategory, $this->record->getRemoteLang(), 'customers'),
            'customerYearCounts'   => $oapCustomerYearCounts = ($oapInquiryCounts + ($oapCustomerCounts * 4)),
            'oapPricePerCustomer'  => $oapCustomerYearCounts ? round($this->price() / $oapCustomerYearCounts, 2) : 0,
            'oap'                  => $this->oapCategory,
            'recPrice'             => $recPrice,
            'inquiryServiceActive' => $inquiryServiceActive
        ];
    }


    /**
     * @return \Illuminate\Database\Query\Builder|object
     */
    private function oapCategory()
    {
        return $this->manager->store('file')->remember(sprintf('widget_oap_%s', $this->record->getRemoteId()), 1440, function () {
            return $this->db()->table('business.Firm_OapCategory AS FOC')
                ->select([
                    'CL.title',
                    'FOC.oapCategory_id' => 'category_id',
                    'F.ID_BCFirm'
                ])
                ->join('business.CategoryLang AS CL', 'CL.category_id', '=', 'FOC.oapCategory_id')
                ->join('business.Firm AS F', 'F.firm_id', '=', 'FOC.firm_id')
                ->where('FOC.firm_id', $this->record->getRemoteId())
                ->first();
        });
    }

    /**
     *
     */
    private function activeServices()
    {
        $services = $this->db()->table('business.Firm_Service AS FS')
            ->select([
                'FS.firm_id', 'FS.service', 'FS.lang', 'FS.beginsOn', 'FS.endsOn', 'FS.order_id'
            ])
            ->where('FS.firm_id', $this->record->getRemoteId());

        return collect($this->db()->table('business.Firm_CreditService AS FCS')
            ->select([
                'FCS.firm_id',
                'FCS.service',
                'FCS.lang',
                'FCS.beginsOn',
                DB::raw('DATE(FCS.beginsOn + INTERVAL 60 MONTH) AS endsOn'),
                'FCS.order_id'
            ])
            ->where('FCS.firm_id', $this->record->getRemoteId())
            ->union($services)
            ->get());
    }

    /**
     * @return Connection
     */
    protected function db()
    {
        return $this->sources->get('b2m')->getConnection();
    }

    /**
     * @return mixed
     */
    private function price()
    {
        return $this->price[$this->recordLang()];
    }

    /**
     * @return mixed
     */
    private function recordLang()
    {
        return $this->record->getRemoteLang();
    }

    /**
     * @return array
     */
    private function inquiryService()
    {
        $price = $this->price();
        $active = null;

        if ($service = $this->activeServices()->whereIn('service', ['inquiries', 'inquiries-keywords'])->first()) {
            $active = abs(strtotime($service->beginsOn) - time());
            $date = Carbon::createFromTimestamp(strtotime($service->beginsOn));
            $date->addMonth(12);
        };

        return [$active, $price];
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return 'activeselling';
    }

    /**
     * @return string
     */
    public function toViewName()
    {
        return 'types/' . $this->getKey();
    }
}
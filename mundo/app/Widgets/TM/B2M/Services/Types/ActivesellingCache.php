<?php

namespace Mundo\Widgets\TM\B2M\Services\Types;

use Carbon\Carbon;
use DB;
use Illuminate\Cache\Repository;
use Illuminate\Console\Command;
use Illuminate\Console\OutputStyle;
use Illuminate\Database\Connection;
use Illuminate\Database\Query\JoinClause;
use Mundo\Kernel\Cron\Cronable;
use Mundo\Models\User;

/**
 * Class ActivesellingCache
 * @package Mundo\Widgets\TM\B2M\Services\Types
 */
class ActivesellingCache implements Cronable
{
    /**
     *
     */
    const KEY = 'widget.services.activeselling.oap';

    /** @var \Illuminate\Cache\Repository */
    protected $cache;

    /**
     * ActivesellingCache constructor.
     *
     * @param Repository $cache
     */
    public function __construct(Repository $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @param $oapCategory
     * @param $lang
     *
     * @param $type
     *
     * @return int
     */
    public function get($oapCategory, $lang, $type)
    {
        if($oapCategory === null) {
            return 0;
        }

        $data = $this->cache->get(self::KEY, []);
        if (!isset($data[$oapCategory->category_id][$lang][$type])) {
            return 0;
        }

        return $data[$oapCategory->category_id][$lang][$type];
    }

    /**
     * @param Connection|null $db
     */
    public function run(Connection $db = null)
    {
        $this->cache->forever(self::KEY, $this->getOapCategories($db));
    }

    /**
     * @param Connection $db
     *
     * @return array
     */
    private function getOapCategories(Connection $db)
    {
        $categories = $db->table('business.Category')->where('type', 'oap')->get();

        $newArray = [];
        foreach ($categories as $category) {
            echo "Výpočet dokončet pro " . $category->category_id . "\n";

            $newArray[$category->category_id] = [
                'cs' => [
                    'customers'  => $this->oapCustomersCount($db, $category->category_id, 'cs'),
                    'inquiries' => $this->oapInquiriesCount($db, $category->category_id, 'cs')
                ],
                'sk' => [
                    'customers'  => $this->oapCustomersCount($db, $category->category_id, 'sk'),
                    'inquiries' => $this->oapInquiriesCount($db, $category->category_id, 'sk')
                ]
            ];
        }

        return $newArray;
    }

    /**
     * @param Connection $db
     * @param            $oap
     * @param            $lang
     *
     * @return int
     */
    private function oapCustomersCount(Connection $db, $oap, $lang)
    {
        return $db->table('business.OapCategory_FirmCategory AS OA')
            ->select([
                DB::raw('COUNT(DISTINCT I.userAccount_id) AS count')
            ])
            ->join('business.Inquiry_Category AS IC', 'IC.category_id', '=', 'OA.firmCategory_id')
            ->join('business.Inquiry AS I', 'I.inquiry_id', '=', 'IC.inquiry_id')
            ->where('OA.oapCategory_id', $oap)
            ->where('I.lang', $lang)
            ->where('I.state', 'published')
            ->whereRaw('I.publishedOn > NOW() - INTERVAL 3 YEAR')
            ->first()
            ->count;
    }

    /**
     * @param Connection $db
     * @param            $oap
     * @param            $lang
     *
     * @return float|int
     */
    private function oapInquiriesCount(Connection $db, $oap, $lang)
    {
        $counts = $db->table('business.OapCategory_FirmCategory AS OA')
            ->select([
                DB::raw('COUNT(DISTINCT I.inquiry_id) AS count')
            ])
            ->join('business.Inquiry_Category AS IC', 'IC.category_id', '=', 'OA.firmCategory_id')
            ->join('business.Inquiry AS I', 'I.inquiry_id', '=', 'IC.inquiry_id')
            ->where('OA.oapCategory_id', $oap)
            ->where('I.lang', $lang)
            ->where('I.state', 'published')
            ->whereRaw('I.publishedOn > NOW() - INTERVAL 1 YEAR')
            ->first();

        return floor($counts->count + (($counts->count / 100) * 20));
    }
}

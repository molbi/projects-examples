@if(!empty($prepaid))
    <div class="tiles tiles-framed tiles-default widget widget-prepaid">
        <div class="tiles-title">
            Prepaid
        </div>
        <div class="tiles-body tiles-body-table">
            <table class="table table-bordered">
                <tr>
                    <th>Kategorie</th>
                    <th>Současná pozice</th>
                    <th>Minimální prepaid pozice</th>
                </tr>
                @foreach($prepaid as $row)
                    <tr>
                        <td>
                            {{ $row->title }}
                        </td>
                        <td>
                            {{ $row->actual_position }}
                        </td>
                        <td>
                            {{ $row->prepaid_position }}
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endif
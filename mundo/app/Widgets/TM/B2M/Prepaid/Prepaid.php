<?php

namespace Mundo\Widgets\TM\B2M\Prepaid;

use Illuminate\Database\Connection;
use Illuminate\Database\Query\JoinClause;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\TM\Record;
use Mundo\Modules\TM\Sources\Support\SourceManager;
use Mundo\Widgets\TM\B2M\Constants;

class Prepaid extends BaseWidget
{
    /** @var \Mundo\Models\TM\Record */
    protected $record;
    /** @var  SourceManager */
    protected $sources;

    public function __construct(Record $record, SourceManager $sources)
    {
        $this->record = $record;
        $this->sources = $sources;
    }

    public function render()
    {
        return $this->renderView(__DIR__ . '/views/widget.blade.php', [
            'prepaid' => $this->prepaid(),
        ]);
    }

    protected function prepaid()
    {
        $res = $this->db()->table('business.Firm')->select([
            'CategoryLang.category_id',
            'CategoryLang.lang',
            'CategoryLang.title'
        ])
        ->join('business.Firm_Category', 'Firm_Category.firm_id', '=', 'Firm.firm_id')
        ->join('business.CategoryLang', function(JoinClause $j) {
            $j->on('CategoryLang.category_id', '=', 'Firm_Category.ID_BCfirmRegion')
                ->on('CategoryLang.lang', '=','Firm.lang');
        })
        ->where('Firm.firm_id', $this->record->remote_id)
        ->get();

        $categories = [];

        foreach($res as $category) {
            $category->actual_position = 0;

            $resActual = $this->db()->table('business.Firm')
                ->select(['Firm.firm_id'])
                ->join('business.Firm_Category', 'Firm_Category.firm_id', '=', 'Firm.firm_id')
                ->where('Firm_Category.ID_BCFirmRegion', $category->category_id)
                ->whereNotNull('Firm.ipAddress')
                ->where('Firm.ipAddress', '!=', '')
                ->where('Firm.state', 'visible')
                ->where('Firm.lang', $category->lang)
                ->groupBy('Firm.firm_id')
                ->orderBy('Firm.firmCredit', 'DESC')
                ->orderBy('Firm.quality', 'DESC')
                ->orderBy('Firm.title')
                ->get();

            foreach($resActual as $firm) {
                $category->actual_position++;
                if($firm->firm_id == $this->record->remote_id) break;
            }

            $position = $this->db()->table('business.Firm')
                ->select()
                ->join('business.Firm_Category', 'Firm_Category.firm_id', '=', 'Firm.firm_id')
                ->join('business.Firm_Service', function(JoinClause $j) {
                    $j->on('Firm_Service.firm_id', '=', 'Firm.firm_id')
                        ->where('Firm_Service.service', '=','prepaid');
                })
                ->where('Firm_Category.ID_BCFirmRegion', $category->category_id)
                ->whereNotNull('Firm.ipAddress')
                ->where('Firm.ipAddress', '!=', '')
                ->where('Firm.state', 'visible')
                ->where('Firm.lang', $category->lang);
            $category->prepaid_position = $position->count();

            $categories[] = $category;
        }

        return $categories;
    }

    /**
     * @return Connection
     */
    protected function db()
    {
        return $this->sources->get('b2m')->getConnection();
    }
}
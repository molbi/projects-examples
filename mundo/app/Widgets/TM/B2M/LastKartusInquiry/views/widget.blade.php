@if($inquiry)
    <div class="tiles tiles-framed tiles-default widget widget-last-kartus-inquiry">
        <div class="tiles-title">
            Informace o leadu
        </div>
        <div class="tiles-body">
            <p>Naposledy zobrazená poptávka: <strong>{{ datetime($inquiry->created_at) }}</strong></p>
            <div class="preview">
                <h4>{{ $inquiry->title }}</h4>
                {!! $inquiry->body_html !!}
            </div>
        </div>
    </div>
@endif
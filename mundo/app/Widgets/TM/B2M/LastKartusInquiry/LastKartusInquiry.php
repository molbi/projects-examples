<?php

namespace Mundo\Widgets\TM\B2M\LastKartusInquiry;

use Illuminate\Database\Connection;
use Illuminate\Database\Query\JoinClause;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\TM\Record;
use Mundo\Modules\TM\Sources\Support\SourceManager;
use Mundo\Widgets\TM\B2M\Constants;

class LastKartusInquiry extends BaseWidget
{
    /** @var \Mundo\Models\TM\Record */
    protected $record;
    /** @var  SourceManager */
    protected $sources;

    public function __construct(Record $record, SourceManager $sources)
    {
        $this->record = $record;
        $this->sources = $sources;
    }

    public function render()
    {
        return $this->renderView(__DIR__ . '/views/widget.blade.php', [
            'inquiry' => $this->inquiry(),
        ]);
    }

    protected function inquiry()
    {
        return $this->db()->table('cs_contact.stat_logs')
            ->select([
                'stat_logs.created_at',
                'inquiries.title',
                'inquiries.body_html'
            ])
            ->join('cs_contact.stats', 'stats.id', '=', 'stat_logs.stat_id')
            ->join('cs_contact.inquiries', 'inquiries.id', '=', 'stats.subject_id')
            ->where('stat_logs.initiator_id', $this->record->remote_id)
            ->where('stat_logs.initiator_type', 'business_firm')
            ->where('stat_logs.event', 'lp_entry')
            ->orderBy('stat_logs.created_at', 'DESC')
            ->limit(1)
            ->first();
    }

    /**
     * @return Connection
     */
    protected function db()
    {
        return $this->sources->get('b2m')->getConnection();
    }
}

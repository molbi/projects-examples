<?php

namespace Mundo\Widgets\TM\B2M\Inquiries;

use Illuminate\Database\Connection;
use Illuminate\Database\Eloquent\Model;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\RIS\RenewalFirm;
use Mundo\Models\TM\Record;
use Mundo\Modules\TM\Sources\Support\SourceManager;

/**
 * Class Inquiries
 * @package Mundo\Widgets\TM\B2M\Inquiries
 */
class Inquiries extends BaseWidget
{
    /** @var Record|RenewalFirm */
    protected $record;
    /** @var  SourceManager */
    protected $sources;
    /** @var int */
    private $limit;

    /**
     * Inquiries constructor.
     *
     * @param Model $record
     * @param SourceManager $sources
     * @param int $limit
     */
    public function __construct(Model $record, SourceManager $sources, $limit = 1)
    {
        $this->record = $record;
        $this->sources = $sources;
        $this->limit = $limit;
    }

    /**
     * @return string
     */
    public function render()
    {
        return $this->renderView(__DIR__ . '/views/widget.blade.php', [
            'inquiries' => $this->inquiries(),
        ]);
    }


    /**
     * @return \Illuminate\Support\Collection
     */
    protected function inquiries()
    {
        switch ($this->record->campaign->source) {
            case 'kartus':
                return $this->getInquiriesByKartus();
                break;
            default:
                return $this->getInquiriesByUserAccount();
                break;
        }
    }

    /**
     * @return Connection
     */
    protected function db()
    {
        return $this->sources->get('b2m')->getConnection();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    private function getInquiriesByUserAccount()
    {
        return collect(
            $this->db()->table('business.Inquiry AS I')
                ->select([
                    'I.inquiry_id',
                    'I.createdOn',
                    'I.state',
                    'IL.title',
                    'IL.body',
                    'E.realname'
                ])
                ->join('business.InquiryLang AS IL', 'IL.inquiry_id', '=', 'I.inquiry_id')
                ->join('zis.Employee AS E', 'E.employee_id', '=', 'I.operator_id')
                ->where('I.userAccount_id', $this->record->getRemoteId())
                ->where('I.state', 'published')
                ->orderBy('I.createdOn', 'desc')
                ->limit($this->limit)
                ->get()
        );
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    private function getInquiriesByKartus()
    {
        return collect(
            $this->db()->table('cs_contact.inquiries AS I')
                ->select([
                    'I.id AS inquiry_id',
                    'I.created_at AS createdOn',
                    'I.state',
                    'I.title',
                    'I.body',
                    'E.realname'
                ])
                ->join('cs_contact.inquiry_employee AS IE', 'IE.inquiry_id', '=', 'I.id')
                ->join('zis.Employee AS E', 'E.employee_id', '=', 'IE.employee_id')
                ->where('I.demander_id', $this->record->getRemoteId())
                ->whereNotNull('I.sent_at')
                ->orderBy('I.created_at', 'desc')
                ->limit($this->limit)
                ->get()
        );
    }

}

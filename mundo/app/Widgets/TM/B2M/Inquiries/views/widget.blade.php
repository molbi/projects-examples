<div class="tiles tiles-framed tiles-default widget">
    <div class="tiles-title">
        Zadané poptávky
    </div>
    <div class="tiles-body tiles-body-table">
        <table class="table table-bordered">
            <tr>
                <th>Název</th>
                <th>Stav</th>
                <th></th>
            </tr>
            @foreach($inquiries as $inquiry)
                <tr>
                    <td>{{ $inquiry->title }}</td>
                    <td>{{ state_label($inquiry->state, true) }}</td>
                    <td style="width: 1px; white-space: nowrap; text-align: right;">
                        <small>{{ datetime($inquiry->createdOn) }} <i class="fa fa-fw fa-clock-o"></i></small>
                        <br>
                        <small> {{ $inquiry->realname }} <i class="fa fa-fw fa-user"></i></small>
                    </td>
                    <td class="table-rest">
                        <a data-toggle="modal" data-target="#inquiry-modal-{{ $inquiry->inquiry_id }}" class="action">
                            <i class="fa fa-search"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
</div>

@foreach ($inquiries as $inquiry)
    <div id="inquiry-modal-{{ $inquiry->inquiry_id }}" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Obsah poptávky</h4>
                </div>
                <div class="modal-body">
                    {!! nl2br($inquiry->body) !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Zavřít</button>
                </div>
            </div>
        </div>
    </div>
@endforeach
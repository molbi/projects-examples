<?php

namespace Mundo\Widgets\TM\B2M\FirmPaymentMorals;

use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\TM\Record;

class FirmPaymentMorals extends BaseWidget
{
    /** @var \Mundo\Models\TM\Record */
    protected $record;
    /**
     * @var
     */
    private $limit;
    private $type;


    public function __construct(Record $record, $limit = 0.3)
    {
        $this->record = $record;
        $this->limit = $limit;
        $this->type = $this->record->campaign->source;

//        dd($this->record->remoteData);
        }

    public function render()
    {
        return $this->renderView(__DIR__ . '/views/widget.blade.php', [
            'record'         => $this->record,
            'limit'          => $this->limit

        ]);
    }

}

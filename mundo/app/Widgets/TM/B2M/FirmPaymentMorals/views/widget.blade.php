@if(!is_null($record->remoteData->pm_info))
    @unless($record->remoteData->pm_info->all_orders == 1 && $record->remoteData->pm_info->cancelled == 1)
        <div class="tiles tiles-framed tiles-red widget">
            <div class="tiles-body text-center">
                <h3 style="margin: 0;" class="text-danger">
                <span style="color:#000;">
                    <strong>Riziko neuhrazení</strong>:
                    {{ $record->remoteData->pm_info->payment_morals*100 }}
                    % ({{ $record->remoteData->pm_info->cancelled }}/{{ $record->remoteData->pm_info->all_orders }})
                </span>
                    @if($record->remoteData->pm_info->payment_morals==1 && $record->remoteData->pm_info->cancelled >= 2)
                        - <strong>ZÁKAZ PRODEJE</strong>
                    @elseif($record->remoteData->pm_info->payment_morals >= 0.3 && $record->remoteData->pm_info->cancelled >= 2 )
                        - <span style="color:#23b9a9;">Vyžadován souhlas e-mailem!</span>
                    @endif
                </h3>
            </div>
        </div>
    @endunless
@endif
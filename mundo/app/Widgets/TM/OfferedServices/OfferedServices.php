<?php

namespace Mundo\Widgets\TM\OfferedServices;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\MessageBag;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\ServiceLog;
use Mundo\Models\TM\Record;
use Mundo\Widgets\TM\B2M\Constants;

/**
 * Class OfferedServices
 * @package Mundo\Widgets\TM\OfferedServices
 */
class OfferedServices extends BaseWidget
{

    /**
     * @var Record
     */
    protected $record;

    /**
     * OfferedServices constructor.
     *
     * @param Record $record
     */
    public function __construct(Record $record)
    {
        $this->record = $record;
    }

    /**
     * @return string
     */
    public function render()
    {
        return $this->renderView(__DIR__ . '/views/widget.blade.php', [
            'record'          => $this->record,
            'lastServiceLog'  => $this->record->lastServiceLog(),
            'logs'            => $this->allLogsForRecords(),
            'allowedServices' => $this->allowedServices()
        ]);
    }

    /**
     * @return Collection
     */
    public function allowedServices()
    {
        return collect(array_filter($this->record->campaign->getServices(), function ($group) {
            if (isset($group['excludeWhenHasService']) && $this->record->hasAnyServices($group['excludeWhenHasService'])) {
                return false;
            }

            return true;
        }))->pluck('offeredService', 'title');
    }

    /**
     * @param $service
     *
     * @return string
     */
    public static function serviceName($service)
    {
        if (is_array($services = explode('|', $service))) {
            return implode(', ', array_map(function ($service) {
                return Constants::SERVICE_TITLES[$service];
            }, $services));
        }

        return Constants::SERVICE_TITLES[$service];
    }

    /**
     * @param MessageBag $bag
     * @param Record     $record
     *
     * @param            $state
     *
     * @return MessageBag
     */
    public function validateState(MessageBag $bag, Record $record, $state)
    {
        if (is_null($record->lastServiceLog()) && $state != Record::STATE_REMOVED) {
            $bag->add('offered-services', 'offered-services.no-select');
        }

        return $bag;
    }

    private function allLogsForRecords()
    {
        return ServiceLog::select('service_logs.*')
            ->with('user')
            ->where('target_type', Record::class)
            ->whereIn('target_id', $this->otherRecordsByRemoteId())
            ->orderBy('created_at', 'desc')
            ->get();
    }

    private function otherRecordsByRemoteId()
    {
        return Record::where('campaign_id', $this->record->campaign_id)
            ->where('remote_id', $this->record->getRemoteId())
            ->where('id', '!=', $this->record->id)
            ->get()
            ->pluck('id');
    }
}

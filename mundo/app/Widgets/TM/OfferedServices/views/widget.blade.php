<div class="tiles tiles-framed tiles-purple widget">
    <div class="tiles-title">
        SLužby k prodeji
    </div>
    <div class="tiles-body">
        @if(is_null($lastServiceLog))
            <h5 class="text-danger">Nezvolili jste nabízenou službu!</h5>
        @else
            <p>Poslední nabízená služba: <strong>{{ $widget->serviceName($lastServiceLog->service) }}</strong>
                <small>({{ datetime($lastServiceLog->created_at) }})</small>
            </p>
        @endif
        <h3></h3>
        <hr/>
        <div class="btn-group">
            @foreach($allowedServices as $title => $service)
                <a href="{{ route('tm.records.offered-services.store', [$record, 'service' => $service]) }}"
                   data-confirm="Opravdu chcete zvolit tuto možnost?" data-remote="true" data-method="post"
                   class="btn btn-primary btn-sm" style="margin:2px">{{ $title }}</a>
            @endforeach
        </div>

        @if(!$logs->isEmpty())
            <hr/>
            <h5>Historie nabízených služeb</h5>
            <table class="table table-bordered">
                <tbody>
                @foreach($logs as $log)
                    <tr>
                        <td>{{ $widget->serviceName($log->service) }}</td>
                        <td style="width: 1px; white-space: nowrap; text-align: right;">
                            <small>{{ $log->created_at->format('j. n. Y H:i:s') }} <i class="fa fa-fw fa-clock-o"></i></small>
                            <small> {{ $log->user->name }} <i class="fa fa-fw fa-user"></i></small>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif


    </div>
</div>


<div class="modal fade" id="offered-services-modal" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['url' => route('tm.records.offered-services.store', [$record]), 'method' => 'post', 'data-remote']) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title"><i class="fa fa-info-circle"></i> Vybrat nabízenou službu</h4>
            </div>
            <div class="modal-body">
                <div class="form-mundo">
                    {!! Form::groupOpen('services', 'Nabízená služba') !!}
                    <div class="controls">
                        <select name="service" class="form-control">
                            <option value="">-- vyberte --</option>
                            @foreach($allowedServices as $title => $service)
                                <option value="{{ $service }}">{{ $title }}</option>
                            @endforeach
                        </select>
                    </div>
                    {!! Form::groupClose() !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-check"></i>&nbsp;Uložit nabízenou službu
                </button>

                <button type="button" class="btn btn-default" data-dismiss="modal">Zavřít</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
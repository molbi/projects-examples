<?php

namespace Mundo\Widgets\TM\Internal\OldRecordsInfo;

use Illuminate\Database\Connection;
use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Collection;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\TM\Record;
use Mundo\Modules\TM\Sources\Support\SourceManager;

/**
 * Class OldRecordsInfo
 * @package Mundo\Widgets\TM\Internal\OldRecordsInfo
 */
class OldRecordsInfo extends BaseWidget
{
    /** @var \Mundo\Models\TM\Record */
    private $record;
    /** @var  SourceManager */
    protected $sources;
    /** @var DatabaseManager */
    private $manager;

    /**
     * OldRecordsInfo constructor.
     *
     * @param Record          $record
     * @param SourceManager   $sources
     * @param DatabaseManager $manager
     */
    public function __construct(Record $record, SourceManager $sources, DatabaseManager $manager)
    {
        $this->record = $record;
        $this->sources = $sources;
        $this->manager = $manager;
    }

    /**
     * @return string
     */
    public function render()
    {
        return $this->renderView(__DIR__ . '/views/widget.blade.php', [
            'stacks' => $this->stacks(),
        ]);
    }

    /**
     * @return mixed
     */
    private function stacks()
    {
        $res = $this->manager->connection()
            ->table('mundo.tm_syncs')
            ->select(['tm_syncs.remote_id'])
            ->where('local_id', $this->record->id)->first();


        return with(new Collection($this->db()
            ->table('telemarketing.Stack as S')
            ->select(['H.*',])
            ->join('telemarketing.Campaign as C', 'C.campaign_id', '=', 'S.campaign_id')
            ->leftJoin('telemarketing.Stack as S1', function (JoinClause $clause) {
                $clause->on('S1.targetSubject_id', '=', 'S.targetSubject_id')
                    ->on('S1.stack_id', '!=', 'S.stack_id');
            })->leftJoin('telemarketing.Campaign as C1', 'C1.campaign_id', '=', 'S1.campaign_id')
            ->leftJoin('telemarketing.History as H', 'H.stack_id', '=', 'S1.stack_id')
            ->where('S.stack_id', $res->remote_id)
            #
            ->whereRaw('C.targetGroup_id = C1.targetGroup_id')
            ->get()))
            ->groupBy('stack_id');
    }


    /**
     * @return Connection
     */
    protected function db()
    {
        return $this->sources->get('b2m')->getConnection();
    }
}

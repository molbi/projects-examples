<div class="tiles tiles-framed tiles-default widget widget-action-history">
    <div class="tiles-title">
        Historie z B2M
    </div>
    <div class="tiles-body tiles-body-table" style="overflow-y: hidden">
        <table class="table table-bordered">
            @foreach($stacks as $stack => $histories)
                @unless($stack == '')
                    <tr>
                        <td>
                            <ul>
                            @foreach($histories as $history)
                                <li>
                                    {!! $history->note !!}
                                </li>
                            @endforeach
                            </ul>
                        </td>
                        <td style="width: 1px; white-space: nowrap; text-align: right;">
                            <small>{{ datetime($histories->first()->createdAt) }} <i class="fa fa-fw fa-clock-o"></i></small><br>
                            <small> #{{ $stack }}</small>
                        </td>
                    </tr>
                @endunless
            @endforeach
        </table>
    </div>
</div>
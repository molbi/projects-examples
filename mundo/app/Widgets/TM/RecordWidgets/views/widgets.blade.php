@foreach($widgets as $name => $def)
    @if ($def['type'] === 'split')
    <div class="split split-height">
        <div class="split-half">
            @partial('widget', ['name' => "${name}_widgets_0"])
        </div>
        <div class="split-half">
            @partial('widget', ['name' => "${name}_widgets_1"])
        </div>
    </div>
    @else
        @partial('widget', ['name' => $name])
    @endif
@endforeach

<?php

namespace Mundo\Widgets\TM\RecordWidgets;

use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\TM\Record;

class RecordWidgets extends BaseWidget
{

    /** @var Record */
    protected $record;

    public function __construct(Record $record)
    {
        $this->record = $record;
    }

    public function render()
    {
        $widgets = $this->getRecordWidgets();

        return $this->renderView(__DIR__ . '/views/widgets.blade.php', compact('widgets'));
    }

    protected function createWidget($name)
    {
        $def = (object)array_get($this->getRecordWidgets(), str_replace('_', '.', $name));
        
        return $this->getRoot()->getApplication()->make(
            $def->type,
            array_merge((array)$def->params, ['record' => $this->record])
        );
    }

    /**
     * @return array();
     */
    protected function getRecordWidgets()
    {
        return $this->record->campaign->getWidgets();
    }
}

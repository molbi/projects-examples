<table class="table table-bordered">
    <tr>
        <th style="width: 150px; background-color: #fafafa;">Kontaktní osoba</th>
        <td style="width: 350px;">{{ $contact->contact_person }} (<a target="_blank"
                                                                     href="http://{{ $record->remoteData->lang == 'cs' ? 'hledat.cz/editace-udaju' : '123dodavatel.sk/editacia-udajov' }}/{{ md5($record->remote_id) }}">editace</a>)
        </td>
        <th style="width: 150px; background-color: #fafafa;">Telefon</th>
        <td>
            {{ phone_number($contact->phone_number, lang_to_country($contact->lang)) }}
            @foreach($record->additionalPhoneNumbers() as $additional)
                <br> {{ phone_number($additional->phone_number, lang_to_country($contact->lang)) }}
                @if ($additional->note)
                    <small>&mdash; {{ $additional->note }}</small>
                @else
                    <small>&mdash; ručně přidané číslo</small>
                @endif
                <a href="@action('TM\AdditionalPhoneNumbersController@destroy', [$record, $additional])"
                   data-remote
                   data-method="delete"
                   data-confirm="Opravdu?|Opravdu chcete telefon smazat?">
                    <i class="fa fa-trash"></i>
                </a>
            @endforeach
        </td>
    </tr>
    <tr>
        <th style="background-color: #fafafa;">E-mail</th>
        <td><a href="mailto:{{ $contact->email }}">{{ $contact->email }}</a></td>
        <th style="background-color: #fafafa;">Adresa</th>
        <td>
            {{ $contact->street }},
            <a target="_blank"
               href="http://www.psc.cz/?search={{ $contact->postal_code }}&send=Odeslat&do=searchForm-submit">{{ $contact->postal_code }}</a> {{ $contact->city }}
            , {{$contact->region}}
        </td>
    </tr>
    <tr>
        <th style="background-color: #fafafa;">IČ</th>
        <td>
            @if($contact->lang === 'cs')
                <a target="_blank"
                   href="http://wwwinfo.mfcr.cz/cgi-bin/ares/ares_es.cgi?jazyk=cz&obch_jm=&ico={{$contact->id_number}}&cestina=cestina&obec=&k_fu=&maxpoc=200&ulice=&cis_or=&cis_po=&setrid=ZADNE&pr_for=&nace=&xml=1&filtr=1">{{$contact->id_number}}</a>
            @else
                {{$contact->id_number}}
            @endif
        </td>
        <th style="background-color: #fafafa;">DIČ</th>
        <td>
            {{$contact->vat_number}}
        </td>
    </tr>

    @unless($contact->web == null)
        <tr>
            <th style="background-color: #fafafa;">Web</th>
            <td colspan="3">
                <a href="{{ company_url($contact->web) }}" target="_blank">{{$contact->web}}</a>
            </td>
        </tr>
    @endunless

    <tr>
        <th style="background-color: #fafafa;">Popis činnosti</th>
        <td colspan="3">
            {{$contact->description}}
        </td>
    </tr>
</table>
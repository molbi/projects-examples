<div class="tiles tiles-framed tiles-blue widget widget-b2m-contact">
    <div class="tiles-title">
        Kontaktní údaje
    </div>
    <div class="tiles-body tiles-body-table">
        @partial('tm-content', ['contact' => $contact])
    </div>
</div>
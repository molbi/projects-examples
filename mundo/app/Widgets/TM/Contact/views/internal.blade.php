<div class="tiles tiles-framed tiles-blue widget widget-b2m-contact">
    <div class="tiles-title">
        Kontaktní údaje
    </div>
    <div class="tiles-body tiles-body-table">
        <table class="table table-bordered">
            <tr>
                <th style="width: 150px; background-color: #fafafa;">Kontaktní osoba</th>
                <td>{{ $contact->contact_person }}</td>
                <th style="width: 150px; background-color: #fafafa;">Telefon</th>
                <td>
                    {{ phone_number($contact->phone, lang_to_country($contact->lang)) }}
                    @foreach($record->additionalPhoneNumbers() as $additional)
                        <br> {{ phone_number($additional->phone_number, lang_to_country($contact->lang)) }}
                        @if ($additional->note)
                            <small>&mdash; {{ $additional->note }}</small>
                        @else
                            <small>&mdash; ručně přidané číslo</small>
                        @endif
                        <a href="@action('TM\AdditionalPhoneNumbersController@destroy', [$record, $additional])"
                           data-remote
                           data-method="delete"
                           data-confirm="Opravdu?|Opravdu chcete telefon smazat?">
                            <i class="fa fa-trash"></i>
                        </a>
                    @endforeach
                </td>
            </tr>
            @if(isset($contact->note))
                <tr>
                    <th style="background-color: #fafafa;">Poznámka</th>
                    <td colspan="3">{!! nl2br($contact->note) !!}</td>
                </tr>
            @endif

            @if(isset($contact->user_id))
                <tr>
                    <th style="background-color: #fafafa;">Zadal operátor</th>
                    <td colspan="3">{!! operator($contact->user_id)->name !!}</td>
                </tr>
            @endif
        </table>
    </div>
</div>

@if(isset($contact->firm_id) && $b2mContact = $widget->b2mContact($contact->firm_id))
    <div class="tiles tiles-framed tiles-blue widget widget-b2m-contact">
        <div class="tiles-title">
            Přiřazená B2M firma
        </div>
        <div class="tiles-body tiles-body-table">
            @partial('b2m-content', ['contact' => $b2mContact])
        </div>
    </div>
@endif
<div class="tiles tiles-framed tiles-blue widget widget-b2m-contact">
    <div class="tiles-title">
        Kontaktní údaje
    </div>
    <div class="tiles-body tiles-body-table">
        <table class="table table-bordered">
            <tr>
                <th style="width: 150px; background-color: #fafafa;">Kontaktní osoba</th>
                <td>{{ $contact->contact_person }}</td>
                <th style="width: 150px; background-color: #fafafa;">Telefon</th>
                <td>
                    {{ phone_number($contact->phone_number, lang_to_country($contact->lang)) }}
                    @foreach($record->additionalPhoneNumbers() as $additional)
                        <br> {{ phone_number($additional->phone_number, lang_to_country($contact->lang)) }}
                        @if ($additional->note)
                            <small>&mdash; {{ $additional->note }}</small>
                        @else
                            <small>&mdash; ručně přidané číslo</small>
                        @endif
                        <a href="@action('TM\AdditionalPhoneNumbersController@destroy', [$record, $additional])"
                           data-remote
                           data-method="delete"
                           data-confirm="Opravdu?|Opravdu chcete telefon smazat?">
                            <i class="fa fa-trash"></i>
                        </a>
                    @endforeach
                </td>
            </tr>
            <tr>
                <th style="background-color: #fafafa;">E-mail</th>
                <td><a href="mailto:{{ $contact->email }}">{{ $contact->email }}</a></td>
                <th style="background-color: #fafafa;">Adresa</th>
                <td>
                    {{ $contact->street }},
                    <a target="_blank"
                       href="http://www.psc.cz/?search={{ $contact->postal_code }}&send=Odeslat&do=searchForm-submit">{{ $contact->postal_code }}</a> {{ $contact->city }}
                </td>
            </tr>
            <tr>
                <th style="background-color: #fafafa;">IČ</th>
                <td>
                    @if($contact->lang === 'cs')
                        <a target="_blank"
                           href="http://wwwinfo.mfcr.cz/cgi-bin/ares/ares_es.cgi?jazyk=cz&obch_jm=&ico={{$contact->id_number}}&cestina=cestina&obec=&k_fu=&maxpoc=200&ulice=&cis_or=&cis_po=&setrid=ZADNE&pr_for=&nace=&xml=1&filtr=1">{{$contact->id_number}}</a>
                    @else
                        {{$contact->id_number}}
                    @endif
                </td>
                <th style="background-color: #fafafa;">DIČ</th>
                <td>
                    {{$contact->vat_number}}
                </td>
            </tr>

            @if(isset($contact->web) && $contact->web != NULL)
                <tr>
                    <th style="background-color: #fafafa;">Web</th>
                    <td colspan="3">
                        <a href="{{ company_url($contact->web) }}" target="_blank">{{$contact->web}}</a>
                    </td>
                </tr>
            @endif
        </table>
    </div>
</div>
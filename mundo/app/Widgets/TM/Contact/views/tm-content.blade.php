<table class="table table-bordered">
    <tr>
        <th style="width: 150px; background-color: #fafafa;">Kontaktní osoba</th>
        <td style="width: 350px;">{{ $contact->contact_person }}
        <th style="width: 150px; background-color: #fafafa;">Telefon</th>
        </td>
        <td>
            {{ phone_number($contact->phone_number, lang_to_country($contact->lang)) }}
            @foreach($record->additionalPhoneNumbers() as $additional)
                <br> {{ phone_number($additional->phone_number, lang_to_country($contact->lang)) }}
                @if ($additional->note)
                    <small>&mdash; {{ $additional->note }}</small>
                @else
                    <small>&mdash; ručně přidané číslo</small>
                @endif
                <a href="@action('TM\AdditionalPhoneNumbersController@destroy', [$record, $additional])"
                   data-remote
                   data-method="delete"
                   data-confirm="Opravdu?|Opravdu chcete telefon smazat?">
                    <i class="fa fa-trash"></i>
                </a>
            @endforeach
        </td>
    </tr>
    <tr>
        <th style="background-color: #fafafa;">E-mail</th>
        <td><a href="mailto:{{ $contact->email }}">{{ $contact->email }}</a></td>
        <th style="background-color: #fafafa;">Adresa</th>
        <td>
            {{ $contact->street }},
            <a target="_blank"
               href="http://www.psc.cz/?search={{ $contact->postal_code }}&send=Odeslat&do=searchForm-submit">{{ $contact->postal_code }}</a> {{ $contact->city }}
        </td>
    </tr>
    <tr>
        <th style="background-color: #fafafa;">Popis činnosti</th>
        <td colspan="3">
            {{$contact->description}}
        </td>
    </tr>
</table>
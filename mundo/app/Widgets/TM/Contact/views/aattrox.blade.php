<div class="tiles tiles-framed tiles-blue widget widget-b2m-contact">
    <div class="tiles-title">
        Kontaktní údaje
    </div>
    <div class="tiles-body tiles-body-table">
        <table class="table table-bordered">
            <tr>
                <th style="width: 150px; background-color: #fafafa;">Kontaktní osoba</th>
                <td style="width: 350px;">{{ $contact->contact_person }}</td>
                <th style="width: 150px; background-color: #fafafa;">Telefon</th>
                <td>
                    {{ phone_number($contact->phone_number, lang_to_country($contact->lang)) }}
                    @foreach($record->additionalPhoneNumbers() as $additional)
                        <br> {{ phone_number($additional->phone_number, lang_to_country($contact->lang)) }}
                        @if ($additional->note)
                            <small>&mdash; {{ $additional->note }}</small>
                        @else
                            <small>&mdash; ručně přidané číslo</small>
                        @endif
                        <a href="@action('TM\AdditionalPhoneNumbersController@destroy', [$record, $additional])"
                           data-remote
                           data-method="delete"
                           data-confirm="Opravdu?|Opravdu chcete telefon smazat?">
                            <i class="fa fa-trash"></i>
                        </a>
                    @endforeach
                </td>
            </tr>
            <tr>
                <th style="background-color: #fafafa;">E-mail</th>
                <td colspan="3"><a href="mailto:{{ $contact->email }}">{{ $contact->email }}</a></td>
            </tr>

        </table>
    </div>
</div>
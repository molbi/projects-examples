<?php

namespace Mundo\Widgets\TM\Contact;

use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\TM\Record;
use Mundo\Modules\TM\Sources\Support\SourceManager;

class Contact extends BaseWidget
{
    /** @var \Mundo\Models\TM\Record */
    protected $record;
    /** @var  SourceManager */
    protected $sources;

    public function __construct(Record $record, SourceManager $sources)
    {
        $this->record = $record;
        $this->sources = $sources;
    }

    public function render()
    {
        return $this->renderView(__DIR__ . '/views/widget.blade.php', [
            'contact' => $this->contact(),
            'record'  => $this->record,
            'view'    => $this->record->campaign->source,
        ]);
    }

    protected function contact()
    {
        $methodName = 'contactFrom' . ucfirst($this->record->campaign->source);
        if (method_exists($this, $methodName)) {
            call_user_func([$this, $methodName]);
        } else {
            return $this->defaultContact();
        }
    }

    protected function defaultContact()
    {
        return $this->record->remoteData;
    }

    protected function db()
    {
        return $this->sources->get($this->record->campaign->source)->getConnection();
    }

    public function b2mContact($firmId)
    {
        $data = $this->sources->get('b2m')->getData([['remote_id' => $firmId]]);
        $data = isset($data[$firmId]) ? $data[$firmId] : null;
        if ($data !== null) {
            return (object)$data;
        }

        return null;
    }
}

<?php

namespace Mundo\Widgets\TM\KartusInfo;

use Illuminate\Database\Connection;
use Illuminate\Database\Query\JoinClause;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\TM\Record;
use Mundo\Modules\TM\Sources\Support\SourceManager;

class KartusInfo extends BaseWidget
{
    const TRANSACTION_NAMES = [
        'auto-trading' => 'Automatický obchodník',
        'free'         => 'Časový zámek',
        'free-offer'   => 'Nabídka z LP',
        'paid'         => 'Mikrotransakce',
        'credit'       => 'Kredit',
    ];
    /** @var \Mundo\Models\TM\Record */
    protected $record;
    /** @var  SourceManager */
    protected $sources;

    public function __construct(Record $record, SourceManager $sources)
    {
        $this->record = $record;
        $this->sources = $sources;
    }

    public function render()
    {
        return $this->renderView(__DIR__ . '/views/widget.blade.php', [
            'record' => $this->record,
            'info'   => $this->info(),
        ]);
    }

    protected function info()
    {
        return (object)array_merge($this->baseInfo(), [
            'regions'          => $this->regions(),
            'categories'       => $this->categories(),
            'transactions'     => $transactions = $this->transactions(),
            'transactionStats' => $this->transactionStats($transactions),
            'opened'           => $this->opened(),
        ]);
    }

    protected function baseInfo()
    {
        return (array)$this->db()->query()
            ->select([
                'created_at as registered_at',
            ])
            ->from('cs_contact.users')
            ->where('id', '=', $this->record->remote_id)
            ->first();
    }

    protected function regions()
    {
        return $this->db()->query()
            ->select([
                'regions.title',
                'regions.id',
            ])
            ->from('cs_contact.regions')
            ->join('cs_contact.region_user', function (JoinClause $join) {
                $join->on('region_user.region_id', '=', 'regions.id')
                    ->where('region_user.user_id', '=', $this->record->remote_id);
            })
            ->where('regions.locale', '=', 'cs_CZ')
            ->orderBy('title')
            ->lists('title', 'id');
    }

    protected function categories()
    {
        return $this->db()->query()
            ->select([
                'category_langs.title',
                'category_langs.category_id as id',
            ])
            ->from('cs_contact.category_langs')
            ->join('cs_contact.category_user', function (JoinClause $join) {
                $join->on('category_user.category_id', '=', 'category_langs.category_id')
                    ->where('category_user.user_id', '=', $this->record->remote_id);
            })
            ->where('category_langs.locale', '=', 'cs_CZ')
            ->orderBy('title')
            ->lists('title', 'id');
    }

    protected function transactions()
    {
        return $this->db()->query()
            ->select([
                'inquiries.title',
                'inquiry_user.created_at',
                'inquiry_user.type',
                'inquiry_user.price',
            ])
            ->from('cs_contact.inquiry_user')
            ->join('cs_contact.inquiries', 'inquiries.id', '=', 'inquiry_user.inquiry_id')
            ->where('inquiry_user.user_id', '=', $this->record->remote_id)
            ->orderBy('inquiry_user.created_at', 'desc')
            ->get();
    }

    protected function transactionStats(array $transactions)
    {
        $stats = array_fill_keys(array_keys(self::TRANSACTION_NAMES), 0);

        return array_reduce($transactions, function ($prod, $item) {
            $prod[$item->type]++;

            return $prod;
        }, $stats);
    }

    protected function opened()
    {
        return $this->db()->query()
            ->select([
                'inquiries.id',
                'inquiries.title',
                'stat_logs.created_at',
            ])
            ->from('cs_contact.stat_logs')
            ->join('cs_contact.stats', 'stats.id', '=', 'stat_logs.stat_id')
            ->join('cs_contact.inquiries', 'stats.subject_id', '=', 'inquiries.id')
            ->where('event', '=', 'opened')
            ->where('stat_logs.initiator_type', '=', 'Kartus\\Model\\User')
            ->where('stat_logs.initiator_id', '=', $this->record->remote_id)
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    /**
     * @return Connection
     */
    protected function db()
    {
        return $this->sources->get('kartus')->getConnection();
    }
}

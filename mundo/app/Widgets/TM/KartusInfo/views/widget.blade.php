<div class="tiles tiles-framed tiles-default widget widget-soraka-info">
    <div class="tiles-title">
        Informace z CSkontaktu

        <div class="btn-group">
            <a href="http://admin.b2m.cz/cs-contact/detail/index/id/{{ $record->remote_id }}"
               target="_blank"
               class="btn btn-default btn-xs">Karta</a>
        </div>
    </div>
    <div class="tiles-body">
        <table class="table table-bordered">
            <tr>
                <th>Registrace</th>
                <td>@datetime($info->registered_at)</td>
            </tr>
            <tr>
                <th>Regiony</th>
                <td>
                    {{ implode(', ', $info->regions) }}
                </td>
            </tr>
            <tr>
                <th>Kategorie</th>
                <td>
                    {{ implode(', ', $info->categories) }}
                </td>
            </tr>
            <tr>
                <th>Transakce</th>
                <td>
                    <table class="table table-bordered">
                        <tr>
                            @foreach($info->transactionStats as $name => $value)
                                <th class="text-right">{{ $widget::TRANSACTION_NAMES[$name] }}</th>
                            @endforeach
                        </tr>
                        <tr>
                            @foreach($info->transactionStats as $name => $value)
                                <td class="text-right">{{ $value }}</td>
                            @endforeach
                        </tr>
                    </table>

                    <hr>

                    <table class="table table-bordered">
                        <tr>
                            <th colspan="4">Posledních 5 popt. transakcí</th>
                        </tr>
                        {? $count = 0 ?}
                        @foreach($info->transactions as $transaction)
                            @if($transaction->type !== 'auto-trading')
                                {? if($count++ >= 5) { break; } ?}
                                <tr>
                                    <td>@datetime($transaction->created_at)</td>
                                    <td>{{ $transaction->title }}</td>
                                    <td>{{ $widget::TRANSACTION_NAMES[$transaction->type] }}</td>
                                    <td>{{ $transaction->price }}</td>
                                </tr>
                            @endif
                        @endforeach
                    </table>

                </td>

            </tr>

        </table>

        <hr>

        <table class="table table-bordered">
            <tr>
                <th colspan="3">Zobrazené poptávky dodavatelem</th>
            </tr>
            <tr>
                <th>ID</th>
                <th>Název</th>
                <th>Datum</th>
            </tr>

            @foreach($info->opened as $opened)
                <tr>
                    <td><code>{{ $opened->id }}</code></td>
                    <td>{{ $opened->title }}</td>
                    <td>@datetime($opened->created_at)</td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
<?php

namespace Mundo\Widgets\Targeting;

use Carbon\Carbon;
use Illuminate\Database\Connection;
use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Query\JoinClause;
use InvalidArgumentException;
use Mundo\Kernel\Constants;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\User;

class Targeting extends BaseWidget
{
    /** @var Cache */
    protected $cache;
    /** @var  User */
    protected $user;
    /** @var  array */
    protected $orders;
    /** @var array */
    protected $_holidays = [
        '1-1',
        '6-4',
        '1-5',
        '8-5',
        '5-7',
        '6-7',
        '28-9',
        '28-10',
        '17-11',
        '24-12',
        '25-12',
        '26-12',
    ];
    /** @var array */
    protected $_weekends = [
        'Saturday',
        'Sunday',
    ];

    public function __construct(User $user, Cache $cache)
    {
        $this->cache = $cache;
        $this->user = $user;
    }

    public function render()
    {
        return $this->renderView(__DIR__ . '/views/panel.blade.php', [
            'today' => $this->getToday(),
            'month' => $this->getMonth(),
        ]);
    }

    public function getMonth()
    {
        return (object)[
            'sum'     => $sum = array_sum($this->getSums()),
            'percent' => $sum / $this->getLimit(),
            'target'  => $this->getLimit(),
        ];
    }

    protected function getToday()
    {
        $todayString = Carbon::now()->toDateString();

        return (object)[
            'sum'     => $sum = isset($this->getSums()[$todayString]) ? $this->getSums()[$todayString] : 0,
            'percent' => $sum / $target = ($this->getLimit() - array_sum($this->getSums())) / $this->monthDays('remainingWeekdays'),
            'target'  => $target,
        ];
    }

    protected function getLimit()
    {
        if ($limit = $this->user->param('tm_mounth_limit') <= 0) {
            throw new InvalidArgumentException("User has not right limit!");
        }

        return $this->user->param('tm_mounth_limit');
    }

    protected function getSums()
    {
        return array_map(function ($orders) {
            return array_sum(array_map([$this, 'convert'], $orders));
        }, $this->cache->get($this->user));
    }

    protected function convert($item)
    {
        return $item->price * Constants::RATES[$item->lang];
    }

    protected function monthDays($type = null)
    {
        $days = $workDays = 0;

        for ($d = 1; $d <= date('t', strtotime('-1 day')); $d++) {
            if (!in_array(date('l', strtotime(date('Y-m', strtotime('-1 day')) . '-' . $d)),
                    $this->_weekends) && !in_array($d . '-' . date('n', strtotime('-1 day')), $this->_holidays)
            ) {
                $days += 1;
            }


            if ($d == date('j', strtotime('-1 day'))) {
                $workDays = $days;
            }
        }

        $remainingWeekdays = (($days == $workDays) ? $days : $days - $workDays);

        if (is_null($type)) {
            return [
                'days'               => $days,
                'work_days'          => $workDays,
                'remaining_weekdays' => $remainingWeekdays,
            ];
        }

        return $$type;
    }
}

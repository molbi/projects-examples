<?php

namespace Mundo\Widgets\Targeting;

use Carbon\Carbon;
use Illuminate\Cache\Repository;
use Illuminate\Database\Connection;
use Illuminate\Database\Query\JoinClause;
use Mundo\Kernel\Cron\Cronable;
use Mundo\Models\User;

class Cache implements Cronable
{
    const KEY = 'widget:targeting';

    /** @var \Illuminate\Cache\Repository */
    protected $cache;

    public function __construct(Repository $cache)
    {
        $this->cache = $cache;
    }

    public function get(User $user)
    {
        $data = $this->cache->get(self::KEY, []);
        if (!isset($data[$user->id])) {
            return [];
        }

        return $data[$user->id];
    }

    public function run(Connection $db = null)
    {
        $this->cache->forever(self::KEY, $this->getOrders($db));
    }

    protected function getBusinessOrdersQuery(Connection $db)
    {
        return $db->query()
            ->select([
                'Order.employee_id',
                $db->raw('DATE(Order.createdOn) as date'),
                'Order.lang',
                'Invoice.price',
            ])
            ->from('obis.Order')
            ->join('obis.Invoice', function (JoinClause $join) {
                $join->on('Invoice.order_id', '=', 'Order.order_id')
                    ->where('Invoice.price', '>', 0);
            })
            ->whereIn('Order.employee_id', User::all()->pluck('id'))
            ->whereBetween('Order.createdOn', $this->getIntervalDateStrings())
            ->groupBy('Order.order_id');
    }

    protected function getInterval()
    {
        return [
            $first = Carbon::now()->startOfMonth(),
            with(clone $first)->endOfMonth(),
        ];
    }

    protected function getIntervalDateStrings()
    {
        return array_map(function (Carbon $date) {
            return $date->toDateString();
        }, $this->getInterval());
    }

    protected function getContactOrdersQuery(Connection $db)
    {
        return $db->query()
            ->select([
                'invoices.employee_id',
                $db->raw('DATE(invoices.created_at) as date'),
                $db->raw('IF (invoices.currency = "CZK", "cs", "sk") as lang'),
                'invoices.price',
            ])
            ->from('cs_contact.invoices')
            ->whereIn('invoices.employee_id', User::all()->pluck('id'))
            ->whereBetween($db->raw('DATE(invoices.created_at)'), $this->getIntervalDateStrings())
            ->where('invoices.price', '>', 0);
    }

    protected function getOrders(Connection $db)
    {
        return array_assoc(
            $this->getBusinessOrdersQuery($db)->unionAll($this->getContactOrdersQuery($db))->get(),
            'employee_id|date[]'
        );
    }
}

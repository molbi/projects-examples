<div class="split">
    <div class="split-half">
        <div class="tiles tiles-blue">
            <div class="tiles-body">
                <div class="tiles-title">Denní plnění</div>
                <div class="heading small">{{ number_format($today->sum, 0, ',', ' ') }}</div>
                <div class="progress transparent progress-small no-radius">
                    <div class="progress-bar progress-bar-white animate-progress-bar"
                         style="width: {{ min($today->percent * 100, 100) }}%;"></div>
                </div>
                <div class="description">
                    váš limit <strong>{{ number_format(floor($today->target), 0, ',', ' ') }} Kč</strong>.
                </div>
            </div>
        </div>
    </div>
    <div class="split-half">
        <div class="tiles tiles-default">
            <div class="tiles-body">
                <div class="tiles-title">Měsíční plnění</div>
                <div class="heading small">{{ number_format($month->sum, 0, ',', ' ') }} Kč</div>
                <div class="progress transparent progress-small no-radius">
                    <div class="progress-bar progress-bar-info animate-progress-bar"
                         style="width: {{ min($month->percent * 100, 100) }}%;"></div>
                </div>
                <div class="description">
                    váš limit <strong>{{ number_format($month->target, 0, ',', ' ') }} Kč</strong>.
                </div>
            </div>
        </div>
    </div>
</div>
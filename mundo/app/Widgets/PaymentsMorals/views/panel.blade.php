<div class="tiles tiles-purple">
    <div class="tiles-body">
        @forelse($payment_morals as $moral)
            <div class="tiles-title"><abbr title="Platební morálka">PM</abbr> {{ $moral->month }}/{{ $moral->year }}</div>
            <div class="heading small">{{ number_format(($moral->paid/$moral->total)*100, 2, ',', ' ') }} %</div>
        @empty
            <div class="description">
                <div class="tiles-title"><abbr title="Platební morálka">PM</abbr></div>
                Nemáte žádnou objednávku v posledních 2 měsících
            </div>
        @endforelse
    </div>
</div>
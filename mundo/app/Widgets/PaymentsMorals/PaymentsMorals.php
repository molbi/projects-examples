<?php

namespace Mundo\Widgets\PaymentsMorals;

use Mundo\Kernel\Cache\Cache;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\User;

class PaymentsMorals extends BaseWidget
{
    /** @var  User */
    protected $user;
    /** @var Cache */
    private $cache;

    /**
     * CommissionsPerformance constructor.
     *
     * @param User  $user
     * @param Cache $cache
     */
    public function __construct(User $user, Cache $cache)
    {
        $this->user = $user;
        $this->cache = $cache;
    }

    /**
     * @return string
     */
    public function render()
    {
        return $this->renderView(__DIR__ . '/views/panel.blade.php', [
            'payment_morals' => $this->getPaymentMorals(),
        ]);
    }

    /**
     * @return array
     */
    protected function getPaymentMorals()
    {
        return $this->cache->getForUser($this->user, Cache::PAYMENTS_MORALS);
    }

}
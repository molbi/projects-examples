<?php

namespace Mundo\Widgets\Splitter;

use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\User;

/**
 * Class Splitter
 * @package Mundo\Widgets\Splitter
 */
class Splitter extends BaseWidget
{

    /** @var  User */
    protected $user;

    /** @var array */
    private $innerWidgets;

    /**
     * Splitter constructor.
     *
     * @param User  $user
     * @param array $widgets
     */
    public function __construct(User $user, array $widgets)
    {
        $this->user = $user;
        $this->innerWidgets = $widgets;
    }

    /**
     * @return string
     */
    public function render()
    {
        $widgets = $this->innerWidgets;

        return $this->renderView(__DIR__ . '/views/splitter.blade.php', compact('widgets'));
    }

    /**
     * @param string $name
     *
     * @return mixed
     */
    protected function createWidget($name)
    {
        $def = array_get($this->innerWidgets, $name);

        return $this->getRoot()->getApplication()->make(
            $def->type,
            array_merge((array)$def->params, ['user' => $this->user])
        );
    }
}

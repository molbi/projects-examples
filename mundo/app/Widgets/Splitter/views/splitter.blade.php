@unless(empty(array_flatten($widgets)))
    <div class="split">
        @foreach($widgets as $index => $values)
            <div class="split-half">
                <?php
                try {
                    echo $widget->getWidget($index)->render();
                } catch (Exception $e) {
                    echo "<div class=\"alert alert-danger\"><strong>{$e->getMessage()}</strong><pre>{$e->getTraceAsString()}</pre></div>";
                }
                ?>
            </div>
        @endforeach
    </div>
@endunless
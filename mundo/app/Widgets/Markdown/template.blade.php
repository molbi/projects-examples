<div class="panel panel-default">
    {{--    {{ json_encode(app(Mundo\Kernel\Widget\Illuminate\RootWidget::class)->getGlobalState()) }}--}}
    <div class="panel-heading">
        <h3 class="panel-title">Markdown</h3>
    </div>
    <div class="panel-body">
        Test ({{ $test }})?<br/>
        <a href="@action([1, 'test' => 0])">A</a><br/>
        <a href="@action([1, 'test' => 1])">B</a><br/>
        <a href="@action([1])">C</a><br/>
        <a href="@signal('a')">C</a><br/>
        @widget('foo')
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Foo</h3>
    </div>
    <div class="panel-body">
        Test ({{ $test }})?<br/>
        <a href="@route('records.show', [1, 'test' => 0])">A</a><br/>
        <a href="@route('records.show', [1, 'test' => 1])">B</a><br/>
        <a href="@route('records.show', [1])">C</a><br/>
    </div>
</div>
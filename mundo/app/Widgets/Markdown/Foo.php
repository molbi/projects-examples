<?php

namespace Mundo\Widgets\Markdown;

use Mundo\Kernel\Widget\Illuminate\BaseWidget;

class Foo extends BaseWidget
{
    /** @persistent */
    public $test = 0;

    /**
     * Get the evaluated contents of the object.
     *
     * @return string
     */
    public function render()
    {
        return $this->renderView(__DIR__ . '/foo.blade.php', ['test' => $this->test]);
    }
}

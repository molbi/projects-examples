<?php

namespace Mundo\Widgets\Markdown;

use Illuminate\View\Factory;
use Mundo\Kernel\Widget\IContainer;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;

class Widget extends BaseWidget
{
    /** @persistent */
    public $test = 0;

    /**
     * Get the evaluated contents of the object.
     *
     * @return string
     */
    public function render()
    {
        return $this->renderView(__DIR__ . '/template.blade.php', ['test' => $this->test]);
    }

    public function createWidgetFoo($name)
    {
        return new Foo();
    }

    public function handleA()
    {
        return redirect()->back();
    }
}

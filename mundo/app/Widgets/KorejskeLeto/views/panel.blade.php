@if(!isset($disabled))
<div class="tiles tiles-purple">
    <div class="tiles-body">
        <div class="tiles-title">Korejské léto</div>

        <div class="heading">{{ round($currentPercent * 100)}} %
        </div>
        <div class="progress transparent progress-small no-radius">
            <div class="progress-bar progress-bar-white animate-progress-bar"
                 style="width: {{ min($currentPercent * 100, 100) }}%;"></div>
        </div>
    </div>
</div>
@endif
<?php

namespace Mundo\Widgets\KorejskeLeto;

use Carbon\Carbon;
use Mundo\Kernel\Constants;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\User;

/**
 * Class EuroCompetition
 * @package Mundo\Widgets\KorejskeLeto
 */
class KorejskeLeto extends BaseWidget
{
    /** When competition starts */
    const DATE_FROM = '2017-07-01';
    /** When competition ends */
    const DATE_TO = '2017-08-31';
    /** List of employees with their limit */
    const EMPLOYEES = [
        1065 => 350000,
        1521 => 550000,
        1745 => 320000,
        1491 => 220000,
        1849 => 650000,
        1047 => 580000,
        1225 => 550000,
        1765 => 480000,
        1869 => 350000,
        2163 => 160000,
        1647 => 340000,
        1815 => 150000,
        2107 => 220000,
        1639 => 350000,
        1985 => 100000,
        2093 => 220000,
        1923 => 320000,
        2153 => 220000,
        2179 => 160000,
        1619 => 180000,
        2039 => 180000,
        1465 => 180000,
        2159 => 180000,
        1955 => 180000,
        2011 => 180000,
        1875 => 180000,
        2167 => 120000,
        2169 => 120000
    ];

    /**
     * @var array
     */
    protected $_holidays = [
        '1-1',
        '6-4',
        '1-5',
        '8-5',
        '5-7',
        '6-7',
        '28-9',
        '28-10',
        '17-11',
        '24-12',
        '25-12',
        '26-12',
    ];

    /** @var  Cache */
    protected $cache;
    /** @var User */
    protected $user;
    /**
     * @var null
     */
    private $target;


    /**
     * EuroCompetition constructor.
     * @param User $user
     * @param Cache $cache
     * @param null $target
     */
    public function __construct(User $user, Cache $cache, $target = null)
    {
        $this->user = $user;
        $this->cache = $cache;
        $this->target = $target;
    }

    /**
     * @return string
     */
    public function render()
    {
        if (!isset(self::EMPLOYEES[$this->user->id])) {
            return $this->renderView(__DIR__ . '/views/panel.blade.php', [
                'disabled' => true
            ]);
        }
        return $this->renderView(__DIR__ . '/views/panel.blade.php', [
            'currentPercent' => $this->getSums() / (self::EMPLOYEES[$this->user->id] ),
        ]);
    }

    /**
     * @return int
     */
    protected function getSums()
    {
        if (empty($this->cache->get($this->user))) {
            return 0;
        }

        return collect($this->cache->get($this->user))->sum(function($orders) {
            return $this->convert($orders);
        });
    }

    /**
     * @param $item
     * @return int
     */
    protected function convert($item)
    {
        return $item->price * Constants::RATES[$item->lang];
    }

    public function days($type = null)
    {
        $from = Carbon::createFromFormat('Y-m-d', self::DATE_FROM);
        $now = Carbon::now();
        $to = Carbon::createFromFormat('Y-m-d', self::DATE_TO);
        $work_days = $past_days = 0;

        while ($from->lte($to)) {
            if ($from->isWeekday() && !in_array($from->format('j-n'), $this->_holidays)) {
                $work_days++;

                if ($from->lte($now)) $past_days++;
            }

            $from->addDay();
        }

        if (is_null($type)) {
            return [
                'work_days' => $work_days,
                'past_days' => $past_days
            ];
        }

        return $$type;
    }
}
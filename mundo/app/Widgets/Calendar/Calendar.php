<?php

namespace Mundo\Widgets\Calendar;

use Carbon\Carbon;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;

class Calendar extends BaseWidget
{
    /** @var Carbon */
    protected $date;

    /** @persistent */
    public $shift = 0;

    public function __construct($date = null)
    {
        if ($date === null) {
            $date = Carbon::now();
        }
        $this->date = $date;
    }

    public function render()
    {
        return $this->renderView(__DIR__ . '/views/panel.blade.php', [
            'date'  => $this->getDate(),
            'month' => $this->month($this->getDate()),
        ]);
    }

    /**
     * First day of week in month
     *
     * @param \Carbon\Carbon $date
     * @return int
     */
    protected function getFirstDayOfWeekInMonth(Carbon $date)
    {
        $start = $date->startOfMonth()->dayOfWeek - 1;

        return $start === -1 ? 6 : $start;
    }

    /**
     * Array of day objects for a month
     *
     * @param  Carbon $date
     * @return array
     */
    public function month(Carbon $date)
    {
        $calendar = [];

        $startDayOfWeek = $this->getFirstDayOfWeekInMonth($date);
        for ($x = 0; $x < 42; $x++) {
            if ($x < $startDayOfWeek) {
                $calendar[] = with(clone $date)->startOfMonth()->addDay(-$startDayOfWeek + $x);
            } else {
                $calendar[] = with(clone $date)->startOfMonth()->addDay($x - $startDayOfWeek);
            }
        }

        return $calendar;
    }

    /**
     * @return Carbon
     */
    public function getDate()
    {
        return with(clone $this->date)->addMonth($this->shift);
    }


    public function handleAddShift($shift)
    {
        $this->shift += (int)$shift;

        return redirect($this->currentUrl());
    }
}

<div class="tiles tiles-default">
    <div class="tiles-body">
        <div class="controller">
            <a href="@signal('addShift', [-1])"><i class="fa fa-chevron-left"></i></a>
            <a href="@current(['shift' => 0])"><i class="fa fa-circle"></i></a>
            <a href="@signal('addShift', [1])"><i class="fa fa-chevron-right"></i></a>
        </div>
        <div class="tiles-title">{{ $date->formatLocalized('%b %Y') }}</div>

        <table class="table table-calendar">
            <tr>
                <th>Po</th>
                <th>Út</th>
                <th>St</th>
                <th>Čt</th>
                <th>Pá</th>
                <th>So</th>
                <th>Ne</th>
            </tr>
            <tr>
                @foreach($widget->month($date) as $index => $d)
                    <td class="{{ $d->isToday() ? 'today' : '' }} {{ $d->month !== $date->month ? 'lighten' : '' }}">
                        {{ $d->day }}
                    </td>

                    @if(($index + 1) % 7 == 0)
                        {!! '</tr><tr>' !!}
                    @endif
                @endforeach
            </tr>

        </table>
    </div>
</div>

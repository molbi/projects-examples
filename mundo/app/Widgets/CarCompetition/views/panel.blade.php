<div class="tiles tiles-purple">
    <div class="tiles-body">
        <div class="tiles-title">Soutěž KIA Ceed</div>
        @if(isset($disabled) && $disabled)
            <div style="padding: 15px 0 0 0; font-size: 12px;">
                <strong>Nejste přihlášen(a) do soutěže o KIA Ceed <i class="fa fa-frown-o"></i></strong>
            </div>
        @else
            <div class="heading">{{ round($currentPercent * 100)}} %
            </div>
            <div class="progress transparent progress-small no-radius">
                <div class="progress-bar progress-bar-white animate-progress-bar"
                     style="width: {{ min($currentPercent * 100, 100) }}%;"></div>
            </div>
            <div class="description">
                <i class="fa fa-star{{ $oneMonthCompleted ? '' : '-o' }}"></i>
                &nbsp; minulý měsíc
                @if($oneMonthCompleted)
                    splněn
                @else
                    nesplněn
                @endif
            </div>
        @endif
    </div>
</div>
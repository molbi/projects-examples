<?php

namespace Mundo\Widgets\CarCompetition;

use Carbon\Carbon;
use Illuminate\Cache\Repository;
use Illuminate\Database\Connection;
use Illuminate\Database\Query\JoinClause;
use Mundo\Kernel\Cron\Cronable;
use Mundo\Models\User;

class Cache implements Cronable
{
    const KEY = 'widget:car-competition';

    /** @var \Illuminate\Cache\Repository */
    protected $cache;

    public function __construct(Repository $cache)
    {
        $this->cache = $cache;
    }

    public function get(User $user)
    {
        $data = $this->cache->get(self::KEY, []);
        if (!isset($data[$user->id])) {
            return [];
        }

        return $data[$user->id];
    }

    public function run(Connection $db = null)
    {
        $this->cache->forever(self::KEY, $this->getOrders($db));
    }

    protected function getBusinessOrdersSelect(Connection $db)
    {
        return $db->query()
            ->select([
                'Order.employee_id',
                $db->raw('MONTH(OrderHistory.changedAt) as month'),
                'Order.lang',
                'Invoice.price',
            ])
            ->from('obis.Order')
            ->join('obis.OrderHistory', function (JoinClause $join) {
                $join->on('OrderHistory.order_id', '=', 'Order.order_id')
                    ->where('OrderHistory.action', '=', 'paid');
            })
            ->leftJoin('obis.OrderExactor', 'OrderExactor.order_id', '=', 'Order.order_id')
            ->join('obis.Invoice', function (JoinClause $join) {
                $join->on('Invoice.order_id', '=', 'Order.order_id')
                    ->where('Invoice.price', '>', 0);
            })
            ->whereIn('Order.employee_id', User::all()->pluck('id'))
            ->whereBetween('OrderHistory.changedAt', [CarCompetition::DATE_FROM, CarCompetition::DATE_TO])
            ->where('Order.state', '=', 'paid')
            ->whereNull('OrderExactor.order_id')
            ->groupBy('Order.order_id');
    }

    protected function getContactOrdersSelect(Connection $db)
    {
        return $db->query()
            ->select([
                'invoices.employee_id',
                $db->raw('MONTH(invoices.paid_at) as month'),
                $db->raw('IF (invoices.currency = "CZK", "cs", "sk") as lang'),
                'invoices.price',
            ])
            ->from('cs_contact.invoices')
            ->whereIn('invoices.employee_id', User::all()->pluck('id'))
            ->whereBetween('invoices.paid_at', [CarCompetition::DATE_FROM, CarCompetition::DATE_TO])
            ->where('invoices.state', '=', 'paid')
            ->where('invoices.price', '>', 0);
    }

    protected function getOrders(Connection $db)
    {
        return array_assoc(
            $this->getBusinessOrdersSelect($db)->unionAll($this->getContactOrdersSelect($db))->get(),
            'employee_id|month[]'
        );
    }
}

<?php

namespace Mundo\Widgets\CarCompetition;

use Carbon\Carbon;
use Mundo\Kernel\Constants;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\User;

class CarCompetition extends BaseWidget
{
    /** When competition starts */
    const DATE_FROM = '2016-09-01';
    /** When competition ends */
    const DATE_TO = '2016-11-30';
    /** @var  Cache */
    protected $cache;
    /** @var User */
    protected $user;
    /** @var int */
    protected $limit;


    public function __construct(User $user, Cache $cache)
    {
        $this->user = $user;
        $this->limit = $this->user->param('tm_kia_limit');
        $this->cache = $cache;
    }

    public function render()
    {
        if ($this->limit === null || $this->limit <= 0) {
            return $this->renderView(__DIR__ . '/views/panel.blade.php', [
                'disabled' => true,
            ]);
        }

        $sums = $this->getSums();

        return $this->renderView(__DIR__ . '/views/panel.blade.php', [
            'oneMonthCompleted' => $this->isAnyMonthCompleted($sums),
            'value'             => $value = (isset($sums[Carbon::now()->month]) ? $sums[Carbon::now()->month] : 0),
            'currentPercent'    => $value / $this->limit,
        ]);
    }

    protected function isAnyMonthCompleted($sums)
    {
        $now = Carbon::now()->month;
        foreach ($sums as $month => $sum) {
            if ($month < $now && $sum > $this->limit) {
                return true;
            }
        }

        return false;
    }

    protected function getSums()
    {
        return array_map(function ($orders) {
            return array_sum(array_map([$this, 'convert'], $orders));
        }, $this->cache->get($this->user));
    }

    protected function convert($item)
    {
        return $item->price * Constants::RATES[$item->lang];
    }
}
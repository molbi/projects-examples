<div class="tiles tiles-default">
    <div class="tiles-body">
        <div class="tiles-title">Zaplacenost</div>
        <div class="heading small">{{ number_format($paidOrders->sum('price'), 0, ',', ' ') }} Kč</div>
        <div class="description">
            {{ \Carbon\Carbon::now()->format('n/Y')  }}
        </div>
    </div>
</div>
<?php

namespace Mundo\Widgets\PaidOrders;

use Mundo\Kernel\Cache\PaidOrders as Cache;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\User;

/**
 * Class PaidOrders
 * @package Mundo\Widgets\PaidOrders
 */
class PaidOrders extends BaseWidget
{

    /**
     * @var Cache
     */
    protected $cache;
    /** @var  User */
    protected $user;

    /**
     * PaidOrders constructor.
     * @param User $user
     * @param Cache $cache
     */
    public function __construct(User $user, Cache $cache)
    {
        $this->cache = $cache;
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function render()
    {
        return $this->renderView(__DIR__ . '/views/panel.blade.php', [
            'paidOrders' => $this->getPaidOrders(),
        ]);
    }

    /**
     * @return array|object
     */
    protected function getPaidOrders()
    {
        if ($result = $this->cache->getForUser($this->user)) {
            return collect($result);
        }

        return collect([]);
    }
}

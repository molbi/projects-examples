<?php

namespace Mundo\Widgets\Logicall\CsKontakt;

use Illuminate\Database\Connection;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Collection;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\TM\Record;
use Mundo\Modules\TM\Sources\Support\SourceManager;
use Mundo\Widgets\TM\B2M\Constants;

/**
 * Class CsKontakt
 * @package Mundo\Widgets\Logicall\CsKontakt
 */
class CsKontakt extends BaseWidget
{
    /**
     *
     */
    const SERVICE_TITLE = [
        'credit' => 'CSkontakt - zasílání poptávek (kredit)',
        'fee'    => 'CSkontakt - zasílání poptávek (paušál)',
        'golden' => 'CSkontakt - zlatý dodavatel'
    ];

    /** @var Record */
    protected $record;
    /**
     * @var SourceManager
     */
    private $sources;

    /**
     * CsKontakt constructor.
     *
     * @param Record        $record
     * @param SourceManager $sources
     */
    public function __construct(Record $record, SourceManager $sources)
    {
        $this->record = $record;
        $this->sources = $sources;
    }


    /**
     * @return string
     */
    public function render()
    {
        return $this->renderView(__DIR__ . '/view/widget.blade.php', [
            'cskontakt' => $this->cskontakt()
        ]);
    }

    /**
     * @return Connection
     */
    protected function db()
    {
        return $this->sources->get('b2m')->getConnection();
    }

    /**
     * @return Collection
     */
    private function cskontakt()
    {
        if(empty($this->record->remoteData->id_number)) {
            return collect();
        }

        return $this->firms()->map(function ($firm) {
            $firm->invoices = $this->mapInvoices($firm);

            return $firm;
        });
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    private function firms()
    {


        return collect($this->db()->table('cs_contact.users AS U')
            ->select([
                'U.*',
                'C.id_number',
                'C.title'
            ])
            ->join('cs_contact.contacts AS C', function (JoinClause $j) {
                $j->on('C.contactable_id', '=', 'U.id')
                    ->where('C.contactable_type', '=', 'Kartus\\Model\\User');
            })
            ->where('U.firm_id', $this->record->remote_id)
            ->orWhere('C.id_number', $this->record->remoteData->id_number)
            ->get());
    }

    /**
     * @param $firm
     *
     * @return \Illuminate\Support\Collection
     */
    private function mapInvoices($firm)
    {
        return collect($this->db()
            ->table('cs_contact.invoices AS I')
            ->whereNotIn('type', ['micro'])
            ->where('supplier_id', $firm->id)
            ->get());
    }

    /**
     * @param $state
     *
     * @return object
     */
    public function getStateTitleAndLabelClass($state)
    {
        return (object)[
            'title' => Constants::ORDER_STATE_TITLES[$state],
            'class' => Constants::ORDER_STATE_CLASSES[$state],
        ];
    }

    /**
     * @param $type
     *
     * @return mixed
     */
    public function getType($type)
    {
        return self::SERVICE_TITLE[$type];
    }
}

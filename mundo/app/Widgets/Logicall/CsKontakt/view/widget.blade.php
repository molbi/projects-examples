@if(!$cskontakt->isEmpty())
    <div class="tiles tiles-framed tiles-green widget-confilcts">
        <div class="tiles-title">
            Záznamy v CSkontaktu
        </div>

        @foreach($cskontakt as $firm)
            <div class="tiles-body tiles-body-table" style="overflow-y: hidden">
                <table class="table table-bordered">
                    <tr>
                        <td style="width: 150px; background-color: #fafafa;"><strong>Firma</strong></td>
                        <td>{{ $firm->title }}</td>
                        <td style="width: 150px; background-color: #fafafa;"><strong>Kredit</strong></td>
                        <td>{{ $firm->credits }}</td>
                    </tr>
                    <tr>
                        <td style="width: 150px; background-color: #fafafa;"><strong>Faktury</strong></td>
                        <td style="padding:0" colspan="3">
                            <table class="table table-bordered">
                                @foreach($firm->invoices as $invoice)
                                    <tr>
                                        <td style="width: 1px; white-space: nowrap;">{{ datetime($invoice->created_at) }}</td>
                                        <td style="width: 1px; white-space: nowrap;">{{ $widget->getType($invoice->type) }}</td>
                                        <td style="width: 1px; white-space: nowrap;">{{ $invoice->price }} {{ $invoice->currency }}</td>
                                        <td style="width: 1px; white-space: nowrap;">
                                            <span class="label label-{{ $widget->getStateTitleAndLabelClass($invoice->state)->class }}">
                                                {{ strtolower($widget->getStateTitleAndLabelClass($invoice->state)->title) }}
                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        @endforeach
    </div>
@endif
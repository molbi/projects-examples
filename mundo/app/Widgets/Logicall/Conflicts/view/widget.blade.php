@if(!$b2mConflicts->isEmpty() && !is_null($b2mConflicts->first()->services))
    <div class="tiles tiles-framed tiles-red widget-confilcts">
        <div class="tiles-title">
            Duplicitní záznamy s B2M databázi se službou
        </div>

        <div class="tiles-body tiles-body-table" style="overflow-y: hidden">
            <table class="table table-bordered">
                @foreach ($b2mConflicts as $conflict)
                    @if( $conflict->services !== null)
                    <tr>
                        <td><strong>{{ $conflict->title }}</strong></td>
                        <td>
                            @foreach($conflict->services as $service)
                                {{ \Mundo\Widgets\TM\B2M\Constants::SERVICE_TITLES[$service] }},
                            @endforeach
                        </td>
                        <td class="table-rest">
                            <a href="http://logicall.b2m.cz/fobis/firms/detail/id/{{ $conflict->firm_id }}"
                               target="_blank"
                               data-pjax="false"
                               class="action">
                                <i class="fa fa-eye"></i>
                            </a>
                        </td>
                    </tr>
                    @endif
                @endforeach
            </table>
        </div>
    </div>
@endif

@if(!$conflicts->isEmpty())
    <div class="tiles tiles-framed tiles-red widget-confilcts">
        <div class="tiles-title">
            Aktivní záznamy v jiných kampaních LgC
        </div>

        <div class="tiles-body tiles-body-table" style="overflow-y: hidden">
            <table class="table table-bordered">
                @foreach ($conflicts as $conflict)
                    <tr>
                        <td>{{ $conflict->title }}</td>
                        <td style="width: 1px; white-space: nowrap; text-align: right;">
                            <small>{{ datetime($conflict->createdAt) }} <i class="fa fa-fw fa-clock-o"></i></small>
                            <br>
                            <small>
                                {!! $conflict->realname ?  $conflict->realname : '<em>nepřiřazeno</em>' !!}
                                <i class="fa fa-fw fa-user"></i>
                            </small>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endif

@if(!$idConflicts->isEmpty())
    <div class="tiles tiles-framed tiles-red widget-confilcts">
        <div class="tiles-title">
            Duplicita v kampani za základě IČO
        </div>

        <div class="tiles-body tiles-body-table" style="overflow-y: hidden">
            <table class="table table-bordered">
                @foreach ($idConflicts as $conflict)
                    <tr>
                        <td>{{ $conflict->title }}</td>
                        <td style="width: 1px; white-space: nowrap; text-align: right;">
                            <small>{{ datetime($conflict->createdAt) }} <i class="fa fa-fw fa-clock-o"></i></small>
                            <br>
                            <small>
                                {!! $conflict->realname ?  $conflict->realname : '<em>nepřiřazeno</em>' !!}
                                <i class="fa fa-fw fa-user"></i>
                            </small>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>

    </div>
@endif



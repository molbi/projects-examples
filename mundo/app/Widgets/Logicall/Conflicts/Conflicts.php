<?php

namespace Mundo\Widgets\Logicall\Conflicts;

use Illuminate\Database\Connection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Collection;
use Mundo\Kernel\Constants;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\TM\Record;
use Mundo\Models\TM\RecordHistory;
use Mundo\Modules\TM\Sources\Support\SourceManager;

class Conflicts extends BaseWidget
{
    /** @var \Mundo\Models\TM\Record */
    protected $record;
    /**
     * @var SourceManager
     */
    private $sources;
    /**
     * @var array
     */
    private $campaigns;

    public function __construct(Record $record, SourceManager $sources, array $campaigns = [])
    {
        $this->record = $record;
        $this->record->campaign->sync_id = 87;

        $this->sources = $sources;
        $this->campaigns = $campaigns;
    }


    public function render()
    {
        return $this->renderView(__DIR__ . '/view/widget.blade.php', [
            'b2mConflicts' => $this->b2mConflicts(),
            'conflicts'    => $this->conflicts(),
            'idConflicts'  => $this->idConflicts()
        ]);
    }

    /**
     * @return Connection
     */
    protected function db()
    {
        return $this->sources->get('b2m')->getConnection();
    }

    private function conflicts()
    {
        return collect($this->db()->table('telemarketing.Stack AS S')
            ->select([
                'S.*',
                'C.title',
                'E.realname'
            ])
            ->join('telemarketing.Campaign AS C', function (JoinClause $j) {
                $j->on('C.campaign_id', '=', 'S.campaign_id')
                    ->where('C.state', '=', 'active');
            })
            ->join('telemarketing.TargetGroup AS TG', 'TG.targetGroup_id', '=', 'C.targetGroup_id')
            ->leftJoin('zis.Employee AS E', 'E.employee_id', '=', 'S.employee_id')
            ->where('S.targetSubject_id', $this->record->remote_id)
            ->where('TG.targetType_id', $this->campaignTargetType())
            ->where('S.campaign_id', '!=', $this->record->campaign->sync_id)
            ->whereIn('S.state', ['forwarded', 'pending', 'reached', 'unreached'])
            ->whereNotNull('S.employee_id')
            ->orderBy('S.createdAt', 'DESC')
            ->get());
    }

    private function campaignTargetType()
    {
        return $this->db()->table('telemarketing.Campaign AS C')
            ->select(['TG.targetType_id'])
            ->join('telemarketing.TargetGroup AS TG', 'TG.targetGroup_id', '=', 'C.targetGroup_id')
            ->where('C.campaign_id', $this->record->campaign->sync_id)
            ->first()
            ->targetType_id;
    }

    private function idConflicts()
    {
        if(empty($this->record->remoteData->id_number)) {
            return collect();
        }

        return collect($this->db()->table('business.Firm AS F')
            ->select([
                'F.title',
                'S.createdAt',
                'E.realname'
            ])
            ->join('telemarketing.Stack AS S', function (JoinClause $j) {
                $j->on('S.targetSubject_id', '=', 'F.firm_id')
                    ->where('S.campaign_id', '=', $this->record->campaign->sync_id);
            })
            ->leftJoin('zis.Employee AS E', 'E.employee_id', '=', 'S.employee_id')
            ->where('F.idNumber', $this->record->remoteData->id_number)
            ->whereNotIn('F.firm_id', [$this->record->remote_id])
            ->whereIn('S.state', ['forwarded', 'pending', 'reached', 'unreached'])
            ->where('F.state', 'visible')
            ->get());
    }

    private function b2mConflicts()
    {
        if(empty($this->record->remoteData->id_number)) {
            return collect();
        }

        return collect($this->db()->table('business.Firm AS F')
            ->select([
                'F.firm_id',
                'F.title',
                'F.lang',
                $this->db()->raw('IF(FS.firm_id IS NULL, FCS.service, FS.service) AS service')
            ])
            ->leftJoin('business.Firm_Service AS FS', 'FS.firm_id', '=', 'F.firm_id')
            ->leftJoin('business.Firm_CreditService AS FCS', 'FCS.firm_id', '=', 'F.firm_id')
            ->where('F.idNumber', $this->record->remoteData->id_number)
            ->where('F.state', 'visible')
            ->whereNotIn('F.firm_id', [$this->record->remote_id])
            ->get())
            ->groupBy('firm_id')
            ->map(function (Collection $services) {
                $first = $services->first();
                return (object)[
                    'title'    => $first->title,
                    'firm_id'  => $first->firm_id,
                    'lang'     => $first->lang,
                    'services' => is_null($first->service) ? null : $services->pluck('service')
                ];
            });
    }
}

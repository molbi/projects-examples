<?php

namespace Mundo\Widgets\Logicall\Targeting;

use Carbon\Carbon;
use Illuminate\Cache\Repository;
use Illuminate\Database\Connection;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Collection;
use Mundo\Kernel\Cron\Cronable;
use Mundo\Models\User;

/**
 * Class Cache
 * @package Mundo\Widgets\Logicall\Targeting
 */
class Cache implements Cronable
{
    /**
     *
     */
    const KEY = 'widget:logicall-targeting';

    /** @var \Illuminate\Cache\Repository */
    protected $cache;

    /**
     * @var static
     */
    private $starDate;
    /**
     * @var
     */
    private $endDate;
    /**
     * @var
     */
    private $operators;

    /**
     * Cache constructor.
     *
     * @param Repository $cache
     */
    public function __construct(Repository $cache)
    {
        $this->cache = $cache;
        $this->starDate = Carbon::now()->startOfMonth();
        $this->endDate = with(clone $this->starDate)->endOfMonth();
        $this->operators = User::withAnyTags(['Logicall'])->get()->pluck('id');
    }

    /**
     * @param Connection|null $db
     */
    public function run(Connection $db = null)
    {
        $this->cache->forever(self::KEY, $this->getOrders($db));
    }

    /**
     * @param User $user
     * @param      $type
     *
     * @return array
     */
    public function get(User $user, $type)
    {
        $data = $this->cache->get(self::KEY)[$type];
        if (!isset($data[$user->id])) {
            return null;
        }

        return $data[$user->id];
    }

    /**
     * @param $teamOperators
     * @param $type
     *
     * @return Collection
     */
    public function getUsers($teamOperators, $type)
    {
        $data = $this->cache->get(self::KEY)[$type];

        $result = [];
        foreach ($teamOperators as $operator) {
            if (isset($data[$operator->id])) {
                $result[$operator->id] = collect($data[$operator->id]);
            }
        }

        return collect($result);
    }

    /**
     * @return array
     */
    protected function getInterval()
    {
        return [
            $this->starDate,
            $this->endDate
        ];
    }

    /**
     * @return array
     */
    protected function getIntervalDateStrings()
    {
        return array_map(function (Carbon $date) {
            return $date->toDateString();
        }, $this->getInterval());
    }

    /**
     * @return array
     */
    protected function getIntervalWeeks()
    {
        return array_map(function (Carbon $date) {
            return $date->weekOfYear;
        }, $this->getInterval());
    }

    /**
     * @param Connection $db
     *
     * @return array
     */
    protected function getOrders(Connection $db)
    {
        return [
            'week'  => $this->getOrdersWeek($db),
            'month' => $this->getOrdersMonth($db),
            'today' => $this->getOrdersToday($db)
        ];
    }

    /**
     * @param Connection $db
     *
     * @return null
     */
    private function getOrdersMonth(Connection $db)
    {
        return array_assoc(
            $this->getOrdersMonthQuery($db)->get(),
            'employee_id=count'
        );
    }

    /**
     * @param Connection $db
     *
     * @return Builder
     */
    private function getOrdersMonthQuery(Connection $db)
    {
        return $db->query()
            ->select([
                'Order.employee_id',
                $db->raw('COUNT(Order.order_id) as count'),
            ])
            ->from('obis.Order')
            ->whereIn('Order.employee_id', $this->operators)
            ->whereBetween('Order.createdOn', $this->getIntervalDateStrings())
            ->groupBy('Order.employee_id');
    }

    /**
     * @param Connection $db
     *
     * @return null
     */
    private function getOrdersWeek(Connection $db)
    {
        return array_assoc(
            $this->getOrdersWeekQuery($db)->get(),
            'employee_id|week'
        );
    }

    /**
     * @param Connection $db
     *
     * @return Builder
     */
    private function getOrdersWeekQuery(Connection $db)
    {
        return $db->query()
            ->select([
                'Order.employee_id',
                $db->raw('WEEK(Order.createdOn) as week'),
                $db->raw('COUNT(Order.order_id) as count'),
            ])
            ->from('obis.Order')
            ->whereIn('Order.employee_id', $this->operators)
            ->whereBetween($db->raw('WEEK(Order.createdOn)'), $this->getIntervalWeeks())
            ->whereYear('Order.createdOn', '=', $this->starDate->year)
            ->groupBy($db->raw('WEEK(Order.createdOn)'), 'Order.employee_id');
    }

    /**
     * @param Connection $db
     *
     * @return null
     */
    private function getOrdersToday(Connection $db)
    {
        return array_assoc(
            $this->getOrdersTodayQuery($db)->get(),
            'employee_id=count'
        );
    }

    private function getOrdersTodayQuery(Connection $db)
    {
        return $db->query()
            ->select([
                'Order.employee_id',
                $db->raw('COUNT(Order.order_id) as count'),
            ])
            ->from('obis.Order')
            ->whereIn('Order.employee_id', $this->operators)
            ->whereRaw('DATE(Order.createdOn) = DATE(NOW())')
            ->groupBy('Order.employee_id');
    }

}

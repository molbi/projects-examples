<?php

namespace Mundo\Widgets\Logicall\Targeting;

use Carbon\Carbon;
use InvalidArgumentException;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\User;

/**
 * Class Targeting
 * @package Mundo\Widgets\Logicall\Targeting
 */
class Targeting extends BaseWidget
{
    /** @var Cache */
    protected $cache;
    /** @var  User */
    protected $user;
    /** @var */
    private $day_plan;
    /** @var */
    private $month_plan;
    /** @var */
    private $team_tag;
    /** @var */
    private $teamOperators;

    /**
     * Targeting constructor.
     *
     * @param User  $user
     * @param Cache $cache
     * @param       $day_plan
     * @param       $month_plan
     * @param       $team_tag
     */
    public function __construct(User $user, Cache $cache, $day_plan, $month_plan, $team_tag)
    {
        $this->cache = $cache;
        $this->user = $user;
        $this->day_plan = $day_plan;
        $this->month_plan = $month_plan;
        $this->team_tag = $team_tag;
        $this->teamOperators = User::withAnyTags([$this->team_tag])->get();
    }

    /**
     * @return string
     */
    public function render()
    {
        return $this->renderView(__DIR__ . '/views/panel.blade.php', [
            'week'       => $this->weekUser(),
            'month'      => $this->monthUser(),
            'day_team'  => $this->dayTeam(),
            'month_team' => $this->monthTeam()
        ]);
    }

    /**
     * @return object
     */
    private function weekUser()
    {
        $todayString = Carbon::now()->weekOfYear;

        return (object)[
            'sum'     => $sum = isset($this->weekSums()[$todayString]) ? $this->weekSums()[$todayString]->count : 0,
            'percent' => $sum / ($target = $this->userWeekLimit()),
            'target'  => $target
        ];
    }

    /**
     * @return object
     */
    private function monthUser()
    {
        return (object)[
            'sum'     =>  !is_null($sum = $this->monthSums()) ? $sum : 0,
            'percent' => $sum / ($target = $this->userMonthLimit()),
            'target'  => $target
        ];
    }

    /**
     * @return object
     */
    private function monthTeam()
    {
        return (object)[
            'sum'     => $sum = $this->monthTeamSums(),
            'percent' => $sum / ($target = $this->month_plan),
            'target'  => $target
        ];
    }

    /**
     * @return object
     */
    private function dayTeam()
    {
        return (object)[
            'sum'     => $sum = $this->dayTeamSums(),
            'percent' => $sum / ($target = $this->day_plan),
            'target'  => $target
        ];
    }

    /**
     *
     * @return array|mixed
     *
     */
    private function weekSums()
    {
        return collect($this->cache->get($this->user, 'week'));
    }

    /**
     *
     * @return array|mixed
     *
     */
    private function monthSums()
    {
        return $this->cache->get($this->user, 'month');
    }

    /**
     * @return mixed
     */
    private function monthTeamSums()
    {
        $data = $this->cache->getUsers($this->teamOperators, 'month');

        return $data->flatten()->sum();
    }

    /**
     * @return mixed
     */
    private function dayTeamSums()
    {
        $data = $this->cache->getUsers($this->teamOperators, 'today');

        return $data->flatten()->sum();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model|int|mixed
     */
    protected function userWeekLimit()
    {
        if ($this->user->param('logicall_week_target') <= 0) {
            throw new InvalidArgumentException("User has not right limit!");
        }

        return $this->user->param('logicall_week_target');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model|int|mixed
     */
    private function userMonthLimit()
    {
        if ($this->user->param('logicall_month_target') <= 0) {
            throw new InvalidArgumentException("User has not right limit!");
        }

        return $this->user->param('logicall_month_target');
    }
}

<div class="split">
    <div class="split-half">
        <div class="tiles tiles-blue">
            <div class="tiles-body">
                <div class="tiles-title">Týdenní plnění</div>
                <div class="heading small">{{ number_format($week->sum, 0, ',', ' ') }}</div>
                <div class="progress transparent progress-small no-radius">
                    <div class="progress-bar progress-bar-white animate-progress-bar"
                         style="width: {{ min($week->percent * 100, 100) }}%;"></div>
                </div>
                <div class="description">
                    váš limit <strong>{{ number_format(floor($week->target), 0, ',', ' ') }}</strong>.
                </div>
            </div>
        </div>
    </div>
    <div class="split-half">
        <div class="tiles tiles-default">
            <div class="tiles-body">
                <div class="tiles-title">Měsíční plnění</div>
                <div class="heading small">{{ number_format($month->sum, 0, ',', ' ') }}</div>
                <div class="progress transparent progress-small no-radius">
                    <div class="progress-bar progress-bar-info animate-progress-bar"
                         style="width: {{ min($month->percent * 100, 100) }}%;"></div>
                </div>
                <div class="description">
                    váš limit <strong>{{ number_format($month->target, 0, ',', ' ') }}</strong>.
                </div>
            </div>
        </div>
    </div>
</div>

<div class="split">
    <div class="split-half">
        <div class="tiles tiles-blue">
            <div class="tiles-body">
                <div class="tiles-title">Denní plnění (team)</div>
                <div class="heading small">{{ number_format($day_team->sum, 0, ',', ' ') }}</div>
                <div class="progress transparent progress-small no-radius">
                    <div class="progress-bar progress-bar-white animate-progress-bar"
                         style="width: {{ min($day_team->percent * 100, 100) }}%;"></div>
                </div>
                <div class="description">
                    team limit <strong>{{ number_format(floor($day_team->target), 0, ',', ' ') }}</strong>.
                </div>
            </div>
        </div>
    </div>
    <div class="split-half">
        <div class="tiles tiles-default">
            <div class="tiles-body">
                <div class="tiles-title">Měsíční plnění (team)</div>
                <div class="heading small">{{ number_format($month_team->sum, 0, ',', ' ') }}</div>
                <div class="progress transparent progress-small no-radius">
                    <div class="progress-bar progress-bar-info animate-progress-bar"
                         style="width: {{ min($month_team->percent * 100, 100) }}%;"></div>
                </div>
                <div class="description">
                    team limit <strong>{{ number_format($month_team->target, 0, ',', ' ') }}</strong>.
                </div>
            </div>
        </div>
    </div>
</div>
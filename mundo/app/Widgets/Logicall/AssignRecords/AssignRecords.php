<?php
namespace Mundo\Widgets\Logicall\AssignRecords;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Mundo\Jobs\TM\Forwarding\ForwardRecordJob;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\Tag;
use Mundo\Models\TM\Campaign;
use Mundo\Models\User;

/**
 * Class AssignRecords
 * @package Mundo\Widgets\Logicall\AssignRecords
 */
class AssignRecords extends BaseWidget
{
    use DispatchesJobs;

    /**
     *
     */
    const LIMIT = 3;

    /** @var  User */
    protected $user;
    /** @var Tag */
    private $tag;
    /** @var int */
    private $limit;
    /**
     * @var string
     */
    private $job;

    /**
     * AssignRecords constructor.
     *
     * @param User $user
     * @param $tag
     * @param int $limit
     * @param string $job
     */
    public function __construct(User $user, $tag, $limit = self::LIMIT, $job = ForwardRecordJob::class)
    {
        $this->user = $user;
        $this->tag = Tag::where('id', $tag)->first();
        $this->limit = $limit;
        $this->job = $job;
    }


    /**
     * @return string
     */
    public function render()
    {
        return $this->renderView(__DIR__ . '/views/panel.blade.php', [
            'campaigns' => $this->campaigns(),
        ]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    private function campaigns()
    {
        return Campaign::with('forwardableRecordsCount')
            ->whereHas('tags', function ($q) {
                $q->where('id', $this->tag->id);
            })
            ->has('forwardableRecords', '>', '0')
            ->get();
    }

    /**
     * @param $campaign_id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function handleSubmit($campaign_id)
    {
        $this->dispatch(new $this->job($this->user, [
            'campaign' => $campaign_id,
            'count'    => $this->limit
        ]));

        return redirect($this->currentUrl());
    }
}

<div class="tiles tiles-default">
    <div class="tiles-body">
        <div class="tiles-title">Přiřazení záznamů</div>
        <div class="btn-group">
            @foreach($campaigns as $campaign)
                <a class="btn btn-primary btn-block"
                   data-pjax="false"
                   style="font-size: 0.7vw;white-space: normal;"
                   href="{{ $widget->signalUrl('submit', ['campaign_id' => $campaign->id]) }}">
                    {{ $campaign->name }} ({{ $campaign->forwardableCount }})
                </a>
            @endforeach
        </div>
    </div>
</div>
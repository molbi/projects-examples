<?php
namespace Mundo\Widgets\Logicall\RisAlert;

use Illuminate\Database\Connection;
use Illuminate\Support\Facades\DB;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Models\TM\Record;
use Mundo\Modules\TM\Sources\Support\SourceManager;


class RisAlert extends BaseWidget
{


    private $record;

    private $sources;

    public function __construct(Record $record, SourceManager $sources)
    {
        $this->record = $record;
        $this->sources = $sources;
    }


    /**
     * @return string
     */
    public function render()
    {
        return $this->renderView(__DIR__ . '/views/panel.blade.php', [
            'endsOn' => $this->endsOn()
        ]);
    }

    /**
     * @return Connection
     */
    protected function db()
    {
        return $this->sources->get('b2m')->getConnection();
    }

    private function endsOn()
    {
        return $this->db()->table('business.Firm_Service AS FS')
            ->select([
                DB::raw('TO_DAYS(FS.endsOn) - TO_DAYS(NOW()) AS endsOn')
            ])
            ->whereRaw('TO_DAYS(FS.endsOn) - TO_DAYS(NOW()) < 25')
            ->where('FS.firm_id', $this->record->remote_id)
            ->orderByRaw('endsOn')
            ->first();
    }
}

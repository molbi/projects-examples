@if(!is_null($endsOn))
    <div class="tiles tiles-red">
        <div class="tiles-body">
            <i class="fa fa-exclamation-circle"></i>
            @if($endsOn->endsOn == 1)
                Firma bude za {{ $endsOn->endsOn }} den v prodloužení!
            @elseif($endsOn->endsOn > 1 && $endsOn->endsOn < 5)

                Firma bude za {{ $endsOn->endsOn }} dny v prodloužení!
            @else
                Firma bude za {{ $endsOn->endsOn }} dní v prodloužení!
            @endif
        </div>
    </div>
@endif
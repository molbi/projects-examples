<div class="tiles tiles-default">
    <div class="tiles-body">
        <div class="btn-group btn-group-justified">
            @foreach($buttons as $button)
                <a href="{{ route($button->route, $widget->params($button)) }}"
                   title="{{ $button->title }}"
                   class="{{ $button->class }}"
                   @if(isset($button->remote) && $button->remote)
                        data-remote="true"
                   @endif
                    @if(isset($button->blank) && $button->blank)
                        target="_blank"
                    @endif
                >
                    {{ $button->title }}
                </a>
            @endforeach
        </div>
    </div>
</div>

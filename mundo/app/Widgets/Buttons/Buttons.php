<?php

namespace Mundo\Widgets\Buttons;

use Carbon\Carbon;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;

class Buttons extends BaseWidget
{
    /**
     * @var array
     */
    private $buttons;

    /**
     * Buttons constructor.
     *
     * @param array $buttons
     */
    public function __construct(array $buttons)
    {
        $this->buttons = $buttons;
    }

    public function render()
    {
        return $this->renderView(__DIR__ . '/views/panel.blade.php', [
            'buttons' => $this->buttons
        ]);
    }

    public function params($button)
    {
        $query = isset($button->query) ? (array)$button->query : [];

        return array_merge($button->params, $query);
    }
}

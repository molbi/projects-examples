<?php

namespace Mundo\Presenters\TM\LeadsForwarding;

use DB;
use Illuminate\Database\Eloquent\Collection;
use Mundo\Models\TM\Campaign;
use Mundo\Models\TM\Record;
use Mundo\Models\User;
use Mundo\Modules\TM\Sources\Support\SourceManager;

/**
 * Class ForwardingPresenter
 * @package Mundo\Presenters
 */
class WithCampaignsPresenter
{
    /** @var User */
    public $leader;
    /** @var Campaign */
    public $campaign;

    /** @var  Collection */
    protected $operators;
    /** @var  Collection */
    protected $campaigns;
    /** @var  SourceManager */
    protected $sources;

    /**
     * ForwardingPresenter constructor.
     *
     * @param User     $leader
     * @param Campaign $campaign
     */
    public function __construct(User $leader, Campaign $campaign = null)
    {
        $this->leader = $leader;
        $this->campaign = $campaign;
    }

    /**
     * @return Collection
     */
    public function campaignsList()
    {
        if (isset($this->campaigns)) {
            return $this->campaigns;
        }

        $this->campaigns = new Collection();

        $tags = $this->leader->leadsCampaignTags()->with('tmCampaigns')->get();

        if (!$tags->isEmpty()) {

            $this->campaigns = $tags->map(function ($tag) {
                return $tag->tmCampaigns()->with([
                    'forwardableLeadsRecordsCount' => function ($q) {
                        $q->whereIn('user_id', explode(',',$this->leader->param('tm_virtual_leaders')));
                    }
                ])->get();
            })->flatten();
        }

        return $this->campaigns;
    }


    /**
     * Does user have any records?
     * @return bool
     */
    public function hasCampaigns()
    {
        return !$this->campaignsList()->isEmpty();
    }

    /**
     *
     */
    public function leadsList()
    {
        return $this->campaign->forwardableLeadsRecords()
            ->with('campaign', 'histories')
            ->whereIn('user_id', explode(',',$this->leader->param('tm_virtual_leaders')))
            ->orderBy('created_at')
            ->get()
            ->withRemote();
    }

    /**
     * @return Collection
     */
    public function operatorsList()
    {
        $this->operators = new Collection();

        $tags = $this->leader->leadsUserTags()->with('users')->get();

        if (!$tags->isEmpty()) {
            $this->operators = $tags->map(function ($tag) {
                return $tag->users;
            })->flatten();
        }

        return $this->operators;
    }
}

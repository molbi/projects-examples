<?php

namespace Mundo\Presenters\TM;

use Illuminate\Database\Eloquent\Collection;
use Mundo\Models\User;
use Mundo\Modules\TM\Sources\Support\SourceManager;

/**
 * Class ForwardingPresenter
 * @package Mundo\Presenters
 */
class ForwardingPresenter
{
    /** @var User */
    public $leader;
    /** @var User */
    public $operator;

    /** @var  Collection */
    protected $operators;
    /** @var  SourceManager */
    protected $sources;

    /**
     * ForwardingPresenter constructor.
     *
     * @param User $leader
     * @param User $operator
     *
     */
    public function __construct(User $leader, User $operator = null)
    {
        $this->leader = $leader;
        $this->operator = $operator;

    }

    /**
     * @return Collection
     */
    public function operatorsList()
    {
        if (!isset($this->operators)) {
            $this->operators = new Collection();

            $tags = $this->leader->leadsUserTags()->with('users.tm_records_count')->get();

            if (!$tags->isEmpty()) {
                $this->operators = $tags->map(function ($tag) {
                    return $tag->users;
                })->flatten();
            }
        }

        return $this->operators;
    }

    /**
     * Does user have any records?
     * @return bool
     */
    public function hasOperators()
    {
        return !$this->operatorsList()->isEmpty();
    }

    public function leadersCampaigns($with = [])
    {
        $campaigns = new Collection();
        $tags = $this->leader->leadsCampaignTags()->with('tmCampaigns')->get();

        if (!$tags->isEmpty()) {
            $campaigns = $tags->map(function ($tag) use ($with) {
                if (empty($with)) {
                    return $tag->tmCampaigns;
                }

                return $tag->tmCampaigns()->with($with)->get();

            })->flatten();
        }

        return $campaigns;
    }
}

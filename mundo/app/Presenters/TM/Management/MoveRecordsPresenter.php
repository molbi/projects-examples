<?php

namespace Mundo\Presenters\TM\Management;

use Illuminate\Support\Collection;
use Mundo\Models\User;

/**
 * Class MoveRecordsPresenter
 * @package Mundo\Presenters\Management
 */
class MoveRecordsPresenter
{
    /** @var Collection */
    private $operators;
    /**
     * RecordsPresenter constructor.
     *
     * @param User $user
     *
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return Collection
     */
    public function operators()
    {
        if ($this->operators === null) {
            $this->operators = $this->user->leadsUserTags->map(function ($tag) {
                return $tag->users;
            })->flatten();
        }

        return $this->operators;
    }
}

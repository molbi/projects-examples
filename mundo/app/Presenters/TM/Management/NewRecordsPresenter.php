<?php

namespace Mundo\Presenters\TM\Management;

use DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Mundo\Models\TM\Campaign;
use Mundo\Models\TM\Record;
use Mundo\Models\User;
use Mundo\Modules\TM\Sources\Eloquent\Collection as B2MCollection;
use Mundo\Modules\TM\Sources\Support\SourceManager;

/**
 * Class ForwardingPresenter
 * @package Mundo\Presenters
 */
class NewRecordsPresenter
{
    /** @var User */
    public $leader;

    /** @var  Collection */
    protected $operators;

    /** @var  SourceManager */
    protected $sources;
    /**
     * @var Request
     */
    private $request;

    /** @var  Collection */
    private $campaigns;

    /**
     * ForwardingPresenter constructor.
     *
     * @param User    $leader
     * @param Request $request
     */
    public function __construct(User $leader, Request $request)
    {
        $this->leader = $leader;
        $this->request = $request;
    }

    /**
     *
     */
    public function leadsList()
    {
        if(!$this->request->has('included')) {
            return collect([]);
        }

        return $this->applyLangFilters(
            $this->applyFilters($this->leadsListQuery())
                ->get()
                ->withRemote()
        );
    }

    public function campaignsForSelect()
    {
        if (isset($this->campaigns)) {
            return $this->campaigns;
        }

        $this->campaigns = new Collection();

        $tags = $this->leader->leadsCampaignTags()->with('tmCampaigns')->get();
        if (!$tags->isEmpty()) {
            $this->campaigns = $tags->map(function ($tag) {
                return $tag->tmCampaigns()->get();
            })->flatten()->pluck('name', 'id');
        }

        return $this->campaigns;
    }

    private function applyFilters($query)
    {
        if ($from = $this->request->get('from')) {
            $query->whereDate('created_at', '>=', $from);
        }

        if ($from = $this->request->get('to')) {
            $query->whereDate('created_at', '<=', $from);
        }

        if($included = $this->request->get('included')) {
            $query->whereIn('campaign_id', $included);
        }

        if($excluded = $this->request->get('excluded')) {
            $query->whereNotIn('campaign_id', $excluded);
        }

        return $query;
    }

    private function applyLangFilters(B2MCollection $records)
    {
        if($lang = $this->request->get('lang')) {
            return $records->where('remoteData.lang', $lang);
        }

        return $records;
    }

    /**
     * @return mixed
     */
    private function leadsListQuery()
    {
        return Record::forwardableLeads()
            ->with('campaign', 'params', 'histories')
            ->whereNull('user_id')
            ->orderBy('created_at');
    }
}

<?php

namespace Mundo\Presenters\TM\Management;

use Elasticsearch\Endpoints\Indices\Validate\Query;
use Illuminate\Support\Collection;
use Mundo\Filters\QueryFilter;
use Mundo\Models\TM\Record;
use Mundo\Models\User;

/**
 * Class RecordsPresenter
 * @package Mundo\Presenters\Management
 */
class RecordsPresenter
{
    /** @var User */
    protected $user;

    /** @var Collection */
    private $operators;
    /** @var Collection */
    private $records;
    /**
     * @var QueryFilter
     */
    private $filters;

    /**
     * RecordsPresenter constructor.
     *
     * @param User        $user
     * @param QueryFilter $filters
     */
    public function __construct(User $user, QueryFilter $filters)
    {
        $this->user = $user;
        $this->filters = $filters;
    }

    /**
     * @return Collection
     */
    public function records()
    {
        if ($this->records === null) {
            $this->records = Record::with('user', 'campaign', 'params', 'histories')
                ->filter($this->filters())
                ->get()
                ->withRemote();
        }

        return $this->records;
    }

    /**
     * @return Collection
     */
    public function operators()
    {
        if ($this->operators === null) {
            $this->operators = $this->user->leadsUserTags->map(function ($tag) {
                return $tag->users;
            })->flatten();
        }

        return $this->operators;
    }

    /**
     *
     */
    public function states()
    {
        return array_combine(Record::STATES, Record::STATES);
    }

    /**
     * @return QueryFilter
     */
    private function filters()
    {
        $this->filters->setDefaults([
            'user_id' => $this->operators()
        ]);

        return $this->filters;
    }
}

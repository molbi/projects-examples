<?php

namespace Mundo\Presenters\TM\Statistics;

use ConsoleTVs\Charts\Facades\Charts;
use Elasticsearch\Endpoints\Indices\Validate\Query;
use Illuminate\Support\Collection;
use Mundo\Filters\QueryFilter;
use Mundo\Models\ServiceLog;
use Mundo\Models\Tag;
use Mundo\Models\TM\Record;
use Mundo\Models\User;
use Mundo\Widgets\TM\B2M\Constants;
use Mundo\Widgets\TM\OfferedServices\OfferedServices;

/**
 * Class OfferedServicesPresenter
 * @package Mundo\Presenters\TM\Statistics
 */
class OfferedServicesPresenter
{
    /** @var User */
    protected $user;
    /** @var Collection */
    private $records;
    /** @var QueryFilter */
    private $filters;
    /**
     * @var
     */
    private $operators;

    /**
     * RecordsPresenter constructor.
     *
     * @param User $user
     * @param QueryFilter $filters
     */
    public function __construct(User $user, QueryFilter $filters)
    {
        $this->user = $user;
        $this->filters = $filters;
    }

    /**
     * @return Collection
     */
    public function records()
    {
        if ($this->records === null) {
            $this->records = ServiceLog::with('user', 'target')
                ->filter($this->filters())
                ->orderBy('created_at', 'desc')
                ->get();
        }

        return $this->records;
    }

    /**
     * @return User
     */
    public function user()
    {
        return $this->user;
    }

    /**
     * @return QueryFilter
     */
    public function filters()
    {
        $this->filters->setDefaults([
            'user_id' => $this->operators()
        ]);

        return $this->filters;
    }

    /**
     * @return mixed
     */
    private function operators()
    {
        if($this->filters->request()->has('tags')) {
            $this->user->leadsUserTags = Tag::whereIn('id', $this->filters->request()->get('tags'))->get();
        }

        if ($this->operators === null) {
            $this->operators = $this->user->leadsUserTags->map(function ($tag) {
                return $tag->users;
            })->flatten();
        }

        return $this->operators;
    }

    /**
     * @return object
     */
    public function charts()
    {

        return (object)[
            'main'      => $this->mainChart(),
            'operators' => $this->operatorsCharts()
        ];
    }

    /**
     * @return mixed
     */
    private function mainChart()
    {
        $servicesLabels = [];
        $data = $this->records();
        foreach ($data->pluck('service')->unique() as $service) {
            $servicesLabels[$service] = OfferedServices::serviceName($service);
        };

        return Charts::database($this->records(), 'pie', 'highcharts')
            ->title('Typy nabízených služeb')
            ->dimensions(1000, 500)
            ->responsive(true)
            ->groupBy('service', null, $servicesLabels);
    }

    /**
     * @return mixed
     */
    private function operatorsCharts()
    {
        $data = $this->preparedDataForOperatorsChart();

        $chart = Charts::multi('bar', 'highcharts')
            ->colors(['#66CCFF', '#9966FF', '#E666FF', '#66FFE6', '#29B8FF', '#009CEB', '#66FF99', '#EB4E00', '#FF7029', '#FFE666', '#FF9966'])
            ->responsive(false)
            ->title('Nabízené služby po operátorech')
            ->dimensions(0, 500)
            ->labels($data->labels->toArray()); //Operátoři

        foreach ($data->services as $row) {
            $chart->dataset($row->name, $row->counts);
        }

        return $chart;
    }

    /**
     * @return object
     */
    private function preparedDataForOperatorsChart()
    {
        //$services = $this->records()->pluck('service')->unique();
        $operators = $this->records()->pluck('user.name', 'user_id')->unique()->sort();
        $op_data = $this->records()->groupBy('service');

        $services_data = $op_data->map(function ($items, $service) use ($operators) {
            $counts = $operators->map(function ($name, $id) use ($items) {
                return $items->where('user_id', $id)->count();
            });

            return (object)[
                'name'   => OfferedServices::serviceName($service),
                'counts' => $counts
            ];
        });

        return (object)[
            'labels'   => $operators->values(),
            'services' => $services_data->toArray()
        ];
    }


}

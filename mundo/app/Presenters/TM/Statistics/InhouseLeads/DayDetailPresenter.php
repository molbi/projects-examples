<?php

namespace Mundo\Presenters\TM\Statistics\InhouseLeads;

use Illuminate\Database\Connection;
use Illuminate\Http\Request;
use Mundo\Models\TM\Record;
use Mundo\Modules\TM\Sources\Support\SourceManager;

/**
 * Class CampaignsPresenter
 * @package Mundo\Presenters\TM\Statistics
 */
class DayDetailPresenter
{
    /**
     * @var SourceManager
     */
    private $source;
    /**
     * @var \Illuminate\Routing\Route|object|string
     */
    public $user;
    /**
     * @var \Illuminate\Routing\Route|object|string
     */
    private $day;


    /**
     * RecordsPresenter constructor.
     *
     * @param Request $request
     * @param SourceManager $source
     */
    public function __construct(Request $request, SourceManager $source)
    {

        $this->user = $request->route()->getParameter('inhouse_leads');
        $this->day = $request->route()->getParameter('day_detail');
        $this->source = $source;
    }

    /**
     * @return false|string
     */
    public function formattedDay()
    {
        return date('j. n. Y', strtotime($this->day));
    }

    public function records()
    {
        return Record::with('campaign', 'user', 'params')
            ->select(['tm_records.*'])
            ->where('user_id', $this->user->id)
            ->where($this->db()->raw('DATE(tm_records.created_at)'), '=', date('Y-m-d', strtotime($this->day)))
            ->get()
            ->withRemote();
    }

    /**
     * @return Connection
     */
    private function db()
    {
        return $this->source->get('b2m')->getConnection();
    }

}

<?php

namespace Mundo\Presenters\TM\Statistics;

use Cache;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Connection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Mundo\Models\Business\ItParam;
use Mundo\Models\TM\Campaign;
use Mundo\Models\TM\FtLeadStats;
use Mundo\Models\User;
use Mundo\Modules\TM\Sources\Support\SourceManager;

/**
 * Class CampaignsPresenter
 * @package Mundo\Presenters\TM\Statistics
 */
class CampaignsPresenter
{
    /**
     * @var Request
     */
    private $request;
    /**
     * @var SourceManager
     */
    private $source;

    private $campaignCache;
    private $overallCache;


    /**
     * RecordsPresenter constructor.
     *
     * @param Request $request
     * @param SourceManager $source
     */
    public function __construct(Request $request, SourceManager $source)
    {
        $this->request = $request;
        $this->source = $source;
    }

    /**
     * @return Carbon
     */
    public function fromDay()
    {
        return $this->request->has('from') ?
            Carbon::createFromTimestamp(strtotime($this->request->get('from'))) :
            Carbon::now()->startOfMonth();
    }

    /**
     * @return Carbon
     */
    public function toDay()
    {
        return $this->request->has('to') ?
            Carbon::createFromTimestamp(strtotime($this->request->get('to'))) :
            Carbon::now()->endOfMonth();
    }

    /**
     * @return Builder
     */
    public function data()
    {
        return FtLeadStats::with('campaign')
            ->select([
                'date_id',
                'campaign_id',
                'state',
                'user_id',
                $this->db()->raw('SUM(amount) AS amount')
            ])
            ->whereIn('campaign_id', ItParam::getInhouseLeadsCampaignIds())
            ->whereBetween($this->db()->raw('date_id'), [$this->fromDay()->format('Ymd'), $this->toDay()->format('Ymd')]);
    }

    /**
     * @return Collection
     */
    public function campaigns()
    {
        return $this->data()
            ->groupBy('campaign_id', 'state')
            ->get()
            ->groupBy('campaign_id');
    }

    public function campaign()
    {
        if ($this->campaignCache) {
            return $this->campaignCache;
        }

        $campaign = $this->request->route('scampaigns');
        return $this->campaignCache = $this->data()
            ->where('campaign_id', $campaign->campaign_id)
            ->groupBy('date_id', 'state')
            ->get();
    }

    /**
     * @return Collection
     */
    public function overall()
    {
        if ($this->overallCache) {
            return $this->overallCache;
        }

        return $this->overallCache = $this->data()
            ->groupBy('state')
            ->get();
    }

    /**
     * @return Connection
     */
    private function db()
    {
        return $this->source->get('b2m')->getConnection();
    }

}

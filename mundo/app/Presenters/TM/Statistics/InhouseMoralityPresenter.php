<?php

namespace Mundo\Presenters\TM\Statistics;

use Carbon\Carbon;
use Illuminate\Database\Connection;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Http\Request;
use Mundo\Models\User;
use Mundo\Modules\TM\Sources\Support\SourceManager;

/**
 * Class InhouseMoralityPresenter
 * @package Mundo\Presenters\TM\Statistics
 */
class InhouseMoralityPresenter
{
    /**
     * @var Request
     */
    private $request;
    /**
     * @var SourceManager
     */
    private $source;

    private $teamsTag = [
        'Hlavní sál',
        'Marek',
        'Inhouse3'
    ];

    /**
     * RecordsPresenter constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->source = app(SourceManager::class);
    }

    /**
     * @return Connection
     */
    private function db()
    {
        return $this->source->get('b2m')->getConnection();
    }

    public function teams()
    {
        return $this->teamsTag;
    }

    public function teamsOrders($tag)
    {
        $ids = User::withAnyTags([$tag])->pluck('id');

        return $this->getOrders($ids);
    }

    private function getOrders($ids)
    {
        return $this->db()->table('obis.Order AS O')
            ->select([
                'O.lang',
                $this->db()->raw('DATE(OH.changedAt) AS date'),
                $this->db()->raw('COUNT(*) AS count'),
                $this->db()->raw('SUM(S.price) AS price'),
            ])
            ->join('obis.OrderHistory AS OH', function (JoinClause $j) {
                $j->on('OH.order_id', '=', 'O.order_id')
                    ->where('OH.action', '=', 'paid');
            })
            ->join('obis.Order_Service AS OS', 'OS.order_id', '=', 'O.order_id')
            ->join('obis.Service AS S', 'S.service_id', '=', 'OS.service_id')
            ->whereIn('O.lang', ['cs', 'sk'])
            ->where('O.state', 'paid')
            ->whereIn('O.employee_id', $ids)
            ->whereDate('OH.changedAt', '>=', $this->fromDay())
            ->whereDate('OH.changedAt', '<=', $this->toDay())
            ->groupBy($this->db()->raw('DATE(OH.changedAt)'))
            ->groupBy('O.lang')
            ->orderBy('OH.changedAt')
            ->get();
    }

    public function fromDay()
    {
        return $this->request->has('from') ?
            Carbon::createFromTimestamp(strtotime($this->request->get('from')))->format('Y-m-d') :
            Carbon::now()->startOfMonth()->format('Y-m-d');
    }

    public function toDay()
    {
        return $this->request->has('to') ?
            Carbon::createFromTimestamp(strtotime($this->request->get('to')))->format('Y-m-d') :
            Carbon::now()->endOfMonth()->format('Y-m-d');
    }


}

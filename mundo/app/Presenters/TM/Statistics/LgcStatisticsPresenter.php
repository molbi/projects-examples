<?php

namespace Mundo\Presenters\TM\Statistics;

use Carbon\Carbon;
use ConsoleTVs\Charts\Facades\Charts;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Mundo\Models\TM\Record;
use Mundo\Models\User;
use Mundo\Widgets\TM\OfferedServices\OfferedServices;

/**
 * Class LgcStatisticsPresenter
 * @package Mundo\Presenters\TM\Statistics
 */
class LgcStatisticsPresenter
{
    /** @var User */
    protected $user;
    /** @var Collection */
    private $records;
    /** @var */
    private $operators;
    /** @var */
    private $orders;
    /**
     * @var Request
     */
    private $request;

    /**
     * RecordsPresenter constructor.
     *
     * @param User $user
     * @param Request $request
     */
    public function __construct(User $user, Request $request)
    {
        $this->user = $user;
        $this->request = $request;
    }

    /**
     * @return string
     */
    public function from()
    {
        $value = $this->request->get('from');
        $date = $value !== null ? Carbon::createFromTimestamp(strtotime($value)) : Carbon::now();

        return $date->format('Y-m-d');
    }

    /**
     * @return string
     */
    public function to()
    {
        $value = $this->request->get('to');
        $date = $value !== null ? Carbon::createFromTimestamp(strtotime($value)) : Carbon::now();

        return $date->format('Y-m-d');
    }

    /**
     * @return Collection
     */
    public function usersRecords()
    {
        if ($this->records === null) {
            $this->records = array_assoc(Record::select([
                'tm_records.*',
                'C.sync_id',
                DB::raw('count(*) AS total_records'),
                DB::raw('SUM(if(state = "accepted", 1, 0)) AS accepted')
            ])
                ->join('mundo.tm_campaigns AS C', 'C.id', '=', 'tm_records.campaign_id')
                ->whereIn('user_id', $this->operators()->pluck('id')->toArray())
                ->whereDate('tm_records.created_at', '>=', $this->from())
                ->whereDate('tm_records.created_at', '<=', $this->to())
                ->groupBY('user_id')
                ->groupBy('campaign_id')
                ->get()->map(function ($row) {
                    return (object)[
                        'user_id'       => $row->user_id,
                        'campaign_id'   => $row->sync_id,
                        'total_records' => $row->total_records,
                        'accepted'      => $row->accepted
                    ];
                })->toArray(), 'user_id|campaign_id');
        }

        return $this->records;
    }

    /**
     *
     */
    public function usersOrders()
    {
        if ($this->orders === null) {

            $this->orders = array_assoc(DB::table('obis.Order AS O')
                ->select([
                    'O.employee_id AS user_id',
                    DB::raw('CONVERT(REPLACE(source,"tm_campaign_", ""),UNSIGNED INTEGER) AS campaign_id'),
                    DB::raw('COUNT(O.order_id) AS total_count'),
                    DB::raw('SUM(I.price) AS total_price'),
                    DB::raw('SUM(IF(O.state = "paid", 1, 0)) AS paid_count'),
                    DB::raw('SUM(IF(O.state = "paid", I.price, 0)) AS paid_price'),
                ])
                ->join('obis.Invoice AS I', function (JoinClause $j) {
                    $j->on('I.order_id', '=', 'O.order_id')
                        ->where('I.price', '>', 0);
                })
                ->whereIn('O.employee_id', $this->operators()->pluck('id')->toArray())
                ->whereDate('O.createdOn', '>=', $this->from())
                ->whereDate('O.createdOn', '<=', $this->to())
                ->groupBy('O.employee_id')
                ->groupBy('O.source')
                ->get(), 'user_id|campaign_id');
        }

        return $this->orders;

    }

    /**
     * @return Collection
     */
    public function operators()
    {
        if ($this->operators === null) {
            $this->operators = $this->user->leadsUserTags->map(function ($tag) {
                return $tag->users;
            })->flatten();
        }

        return $this->operators;
    }
}

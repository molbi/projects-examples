<?php

namespace Mundo\Presenters\TM\Statistics;

use Cache;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Connection;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Mundo\Models\TM\Campaign;
use Mundo\Models\User;
use Mundo\Modules\TM\Sources\Support\SourceManager;

/**
 * Class LeadsSplitterPresenter
 * @package Mundo\Presenters\TM\Statistics
 */
class LeadsSplitterPresenter
{
    /**
     *
     */
    const TAG_LEADS = 'leady';

    /**
     * @var Request
     */
    private $request;
    /**
     * @var SourceManager
     */
    private $source;

    /**
     * @var array
     */
    private $stats = [];

    /**
     * @var array
     */
    private $teamsTag = [
        'Hlavní sál',
        'Marek',
        'Inhouse3'
    ];
    /**
     * @var
     */
    private $data;
    /**
     * @var
     */
    private $employees;
    /**
     * @var
     */
    private $campaigns;


    /**
     * RecordsPresenter constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->source = app(SourceManager::class);

        $this->prepareStats();
    }

    /**
     * @return array
     */
    public function stats()
    {
        return $this->stats;
    }

    /**
     * @return Carbon
     */
    public function fromDay()
    {
        return $this->request->has('from') ?
            Carbon::createFromTimestamp(strtotime($this->request->get('from'))) :
            Carbon::now();
    }

    /**
     * @return Carbon
     */
    public function toDay()
    {
        return $this->request->has('to') ?
            Carbon::createFromTimestamp(strtotime($this->request->get('to'))) :
            Carbon::now();
    }

    /**
     * @return Connection
     */
    private function db()
    {
        return $this->source->get('b2m')->getConnection();
    }


    /**
     * @return mixed
     */
    public function leadCampaigns()
    {
        if ($this->campaigns !== null) {
            return $this->campaigns;
        }

        return $this->campaigns = Campaign::withAnyTags(self::TAG_LEADS)->orderBy('sync_id')->get();
    }

    /**
     * @param $tag
     * @return mixed
     */
    private function employeesByTag($tag)
    {
        if (!is_null($this->employees) && isset($this->employees[$tag])) {
            return $this->employees[$tag];
        }

        return $this->employees[$tag] = User::withAnyTags([$tag])->get();
    }

    /**
     * @param null $user_id
     * @return Collection
     */
    private function data($user_id = null)
    {
        if (is_null($this->data)) {
            $this->data = collect($this->cache());
        }

        if (!is_null($user_id)) {
            return isset($this->data[$user_id]) ? $this->data[$user_id] : 0;
        }

        return $this->data;
    }

    /**
     *
     */
    private function prepareStats()
    {
        foreach ($this->teamsTag as $tag) {
            $this->initTeamStats($tag);
            $this->setEmployeesInTeam($tag);
        }
    }

    /**
     * @param $key
     * @param $value
     */
    private function setStats($key, $value)
    {
        $this->stats = array_add($this->stats, $key, $value);
    }

    /**
     * @return mixed
     */
    private function cache()
    {
        return array_assoc($this->db()->table('mundo.tm_record_histories AS H')
            ->select([
                'R.user_id',
                $this->db()->raw('COUNT(DISTINCT R.id) AS count')
            ])
            ->join('mundo.tm_records AS R', 'R.id', '=', 'H.record_id')
            ->whereDate('H.created_at', '>=', $this->fromDay()->format('Y-m-d'))
            ->whereDate('H.created_at', '<=', $this->toDay()->format('Y-m-d'))
            ->where('H.column', 'user_id')
            ->whereIn('R.campaign_id', $this->leadCampaigns()->pluck('id')->toArray())
            ->groupBy('R.user_id')
            ->get(), 'user_id=count');
    }

    /**
     * @param $tag
     */
    private function initTeamStats($tag)
    {
        if (!isset($this->stats[str_slug($tag)])) {
            $this->setStats(str_slug($tag), [
                'title'     => $tag,
                'employees' => [],
                'total'     => 0
            ]);
        }
    }

    private function setEmployeesInTeam($tag)
    {
        $employees = $this->employeesByTag($tag);
        foreach ($employees as $employee) {
            $total = $this->data($employee->id);
            $this->setStats(sprintf('%s.employees.%s', str_slug($tag), $employee->id), [
                'name'  => $employee->name,
                'total' => $total ? $total : 0
            ]);
            $this->stats[str_slug($tag)]['total'] += $total;
        }
    }
}

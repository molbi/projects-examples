<?php

namespace Mundo\Presenters\TM\Statistics\Campaigns;

use Illuminate\Database\Connection;
use Illuminate\Http\Request;
use Mundo\Models\TM\Record;
use Mundo\Modules\TM\Sources\Support\SourceManager;

/**
 * Class CampaignsPresenter
 * @package Mundo\Presenters\TM\Statistics
 */
class DayDetailPresenter
{
    /**
     * @var SourceManager
     */
    private $source;
    /**
     * @var \Illuminate\Routing\Route|object|string
     */
    public $campaign;
    /**
     * @var \Illuminate\Routing\Route|object|string
     */
    private $day;


    /**
     * RecordsPresenter constructor.
     *
     * @param Request $request
     * @param SourceManager $source
     */
    public function __construct(Request $request, SourceManager $source)
    {

        $this->campaign = $request->route()->getParameter('scampaigns');
        $this->day = $request->route()->getParameter('day_detail');
        $this->source = $source;
    }

    /**
     * @return false|string
     */
    public function formattedDay()
    {
        return date('j. n. Y', strtotime($this->day));
    }

    public function records()
    {
        return Record::with('campaign', 'user', 'params')
            ->select(['tm_records.*'])
            ->join('mundo.tm_campaigns AS C', 'C.id', '=', 'tm_records.campaign_id')
            ->where('C.sync_id', $this->campaign->campaign_id)
            ->where($this->db()->raw('DATE(tm_records.created_at)'), '=', date('Y-m-d', strtotime($this->day)))
            ->get()
            ->withRemote();
    }

    /**
     * @return Connection
     */
    private function db()
    {
        return $this->source->get('b2m')->getConnection();
    }

}

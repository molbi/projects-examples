<?php

namespace Mundo\Presenters\TM\Statistics;

use Carbon\Carbon;
use Illuminate\Database\Connection;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Mundo\Models\Business\ItParam;
use Mundo\Models\TM\Campaign;
use Mundo\Models\TM\FtLeadStats;
use Mundo\Models\User;
use Mundo\Modules\TM\Sources\Support\SourceManager;

/**
 * Class InhouseLeadsPresenter
 * @package Mundo\Presenters\TM\Statistics
 */
class InhouseLeadsPresenter
{
    /**
     *
     */
    const TAG_LEADS = 'leady';

    /**
     * @var Request
     */
    private $request;
    /**
     * @var SourceManager
     */
    private $source;

    /**
     * @var array
     */
    private $stats = [
        'total' => [],
        'teams' => []
    ];

    /**
     * @var array
     */
    private $teamsTag = [
        'Hlavní sál',
        'Marek',
        'Inhouse3'
    ];
    /**
     * @var
     */
    private $data;
    /**
     * @var
     */
    private $employees;
    private $userCache;


    /**
     * RecordsPresenter constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->source = app(SourceManager::class);

    }

    /**
     * @return array
     */
    public function stats()
    {
        $this->prepareStats();

        return $this->stats;
    }

    /**
     * @param $id
     * @return User
     */
    public function employeeById($id)
    {
        return collect($this->employees)
            ->flatten()
            ->where('id', $id)
            ->first();
    }

    public function user($groupBy)
    {
        if (isset($this->userCache[$groupBy]) && $this->userCache[$groupBy]) {
            return $this->userCache[$groupBy];
        }

        $user = $this->request->route('inhouse_leads');

        return $this->userCache = FtLeadStats::with('campaign')
            ->select([
                'date_id',
                'campaign_id',
                'state',
                'user_id',
                $this->db()->raw('SUM(amount) AS amount')
            ])
            ->where('user_id', $user->id)
            ->whereIn('campaign_id', ItParam::getInhouseLeadsCampaignIds())
            ->whereBetween($this->db()->raw('date_id'), [$this->fromDay()->format('Ymd'), $this->toDay()->format('Ymd')])
            ->groupBy($groupBy, 'state')
            ->get();
    }

    public function userCampaignChart()
    {
        return $this->user('campaign_id')->groupBy('campaign.title')->map(function ($items) {
            return $items->sum('amount');
        })->toArray();
    }

    /**
     * @return Carbon
     */
    public function fromDay()
    {
        return $this->request->has('from') ?
            Carbon::createFromTimestamp(strtotime($this->request->get('from'))) :
            Carbon::now()->startOfMonth();
    }

    /**
     * @return Carbon
     */
    public function toDay()
    {
        return $this->request->has('to') ?
            Carbon::createFromTimestamp(strtotime($this->request->get('to'))) :
            Carbon::now()->endOfMonth();
    }

    /**
     * @return Connection
     */
    private function db()
    {
        return $this->source->get('b2m')->getConnection();
    }


    /**
     * @return mixed
     */
    private function leadCampaigns()
    {
        return Campaign::withAnyTags(self::TAG_LEADS)->get();
    }

    /**
     * @param $tag
     * @return mixed
     */
    private function employeesByTag($tag)
    {
        if (!is_null($this->employees) && isset($this->employees[$tag])) {
            return $this->employees[$tag];
        }

        return $this->employees[$tag] = User::withAnyTags([$tag])->get();
    }

    /**
     * @return Collection
     */
    private function data()
    {
        if (!is_null($this->data)) {
            return $this->data;
        }

        return $this->data = collect($this->cache());
    }

    /**
     *
     */
    private function prepareStats()
    {
        $this->prepareTotals();
        $this->prepareTeams();
    }

    /**
     *
     */
    private function prepareTotals()
    {
        $this->setStats('total.all', $this->data()->sum('amount'));

        $this->data()->groupBy('state')->each(function (Collection $records, $state) {
            $this->setStats(sprintf('total.%s', $state), $records->sum('amount'));
        });
    }

    /**
     *
     */
    private function prepareTeams()
    {
        foreach ($this->teamsTag as $tag) {
            $ids = $this->employeesByTag($tag)->pluck('id')->toArray();
            $this->data()->whereIn('user_id', $ids)->groupBy('user_id')
                ->each(function (Collection $records, $user_id) use ($tag) {
                    $this->setStats(sprintf('teams.%s.%s.all', str_slug($tag), $user_id), $records->sum('amount'));
                    $records->groupBy('state')->each(function (Collection $records, $state) use ($user_id, $tag) {
                        $this->setStats(sprintf('teams.%s.%s.%s', str_slug($tag), $user_id, $state), $records->sum('amount'));
                    });
                });
        }
    }

    /**
     * @param $key
     * @param $value
     */
    private function setStats($key, $value)
    {
        $this->stats = array_add($this->stats, $key, $value);
    }

    /**
     * @return array
     */
    private function cache()
    {
        return FtLeadStats::with('campaign')
            ->select([
                'date_id',
                'campaign_id',
                'state',
                'user_id',
                $this->db()->raw('SUM(amount) AS amount')
            ])
            ->whereIn('user_id', User::withAnyTags($this->teamsTag)->get()->pluck('id')->toArray())
            ->whereIn('campaign_id', ItParam::getInhouseLeadsCampaignIds())
            ->whereBetween($this->db()->raw('date_id'), [$this->fromDay()->format('Ymd'), $this->toDay()->format('Ymd')])
            ->groupBy('state', 'user_id')
            ->get();
    }

}

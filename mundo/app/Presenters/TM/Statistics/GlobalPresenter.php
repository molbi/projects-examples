<?php

namespace Mundo\Presenters\TM\Statistics;

use Carbon\Carbon;
use Illuminate\Database\Connection;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Http\Request;
use Mundo\Models\User;
use Mundo\Modules\TM\Sources\Support\SourceManager;

/**
 * Class GlobalPresenter
 * @package Mundo\Presenters\TM\Statistics
 */
class GlobalPresenter
{
    /**
     * @var Request
     */
    private $request;
    /**
     * @var SourceManager
     */
    private $source;

    private $orders;
    private $employees;

    /**
     * RecordsPresenter constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->source = app(SourceManager::class);
    }

    public function orders()
    {
        if (!is_null($this->orders)) {
            return $this->orders;
        }

        $this->orders = collect($this->db()->table('obis.Order AS O')
            ->select([
                'O.employee_id',
                'E.realname',
                $this->db()->raw('IF(O.lang = "cs", S.price, S.price * 27) AS price')
            ])
            ->join('obis.Order_Service AS OS', 'OS.order_id', '=', 'O.order_id')
            ->join('zis.Employee AS E', 'E.employee_id', '=', 'O.employee_id')
            ->join('obis.Service AS S', function (JoinClause $j) {
                $j->on('S.service_id', '=', 'OS.service_id')
                    ->where('S.price', '>', 0);
            })
            ->whereDate('O.createdOn', '>=', $this->fromDay())
            ->whereDate('O.createdOn', '<=', $this->toDay())
            ->whereNotNull('O.employee_id')
            ->whereIn('O.employee_id', $this->employeesByTags()->pluck('id'))
            ->groupBy('O.order_id')
            ->get())
            ->sortBy('realname');

        return $this->orders;
    }

    public function getTags()
    {
        return $this->request->has('tags') ? $this->request->get('tags', null) : 'Hlavní sál,Marek,InHouse3';
    }

    public function employeesByTags()
    {
        if (!is_null($this->employees)) {
            return $this->employees;
        }

        $tags = $this->getTags();
        $this->employees = is_null($tags) ?
            User::withAnyTags(['hlavni-sal', 'inhouse-3', 'marek'])->with('params')->get() :
            User::withAnyTags(explode(',', $tags))->with('params')->get();

        return $this->employees;
    }

    /**
     * @return Connection
     */
    private function db()
    {
        return $this->source->get('b2m')->getConnection();
    }

    public function fromDay()
    {
        return $this->request->has('from') ?
            Carbon::createFromTimestamp(strtotime($this->request->get('from')))->format('Y-m-d') :
            Carbon::now()->startOfMonth()->format('Y-m-d');
    }

    public function toDay()
    {
        return $this->request->has('to') ?
            Carbon::createFromTimestamp(strtotime($this->request->get('to')))->format('Y-m-d') :
            Carbon::now()->endOfMonth()->format('Y-m-d');
    }

    public function monthlyTarget($operator)
    {
        /** @var User $ope */
        $ope = $this->employeesByTags()->where('id', $operator)->first();

        return is_null($ope) ? 0 : $ope->param(4);
    }

    public function monthlyTargetForAll()
    {
        $target = 0;
        foreach ($this->employeesByTags() as $operator) {
            $price = $operator->param(4);
//            dump($operator->name, $price);
            $target += $price;
        }

//        dd($target);

        return $target;


    }


}

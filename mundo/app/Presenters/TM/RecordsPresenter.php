<?php

namespace Mundo\Presenters\TM;

use Illuminate\Database\Eloquent\Collection;
use Mundo\Models\TM\Record;
use Mundo\Models\User;
use Mundo\Modules\TM\Sources\Support\SourceManager;

class RecordsPresenter
{
    /** @var \Mundo\Models\User */
    protected $user;
    /** @var \Mundo\Models\TM\Record */
    public $record;
    /** @var  Collection */
    protected $records;
    /** @var  SourceManager */
    protected $sources;

    public function __construct(User $user, Record $record = null)
    {
        $this->user = $user;
        $this->record = $record !== null ? $record : null;
    }

    /**
     * Record list for current user
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function recordsList()
    {
        if (!isset($this->records)) {
            $this->records = Record::unfinished()
                ->ownedByUser($this->user)
                ->monthAhead()
                ->ordered()
                ->with('histories', 'user', 'campaign', 'params', 'emailLogs')
                ->get()
                ->withRemote();
        }

        return $this->records;
    }

    /**
     * Does user have any records?
     * @return bool
     */
    public function hasRecords()
    {
        return !$this->recordsList()->isEmpty();
    }


    public function __get($name)
    {
        return $this->record->$name;
    }
}

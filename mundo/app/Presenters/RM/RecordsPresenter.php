<?php

namespace Mundo\Presenters\RM;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Mundo\Models\Module;
use Mundo\Models\RM\Record;
use Mundo\Models\User;

/**
 * Class RecordsPresenter
 * @package Mundo\Presenters\RM
 */
class RecordsPresenter
{
    /** @var Record|null */
    public $record;
    /** @var Module */
    public $module;
    /** @var  Collection */
    protected $records;
    /** @var User */
    protected $user;

    /**
     * RecordsPresenter constructor.
     *
     * @param User        $user
     * @param Record|null $record
     * @param Module|null $module
     */
    public function __construct(User $user, Record $record = null, Module $module = null)
    {
        $this->user = $user;
        $this->record = $record !== null ? $record : null;
        $this->module = $module;
    }

    /**
     * Record list for current user
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function recordsList()
    {
        if (!isset($this->records)) {
            $this->records = Record::unfinished()
                ->ownedByUser($this->user)
                ->select([
                    '*',
                    DB::raw('MONTH(created_at) AS month')
                ])
                ->ordered()
                ->with('histories', 'user', 'firm')
                ->get();
        }

        return $this->records;
    }

    /**
     * Does user have any records?
     * @return bool
     */
    public function hasRecords()
    {
        return !$this->recordsList()->isEmpty();
    }


    /**
     * @param $name
     *
     * @return mixed
     */
    public function __get($name)
    {
        return $this->record->$name;
    }
}

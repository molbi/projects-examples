<?php

namespace Mundo\Presenters\Ris;

use Illuminate\Database\Eloquent\Collection;
use Mundo\Models\RIS\RenewalFirm;
use Mundo\Models\User;
use Mundo\Modules\TM\Sources\Support\SourceManager;

class RecordsPresenter
{
    /** @var \Mundo\Models\User */
    protected $user;
    /** @var RenewalFirm|null  */
    public $record;
    /** @var  Collection */
    protected $records;
    /** @var  SourceManager */
    protected $sources;

    public function __construct(User $user, RenewalFirm $record = null)
    {
        $this->user = $user;
        $this->record = $record !== null ? $record : null;
    }

    /**
     * Record list for current user
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function recordsList()
    {
        if (!isset($this->records)) {
            $this->records = RenewalFirm::unfinished()
                ->ownedByUser($this->user)
                ->ordered()
                ->with('histories', 'user', 'firm', 'activeServices')
                ->get();
        }

        return $this->records;
    }

    /**
     * Does user have any records?
     * @return bool
     */
    public function hasRecords()
    {
        return !$this->recordsList()->isEmpty();
    }


    public function __get($name)
    {
        return $this->record->$name;
    }
}

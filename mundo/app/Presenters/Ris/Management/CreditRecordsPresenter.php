<?php

namespace Mundo\Presenters\Ris\Management;

use DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Mundo\Models\TM\Campaign;
use Mundo\Models\TM\Record;
use Mundo\Models\User;
use Mundo\Modules\TM\Sources\Eloquent\Collection as B2MCollection;
use Mundo\Modules\TM\Sources\Support\SourceManager;
use Mundo\Widgets\TM\B2M\Constants;

/**
 * Class ForwardingPresenter
 * @package Mundo\Presenters
 */
class CreditRecordsPresenter
{
    /** @var  Collection */
    protected $operators;

    /** @var  SourceManager */
    protected $sources;
    /**
     * @var Request
     */
    private $request;

    /** @var  Collection */
    private $campaigns;

    protected $recordsList;

    /**
     * ForwardingPresenter constructor.
     *
     * @param User    $leader
     * @param Request $request
     */
    public function __construct(User $leader, Request $request)
    {
        $this->leader = $leader;
        $this->request = $request;

        $this->initCampaigns();
    }

    /**
     *
     */
    public function recordsList()
    {
        if ($this->recordsList !== null) {
            return $this->recordsList;
        }

        $this->recordsList = $this->applyCollectionFilters(
            $this->applyFilters($this->recordsListQuery())
                ->with('histories')
                ->get()
                ->withRemote()
                ->loadServices()
        )->paginate(50);

        return $this->recordsList;
    }

    /**
     * @return Collection
     */
    public function operatorsList()
    {
        $this->operators = new Collection();

        $tags = $this->leader->leadsUserTags()->with('users')->get();

        if (!$tags->isEmpty()) {
            $this->operators = $tags->map(function ($tag) {
                return $tag->users;
            })->flatten();
        }

        return $this->operators;
    }

    public function campaignsForSelect()
    {
        return $this->campaigns;
    }

    private function applyFilters($query)
    {
        if ($from = $this->request->get('from')) {
            $query->whereDate('created_at', '>=', $from);
        }

        if ($from = $this->request->get('to')) {
            $query->whereDate('created_at', '<=', $from);
        }

        if ($included = $this->request->get('included')) {
            $query->whereIn('campaign_id', $included);
        }

        if ($excluded = $this->request->get('excluded')) {
            $query->whereNotIn('campaign_id', $excluded);
        }

        return $query;
    }

    private function applyCollectionFilters(B2MCollection $records)
    {
        if ($lang = $this->request->get('lang')) {
            $records = $records->where('remoteData.lang', $lang);
        }

        if ($services = $this->request->get('services')) {
            $records = $records->filter(function(Record $record) use ($services) {
                $count = $record->hasActiveServicesWithout([Constants::SERVICE_CSKONTAKT, Constants::SERVICE_CSKONTAKT_GOLDEN]);
                if(($services == 1 && $count == 0) || ($services == 2 && $count > 0)) {
                    return true;
                }
                return false;
            });
        }

        return $records;
    }

    /**
     * @return mixed
     */
    private function recordsListQuery()
    {
        return Record::forwardableLeads()
            ->with('campaign', 'params')
            ->whereIn('campaign_id', $this->campaigns->toArray())
            ->whereNull('user_id')
            ->orderBy('created_at');
    }

    private function initCampaigns()
    {
        $this->campaigns = new Collection();

        $tags = $this->leader->leadsCampaignTags()->with('tmCampaigns')->get();

        if (!$tags->isEmpty()) {
            $this->campaigns = $tags->map(function ($tag) {
                return $tag->tmCampaigns()->get();
            })->flatten()->pluck('id');
        }
    }
}

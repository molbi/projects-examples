<?php

namespace Mundo\Filters;


use Carbon\Carbon;
use Illuminate\Support\Collection;

/**
 * Class RecordFilter
 * @package Mundo\Filters
 */
class BaseFilters extends QueryFilter
{

    /**
     * @var array
     */
    public $defaults = [
        'from'       => null,
        'to'         => null,
    ];

    /**
     * @param null $value
     */
    public function from($value = null)
    {
        $date = $value !== null ? Carbon::createFromTimestamp(strtotime($value)) : Carbon::now();

        $this->builder->whereDate('created_at', '>=', $date->format('Y-m-d'));
    }

    /**
     * @param null $value
     */
    public function to($value = null)
    {
        $date = $value !== null ? Carbon::createFromTimestamp(strtotime($value)) : Carbon::now();

        $this->builder->whereDate('created_at', '<=', $date->format('Y-m-d'));
    }

    /**
     *
     */
    public function latest()
    {
        $this->builder->orderBy('updated_at', 'desc');
    }

    /**
     * @param $value
     */
    public function user_id($value = null)
    {
        if (is_a($value, Collection::class)) {
            $this->builder->whereIn('user_id', $value->pluck('id')->toArray());
        }
    }

    public function operator($value = null)
    {
        if ($value !== null) {
            $this->builder->where('user_id', $value);
        }
    }

    /**
     * @param $value
     */
    public function state($value)
    {
        $this->builder->where('state', $value);
    }

    /**
     *
     */
    public function withoutNew()
    {
        $this->builder->whereNotIn('state', ['new']);
    }

    /**
     * @param array $defaults
     */
    public function setDefaults(array $defaults)
    {
        $this->defaults = array_merge($this->defaults, $defaults);
    }
}
<?php

namespace Mundo\Kernel\Ajax\Http;

use Illuminate\Http\Response as BaseResponse;

/**
 * Class Response
 * @package Sion\Http
 */
class Response extends BaseResponse
{
    use Ajaxify;

    /**
     * @param ...$selectors
     *
     * @return $this
     */
    public function ajaxReplace(...$selectors)
    {
        return $this->addPayload('replace', $selectors, true);
    }

    /**
     * @param $selector
     *
     * @return $this
     */
    public function ajaxTab($selector)
    {
        return $this->addPayload('tab', $selector, true);
    }

    /**
     * @param $id
     *
     * @return $this
     */
    public function ajaxModal($id)
    {
        $this->payload['modal'] = $id;

        return $this;
    }

    /**
     * @param $what
     * @param $where
     *
     * @return $this
     */
    public function ajaxAppend($what, $where)
    {
        return $this->addPayload('append', [$what, $where], false);
    }

    /**
     * @param ...$selectors
     *
     * @return $this
     */
    public function ajaxRemove(...$selectors)
    {
        return $this->addPayload('remove', $selectors);
    }


    /**
     * @param $selector
     *
     * @return $this
     */
    public function ajaxScrollTo($selector)
    {
        $this->payload['scrollTo'] = $selector;

        return $this;
    }

    /**
     * @return mixed
     */
    public function sendContent()
    {
        if ($this->handlePayload()) {
            $this->payload['content'] = $this->content;
            $this->content = json_encode($this->payload);
        }

        return parent::sendContent();
    }
}

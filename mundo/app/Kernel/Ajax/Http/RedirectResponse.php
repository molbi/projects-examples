<?php

namespace Mundo\Kernel\Ajax\Http;

use Illuminate\Http\RedirectResponse as BaseResponse;

/**
 * Class RedirectResponse
 * @package Sion\Http
 */
class RedirectResponse extends BaseResponse
{
    use Ajaxify;

    /**
     * Přesměrování na jinou stárnku
     *
     * @return $this
     */
    public function ajaxRedirect()
    {
        $this->payload['redirect'] = $this->headers->get('Location');
        $this->headers->remove('Location');

        return $this;
    }

    /**
     * @return mixed
     */
    public function sendContent()
    {
        if ($this->handlePayload()) {
            $this->payload['content'] = $this->content;
            $this->content = json_encode($this->payload);
        }

        return parent::sendContent();
    }


    /**
     * Reload stárnky
     *
     * @return $this
     */
    public function ajaxReload()
    {
        $this->payload['reload'] = true;
        $this->headers->remove('Location');

        return $this;
    }
}

<?php

namespace Mundo\Kernel\Ajax\Http;

use Illuminate\Contracts\View\View;

/**
 * Class Ajaxify
 *
 * @package Mundo\Kernel\Ajax\Http
 */
trait Ajaxify
{
    /**
     * Nese si v sobě hodnoty pro AJAX payload
     *
     * @var  array
     */
    protected $payload = [];

    /**
     * Signalizuje, jestli byl request ajax
     *
     * @var bool
     */
    protected $handlePayload;

    /**
     * @return $this
     */
    public function ajaxCloseModal($selector = null)
    {
        $this->payload['closeModal'] = true;

        if($selector) {
            $this->payload['closeModalSelector'] = $selector;
        }

        return $this;
    }

    /**
     * @return boolean
     */
    public function handlePayload($handle = null)
    {
        if (is_bool($handle)) {
            $this->handlePayload = $handle;

            return $this;
        }

        return $this->handlePayload;
    }

    /**
     * @param View $view
     *
     * @return $this
     */
    public function ajaxEval(View $view)
    {
        $this->addPayload('eval', $view->render());

        return $this;
    }


    /**
     * @return mixed
     */
    public function sendHeaders()
    {
        if ($this->handlePayload()) {
            $this->header('Content-Type', 'application/json');
            $this->setStatusCode(200);
        }

        return parent::sendHeaders();
    }

    /**
     * @param           $key
     * @param null $value
     * @param bool|true $flatten
     *
     * @return $this
     */
    public function addPayload($key, $value = null, $flatten = true)
    {
        if (!isset($this->payload[$key])) {
            $this->payload[$key] = [];
        }
        $this->payload[$key][] = $value;
        if (is_array($value) && $flatten) {
            $this->payload[$key] = array_flatten($this->payload[$key]);
        }

        return $this;
    }
}

<?php

namespace Mundo\Kernel\Ajax\Routing;

use Illuminate\Contracts\View\Factory as ViewFactory;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector as BaseRedirector;
use Illuminate\Routing\ResponseFactory as BaseFactory;
use Mundo\Kernel\Ajax\Http\Response;

/**
 * Class ResponseFactory
 * @package Sion\Routing
 */
class ResponseFactory extends BaseFactory
{
    /** @var  Request */
    protected $request;

    /**
     * ResponseFactory constructor.
     *
     * @param ViewFactory $view
     * @param BaseRedirector $redirector
     * @param Request $request
     */
    public function __construct(ViewFactory $view, BaseRedirector $redirector, Request $request)
    {
        parent::__construct($view, $redirector);
        $this->request = $request;
    }


    /**
     * Return a new response from the application.
     *
     * @param  string $content
     * @param  int $status
     * @param  array $headers
     *
     * @return \Illuminate\Http\Response
     */
    public function make($content = '', $status = 200, array $headers = [])
    {
        $response = new Response($content, $status, $headers);
        $response->handlePayload($this->request->ajax() && !$this->request->pjax());

        return $response;
    }
}

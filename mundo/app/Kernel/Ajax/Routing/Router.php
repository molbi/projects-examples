<?php

namespace Mundo\Kernel\Ajax\Routing;

use Illuminate\Routing\Router as BaseRouter;
use Mundo\Kernel\Ajax\Http\Response;
use Psr\Http\Message\ResponseInterface as PsrResponseInterface;
use Symfony\Bridge\PsrHttpMessage\Factory\HttpFoundationFactory;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

/**
 * Class Router
 * @package Sion\Routing
 */
class Router extends BaseRouter
{
    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param mixed $response
     *
     * @return SymfonyResponse
     */
    public function prepareResponse($request, $response)
    {
        if ($response instanceof PsrResponseInterface) {
            $response = (new HttpFoundationFactory)->createResponse($response);
        } elseif (!$response instanceof SymfonyResponse) {
            $response = new Response($response);
            $response->handlePayload($request->isXmlHttpRequest() && $request->headers->get('X-PJAX') != true);
        }

        return $response->prepare($request);
    }
}

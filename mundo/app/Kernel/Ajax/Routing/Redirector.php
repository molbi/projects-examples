<?php

namespace Mundo\Kernel\Ajax\Routing;

use Illuminate\Routing\Redirector as BaseRedirector;
use Mundo\Kernel\Ajax\Http\RedirectResponse;

/**
 * Class Redirector
 * @package Sion\Routing
 */
class Redirector extends BaseRedirector
{
    /**
     * @param string $path
     * @param int $status
     * @param array $headers
     *
     * @return RedirectResponse
     */
    protected function createRedirect($path, $status, $headers)
    {
        $redirect = new RedirectResponse($path, $status, $headers);
        $redirect->handlePayload($this->generator->getRequest()->ajax() && !$this->generator->getRequest()->pjax());
        if (isset($this->session)) {
            $redirect->setSession($this->session);
        }

        $redirect->setRequest($this->generator->getRequest());

        return $redirect;
    }
}

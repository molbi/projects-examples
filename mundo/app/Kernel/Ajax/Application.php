<?php

namespace Mundo\Kernel\Ajax;

use Illuminate\Events\EventServiceProvider;
use Illuminate\Foundation\Application as BaseApplication;
use Mundo\Kernel\Ajax\Routing\RoutingServiceProvider;

/**
 * Class Application
 *
 * @package Sion
 * @author  Jakub Truneček <jakub.trunecek@gmail.com>
 * @author  Kuba Havle <kuba@havle.cz>
 */
class Application extends BaseApplication
{
    /**
     * Register own RoutingServiceProvider
     */
    protected function registerBaseServiceProviders()
    {
        $this->register(new EventServiceProvider($this));
        $this->register(new RoutingServiceProvider($this));
    }
}

<?php

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\HtmlString;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Mundo\Kernel\Constants;
use Mundo\Models\RM\Record as RMRecord;
use Mundo\Models\TM\Reminder;
use Mundo\Widgets\TM\B2M\Constants as TMConstants;
use Mundo\Models\RIS\RenewalFirm;
use Mundo\Models\TM\Record;
use Mundo\Models\User;

if (!function_exists('num_cpus')) {
    function num_cpus()
    {
        $numCpus = 1;
        if (is_file('/proc/cpuinfo')) {
            $cpuinfo = file_get_contents('/proc/cpuinfo');
            preg_match_all('/^processor/m', $cpuinfo, $matches);
            $numCpus = count($matches[0]);
        } else {
            if ('WIN' == strtoupper(substr(PHP_OS, 0, 3))) {
                $process = @popen('wmic cpu get NumberOfCores', 'rb');
                if (false !== $process) {
                    fgets($process);
                    $numCpus = intval(fgets($process));
                    pclose($process);
                }
            } else {
                $process = @popen('sysctl -a', 'rb');
                if (false !== $process) {
                    $output = stream_get_contents($process);
                    preg_match('/hw.ncpu: (\d+)/', $output, $matches);
                    if ($matches) {
                        $numCpus = intval($matches[1][0]);
                    }
                    pclose($process);
                }
            }
        }

        return $numCpus;
    }
}


if (!function_exists('array_assoc')) {
    function array_assoc($array, $descriptor)
    {
        $data = null;
        $descriptors = preg_split('#(\[\]|->|=|\|)#', $descriptor, null,
            PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
        $row = array_first($array);
        if (!$row) {
            return [];
        }
        $as = '';
        // check columns
        foreach ($descriptors as $as) {
            // offsetExists ignores NULL in PHP 5.2.1, isset() surprisingly NULL accepts
            if ($as !== '[]' && $as !== '=' && $as !== '->' && $as !== '|' && !property_exists($row, $as)) {
                throw new InvalidArgumentException("Unknown column '$as' in associative descriptor.");
            }
        }

        if ($as === '->') { // must not be last
            array_pop($descriptors);
        }

        if (empty($descriptors)) {
            $descriptors[] = '[]';
        }

        foreach ($array as $row) {
            $x = &$data;
            foreach ($descriptors as $i => $as) {
                if ($as === '[]') { // indexed-array node
                    $x = &$x[];
                } elseif ($as === '=') { // "value" node
                    $x = $row->{$descriptors[$i + 1]};
                    continue 2;
                } elseif ($as === '->') { // "object" node
                    if ($x === null) {
                        $x = clone $row;
                        $x = &$x->{$descriptors[$i + 1]};
                        $x = null; // prepare child node
                    } else {
                        $x = &$x->{$descriptors[$i + 1]};
                    }
                } elseif ($as !== '|') { // associative-array node
                    $x = &$x[$row->$as];
                }
            }
            if ($x === null) { // build leaf
                $x = $row;
            }
        }

        return $data;
    }
}


if (!function_exists('get_sql')) {
    /**
     * @param $builder
     *
     * @return mixed
     */
    function get_sql($builder)
    {
        $sql = $builder->toSql();
        foreach ($builder->getBindings() as $binding) {
            $value = is_numeric($binding) ? $binding : "'" . $binding . "'";
            $sql = preg_replace('/\?/', $value, $sql, 1);
        }

        return $sql;
    }
}

if (!function_exists('uuid')) {
    function uuid($data = null)
    {
        return Webpatser\Uuid\Uuid::generate(4)->string;
    }
}

if (!function_exists('record_flag')) {
    function record_flag(Model $record)
    {
        switch (get_class($record)) {
            case RenewalFirm::class:
                $lang = $record->firm->lang;
                break;
            case Record::class:
                if (!isset($record->remoteData->lang)) {
                    dd($record);
                }
                $lang = $record->remoteData->lang;
                break;
            case RMRecord::class:
                $lang = $record->firm->lang;
                break;

        }

        return flag($lang);
    }
}

if (!function_exists('flag')) {
    function flag($lang)
    {
        $lang = $lang === 'cs' ? 'cz' : $lang;

        return new HtmlString(sprintf("<span class=\"flag-icon flag-icon-%s\"></span>", $lang));
    }
}

if (!function_exists('record_label')) {
    function record_label(Model $record)
    {
        switch(get_class($record)) {
            case RenewalFirm::class:
                $state = $record->state;
                break;
            case Record::class:
                $state = $record->state;
                $hist = $record->histories->filter(function ($history) {
                    return in_array($history->column, ['state', 'user_id']);
                })->first();
                if ($hist) {
                    $state = $hist->column === 'state' ? $hist->next : 'forwarded';
                }
                break;
            case RMRecord::class:
                $state = $record->state;
                break;
        }


        return state_label($state);
    }
}

if (!function_exists('state_label')) {
    function state_label($state, $long = false, $addition_text = null)
    {
        $label = Constants::LABELS[$state];

        return new HtmlString(sprintf(
            "<span title=\"%s\" class=\"label label-%s\">%s%s</span>",
            $label[2],
            $label[1],
            $long ? $label[2] : $label[0],
            $addition_text
        ));
    }
}


if (!function_exists('record_title')) {
    /**
     * @param Model $record
     *
     * @return mixed
     */
    function record_title($record)
    {
        switch (get_class($record)) {
            case RenewalFirm::class:
                return $record->firm->title;
                break;
            case Record::class:
                return isset($record->remoteData->company) ? $record->remoteData->company : $record->remoteData->contact_person;
                break;
            case Reminder::class:
                return $record->subject_company;
                break;

            case RMRecord::class:
                return $record->firm->title;
                break;
        }
    }
}


if (!function_exists('dom_id')) {
    /**
     * The DOM id convention is to use the singular form of an object or class with the id following an underscore.
     * If no id is found, prefix with “create_” instead.
     *
     * @param  object|\Illuminate\Database\Eloquent\Model $record
     * @param  string $prefix
     * @param  string $fallbackPrefix By default it's 'create'
     *
     * @return string
     */
    function dom_id($record, $prefix = null, $fallbackPrefix = 'create')
    {
        return Mundo\Kernel\Html\EloquentHelper::domId($record, $prefix, $fallbackPrefix);
    }
}
if (!function_exists('form_id')) {
    /**
     * The Form id convention is to use the singular form of an object or class with the id following an underscore.
     * If id is found, prefix with “edit_”.
     * If no id is found, prefix with “create_” instead.
     *
     * @param  object|\Illuminate\Database\Eloquent\Model $record
     * @param  string $fallbackPrefix By default it's 'create'
     *
     * @return string
     */
    function form_id($record, $fallbackPrefix = 'create')
    {
        return Mundo\Kernel\Html\EloquentHelper::formId($record, $fallbackPrefix);
    }
}
if (!function_exists('record_key_for_dom_id')) {
    /**
     * @param  object|\Illuminate\Database\Eloquent\Model $record
     *
     * @return string
     */
    function record_key_for_dom_id($record)
    {
        return Mundo\Kernel\Html\EloquentHelper::recordKeyForDomId($record);
    }
}

if (!function_exists('dom_class')) {
    /**
     * The DOM class convention is to use the singular form of an object or class.
     *
     * @param  string|object|\Illuminate\Database\Eloquent\Model $recordOrClass
     * @param  string $prefix
     *
     * @return string
     */
    function dom_class($recordOrClass, $prefix = null)
    {
        return Mundo\Kernel\Html\EloquentHelper::domClass($recordOrClass, $prefix);
    }
}

if (!function_exists('send_mail_url')) {
    function send_mail_url(Model $target, array $suggestions = [], $allowSelection = false)
    {
        return action('EmailTemplatesController@select', [
            get_class($target),
            $target->id,
            'suggestions'    => $suggestions,
            'allowSelection' => $allowSelection,
        ]);
    }
}

if (!function_exists('operator')) {
    function operator($user_id)
    {
        return User::where('id', $user_id)->first();
    }
}

if (!function_exists('service_title')) {
    function service_title($service)
    {
        return TMConstants::SERVICE_TITLES[$service];
    }
}

if (!function_exists('datetime')) {
    function datetime($date, $format = 'd. m. Y H:i')
    {
        if (is_string($date)) {
            $date = Carbon::parse($date);
        }
        if (!$date instanceof Carbon) {
            throw new InvalidArgumentException('Date have to be string or Carbon instance');
        }

        return $date->format($format);
    }
}

if (!function_exists('phone_number')) {
    function phone_number($numbers, $country = 'CZ')
    {
        $numbers = preg_split('|,( )*|', $numbers);
        /** @var PhoneNumberUtil $util */
        $util = App::make('phonenumberlib');
        $result = [];
        foreach ($numbers as $number) {
            if (preg_match('|^[\+0-9 ]+$|', $number)) {
                try {
                    $p = $util->parse($number, $country);
                    $number = $util->format($p, PhoneNumberFormat::INTERNATIONAL);
                    $result[] = '<a href="callto:' . str_replace(['+', ' ', '/'], ['00', '', ''],
                            $number) . '">' . $number . '</a>';
                } catch (Throwable $e) {
                    $result[] = $number;
                }
            } else {
                $result[] = $number;
            }
        }

        return new HtmlString(implode(', ', $result));
    }
}

if (!function_exists('lang_to_country')) {
    function lang_to_country($lang)
    {
        if ($lang === 'cs') {
            return 'CZ';
        }

        return strtoupper($lang);
    }
}

if (!function_exists('due_class')) {
    function due_class($days)
    {
        if ($days >= 3 && $days <= 9) {
            return 'reminder-success';
        } elseif ($days >= 10 && $days <= 13) {
            return 'reminder-info';
        } elseif ($days >= 14 && $days <= 19) {
            return 'reminder-warning';
        } elseif ($days >= 20) {
            return 'reminder-danger';
        }

        return 'reminder-default';
    }
}

if (!function_exists('ddb')) {
    function ddb() {
        \DB::listen(function($e) { echo $e->sql . " (" . json_encode($e->bindings) . ") \n"; });
    }
}

if (!function_exists('tags_select')) {
    function tags_select(Model $model, $name = 'tags') {
        return new HtmlString(Form::text(
            $name, $model->tagsLists()->implode(','),
            ['v-tags', 'data-options' => json_encode(Mundo\Models\Tag::optionsForSelect())]
        ));
    }
}

if(!function_exists('company_url')) {
    function company_url($link) {
        return preg_match('/^(http|https):\/\//', $link) ? $link :  'http://' . $link;
    }
}

if(!function_exists('time_since')) {
    function time_since($time)
    {
        $chunks = array(
            //array(60 * 60 * 24 * 365 , 'rok'),
            array(60 * 60 * 24 * 30 , 'měsíců'),
            array(60 * 60 * 24 , 'den'),
            array(60 * 60 , 'hodina'),
            array(60 , 'minuta'),
            array(1 , 'sekunda')
        );

        for ($i = 0, $j = count($chunks); $i < $j; $i++) {
            $seconds = $chunks[$i][0];
            $name = $chunks[$i][1];
            if (($count = floor($time / $seconds)) != 0) {
                break;
            }
        }

        $print = ($count == 1) ? '1 '.$name : "$count {$name}";
        return $print;
    }
}
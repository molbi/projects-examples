<?php

namespace Mundo\Kernel\Sync;

use Illuminate\Database\DatabaseManager;
use Mundo\Models\TM\RecordHistory;

class RecordHistoryObserver
{
    const STATE_MAP = [
        'accepted'    => 'accepted',
        'declined'    => 'declined',
        'removed'     => 'removed',
        'new'         => 'pending',
        'not-reached' => 'unreached',
        'reached'     => 'reached',
    ];

    /** @var  DatabaseManager */
    protected $manager;
    /** @var  integer */
    protected $remoteId;

    public function __construct(DatabaseManager $manager)
    {
        $this->manager = $manager;
    }

    public function created(RecordHistory $history)
    {
        // To sync this we have to:
        // 1. reflect the change to Stack
        // 2. insert row to History
        // 3. save this to tm_syncs
        if (in_array($history->column, ['user_id', 'state', 'next_call'])) {
            $this->updateStack($history);
            if ($id = $this->createHistory($history)) {
                $this->createSync($id, $history);
            }
        }
    }

    protected function db()
    {
        return $this->manager->connection();
    }

    protected function b2mDb()
    {
        return $this->manager->connection('b2m');
    }

    protected function updateStack(RecordHistory $history)
    {
        $data = [];
        switch ($history->column) {
            case 'state':
                $data['state'] = self::STATE_MAP[$history->next];
                break;
            case 'next_call':
                $data['callDate'] = $history->next;
                break;
            case 'user_id':
                $data['state'] = 'forwarded';
                $data['employee_id'] = $history->next;
                $data['assignedAt'] = $history->created_at;
                break;
        }
        
        $this->b2mDb()->table('telemarketing.Stack')
            ->where('stack_id', '=', $this->remoteId($history))
            ->update($data);
    }

    protected function createHistory(RecordHistory $history)
    {
        if (in_array($history->column, ['user_id', 'state'])) {
            $data = [
                'stack_id'    => $this->remoteId($history),
                'createdAt'   => $history->created_at,
                'note'        => $history->note,
                'action'      => $history->column == 'user_id' ? 'forwarded' : self::STATE_MAP[$history->next],
                // This is on purpose, old system works this way!
                'employee_id' => $history->column == 'user_id' ? $history->next : $history->user_id,
            ];

            return $this->b2mDb()->table('telemarketing.History')->insertGetId($data);
        }
    }

    protected function createSync($id, RecordHistory $history)
    {
        $this->db()->table('tm_syncs')
            ->insert([
                'remote_id'   => $id,
                'local_id'    => $history->id,
                'campaign_id' => $history->record->campaign_id,
                'table'       => $history->getTable(),
                'direction'   => 'out',
            ]);
    }

    /**
     * Converts local ID to remote one
     *
     * @param \Mundo\Models\TM\RecordHistory $history
     * @return int
     */
    protected function remoteId(RecordHistory $history)
    {
        if (!isset($this->remoteId)) {
            $record = $this->db()->table('tm_syncs')
                ->where('table', '=', 'tm_records')
                ->where('local_id', '=', $history->record_id)
                ->where('campaign_id', '=', $history->record->campaign_id)
                ->first();

            $this->remoteId = $record ? $record->remote_id : null;
        }

        return $this->remoteId;
    }
}

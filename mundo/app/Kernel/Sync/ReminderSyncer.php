<?php

namespace Mundo\Kernel\Sync;

use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Query\Builder;
use Mundo\Models\TM\Campaign as CampaignModel;
use Mundo\Models\TM\ReminderHistory;

class ReminderSyncer
{
    const TYPES_MAP = [
        'obis.Order.order_id'    => 'b2m',
        'cs_contact.invoices.id' => 'kartus',
    ];
    /** @var  DatabaseManager */
    protected $manager;
    /** @var  integer */
    protected $lastSyncedTimestamp;
    /** @var  integer */
    protected $count;

    /**
     * Count of steps
     *
     * @return integer
     */
    public function count()
    {
        if (!isset($this->count)) {
            $this->count = $this->changesSelect()->count();
        }

        return $this->count;
    }

    /**
     * Do the work and report progress by closure
     * @param \Closure $closure
     * @return void
     */
    public function work(\Closure $closure)
    {
        $index = 0;
        do {
            $items = $this->changesBatch($index++, 1000);
            $this->db()->beginTransaction();
            $histories = [];
            $syncs = [];
            foreach ($items as $item) {
                $histories[] = $history = $this->historyData($item);
                $syncs[] = $this->syncData($history['id'], $item);
            }
            $this->db()->table('tm_reminder_histories')->insert($histories);
            $this->db()->table('tm_syncs')->insert($syncs);
            $this->db()->commit();
            $closure && $closure(count($items));
        } while (!empty($items));
    }

    /**
     * Insert data for sync record
     *
     * @param  $id
     * @param  object $item
     * @return array
     */
    protected function syncData($id, $item)
    {
        return [
            'remote_id'   => $item->orderReminderingHistory_id,
            'local_id'    => $id,
            'direction'   => 'in',
            'campaign_id' => null,
            'table'       => 'tm_reminder_histories',
            'created_at'  => $item->created_at,
        ];
    }


    protected function historyData($item)
    {
        return [
            'id'         => uuid(),
            'type'       => self::TYPES_MAP[$item->type],
            'remote_id'  => $item->order_id,
            'action'     => 'reached',
            'user_id'    => null,
            'note'       => $item->note,
            'created_at' => $item->created_at,
            'updated_at' => $item->created_at,
            'next_call'  => $item->callDate,
        ];
    }

    /**
     * Batch of data to sync
     *
     * @param  $index
     * @param  int $batchSize
     * @return array
     */
    protected function changesBatch($index, $batchSize = 1000)
    {
        return $this->changesSelect()->skip($index * $batchSize)->take($batchSize)->get();
    }

    /**
     * Worker constructor.
     * @param \Illuminate\Database\DatabaseManager $manager
     */
    public function __construct(DatabaseManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Select for history entries
     *
     * @return Builder
     */
    protected function changesSelect()
    {
        return $this->b2mDb()->table('telemarketing.OrderReminderingHistory')
            ->where($this->b2mDb()->raw('UNIX_TIMESTAMP(OrderReminderingHistory.created_at)'), '>',
                $this->lastSyncedTimestamp())
            ->whereNotNull('OrderReminderingHistory.created_at')
            ->whereNotIn('OrderReminderingHistory.orderReminderingHistory_id', $this->getSyncedFromLocal())
            ->orderBy('OrderReminderingHistory.created_at');
    }

    protected function getSyncedFromLocal()
    {
        if (!isset($this->syncedFromLocal)) {
            $this->syncedFromLocal = $this->db()->table('tm_syncs')
                ->where('direction', '=', 'out')
                ->where('table', '=', 'tm_reminder_histories')
                ->pluck('remote_id');
        }

        return $this->syncedFromLocal;
    }

    protected function lastSyncedTimestamp()
    {
        if (!isset($this->lastSyncedTimestamp)) {
            $record = $this->db()->table('tm_syncs')
                ->select([$this->db()->raw('UNIX_TIMESTAMP(created_at) as timestamp')])
                ->where('table', '=', 'tm_reminder_histories')
                ->where('direction', '=', 'in')
                ->orderBy('created_at', 'desc')
                ->take(1)
                ->first();

            $this->lastSyncedTimestamp = ($record === null) ? 0 : $record->timestamp;
        }

        return $this->lastSyncedTimestamp;
    }

    /**
     * Connection to destination DB
     * @return \Illuminate\Database\Connection
     */
    protected function db()
    {
        return $this->manager->connection();
    }

    /**
     * Connection to source DB
     *
     * @return \Illuminate\Database\Connection
     */
    protected function b2mDb()
    {
        return $this->manager->connection('b2m');
    }
}

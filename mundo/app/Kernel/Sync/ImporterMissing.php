<?php

namespace Mundo\Kernel\Sync;

use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Mundo\Models\TM\Campaign as CampaignModel;
use Mundo\Models\TM\Record;

/**
 * Importer of records in campaign from old structures
 *
 * Class Importer
 * @package Mundo\Kernel\Sync
 */
class ImporterMissing extends Worker
{
    const PARAMS_DEFINITION = [
        129 => [
            'contact_person' => 'string',
            'phone'          => 'string',
            'lang'           => 'string',
            'firm_id'        => 'integer',
            'note'           => 'string',
            'user_id'        => 'integer',
        ],
        121 => [
            'contact_person' => 'string',
            'phone'          => 'string',
            'lang'           => 'string',
        ],
        77  => [
            'email'          => 'string',
            'phone'          => 'string',
            'contact_person' => 'string',
            'lang'           => 'string',
        ],
        93  => [
            'phone'          => 'string',
            'contact_person' => 'string',
            'lang'           => 'string',
        ],
    ];
    /** @var  array */
    protected $alreadyImported;
    /** @var  int */
    protected $count;
    /** @var  object */
    protected $remoteCampaign;
    /** @var array */
    protected $params;

    public function __construct(DatabaseManager $manager, CampaignModel $campaign)
    {
        parent::__construct($manager, $campaign);
        $this->remoteCampaign = $this->b2mDb()->table('telemarketing.Campaign')
            ->where('campaign_id', '=', $campaign->sync_id)
            ->take(1)
            ->first();
    }

    /**
     * Import all data
     *
     * @param \Closure $closure
     * @param array    $ids
     */
    public function work(\Closure $closure = null, array $ids = [])
    {
        $index = 0;
        do {
            $items = empty($ids) ? $this->importBatch($index++, 1000) : $this->specificImportSelect($ids);
            $this->preloadParams($items);
            list($records, $syncs, $params) = array_reduce($items, function ($product, $item) {
                $product[0][] = $record = $this->recordData($item);
                $product[1][] = $this->syncData($record['id'], $item);
                if ($this->params !== false) {
                    $product[2] = array_merge($product[2], $this->paramsData($record['id'], $item));
                }

                return $product;
            }, array_fill(0, 3, []));
            $this->db()->table('tm_records')->insert($records);
            $this->db()->table('tm_record_params')->insert($params);
            $this->db()->table('tm_syncs')->insert($syncs);
            $ids = Arr::pluck($records, 'id');
            Record::whereIn('id', $ids)->with('campaign', 'histories', 'params')
                ->get()
                ->withRemote()
                ->searchable();
            unset($this->params);
            $closure && $closure(count($items));
        } while (!empty($items));
    }


    /**
     * Insert data for record
     *
     * @param  object $item
     * @return array
     */
    protected function recordData($item)
    {
        return [
            'id'          => uuid(),
            'campaign_id' => $this->campaign->id,
            'remote_id'   => $item->targetSubject_id,
            'created_at'  => $item->createdAt,
            'updated_at'  => $item->createdAt,
            'user_id'     => $item->employee_id
        ];
    }

    /**
     * Insert data for sync record
     *
     * @param  $id
     * @param  object $item
     * @return array
     */
    protected function syncData($id, $item)
    {
        return [
            'remote_id'   => $item->stack_id,
            'local_id'    => $id,
            'direction'   => 'in',
            'campaign_id' => $this->campaign->id,
            'table'       => 'tm_records',
        ];
    }

    protected function specificImportSelect(array $ids) {
        return $this->b2mDb()->query()
            ->select('*')
            ->from('telemarketing.Stack')
            ->where('campaign_id', '=', $this->campaign->sync_id)
            ->whereIn('stack_id', $ids)
            ->orderBy('Stack.stack_id')
            ->get();
    }

    protected function importSelect()
    {
        return $this->b2mDb()->query()
            ->select('S.*')
            ->from('telemarketing.Stack AS S')
            ->leftJoin('mundo.tm_records AS R', function(JoinClause $j) {
                $j->on('R.remote_id', '=', 'S.targetSubject_id')
                    ->where('R.campaign_id', '=', $this->campaign->id);
            })
            ->where('S.campaign_id', '=', $this->campaign->sync_id)
            ->whereNull('R.id')
            ->orderBy('S.stack_id');
    }

    protected function preloadParams($items)
    {
        if (!isset($this->params)) {
            if (method_exists($this, $method = 'preloadData' . $this->remoteCampaign->targetGroup_id)) {
                $this->params = call_user_func([$this, $method], $items);
            } else {
                $this->params = false;
            }
        }
    }

    protected function preloadData121($items)
    {
        $data = $this->b2mDb()->table('cs_contact.leads')
            ->select(['id', 'extra', 'lang'])
            ->whereIn('id', array_pluck($items, 'targetSubject_id'))
            ->get();
        $data = array_map(function ($item) {
            $json = json_decode($item->extra);

            return (object)[
                'id'             => $item->id,
                'lang'           => $item->lang,
                'contact_person' => $json->name,
                'phone'          => $json->phone,
            ];
        }, $data);

        return array_assoc($data, 'id');
    }

    protected function preloadData129($items)
    {
        return array_assoc($this->b2mDb()->table('log.InternalLeads')
            ->select([
                'id',
                'contact_person',
                'phone_number as phone',
                'firm_id',
                'employee_id as user_id',
                'lang',
                'note',
            ])
            ->whereIn('id', array_pluck($items, 'targetSubject_id'))
            ->get(), 'id');
    }

    protected function preloadData77($items)
    {
        return array_assoc($this->b2mDb()->table('log.EpoptavkaLeads')
            ->select([
                'id',
                'email',
                'phoneNumber as phone',
                'contactPerson as contact_person',
                'lang',
            ])
            ->whereIn('id', array_pluck($items, 'targetSubject_id'))
            ->get(), 'id');
    }

    protected function preloadData93($items)
    {
        return array_assoc($this->b2mDb()->table('log.UNLeads')
            ->select([
                'id',
                'phoneNumber as phone',
                'contactPerson as contact_person',
                'lang',
            ])
            ->whereIn('id', array_pluck($items, 'targetSubject_id'))
            ->get(), 'id');
    }

    protected function paramsData($id, $item)
    {
        $mapping = self::PARAMS_DEFINITION[$this->remoteCampaign->targetGroup_id];
        if (!isset($this->params[$item->targetSubject_id])) {
            return [];
        }
        $params = $this->params[$item->targetSubject_id];
        $result = [];
        foreach ($mapping as $column => $type) {
            $result[] = [
                'record_id'  => $id,
                'key'        => $column,
                'type'       => $type,
                'value'      => $params->$column,
                'created_at' => $item->createdAt,
                'updated_at' => $item->createdAt,
            ];
        }

        return $result;
    }

    /**
     * How many items will be imported
     *
     * @return int
     */
    public function count()
    {
        if (!isset($this->count)) {
            $this->count = $this->importSelect()->count();
        }

        return $this->count;
    }

    /**
     * Batch of data to import
     *
     * @param  $index
     * @param  int $batchSize
     * @return array
     */
    protected function importBatch($index, $batchSize = 1000)
    {
        return $this->importSelect()->skip($index * $batchSize)->take($batchSize)->get();
    }
}

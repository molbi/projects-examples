<?php

namespace Mundo\Kernel\Sync;

use Illuminate\Database\DatabaseManager;
use Mundo\Models\TM\ReminderHistory;

class ReminderHistoryObserver
{
    /** @var  DatabaseManager */
    protected $manager;

    /**
     * ReminderHistoryObserver constructor.
     * @param \Illuminate\Database\DatabaseManager $manager
     */
    public function __construct(\Illuminate\Database\DatabaseManager $manager)
    {
        $this->manager = $manager;
    }

    public function created(ReminderHistory $history)
    {
        $id = $this->createHistory($history);
        $this->createSync($id, $history);
    }

    protected function db()
    {
        return $this->manager->connection();
    }

    protected function b2mDb()
    {
        return $this->manager->connection('b2m');
    }

    /**
     * @param \Mundo\Models\TM\ReminderHistory $history
     */
    protected function createHistory(ReminderHistory $history)
    {
        return $this->b2mDb()->table('telemarketing.OrderReminderingHistory')
            ->insertGetId([
                'order_id'   => $history->remote_id,
                'type'       => array_flip(ReminderSyncer::TYPES_MAP)[$history->type],
                'callDate'   => $history->next_call,
                'created_at' => $history->created_at,
                'note'       => $history->note,
            ]);
    }

    protected function createSync($id, ReminderHistory $history)
    {
        $this->db()->table('tm_syncs')
            ->insert([
                'remote_id'   => $id,
                'local_id'    => $history->id,
                'campaign_id' => null,
                'table'       => $history->getTable(),
                'direction'   => 'out',
            ]);
    }
}

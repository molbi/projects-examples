<?php

namespace Mundo\Kernel\Sync;

use Illuminate\Console\Command as BaseCommand;
use Illuminate\Database\DatabaseManager;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Filesystem\FilesystemManager;
use Mundo\Models\TM\Campaign as CampaignModel;

/**
 * Sync between old and new TM in both ways
 *
 * It needs connection with name `b2m`
 *
 * @package Mundo\Kernel\Synchro
 */
class Command extends BaseCommand
{
    protected $signature = 'mundo:sync';

    protected $description = 'Sync new and old data';

    /** @var DatabaseManager */
    protected $manager;
    /** @var  array */
    protected $alreadyImported;
    /** @var  Filesystem */
    protected $fs;

    /**
     * Campaign constructor.
     * @param \Illuminate\Database\DatabaseManager $manager
     * @param \Illuminate\Filesystem\FilesystemManager $fs
     */
    public function __construct(DatabaseManager $manager, FilesystemManager $fs)
    {
        $this->manager = $manager;
        $this->fs = $fs->drive('local');
        parent::__construct();
    }

    /**
     * Execute the command
     */
    public function handle()
    {
        foreach (CampaignModel::whereNotNull('sync_id')->get() as $campaign) {
            $this->handleCampaign($campaign);
        }
        $workers = [
            [new ReminderSyncer($this->manager), 'Sync Reminders']
        ];

        foreach ($workers as $worker) {
            list($worker, $message) = $worker;
            $this->runWorker($worker, $message);
        }
    }

    protected function handleCampaign(CampaignModel $campaign)
    {
        $this->output->title($campaign->name);

        $workers = [
            [new Importer($this->manager, $campaign), 'Import'],
            [new Syncer($this->manager, $campaign), 'Sync'],
        ];

        foreach ($workers as $worker) {
            list($worker, $message) = $worker;
            $this->runWorker($worker, $message);
        }
    }

    /**
     * Run the worker
     * @param \Mundo\Kernel\Sync\Worker $worker
     * @param null $title
     */
    protected function runWorker($worker, $title = null)
    {
        $count = $worker->count();
        $this->output->title(is_null($title) ? get_class($worker) : $title);

        if ($count > 0) {
            $bar = $this->output->createProgressBar($count);
            $worker->work(function ($processed) use ($bar) {
                $bar->advance($processed);
            });
            $bar->finish();
            $this->info('');
        } else {
            $this->info("Nothing to do");
        }
    }
}

<?php

namespace Mundo\Kernel\Sync;

use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\JoinClause;
use Log;
use Mundo\Models\TM\Record;

class Syncer extends Worker
{
    const STATE_MAP = [
        'accepted'    => ['to' => 'accepted', 'column' => 'state'],
        'declined'    => ['to' => 'declined', 'column' => 'state'],
        'nonexisting' => ['to' => 'removed', 'column' => 'state'],
        'pending'     => ['to' => 'new', 'column' => 'state'],
        'removed'     => ['to' => 'removed', 'column' => 'state'],
        'unreached'   => ['to' => 'not-reached', 'column' => 'state'],
        'reached'     => ['to' => 'reached', 'column' => 'state'],
        'forwarded'   => ['to' => null, 'column' => 'user_id'],
    ];

    /** @var int */
    protected $lastSyncedTimestamp;
    /** @var  int */
    protected $count;
    /** @var  array */
    protected $syncedFromLocal;

    /**
     * Do the work and report progress by closure
     *
     * @param \Closure $closure
     * @param array    $ids
     */
    public function work(\Closure $closure = null, array $ids = [])
    {
        $index = 0;
        do {
            $items = $this->changesBatch($index++, 1000);
            $ids = $this->localIds($items);
            $records = $this->records($ids);
            $this->db()->beginTransaction();
            $histories = [];
            $syncs = [];
            foreach ($items as $item) {
                if (isset($ids[$item->stack_id]) && $id = $ids[$item->stack_id]) {
                    $this->updateRecord($id, $item);
                    $histories[] = $history = $this->historyData($records[$id], $item);
                    $syncs[] = $this->syncData($history['id'], $item);
                }
            }
            $this->db()->table('tm_record_histories')->insert($histories);
            $this->db()->table('tm_syncs')->insert($syncs);
            $this->reindexRecords($ids);
            $this->db()->commit();
            $closure && $closure(count($items));
        } while (!empty($items));
    }

    /**
     * Count of steps
     *
     * @return integer
     */
    public function count()
    {
        if (!isset($this->count)) {
            $this->count = $this->changesSelect()->count();
        }

        return $this->count;
    }

    /**
     * Tranform remote id to local id
     *
     * @param  array $items
     * @return null|string ID if the record exists and is under the sync
     */
    protected function localIds($items)
    {
        return array_assoc($this->db()->table('tm_syncs')
            ->select(['local_id', 'remote_id'])
            ->whereIn('remote_id', array_pluck($items, 'stack_id'))
            ->where('table', '=', 'tm_records')
            ->where('campaign_id', '=', $this->campaign->id)
            ->get(), 'remote_id=local_id');
    }

    /**
     * History data
     *
     * @param  object $record
     * @param  object $item
     * @return array
     */
    protected function historyData($record, $item)
    {
        return [
            'id'         => uuid(),
            'record_id'  => $record->id,
            'user_id'    => $item->creator_id,
            'updated_at' => $item->createdAt,
            'created_at' => $item->createdAt,
            'column'     => self::STATE_MAP[$item->action]['column'],
            'next'       => ($next = self::STATE_MAP[$item->action]['to']) ? $next : $item->assigned_id,
            'previous'   => $record->{self::STATE_MAP[$item->action]['column']},
            'note'       => (self::STATE_MAP[$item->action]['to']) ? $item->note : null,
        ];
    }

    /**
     * Insert data for sync record
     *
     * @param  $id
     * @param  object $item
     * @return array
     */
    protected function syncData($id, $item)
    {
        return [
            'remote_id'   => $item->history_id,
            'local_id'    => $id,
            'direction'   => 'in',
            'campaign_id' => $this->campaign->id,
            'table'       => 'tm_record_histories',
            'created_at'  => $item->createdAt,
        ];
    }

    protected function records($ids)
    {
        return array_assoc($this->db()->table('tm_records')
            ->whereIn('id', $ids)
            ->get(), 'id');
    }

    /**
     * Sync the record object
     *
     * @param string $id
     * @param object $item
     */
    protected function updateRecord($id, $item)
    {
        $record = $this->recordSelect($id);
        $data = ['updated_at' => $item->createdAt, 'next_call' => $item->callDate];
        if ($item->action === 'forwarded') {
            $data['user_id'] = $item->assigned_id;
            $data['state'] = 'new';
        } else {
            $data['state'] = self::STATE_MAP[$item->action]['to'];
        }
        $record->update($data);
    }

    /**
     * Reindex the record
     *
     * @param array $ids
     */
    protected function reindexRecords(array $ids)
    {
        Record::whereIn('id', $ids)->with('campaign', 'histories', 'params')
            ->get()
            ->withRemote()
            ->searchable();
    }


    /**
     * Record select
     *
     * @param  string $id
     * @return Builder
     */
    protected function recordSelect($id)
    {
        return $this->db()->table('tm_records')->where('id', '=', $id);
    }

    /**
     * Batch of data to sync
     *
     * @param  $index
     * @param  int $batchSize
     * @return array
     */
    protected function changesBatch($index, $batchSize = 1000)
    {
        return $this->changesSelect()->skip($index * $batchSize)->take($batchSize)->get();
    }

    /**
     * Select for history entries
     *
     * @return Builder
     */
    protected function changesSelect()
    {
        return $this->b2mDb()->table('telemarketing.Stack')
            ->select([
                'History.history_id',
                'Stack.stack_id',
                'Stack.callDate',
                'History.createdAt',
                'History.action',
                'History.note',
                'History.employee_id as creator_id',
                'Stack.employee_id as assigned_id',
            ])
            ->join('telemarketing.History', 'History.stack_id', '=', 'Stack.stack_id')
            ->leftJoin('mundo.tm_syncs AS S', function(JoinClause $j) {
                $j->on('S.remote_id', '=', 'History.history_id')
                    ->where('S.campaign_id', '=', $this->campaign->id)
                    ->where('S.direction', '=', 'out')
                    ->where('S.table', '=', 'tm_record_histories');
            })
            ->where('Stack.campaign_id', '=', $this->campaign->sync_id)
            ->whereNull('S.id')
            ->where($this->b2mDb()->raw('UNIX_TIMESTAMP(History.createdAt)'), '>', $this->lastSyncedTimestamp())
            ->orderBy('History.createdAt');
    }

    protected function getSyncedFromLocal()
    {
        if (!isset($this->syncedFromLocal)) {
            $this->syncedFromLocal = $this->db()->table('tm_syncs')
                ->where('direction', '=', 'out')
                ->where('campaign_id', '=', $this->campaign->id)
                ->where('table', '=', 'tm_record_histories')
                ->pluck('remote_id');
        }

        return $this->syncedFromLocal;
    }

    /**
     * Last timestamp, when the sync was performed
     *
     * @return int
     */
    protected function lastSyncedTimestamp()
    {
        if (!isset($this->lastSyncedTimestamp)) {
            $record = $this->db()->table('tm_syncs')
                ->select([$this->db()->raw('UNIX_TIMESTAMP(created_at) as timestamp')])
                ->where('table', '=', 'tm_record_histories')
                ->where('campaign_id', '=', $this->campaign->id)
                ->where('direction', '=', 'in')
                ->orderBy('created_at', 'desc')
                ->take(1)
                ->first();

            $this->lastSyncedTimestamp = ($record === null) ? 0 : $record->timestamp;
        }

        return $this->lastSyncedTimestamp;
    }
}

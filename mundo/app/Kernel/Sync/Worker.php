<?php

namespace Mundo\Kernel\Sync;

use Illuminate\Database\DatabaseManager;
use Mundo\Models\TM\Campaign as CampaignModel;

abstract class Worker
{
    /** @var  DatabaseManager */
    protected $manager;
    /** @var  CampaignModel */
    protected $campaign;

    /**
     * Count of steps
     *
     * @return integer
     */
    abstract public function count();

    /**
     * Do the work and report progress by closure
     *
     * @param \Closure $closure
     * @param array    $ids
     */
    abstract public function work(\Closure $closure, array $ids = []);

    /**
     * Worker constructor.
     * @param \Illuminate\Database\DatabaseManager $manager
     * @param \Mundo\Models\TM\Campaign $campaign
     */
    public function __construct(DatabaseManager $manager, CampaignModel $campaign)
    {
        $this->manager = $manager;
        $this->campaign = $campaign;
    }

    /**
     * Connection to destination DB
     * @return \Illuminate\Database\Connection
     */
    protected function db()
    {
        return $this->manager->connection();
    }

    /**
     * Connection to source DB
     *
     * @return \Illuminate\Database\Connection
     */
    protected function b2mDb()
    {
        return $this->manager->connection('b2m');
    }
}

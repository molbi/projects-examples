<?php

namespace Mundo\Kernel\EmailTemplate;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\View\Compilers\BladeCompiler;
use InvalidArgumentException;
use Mockery\CountValidator\Exception;
use Mundo\Kernel\Eloquent\CastableParams;
use Mundo\Models\EmailTemplate;
use Mundo\Models\EmailTemplate as EmailTemplateModel;
use Mundo\Models\User;

class Template
{
    use CastableParams;

    /** @var array */
    protected $initials = [];
    /** @var array */
    protected $defaults = [];
    /** @var  EmailTemplate */
    protected $model;

    /**
     * Template constructor.
     *
     * @param EmailTemplateModel                  $model
     * @param \Mundo\Models\User                  $user
     * @param \Illuminate\Database\Eloquent\Model $target
     */
    public function __construct(EmailTemplateModel $model, User $user, Model $target = null)
    {
        $this->initials = [
            'user'   => $user,
            'target' => $target,
        ];
        $this->model = $model;
        $this->validateInitials();
        $this->fillDefaults();
    }

    protected function validateInitials()
    {
        if (!$this->initials['user']) {
            throw new InvalidArgumentException("User have to be provided");
        }

        if (!empty($this->model->target)) {
            if (!$this->initials['target'] || !is_a($this->initials['target'], $this->model->target)) {
                throw new InvalidArgumentException("Target have to be provided and have to be subclas of [{$this->model->target}]");
            }
        }
    }

    protected function castVariables(array &$variables)
    {
        foreach ($this->model->getVariables() as $variable => $definition) {
            if (isset($definition->cast) && isset($variables[$variable])) {
                $variables[$variable] = $this->cast($variables[$variable], $definition->cast);
            }
        }
    }

    protected function castEnums(array &$variables)
    {
        foreach ($this->model->getVariables() as $variable => $definition) {
            if (isset($definition->type) && $definition->type === 'enum' && isset($definition->options)) {
                $variables[$variable] = $definition->options[$variables[$variable]];
            }
        }
    }

    protected function fillDefaults()
    {
        foreach ($this->model->getVariables() as $variable => $definition) {
            // Default values
            if (isset($definition->default) && (!isset($this->defaults[$variable]) || empty($this->defaults[$variable]))) {
                $this->defaults[$variable] = $definition->default;
            }
            // Blade && php
            if (is_string($this->defaults[$variable])) {
                $this->defaults[$variable] = $this->interpolateString($this->defaults[$variable], $this->initials);
            }
        }
        $this->castVariables($this->defaults);
    }

    public function getDefaults()
    {
        return $this->defaults;
    }

    protected function cast($value, $type)
    {
        if (is_null($value)) {
            return $value;
        }

        switch ($type) {
            case 'int':
            case 'integer':
                return (int)$value;
            case 'real':
            case 'float':
            case 'double':
                return (float)$value;
            case 'string':
                return (string)$value;
            case 'bool':
            case 'boolean':
                return (bool)$value;
            case 'object':
                return $this->fromJson($value, true);
            case 'array':
            case 'json':
                return $this->fromJson($value);
            case 'date':
            case 'datetime':
                return $this->asDateTime($value);
            case 'timestamp':
                return $this->asTimeStamp($value);
            default:
                return $value;
        }
    }

    /**
     * Return a timestamp as DateTime object.
     *
     * @param  mixed $value
     *
     * @return \Carbon\Carbon
     */
    protected function asDateTime($value)
    {
        if ($value instanceof Carbon) {
            return $value;
        }

        if ($value instanceof \DateTimeInterface) {
            return new Carbon(
                $value->format('Y-m-d H:i:s.u'), $value->getTimeZone()
            );
        }

        if (is_numeric($value)) {
            return Carbon::createFromTimestamp($value);
        }

        if (preg_match('/^(\d{4})-(\d{1,2})-(\d{1,2})$/', $value)) {
            return Carbon::createFromFormat('Y-m-d', $value)->startOfDay();
        }

        return Carbon::createFromFormat('Y-m-d H:i:s', $value);
    }

    /**
     * Decode the given JSON back into an array or object.
     *
     * @param  string $value
     * @param  bool   $asObject
     *
     * @return mixed
     */
    public function fromJson($value, $asObject = false)
    {
        return json_decode($value, !$asObject);
    }

    /**
     * Return a timestamp as unix timestamp.
     *
     * @param  mixed $value
     *
     * @return int
     */
    protected function asTimeStamp($value)
    {
        return $this->asDateTime($value)->getTimestamp();
    }


    protected function interpolateAttachments(array $variables)
    {
        $result = [];
        foreach ($this->model->getAttachments() as $filename => $data) {
            $data = (array)$data;
            foreach ($data as $key => $value) {
                if (is_string($value)) {
                    $data[$key] = $this->interpolateString($value, $variables);
                }
            }
            $result[$this->interpolateString($filename, $variables)] = (object)$data;
        }

        return $result;
    }

    public function render(array $variables = [])
    {
        $variables = array_merge([], $this->initials, $this->defaults, $variables);
        $this->castVariables($variables);
        $this->castEnums($variables);

        return (object)[
            'subject'     => $this->interpolateString($this->model->subject, $variables),
            'fromName'    => isset($variables['fromName']) ? $variables['fromName'] : $variables['user']->name,
            'fromEmail'   => isset($variables['fromEmail']) ? $variables['fromEmail'] : $variables['user']->email,
            'html'        => $this->interpolateString($this->model->html, $variables),
            'plain'       => $this->interpolateString($this->model->plain, $variables),
            'email'       => $this->interpolateString($this->model->email, $variables),
            'attachments' => $this->interpolateAttachments($variables),
        ];
    }

    /**
     * @return BladeCompiler
     */
    protected function getBlade()
    {
        return app(BladeCompiler::class);
    }

    protected function interpolateString($string, array $data = [])
    {
        $obLevel = ob_get_level();
        $string = $this->getBlade()->compileString($string);
        ob_start();
        extract($data, EXTR_SKIP);

        try {
            eval('?>' . $string);
        } catch (\Exception $e) {
            throw new \Exception("Error during interpolation: $string");
        } catch (\Throwable $e) {
            throw new $e;
        }

        return ltrim(ob_get_clean());
    }
}

<?php

namespace Mundo\Kernel\Config;

use Illuminate\Support\ServiceProvider as BaseProvider;

/**
 * Common service provider, all minor stuff is going here
 *
 * Class ServiceProvider
 * @package Mundo\Kernel
 */
class ServiceProvider extends BaseProvider
{

    /**
     * Boot the application
     */
    public function boot()
    {
        UseConfig::setConfig($this->app['config']);
    }

    /**
     * Register services
     */
    public function register()
    {
    }
}

<?php

namespace Mundo\Kernel\Config;

use Illuminate\Config\Repository;

/**
 * Trait for using config in objects
 *
 * Class UseConfig
 * @package Mundo\Kernel\Config
 */
trait UseConfig
{
    /** @var  Repository */
    private static $config;

    /**
     * Setter for config
     * s
     * @param \Illuminate\Config\Repository $config
     */
    public static function setConfig(Repository $config)
    {
        static::$config = $config;
    }

    /**
     * Config
     *
     * @return \Illuminate\Config\Repository
     */
    public static function getConfig()
    {
        return static::$config;
    }

    /**
     * Get the config value
     *
     * @param  string $key
     * @param  null|mixed $default
     * @return mixed
     */
    public static function config($key, $default = null)
    {
        return static::getConfig()->get($key, $default);
    }
}

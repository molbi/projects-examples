<?php

namespace Mundo\Kernel\Cache;

use Illuminate\Cache\Repository;
use Illuminate\Database\Connection;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\JoinClause;
use Mundo\Kernel\Constants;
use Mundo\Kernel\Cron\Cronable;
use Mundo\Models\User;

/**
 * Class PaidOrders
 * @package Mundo\Kernel\Cache
 */
class PaidOrders implements Cronable, ICache
{
    /** Key for cache */
    const KEY = 'paid-orders';

    /** @var \Illuminate\Cache\Repository */
    protected $cache;

    /**
     * Cache constructor.
     *
     * @param Repository $cache
     */
    public function __construct(Repository $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @return array
     *
     */
    public function get()
    {
        return $this->cache->get(self::KEY, []);
    }

    /**
     * @param User $user
     *
     * @return array
     */
    public function getForUser(User $user)
    {
        $data = $this->cache->get(self::KEY, []);

        if (!isset($data[$user->id])) {
            return [];
        }

        return $data[$user->id];
    }

    /**
     * @param Connection|null $db
     */
    public function run(Connection $db = null)
    {
        $this->cache->forever(self::KEY, $this->getPaidOrders($db));
    }


    /**
     * @param Connection $db
     *
     * @return null
     */
    protected function getPaidOrders(Connection $db)
    {
        return array_assoc($this->getPaidOrdersSelect($db)->get(), 'employee_id|order_id');
    }

    /**
     * @param Connection $db
     *
     * @return Builder
     */
    protected function getPaidOrdersSelect(Connection $db)
    {
        $select = $this->getPaidOrdersBusinessSelect($db)->unionAll($this->getPaidOrdersContactSelect($db));

        $query = $db->table($db->raw(sprintf('(%s) as Z', $select->toSql())))->select([
            'employee_id',
            'order_id',
            'source',
            'price',
        ]);

        $query->mergeBindings($select);

        return $query;
    }

    /**
     * @param Connection $db
     *
     * @return Builder
     */
    protected function getPaidOrdersBusinessSelect(Connection $db)
    {
        return $db->table('obis.OrderHistory as OH')->select([
            'O.employee_id',
            'OH.order_id',
            $db->raw('O.source COLLATE utf8_general_ci as source'),
            $db->raw(sprintf('IF(O.lang = "cs", I.price, I.price * %s) AS price', Constants::RATES['sk'])),
        ])
            ->join('obis.Order as O', 'O.order_id', '=', 'OH.order_id')
            ->join('obis.Invoice as I', function (JoinClause $j) {
                $j->on('I.order_id', '=', 'OH.order_id')->where('I.price', '>', 0);
            })
            ->leftJoin('obis.OrderExactor as OE', 'OE.order_id', '=', 'OH.order_id')
            ->where('OH.action', 'paid')
            ->where('O.state', 'paid')
            ->whereRaw('YEAR(OH.changedAt) = YEAR(NOW())')
            ->whereRaw('MONTH(OH.changedAt) = MONTH(NOW())')
            ->whereNull('OE.order_id')
            ->whereNotNull('O.employee_id');


    }

    /**
     * @param Connection $db
     *
     * @return Builder
     */
    protected function getPaidOrdersContactSelect(Connection $db)
    {
        return $db->table('cs_contact.invoices as I')->select([
            'I.employee_id',
            'I.id AS order_id',
            'I.source',
            $db->raw(sprintf('IF(I.currency = "CZK", I.price, I.price * %s) AS price', Constants::RATES['sk'])),
        ])->whereRaw('YEAR(I.paid_at) = YEAR(NOW())')->whereRaw('MONTH(I.paid_at) = MONTH(NOW())')->where('I.state',
            'paid')->whereNotNull('I.employee_id')->where('I.price', '>', 0);
    }

}

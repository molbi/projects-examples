<?php

namespace Mundo\Kernel\Cache;

use Illuminate\Foundation\Application;
use Mundo\Models\User;

/**
 * Class Cache
 * @package Mundo\Kernel\Cache
 */
class Cache
{
    const PAID_ORDERS = 'paid-orders';
    const PAYMENTS_MORALS = 'payments-morals';

    /**
     * @var Application
     */
    private $app;

    /**
     * Cache constructor.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @param $type
     *
     * @return mixed
     */
    public function get($type)
    {
        return $this->getCacheClass($type)->get();
    }

    /**
     * @param User $user
     * @param      $type
     *
     * @return mixed
     */
    public function getForUser(User $user, $type)
    {
        return $this->getCacheClass($type)->getForUser($user);
    }


    /**
     * @param $type
     *
     * @return ICache
     */
    protected function getCacheClass($type)
    {
        $object = __NAMESPACE__ . '\\' . studly_case($type);

        return $this->app->make($object);
    }
}
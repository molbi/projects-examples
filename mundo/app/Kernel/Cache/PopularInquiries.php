<?php

namespace Mundo\Kernel\Cache;

use Illuminate\Cache\Repository;
use Illuminate\Database\Connection;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\JoinClause;
use Mundo\Kernel\Constants;
use Mundo\Kernel\Cron\Cronable;
use Mundo\Models\User;

/**
 * Class PopularInquiries
 * @package Mundo\Kernel\Cache
 */
class PopularInquiries implements Cronable, ICache
{
    /** Key for cache */
    const KEY_CS = 'find-client-popular-inquiries-cs';
    /**
     *
     */
    const KEY_SK = 'find-client-popular-inquiries-sk';

    /**
     * @var array
     */
    private $keyMap = [
        'cs' => self::KEY_CS,
        'sk' => self::KEY_SK
    ];

    /** @var \Illuminate\Cache\Repository */
    protected $cache;

    /**
     * Cache constructor.
     *
     * @param Repository $cache
     */
    public function __construct(Repository $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @return array
     *
     */
    public function get()
    {
        return [];
    }

    /**
     * @param $lang
     * @return mixed
     */
    public function getForLang($lang)
    {
        return $this->cache->get($this->keyMap[$lang]);
    }

    /**
     * @param Connection|null $db
     */
    public function run(Connection $db = null)
    {
        $this->cache->forever(self::KEY_CS, $this->getPopularInquiries($db, 'cs'));
        $this->cache->forever(self::KEY_SK, $this->getPopularInquiries($db, 'sk'));
    }

    /**
     * @param Connection $db
     * @param $lang
     * @return array|static[]
     */
    protected function getPopularInquiries(Connection $db, $lang)
    {
        return $db->table('ashe.log_sent_inquiries AS L')
            ->select([
                'L.inquiry_id',
                $db->raw('SUM(L.visited) AS visited')
            ])
            ->join('business.Inquiry AS I', 'I.inquiry_id', '=', 'L.inquiry_id')
            ->where('I.lang', $lang)
            ->whereRaw('DATE(L.sent_at) >= DATE(NOW() - INTERVAL 7 DAY)')
            ->where('L.visited', '>', 0)
            ->groupBy('L.inquiry_id')
            ->orderBy('L.visited', 'desc')
            ->limit(50)
            ->get();
    }

    /**
     * @param User $user
     */
    public function getForUser(User $user)
    {
        // TODO: Implement getForUser() method.
    }
}

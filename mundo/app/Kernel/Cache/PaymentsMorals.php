<?php

namespace Mundo\Kernel\Cache;

use Illuminate\Cache\Repository;
use Illuminate\Database\Connection;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\JoinClause;
use Mundo\Kernel\Constants;
use Mundo\Kernel\Cron\Cronable;
use Mundo\Models\User;


/**
 * Class Cache
 * @package Mundo\Widgets\PaymentsMorals
 */
class PaymentsMorals implements Cronable, ICache
{
    /** Key for cache */
    const KEY = 'payments-morals';

    /** @var \Illuminate\Cache\Repository */
    protected $cache;

    /**
     * Cache constructor.
     *
     * @param Repository $cache
     */
    public function __construct(Repository $cache)
    {
        $this->cache = $cache;
    }

    /**
     *
     * @return array
     */
    public function get()
    {
        return $this->cache->get(self::KEY);
    }

    /**
     * @param User $user
     *
     * @return array
     */
    public function getForUser(User $user)
    {
        $data = $this->cache->get(self::KEY);

        if (!isset($data[$user->id])) {
            return [];
        }

        return $data[$user->id];
    }

    /**
     * @param Connection|null $db
     */
    public function run(Connection $db = null)
    {
        $this->cache->forever(self::KEY, $this->getPaymentMorals($db));
    }

    /**
     * @param Connection $db
     *
     * @return null
     */
    protected function getPaymentMorals(Connection $db)
    {
        return array_assoc($this->getPaymentMoralsSelect($db)->get(), 'employee_id|month');
    }

    /**
     * @param Connection $db
     *
     * @return Builder
     */
    protected function getPaymentMoralsSelect(Connection $db)
    {
        $select = $this->getPaymentMoralsBusinessSelect($db);

        $query = $db->table($db->raw(sprintf('(%s) as Z', $select->toSql())))->select([
            'employee_id',
            'month',
            'year',
            $db->raw('SUM(total) AS total'),
            $db->raw('SUM(paid) AS paid'),
        ])->groupBy('Z.employee_id')->groupBy('Z.month');

        $query->mergeBindings($select);

        return $query;
    }

    /**
     * @param Connection $db
     *
     * @return Builder
     */
    protected function getPaymentMoralsContactSelect(Connection $db)
    {
        return $db->table('cs_contact.invoices as I')->select([
            'I.employee_id',
            $db->raw('MONTH(I.created_at) AS month'),
            $db->raw('YEAR(I.created_at) AS year'),
            $db->raw(sprintf('SUM(IF(I.currency = "CZK", I.price, I.price * %s)) AS total', Constants::RATES['sk'])),
            $db->raw(sprintf('SUM(IF(I.state = "paid", IF(I.currency = "CZK", I.price, I.price * %s), 0)) AS paid',
                Constants::RATES['sk'])),
        ])->whereRaw('DATE(I.created_at) >= DATE_FORMAT(NOW() - INTERVAL 2 MONTH, "%Y-%m-01")')
            ->whereNotNull('I.employee_id')
            ->whereNull('I.original_id')
            ->groupBy('I.employee_id')
            ->groupBy($db->raw('MONTH(I.created_at)'));
    }

    /**
     * @param Connection $db
     *
     * @return Builder
     */
    protected function getPaymentMoralsBusinessSelect(Connection $db)
    {
        return $db->table('obis.Order as O')->select([
            'O.employee_id',
            $db->raw('MONTH(O.createdOn) AS month'),
            $db->raw('YEAR(O.createdOn) AS year'),
            $db->raw(sprintf('SUM(IF(O.lang = "cs", I.price, I.price * %s)) AS total', Constants::RATES['sk'])),
            $db->raw(sprintf('SUM(IF(O.state = "paid" AND OE.order_id IS NULL, IF(O.lang = "cs", I.price, I.price * %s), 0)) AS paid',
                Constants::RATES['sk'])),
        ])->join('obis.Invoice as I', function (JoinClause $j) {
                $j->on('I.order_id', '=', 'O.order_id')->where('I.price', '>', 0);
            })->leftJoin('obis.OrderExactor as OE', 'OE.order_id', '=', 'O.order_id')
            ->whereRaw('DATE(O.createdOn) >= DATE_FORMAT(NOW() - INTERVAL 2 MONTH, "%Y-%m-01")')
            ->whereRaw('DATE(O.createdOn) <= LAST_DAY(NOW()-INTERVAL 1 MONTH)')
            ->whereNotNull('O.employee_id')->groupBy('O.employee_id')->groupBy($db->raw('MONTH(O.createdOn)'));
    }
}

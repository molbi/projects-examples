<?php

namespace Mundo\Kernel\Cache;

use Mundo\Models\User;

interface ICache
{
    public function get();

    public function getForUser(User $user);
}
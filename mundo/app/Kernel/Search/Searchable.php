<?php

namespace Mundo\Kernel\Search;

use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as BaseCollection;

trait Searchable
{
    /**
     * Boot the trait.
     *
     * @return void
     */
    public static function bootSearchable()
    {
        static::addGlobalScope(new SearchableScope);
        static::observe(new ModelObserver);
    }


    /**
     * Creates query from ids
     *
     * @param  array $ids
     * @return EloquentBuilder mixed
     */
    public function searchableQuery(array $ids)
    {
        $strIds = implode(',', array_map(function ($id) { return "'$id'"; }, $ids));

        $query = $this->newQuery()
            ->whereIn($this->getKeyName(), $ids);
        if (!empty($ids)) {
            $query->orderBy($this->getConnection()->raw('FIELD (' . $this->getKeyName() . ',' . $strIds . ')'), 'ASC');
        }

        return $query;
    }

    /**
     * Perform search against indexed data
     *
     * @param  string|array $query

     * @return Builder
     */
    public static function search($query)
    {
        return new Builder(new static, static::makeSearchQuery($query), is_array($query));
    }

    /**
     * Create search builder
     *
     * @param  string $query
     * @return array
     */
    protected static function makeSearchQuery($query)
    {
        return [
            'query' => [
                'simple_query_string' => [
                    'query'            => $query,
                    'analyzer'         => 'default',
                    'fields'           => ['_all'],
                    'default_operator' => 'and',
                ],
            ],
        ];
    }

    /**
     * Mappings for search
     *
     * @return array
     */
    public function searchMapping()
    {
        return [
            'properties' => [
                'id'  => [
                    'type'   => 'integer',
                    'index'  => 'not_analyzed',
                ],
            ],
        ];
    }

    /**
     * Data to be indexed for search
     *
     * @return array
     */
    public function toSearchableArray()
    {
        return $this->toArray();
    }

    /**
     * What's the model's type name
     *
     * @return string
     */
    public function searchableAs()
    {
        return config('search.prefix') . $this->getTable();
    }

    /**
     * Get the Scout engine for the model.
     *
     * @return Driver
     */
    public function searchableUsing()
    {
        return app(ConnectionManager::class)->connection();
    }

    /**
     * Automatically decorate collection
     *
     * @param  array $models
     * @return \Illuminate\Support\Collection
     */
    public function newCollection(array $models = [])
    {
        return $this->searchableCollection(parent::newCollection($models));
    }

    /**
     * Decorate collection
     *
     * @param \Illuminate\Support\Collection $collection
     * @return \Illuminate\Support\Collection
     */
    protected function searchableCollection(BaseCollection $collection)
    {
        $self = $this;
        $collection->macro('searchable', function () use ($self) {
            $self->makeSearchable($this);
        });
        $collection->macro('unsearchable', function () use ($self) {
            $self->removeFromSearch($this);
        });

        return $collection;
    }

    public function removeFromSearch(BaseCollection $models)
    {
        if (!$models->isEmpty()) {
            return $models->first()->searchableUsing()->delete($models);
        }
    }

    public function makeSearchable(BaseCollection $models)
    {
        if (!$models->isEmpty()) {
            return $models->first()->searchableUsing()->update($models);
        }
    }

    /**
     * Make the given model instance searchable.
     *
     * @return void
     */
    public function searchable()
    {
        $this->newCollection([$this])->searchable();
    }

    /**
     * Remove the given model instance from the search index.
     *
     * @return void
     */
    public function unsearchable()
    {
        $this->newCollection([$this])->unsearchable();
    }

    /**
     * Make all instances of the model searchable.
     *
     * @return void
     */
    public static function makeAllSearchable()
    {
        (new static)->newQuery()->searchable();
    }

    /**
     * Remove all instances of the model from the search index.
     *
     * @return void
     */
    public static function removeAllFromSearch()
    {
        (new static)->newQuery()->unsearchable();
    }
}

<?php

namespace Mundo\Kernel\Search\Console;

use Illuminate\Console\Command;
use Illuminate\Contracts\Events\Dispatcher;
use Mundo\Kernel\Search\Events\BulkImportStarted;
use Mundo\Kernel\Search\Events\ModelsImported;

class ImportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mundo:search:import {model}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import the given model into the search index';

    /**
     * Execute the console command.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function handle(Dispatcher $events)
    {
        $class = $this->argument('model');
        $model = new $class;

        $this->output->title('Importing ['.$class.'] records to index.');

        $events->listen(ModelsImported::class, function (ModelsImported $event) {
            $this->output->progressAdvance($event->models->count());
        });

        $events->listen(BulkImportStarted::class, function (BulkImportStarted $event) {
            $this->output->progressStart($event->count);
        });

        $model::makeAllSearchable();
        $this->output->progressFinish();

        $events->forget(ModelsImported::class);
        $events->forget(BulkImportStarted::class);

        $this->line('');

        $this->info('All ['.$class.'] records have been imported.');
    }
}

<?php

namespace Mundo\Kernel\Search\Console;

use Illuminate\Console\Command;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Database\Events\QueryExecuted;

class ImportAllCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mundo:search:import-all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import all models defined in config into the search index (with swap).';

    /**
     * Execute the console command.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher $events
     * @return void
     */
    public function handle(Dispatcher $events)
    {
        $models = config('search.models', []);

        $indexes = $this->reduceMappings($models);
        $this->initIndexes($indexes);
        $this->useMaps($indexes);

        foreach ($models as $class) {
            $this->call('mundo:search:import', ['model' => $class]);
        }

        $this->forgotMaps($indexes);
        $this->swapIndexes($indexes);
    }

    protected function initIndexes($indexes)
    {
        foreach ($indexes as $index) {
            list($connection, $indexMappings, $map) = $index;

            foreach ($indexMappings as $index => $mappings) {
                $params = [
                    'index' => $map[$index],
                    'body'  => [
                        'settings' => $connection->settings,
                        'mappings' => collect($mappings)->filter(function ($mapping) {
                            return !empty($mapping);
                        })->all(),
                    ],
                ];
                if (empty($params['body']['mappings'])) {
                    unset($params['body']['mappings']);
                }
                if (empty($params['body']['settings'])) {
                    unset($params['body']['settings']);
                }

                $connection->base()->indices()->create($params);
            }
        }
    }

    protected function useMaps($indexes)
    {
        foreach ($indexes as $index) {
            list($connection, $indexMappings, $map) = $index;
            $connection->withIndexesNameMap($map);
        }
    }

    protected function forgotMaps($indexes)
    {
        foreach ($indexes as $index) {
            list($connection, $indexMappings, $map) = $index;
            $connection->withoutIndexesNameMap();
        }
    }

    protected function swapIndexes($indexes)
    {
        foreach ($indexes as $index) {
            list($connection, $indexMappings, $map) = $index;
            foreach ($indexMappings as $index => $mappings) {
                try {
                    $connection->base()->indices()->delete(compact('index'));
                } catch (\Exception $ex) {
                }

                $connection->base()->indices()->updateAliases([
                    'body' => ['actions' => [['add' => ['index' => $map[$index], 'alias' => $index]]]],
                ]);
            }
        }
    }


    /**
     * @param $models
     * @return mixed
     */
    protected function reduceMappings($models)
    {
        return collect($models)->reduce(function (array $product, $class) {
            $model = (new $class);
            $key = spl_object_hash($model->searchableUsing());


            $product[$key][0] = $model->searchableUsing();
            $product[$key][1][$model->searchableAs()][$model->searchableAs()] = $model->searchMapping();
            $product[$key][2][$model->searchableAs()] = $model->searchableAs() . '_' . uuid();

            return $product;
        }, []);
    }
}

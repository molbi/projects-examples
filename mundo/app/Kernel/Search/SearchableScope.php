<?php

namespace Mundo\Kernel\Search;

use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Mundo\Kernel\Search\Events\BulkImportStarted;
use Mundo\Kernel\Search\Events\ModelsImported;

class SearchableScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @param  \Illuminate\Database\Eloquent\Model $model
     * @return void
     */
    public function apply(EloquentBuilder $builder, Model $model)
    {
        //
    }

    public function extend(EloquentBuilder $builder)
    {
        $builder->macro('searchable', function (EloquentBuilder $builder) {
            event(new BulkImportStarted($builder->count()));
            $builder->chunk(500, function ($models) use ($builder) {
                $models->searchable();

                event(new ModelsImported($models));
            });
        });


        $builder->macro('unsearchable', function (EloquentBuilder $builder) {
            $builder->chunk(500, function ($models) use ($builder) {
                $models->unsearchable();
            });
        });
    }
}

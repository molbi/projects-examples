<?php

namespace Mundo\Kernel\Search\Events;

use Illuminate\Support\Collection;

class ModelsImported
{
    /**
     * Models beeing imported
     * @var Collection
     */
    public $models;

    /**
     * ModelsImported constructor.
     * @param \Illuminate\Support\Collection $models
     */
    public function __construct(\Illuminate\Support\Collection $models)
    {
        $this->models = $models;
    }
}

<?php

namespace Mundo\Kernel\Search\Events;

class BulkImportStarted
{
    /**
     * How many models will be imported
     * @var int
     */
    public $count;

    /**
     * BulkImportedStarted constructor.
     * @param int $count
     */
    public function __construct($count)
    {
        $this->count = $count;
    }
}

<?php
namespace Mundo\Kernel\Search;

use Illuminate\Support\ServiceProvider;
use Mundo\Kernel\Search\Console\ImportAllCommand;
use Mundo\Kernel\Search\Console\ImportCommand;

class SearchServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ConnectionManager::class, function ($app) {
            return new ConnectionManager($app);
        });


        $this->commands([
            ImportCommand::class,
            ImportAllCommand::class,
        ]);
    }
}

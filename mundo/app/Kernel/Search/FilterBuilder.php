<?php

namespace Mundo\Kernel\Search;

class FilterBuilder
{
    public $filters = [];

    /**
     * Add term filter
     *
     * @param  string $field
     * @param  mixed $term
     * @return $this
     */
    public function where($field, $term)
    {
        $this->filters[] = ['term' => [$field => $term]];

        return $this;
    }

    /**
     * Add terms filter
     *
     * @param  string $field
     * @param  array $items
     * @return $this
     */
    public function whereIn($field, array $items)
    {
        $this->filters[] = ['terms' => [$field => $items]];

        return $this;
    }

    /**
     * Add range filter
     *
     * @param  string $field
     * @param  mixed $from
     * @param  mixed $to
     * @return $this
     */
    public function whereBetween($field, $from, $to)
    {
        $this->filters[] = [
            'range' => [
                $field => [
                    'gte' => $from,
                    'lte' => $to,
                ],
            ],
        ];

        return $this;
    }

    /**
     * Add exists filter
     *
     * @param  string $field
     * @return $this
     */
    public function whereNotNull($field)
    {
        $this->filters[] = ['exists' => ['field' => $field]];

        return $this;
    }

    /**
     * Add missing filter
     *
     * @param  string $field
     * @return $this
     */
    public function whereNull($field)
    {
        $this->filters[] = ['missing' => ['field' => $field]];

        return $this;
    }

    /**
     * Add prefix filter
     *
     * @param  string $field
     * @param  string $prefix
     * @return $this
     */
    public function whereStartsWith($field, $prefix)
    {
        $this->filters[] = ['prefix' => [$field => $prefix]];

        return $this;
    }

    /**
     * Add wildcard query
     *
     * @param  string $field
     * @param  string $expression
     * @return $this
     */
    public function whereWildcard($field, $expression)
    {
        $this->filters[] = ['wildcard' => [$field => $expression]];

        return $this;
    }

    /**
     * Add regexp filter
     *
     * @param  string $field
     * @param  string $expression
     * @return $this
     */
    public function whereRegexp($field, $expression)
    {
        $this->filters[] = ['regexp' => [$field => $expression]];

        return $this;
    }

    /**
     * Add fuzzy filter
     *
     * @param  string $field
     * @param  string $expression
     * @return $this
     */
    public function whereFuzzy($field, $expression)
    {
        $this->filters[] = ['fuzzy' => [$field => $expression]];

        return $this;
    }

    /**
     * Add type filter
     *
     * @param  string $field
     * @param  string $type
     * @return $this
     */
    public function whereType($field, $type)
    {
        $this->filters[] = ['type' => [$field => $type]];

        return $this;
    }

    /**
     * Add ids filter
     *
     * @param  array $ids
     * @return $this
     */
    public function whereIds(array $ids)
    {
        $this->filters[] = ['ids' => ['values '=> $ids]];

        return $this;
    }

    /**
     * Add and filter
     *
     * @param  \Closure $callback
     * @return $this
     */
    public function andQuery(\Closure $callback)
    {
        $this->filters[] = ['and' => ['filters' => $this->compoundQuery($callback)->filters]];

        return $this;
    }

    /**
     * Add or filter
     *
     * @param  \Closure $callback
     * @return $this
     */
    public function orQuery(\Closure $callback)
    {
        $this->filters[] = ['or' => ['filters' => $this->compoundQuery($callback)->filters]];

        return $this;
    }


    /**
     * Add not filter (it takes only first filter from result)
     * 
     * @param  \Closure $callback
     * @return $this
     */
    public function notQuery(\Closure $callback)
    {
        $this->filters[] = ['not' => ['filter' => array_first($this->compoundQuery($callback)->filters)]];

        return $this;
    }

    /**
     * @param \Closure $callback
     * @return \Mundo\Kernel\Search\FilterBuilder|static
     */
    protected function compoundQuery(\Closure $callback)
    {
        $result = $callback($builder = new self);
        if ($result && $result instanceof FilterBuilder) {
            return $result;
        }

        return $builder;
    }
}

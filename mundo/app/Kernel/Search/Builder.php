<?php

namespace Mundo\Kernel\Search;

use BadMethodCallException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class Builder
 * @package Mundo\Kernel\Search
 */
class Builder extends FilterBuilder
{
    /**
     * The model instance
     *
     * @var Model
     */
    public $model;

    /**
     * Query expression
     *
     * @var array
     */
    public $query;

    /**
     * How many items to take
     *
     * @var int
     */
    public $limit;

    /**
     * How many item to skip
     *
     * @var int
     */
    public $offset;

    /**
     * Where to search
     *
     * @var string
     */
    public $index;

    /**
     * @var
     */
    public $sort;
    /**
     * @var bool
     */
    public $pure;

    /**
     * Builder constructor.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param array $query
     * @param bool $pure
     */
    public function __construct(Model $model, array $query, $pure)
    {
        $this->model = $model;
        $this->query = $query;
        $this->pure = $pure;
    }

    /**
     * Result of search
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function get()
    {
        return $this->asQuery()->get();
    }

    /**
     * Set custom index to search in
     *
     * @param  string $index
     * @return $this
     */
    public function within($index)
    {
        $this->index = $index;

        return $this;
    }

    /**
     * Query with found results contraints and propper sorting
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function asQuery()
    {
        return $this->model->searchableUsing()->search($this);
    }

    /**
     * @param bool $onlyHits
     * @return mixed
     */
    public function asEsResult($onlyHits = true)
    {
        return $this->model->searchableUsing()->result($this, $onlyHits);
    }

    /**
     * Set the "limit" for search query
     * @param  int $limit
     * @return $this
     */
    public function take($limit)
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * How many items to skip
     *
     * @param  int $offset
     * @return $this
     */
    public function skip($offset)
    {
        $this->offset = $offset;

        return $this;
    }

    /**
     * Get the firts result of the search
     *
     * @return Model
     */
    public function first()
    {
        return $this->get()->first();
    }

    /**
     * Handle scope calls
     *
     * @param  string $name
     * @param  array $arguments
     * @return $this
     */
    public function __call($name, $arguments)
    {
        $scopeMethodName = 'searchScope' . Str::studly($name);
        if (method_exists($this->model, $scopeMethodName)) {
            $result = $this->model->$scopeMethodName(...array_flatten([$this, $arguments]));
            if ($result instanceof self) {
                return $result;
            }

            return $this;
        } else {
            throw new BadMethodCallException("No scope '$name' was found. You have to define '$scopeMethodName' method");
        }
    }
}

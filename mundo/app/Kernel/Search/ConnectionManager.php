<?php
namespace Mundo\Kernel\Search;

use Illuminate\Foundation\Application;

class ConnectionManager
{
    protected $conections = [];

    /** @var  Application */
    protected $app;

    /**
     * ConnectionManager constructor.
     * @param \Illuminate\Foundation\Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }


    /**
     * Get the default driver name.
     *
     * @return string
     */
    public function getDefaultConnection()
    {
        return $this->app['config']['search.default'];
    }

    /**
     * Create connection
     *
     * @param  null|string $name
     * @return mixed
     */
    public function connection($name = null)
    {
        $name = $name ?: $this->getDefaultConnection();
        $config = $this->app['config']['search.connections.' . $name];

        if (!isset($this->conections[$name])) {
            $this->conections[$name] = new Connection($config);
        }

        return $this->conections[$name];
    }
}

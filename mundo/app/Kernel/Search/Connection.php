<?php

namespace Mundo\Kernel\Search;

use Elasticsearch\ClientBuilder;
use Elasticsearch\Connections\Connection as BaseConnection;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Mundo\Models\Log\PSCRegion;

/**
 * Class Connection
 * @package Mundo\Kernel\Search
 */
class Connection
{
    /**
     * Official ES client connection
     *
     * @var BaseConnection
     */
    protected $connection;

    /**
     * Index map for naming
     *
     * @var array
     */
    protected $indexesNameMap;

    /**
     * Index settings
     *
     * @var array
     */
    public $settings;

    /**
     * Connection constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->settings = $config['settings'];
        $this->connection = ClientBuilder::create()->setHosts($config['hosts'])->build();
    }

    /**
     * Update given models in index
     *
     * @param \Illuminate\Database\Eloquent\Collection $models
     * @return void
     */
    public function update(Collection $models)
    {
        $params = [];
        foreach ($models as $model) {
            $params['body'][] = $this->bulkOperation('index', $model);
            $params['body'][] = $model->toSearchableArray();
        }

        $this->connection->bulk($params);
    }

    /**
     * Delete given models from index
     *
     * @param \Illuminate\Database\Eloquent\Collection $models
     * @return mixed
     */
    public function delete(Collection $models)
    {
        $params = [];
        foreach ($models as $model) {
            $params['body'][] = $this->bulkOperation('delete', $model);
        }

        $this->connection->bulk($params);
    }

    /**
     * Get the results of the given query mapped onto models.
     *
     * @param  \Mundo\Kernel\Search\Builder $builder
     * @return Collection
     */
    public function get(Builder $builder)
    {
        return $this->search($builder)->get();
    }

    /**
     * Bulk operation
     * @param  string $operation
     * @param  \Illuminate\Database\Eloquent\Model $model
     * @return array
     */
    protected function bulkOperation($operation, Model $model)
    {
        return [
            $operation => [
                '_index' => $this->bulkIndexName($model->searchableAs()),
                '_type'  => $model->searchableAs(),
                '_id'    => $model->getKey(),
            ],
        ];
    }

    /**
     * @param $index
     * @return mixed
     */
    protected function bulkIndexName($index)
    {
        if (isset($this->indexesNameMap) && isset($this->indexesNameMap[$index])) {
            return $this->indexesNameMap[$index];
        }

        return $index;
    }

    /**
     * Perfom search on index
     *
     * @param \Mundo\Kernel\Search\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function search(Builder $query)
    {
        return $query->model->searchableQuery($this->performSearch($query));
    }

    /**
     * Perform search on index and return ES result
     *
     * @param Builder $query
     * @param bool $onlyHits
     * @return array|\Illuminate\Support\Collection
     */
    public function result(Builder $query, $onlyHits = true)
    {
        $params = $this->queryParams($query);

        $result = $this->connection->search($params);

        if ($onlyHits && isset($result['hits']['hits'])) {
            return collect($result['hits']['hits'])
                ->map(function ($item) {
                    return array_merge(['id' => $item['_id']], $item['_source']);
                });
        }

        return $result;
    }

    /**
     * @param Builder $query
     * @return array
     */
    protected function performSearch(Builder $query)
    {
        $params = $this->queryParams($query);
        $result = $this->connection->search($params);

        if ($this->shouldGetAllHits($query)) {
            $result = $this->scroll($result);
        } else {
            $result = $result['hits']['hits'];
        }

        return collect($result)->pluck('_id')->all();
    }

    /**
     * Scroll through the es data
     *
     * @param array $result
     * @return array
     */
    protected function scroll(array $result)
    {
        $data = $result['hits']['hits'];
        $scrollId = $result['_scroll_id'];
        while (true) {
            $response = $this->connection->scroll(['scroll_id' => $scrollId, 'scroll' => '10s']);
            if (count($response['hits']['hits']) > 0) {
                $data = array_merge($data, $response['hits']['hits']);
                $scrollId = $response['_scroll_id'];
            } else {
                break;
            }
        }

        return $data;
    }


    /**
     * Prepare params for search
     *
     * @param \Mundo\Kernel\Search\Builder $builder
     * @return array
     */
    protected function queryParams(Builder $builder)
    {
        if($builder->pure) {
            $body = $builder->query;
        } else {
            $body = [
                'query' => [
                    'filtered' => [
                        'query'  => $builder->query,
                        'filter' => [
                            'and' => ['filters' => $builder->filters],
                        ],
                        'fields' => [],
                    ],
                ],
            ];
        }

        $params = [
            'index' => $builder->model->searchableAs(),
            'type'  => $builder->model->searchableAs(),
            'size'  => $builder->limit ?: 10000,
            'from'  => $builder->offset ?: 0,
            'body'  => $body
        ];

        if ($this->shouldGetAllHits($builder)) {
            $params['scroll'] = '10s';
        }

//        dd($params);

        return $params;
    }

    /**
     * @param \Mundo\Kernel\Search\Builder $builder
     * @return bool
     */
    protected function shouldGetAllHits(Builder $builder)
    {
        return !$builder->limit && !$builder->offset;
    }


    /**
     * @return \Elasticsearch\Client|BaseConnection
     */
    public function base()
    {
        return $this->connection;
    }

    /**
     * @param array $map
     */
    public function withIndexesNameMap(array $map)
    {
        $this->indexesNameMap = $map;
    }

    /**
     *
     */
    public function withoutIndexesNameMap()
    {
        unset($this->indexesNameMap);
    }
}

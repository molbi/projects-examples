<?php

namespace Mundo\Kernel\Http\Middleware;

use Closure;
use Illuminate\Support\MessageBag;
use Mundo\Models\TM\Record;
use ReflectionClass;

/**
 * Class ValidateState
 * @package Mundo\Kernel\Http\Middleware
 */
class ValidateState
{
    /**
     * @var null
     */
    private $state = null;
    /**
     * @var
     */
    private $record;
    /**
     * @var MessageBag
     */
    private $bag;

    /**
     * ValidateState constructor.
     *
     * @param MessageBag $bag
     */
    public function __construct(MessageBag $bag)
    {
        $this->bag = $bag;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @param null                      $state
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $state = null)
    {
        $this->record = $request->route('records');
        $this->state = is_null($state) ? $request->route('state') : $state;

        if (in_array($this->state, Record::TERMINAL_STATES)) {
            foreach ($this->getWidgets() as $widget) {
                $this->bag = $this->callValidateMethod($widget);
            }

            if (!$this->bag->isEmpty()) {
                return redirect()->back()->withErrors($this->bag)->ajaxReload();
            }
        }

        return $next($request);
    }

    /**
     * @return array
     */
    private function getWidgets()
    {
        return array_filter($this->record->campaign->getWidgets(), [$this, 'hasValidateMethod']);
    }

    /**
     * @param $widget
     *
     * @return bool
     */
    private function hasValidateMethod($widget)
    {
        return (new ReflectionClass($widget['type']))->hasMethod('validateState');

    }

    /**
     * @param $widget
     *
     * @return mixed
     */
    private function callValidateMethod($widget)
    {
        return app()->make($widget['type'])->validateState($this->bag, $this->record, $this->state);
    }
}

<?php

namespace Mundo\Kernel\Contracts\Widget;

interface Factory {
    /**
     * Determine if given widget exists
     * 
     * @param  string $widget
     * @return bool
     */
    public function exists($widget);

    /**
     * Creates widget instance
     * 
     * @param  string $widget
     * @param  array $params
     * @return Widget
     */
    public function make($widget, $params = []);
}
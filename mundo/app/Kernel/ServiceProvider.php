<?php

namespace Mundo\Kernel;

use Illuminate\Support\ServiceProvider as BaseProvider;

/**
 * Common service provider, all minor stuff is going here
 *
 * Class ServiceProvider
 * @package Mundo\Kernel
 */
class ServiceProvider extends BaseProvider
{

    protected $providers = [
        Config\ServiceProvider::class,
        Search\SearchServiceProvider::class,
        Widget\Illuminate\WidgetServiceProvider::class,
        Html\ServiceProvider::class,
    ];


    /**
     * Register services
     */
    public function register()
    {
        foreach ($this->providers as $provider) {
            $this->app->register($provider);
        }
    }
}

<?php

namespace Mundo\Kernel\Eloquent;

trait UuidPrimaryKey
{
    /**
     * Boot the Uuid trait for the model.
     *
     * @return void
     */
    public static function bootUuidPrimaryKey()
    {
        static::creating(function ($model) {
            $model->{$model->getKeyName()} = uuid();
        });
    }

    public function getIncrementing()
    {
        return false;
    }
}

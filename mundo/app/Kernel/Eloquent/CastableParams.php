<?php

namespace Mundo\Kernel\Eloquent;

trait CastableParams
{
    public function serializeParam($value)
    {
        if ($value === null) {
            return null;
        }
        switch ($this->type) {
            case 'array':
            case 'json':
            case 'object':
            case 'collection':
                return $this->asJson($value);
            case 'date':
            case 'datetime':
                return $this->asDateTime($value);
            default:
                return $value;
        }
    }

    public function castParam($value)
    {
        if (is_null($value)) {
            return $value;
        }

        switch ($this->type) {
            case 'int':
            case 'integer':
                return (int)$value;
            case 'real':
            case 'float':
            case 'double':
                return (float)$value;
            case 'string':
                return (string)$value;
            case 'bool':
            case 'boolean':
                return (bool)$value;
            case 'object':
                return $this->fromJson($value, true);
            case 'array':
            case 'json':
                return $this->fromJson($value);
            case 'date':
            case 'datetime':
                return $this->asDateTime($value);
            case 'timestamp':
                return $this->asTimeStamp($value);
            default:
                return $value;
        }
    }
}

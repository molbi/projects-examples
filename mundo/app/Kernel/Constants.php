<?php

namespace Mundo\Kernel;

class Constants
{
    /** Exchange rates */
    const RATES = [
        'cs' => 1,
        'sk' => 27,
    ];

    const LABELS = [
        'accepted'           => ['PŘI', 'success', 'přijato'],
        'declined'           => ['ODM', 'danger', 'odmítnuto'],
        'new'                => ['NOV', 'default', 'nový'],
        'pending'            => ['NOV', 'default', 'nový'],
        'reached'            => ['DOV', 'primary', 'dovoláno'],
        'not-reached'        => ['NED', 'info', 'nedovoláno'],
        'unreachable'        => ['NED', 'info', 'nedovoláno'],
        'removed'            => ['ODE', 'warning', 'odebráno'],
        'forwarded'          => ['PŘE', 'default', 'předáno'],
        'to-remove'          => ['VYŘ', 'danger', 'k vyřazení'],
        'renewal'            => ['PROD', 'success', 'prodlouženo'],
        'returned'           => ['VRA', 'danger', 'vráceno'],
        'order-call-reached' => ['DOV', 'primary', 'dovoláno'],
        'deleted'            => ['DEL', 'danger', 'smazáno'],
        'published'          => ['PUB', 'success', 'publikováno'],
        'formated'           => ['FOR', 'warning', 'zformátováno'],
        'solved'             => ['VYŘ', 'success', 'vyřešeno'],
        'blocked'            => ['BLO', 'warning', 'blokováno'],
        'closed'             => ['CLO', 'danger', 'uzavřeno']
    ];

    const RECORD_STATES = [
        'accepted'    => 'přijato',
        'declined'    => 'odmítnuto',
        'new'         => 'nový',
        'reached'     => 'dovoláno',
        'not-reached' => 'nedovoláno',
        'removed'     => 'odebráno',
    ];
}

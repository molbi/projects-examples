<?php

namespace Mundo\Kernel\Cron;

use Illuminate\Container\Container;
use Illuminate\Database\DatabaseManager;

class Command extends \Illuminate\Console\Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mundo:run-scheduled
                            {class : Class to run}
                            {db : Database}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs job';
    /** @var \Illuminate\Container\Container */
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
        parent::__construct();
    }


    public function handle()
    {
        $connection = $this->container->make(DatabaseManager::class)->connection($this->argument('db'));
        $instance = $this->container->make($this->argument('class'));
        if (!$instance instanceof Cronable) {
            throw new \InvalidArgumentException('Class is not instance of Cronable interface.');
        }
        $instance->run($connection);
    }
}

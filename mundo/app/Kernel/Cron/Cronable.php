<?php

namespace Mundo\Kernel\Cron;

interface Cronable
{
    public function run();
}

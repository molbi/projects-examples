<?php

namespace Mundo\Kernel\Widget;

use CallbackFilterIterator;
use InvalidArgumentException;
use LogicException;
use RecursiveIteratorIterator;

class Container extends Widget implements IContainer
{
    /**
     * @var IWidget|null
     */
    private $cloning;

    /**
     * Instances of all widgets in the container
     *
     * @var IWidget[]
     */
    protected $widgets = [];


    /**
     * Add a widget to the container
     *
     * @param  IWidget $widget
     * @param  string $name
     * @return self
     */
    public function addWidget(IWidget $widget, $name)
    {
        $name = (string)$name;
        $this->validateName($name);
        $this->checkUniqueName($name);
        $this->checkCircularReference($widget, $name);
        $this->validateChildWidget($widget);

        $widget->setName($name);
        $widget->setParent($this);
        
        $this->widgets[$name] = $widget;

        return $this;
    }

    /**
     * Return widget by it's name
     *
     * @param  string $name
     * @param  bool $strict throw exception if a widget is not found
     * @return IWidget|null
     */
    public function getWidget($name, $strict = true)
    {
        if (isset($this->widgets[$name])) {
            return $this->widgets[$name];
        }
        list($name, $rest) = array_pad(explode(Widget::NAME_SEPARATOR, $name, 2), 2, null);
        $this->validateName($name);

        if (!isset($this->widgets[$name])) {
            $widget = $this->createWidget($name);

            if ($widget) {
                if (!isset($this->widgets[$name])) {
                    $this->addWidget($widget, $name);
                }
            }
        }

        if (isset($this->widgets[$name])) {
            if ($rest === null) {
                return $this->widgets[$name];
            }
            if (!$this->widgets[$name] instanceof IContainer && $strict) {
                throw new InvalidArgumentException("Widget with name '$name' is not container and cannot have [$rest] widget");
            }

            return $this->widgets[$name]->getWidget($rest, $strict);
        } elseif ($strict) {
            throw new InvalidArgumentException("Widget with name [$name] does not exist");
        }
    }

    /**
     * Iterates over widgets
     *
     * @param  bool $deep Should be recursive
     * @param  string $type Filter type
     * @return \Iterator
     */
    public function getWidgets($deep = false, $type = null)
    {
        $iterator = new RecursiveWidgetIterator($this->widgets);
        if ($deep) {
            $deep = $deep > 0 ? RecursiveIteratorIterator::SELF_FIRST : RecursiveIteratorIterator::CHILD_FIRST;
            $iterator = new RecursiveIteratorIterator($iterator, $deep);
        }
        if ($type) {
            $iterator = new CallbackFilterIterator($iterator, function ($item) use ($type) {
                return $item instanceof $type;
            });
        }

        return $iterator;
    }

    /**
     * Remove a widget instance from the container
     *
     * @param  IWidget $widget
     * @return void
     */
    public function removeWidget(IWidget $widget)
    {
        $name = $widget->getName();
        if (!isset($this->widgets[$name]) || $this->widgets[$name] !== $widget) {
            throw new InvalidArgumentException("This container does not contain widget named [$name]");
        }
        unset($this->widgets[$name]);
        $widget->setParent(null);
    }

    /**
     * Create instance by calling factory method
     *
     * @param  string $name
     * @return IWidget|null
     */
    protected function createWidget($name)
    {
        $method = $this->getCreateMethod($name);
        if ($method !== null) {
            $widget = $this->tryCall($method, ['name' => $name]);
            if (!$widget instanceof IWidget && !isset($this->widgets[$name])) {
                $class = get_class($method[0]);
                $method = $method[1];
                throw new LogicException("Method [$class::$method()] did not return or create a widget");
            }

            return $widget;
        }
    }

    /**
     * Where to find create methods
     * @return mixed
     */
    protected function getCreator()
    {
        return $this;
    }

    /**
     * Returns create method array
     * @param  string $name
     * @return array|null
     */
    protected function getCreateMethod($name)
    {
        $ucname = ucfirst($name);
        $method = 'createWidget' . $ucname;
        $obj = $this->getCreator();
        if ($ucname !== $name
            && method_exists($obj, $method)
            && (new \ReflectionMethod($obj, $method))->getName() === $method
        ) {
            return [$obj, $method];
        }

        return null;
    }

    /**
     * Used for calling methods, this can be overloaded for DI
     *
     * @param  array $method
     * @param  array $args
     * @return mixed
     */
    protected function tryCall($method, array $args)
    {
        return $this->getMethod($method)->invokeArgs($method[0], $args);
    }

    /**
     * Check if method exists
     *
     * @param $method
     * @return bool|\ReflectionMethod
     */
    protected function getMethod(array $method)
    {
        $class = $method[0];
        $method = $method[1];
        $rc = new \ReflectionClass($class);
        if ($rc->hasMethod($method)) {
            $rm = $rc->getMethod($method);
            if ($rm->isPublic() && !$rm->isAbstract() && !$rm->isStatic()) {
                return $rm;
            }
        }
        $class = is_string($class) ? $class : get_class($class);
        throw new InvalidArgumentException("Method [$class::$method()] does not exist or isn't accessible.");
    }

    /**
     * User validation to disallow adding of a widget
     *
     * @param IWidget $child
     */
    protected function validateChildWidget(IWidget $child)
    {
    }

    /**
     * @param $name
     */
    private function validateName($name)
    {
        if (!preg_match('#^[a-zA-Z0-9_]+\z#', $name)) {
            throw new InvalidArgumentException("Widget name must be non-empty alphanumeric string, [$name] given.");
        }
    }

    /**
     * @param IWidget $widget
     * @param $name
     */
    private function checkCircularReference(IWidget $widget, $name)
    {
        $obj = $this;
        do {
            if ($obj === $widget) {
                throw new LogicException("Circular reference detected while adding widget [$name].");
            }
            $obj = $obj->getParent();
        } while ($obj !== null);
    }

    /**
     * @param $name
     */
    private function checkUniqueName($name)
    {
        if (isset($this->widgets[$name])) {
            throw new LogicException("Widget with name [$name] already exists.");
        }
    }

    /**
     * Object cloning.
     */
    public function __clone()
    {
        if ($this->widgets) {
            $oldMyself = reset($this->widgets)->getParent();
            $oldMyself->cloning = $this;
            foreach ($this->widgets as $name => $component) {
                $this->widgets[$name] = clone $component;
            }
            $oldMyself->cloning = null;
        }
        parent::__clone();
    }

    /**
     * Is container cloning now?
     * @return null|IWidget
     * @internal
     */
    public function _isCloning()
    {
        return $this->cloning;
    }
}

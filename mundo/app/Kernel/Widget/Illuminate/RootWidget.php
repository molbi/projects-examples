<?php

namespace Mundo\Kernel\Widget\Illuminate;

use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Mundo\Http\Controllers\TestController;
use Mundo\Kernel\Widget\Illuminate\Reflection\WidgetReflection;
use Mundo\Kernel\Widget\Illuminate\Routing\UrlGenerator;
use Mundo\Kernel\Widget\Illuminate\Routing\WidgetsController;

class RootWidget extends BaseWidget
{
    /** @var  array */
    protected $globalState;
    /** @var Application */
    private $app;
    /** @var  WidgetsController */
    private $controller = null;
    /** @var string */
    private $action = null;
    /** @var array */
    protected $globalParams = [];
    /** @var array */
    protected $globalStateSinces;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function getRoot()
    {
        return $this;
    }

    /**
     * Returns view factory
     * @return Factory
     */
    public function getViewFactory()
    {
        return $this->app->make('view');
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->app['request'];
    }

    /**
     * Url generator instance
     * @return UrlGenerator
     */
    public function getUrlGenerator()
    {
        return $this->app->make('url');
    }

    /**
     * Application context
     * @return \Illuminate\Foundation\Application
     */
    public function getApplication()
    {
        return $this->app;
    }

    /**
     * Sets the proxy for widget creation
     * @param WidgetsController $controller
     * @param string action
     */
    public function setController(WidgetsController $controller, $action = null)
    {
        $this->controller = $controller;
        $this->action = $action;
    }

    /**
     * Unique id
     * @return string
     */
    public function getUniqueId()
    {
        return '';
    }

    /**
     * Returns create proxy
     * @return WidgetsController
     */
    public function getCreator()
    {
        return $this->controller;
    }

    public function getController()
    {
        return '\\' . get_class($this->controller);
    }

    public function getAction()
    {
        return sprintf("%s@%s", $this->getController(), $this->action);
    }

    /**
     * Loads params
     */
    public function initGlobalParameters()
    {
        $this->globalParams = [];
        $selfParams = [];
        $params = $this->getUrlGenerator()->getRequest()->all();
        foreach ($params as $key => $value) {
            if (!preg_match('#^((?:[a-z0-9_]+-)*)((?!\d+\z)[a-z0-9_]+)\z#i', $key, $matches)) {
                continue;
            } elseif (!$matches[1]) {
                $selfParams[$key] = $value;
            } else {
                $this->globalParams[substr($matches[1], 0, -1)][$matches[2]] = $value;
            }
        }
        $this->loadState($selfParams);
    }

    /**
     * Pops global params
     *
     * @param  string $id
     * @return array
     */
    public function popGlobalParameters($id)
    {
        if (isset($this->globalParams[$id])) {
            $res = $this->globalParams[$id];
            unset($this->globalParams[$id]);

            return $res;
        } else {
            return [];
        }
    }

    public function saveGlobalState()
    {
        $this->globalParams = [];
        $this->globalState = null;

        return $this->getGlobalState();
    }

    public function getGlobalState($forClass = null)
    {
        $sinces = &$this->globalStateSinces;
        if ($this->globalState === null) {
            $state = [];
            foreach ($this->globalParams as $id => $params) {
                $prefix = $id . self::NAME_SEPARATOR;
                foreach ($params as $key => $val) {
                    $state[$prefix . $key] = $val;
                }
            }
            $this->saveState($state, $forClass ? new WidgetReflection($forClass) : null);
            if ($sinces === null) {
                $sinces = [];
                foreach ($this->getReflection()->getPersistentParams() as $name => $meta) {
                    $sinces[$name] = $meta['since'];
                }
            }
            $widgets = $this->getReflection()->getPersistentWidgets();
            $iterator = $this->getWidgets(true, IStatePersistent::class);
            foreach ($iterator as $name => $widget) {
                if ($iterator->getDepth() === 0) {
                    // counts with Nette\Application\RecursiveIteratorIterator::SELF_FIRST
                    $since = isset($widgets[$name]['since']) ? $widgets[$name]['since'] : false; // FALSE = nonpersistent
                }
                $prefix = $widget->getUniqueId() . self::NAME_SEPARATOR;
                $params = [];

                $widget->saveState($params);
                foreach ($params as $key => $val) {
                    $state[$prefix . $key] = $val;
                    $sinces[$prefix . $key] = $since;
                }
            }
        } else {
            $state = $this->globalState;
        }

        if ($forClass !== null) {
            $since = null;
            foreach ($state as $key => $foo) {
                if (!isset($sinces[$key])) {
                    $x = strpos($key, self::NAME_SEPARATOR);
                    $x = $x === false ? $key : substr($key, 0, $x);
                    $sinces[$key] = isset($sinces[$x]) ? $sinces[$x] : false;
                }
                if ($since !== $sinces[$key]) {
                    $since = $sinces[$key];
                    $ok = $since && is_a($forClass, $since, true);
                }
                if (!$ok) {
                    unset($state[$key]);
                }
            }
        }

        return $state;
    }

    public function autoloadWidgets()
    {
        foreach ($this->globalParams as $id => $foo) {
            $this->getWidget($id, false);
        }
    }

    public function render()
    {
        return '';
    }

    /**
     * Conditional redirect to canonicalized URI.
     * @return null|RedirectResponse
     */
    public function canonicalize()
    {
        if (!$this->getRequest()->ajax()
            && ($this->getRequest()->isMethod('get') || $this->getRequest()->isMethod('head'))
        ) {
            if ($this->getUrlGenerator()->current() !== $this->getRequest()->fullUrl()) {
                return new RedirectResponse($this->getUrlGenerator()->current(),
                    RedirectResponse::HTTP_MOVED_PERMANENTLY);
            }
        }
    }
}

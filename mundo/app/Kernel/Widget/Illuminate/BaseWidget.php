<?php

namespace Mundo\Kernel\Widget\Illuminate;

use InvalidArgumentException;
use Mundo\Kernel\Widget\Container;
use Mundo\Kernel\Widget\IContainer;
use Mundo\Kernel\Widget\Illuminate\Reflection\PersistentParamsByAnnotation;
use Mundo\Kernel\Widget\Illuminate\Reflection\WidgetReflection;
use Mundo\Kernel\Widget\Illuminate\Routing\WidgetsController;
use Mundo\Kernel\Widget\Illuminate\Signalling\BadSignalException;
use Mundo\Kernel\Widget\Illuminate\Signalling\ISignalReceiver;
use Mundo\Kernel\Widget\IWidget;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

abstract class BaseWidget extends Container implements IRenderable, ISignalReceiver, \ArrayAccess, IStatePersistent
{
    use PersistentParamsByAnnotation;

    /** @var array */
    protected $params = [];

    /**
     * @return RootWidget
     */
    public function getRoot()
    {
        return $this->lookup(RootWidget::class, true);
    }

    /**
     * Render view file
     *
     * @return string
     */
    public function renderView($path, $data = [])
    {
        return $this->getView($path, $data)->render();
    }


    /**
     * Used for calling methods, this can be overloaded for DI
     *
     * @param  array $method
     * @param  array $args
     * @return mixed
     */
    protected function tryCall($method, array $args)
    {
        return $this->getRoot()->getApplication()->call($method, $args);
    }


    /**
     * Get view by filename
     *
     * @param  string $path
     * @param   array $data
     * @return \Illuminate\Contracts\View\View
     */
    public function getView($path, $data = [])
    {
        $data['__widgetId'] = $this->getUniqueId();

        $data['widget'] = $this;

        return $this->getRoot()->getViewFactory()->file($path, $data);
    }

    /**
     * Process signal
     * @param  string $signal
     * @param  array $params
     * @return mixed
     */
    public function signalReceived($signal, array $params)
    {
        try {
            return $this->tryCall([$this->getCreator(), $this->formatSignalMethod($signal)], $params);
        } catch (\ReflectionException $e) {
            $class = get_class($this->getCreator());
            throw new BadSignalException("There is no handler for signal [$signal] in class [$class].", $e);
        }
    }

    /**
     * Format signal method name
     *
     * @param  string $signal
     * @return string
     */
    protected function formatSignalMethod($signal)
    {
        return 'handle' . ucfirst($signal);
    }

    /**
     * Check if this widget can handle signal
     *
     * @param  string $signal
     * @param  array $args
     */
    protected function canHandleSignal($signal, array $args)
    {
        $methodName = $this->formatSignalMethod($signal);
        try {
            $method = $this->getMethod([$this->getCreator(), $methodName]);
        } catch (InvalidArgumentException $e) {
            $class = get_class($this->getCreator());
            throw new BadSignalException("The class [$class] has no signal handler for [$signal]", $e);
        }

        if (($accept = $method->getNumberOfParameters()) < ($given = count($args))) {
            $class = get_class($this->getCreator());
            throw new BadSignalException("The method [$class::$methodName()] accept [$accept] arguments at most, you are trying to send [$given]");
        }
    }


    /**
     * Syntactic sugar, enables check of widget existence by `is_set($widget['foo'])`
     *
     * @param mixed $name
     * @return bool
     */
    public function offsetExists($name)
    {
        return $this->getWidget($name, false) !== null;
    }

    /**
     * Syntactic sugar, enables to obtain widget by `$widget['foo']` it is same like `$widget->getWidget('foo')`
     *
     * @param mixed $name
     * @return bool
     */
    public function offsetGet($name)
    {
        return $this->getWidget($name, true);
    }

    /**
     * Syntactic sugar, enables to add widget by `$widget['foo'] = new Foo;` it is same
     * like `$widget->addWidget(new Foo, 'foo')`
     *
     * @param mixed $name
     * @return bool
     */
    public function offsetSet($name, $widget)
    {
        return $this->addWidget($widget, $name);
    }

    /**
     * Syntactic sugar, enables to remove widget by `unset($widget['foo'])` it is same
     * like `$widget->removeWidget($widget->getWidget('foo'))`
     *
     * @param mixed $name
     * @return bool
     */
    public function offsetUnset($name)
    {
        if (($widget = $this->getWidget($name, false)) !== null) {
            $this->removeWidget($widget);
        }
    }

    /**
     * Load persistent state
     *
     * @param IContainer $root
     */
    protected function attached(IContainer $root)
    {
        if ($root instanceof RootWidget) {
            $this->loadState($root->popGlobalParameters($this->getUniqueId()));
        }
    }

    /**
     * Unique fully qualified name
     *
     * @return string
     */
    public function getUniqueId()
    {
        return $this->lookupPath(RootWidget::class, true);
    }

    /**
     * Set monitoring for root widget
     * @param IContainer $parent
     */
    protected function validateParent(IContainer $parent)
    {
        $this->monitor(RootWidget::class);
        parent::validateParent($parent);
    }

    /**
     * Loads the state
     *
     * @param array $params
     */
    public function loadState(array $params)
    {
        $reflection = new WidgetReflection($this->getCreator());
        foreach ($reflection->getPersistentParams() as $name => $meta) {
            if (isset($params[$name])) {
                $type = gettype($meta['def']);
                if (!WidgetReflection::convertType($params[$name], $type)) {
                    throw new BadRequestHttpException(sprintf(
                        "Value passed to persistent parameter [%s] in %s must be [%s], [%s] given.",
                        $name,
                        $this->getCreator() instanceof WidgetsController ? "controller [{$reflection->getName()}]" : "widget [{$this->getUniqueId()}]",
                        $type === 'NULL' ? 'scalar' : $type,
                        is_object($params[$name]) ? get_class($params[$name]) : gettype($params[$name])
                    ));
                }
                try {
                    $reflection->getProperty($name)->setValue($this->getCreator(), $params[$name]);
                } catch (\ReflectionException $ex) {
                }
            } else {
                $params[$name] = $meta['def'];
            }
        }
        $this->params = $params;
    }

    /**
     * Returns widget param.
     * @param  string $name key
     * @param  mixed $default default value
     * @return mixed
     */
    public function getParameter($name, $default = null)
    {
        if (isset($this->params[$name])) {
            return $this->params[$name];
        } else {
            return $default;
        }
    }

    /**
     * Returns component parameters.
     * @return array
     */
    public function getParameters()
    {
        return $this->params;
    }

    /**
     * Returns a fully-qualified name that uniquely identifies the parameter.
     * @param  string
     * @return string
     */
    public function getParameterId($name)
    {
        $uid = $this->getUniqueId();

        return $uid === '' ? $name : $uid . self::NAME_SEPARATOR . $name;
    }

    /**
     * Saves the state
     *
     * @param array $params
     * @param null $reflection
     */
    public function saveState(array & $params, $reflection = null)
    {
        $reflection = $reflection === null ? $this->getReflection() : $reflection;
        foreach ($reflection->getPersistentParams() as $name => $meta) {
            if (isset($params[$name])) {
                // injected value
            } elseif (array_key_exists($name, $params)) { // NULLs are skipped
                continue;
            } elseif ((!isset($meta['since']) || $this->getCreator() instanceof $meta['since']) && isset($this->getCreator()->$name)) {
                $params[$name] = $this->getCreator()->$name; // object property value
            } else {
                continue; // ignored parameter
            }
            $type = gettype($meta['def']);
            if (!WidgetReflection::convertType($params[$name], $type)) {
                throw new InvalidArgumentException(sprintf(
                    "Value passed to persistent parameter [%s] in %s must be [%s], [%s] given.",
                    $name,
                    $this->getCreator() instanceof WidgetsController ? "controller [{$reflection->getName()}]" : "widget [{$this->getUniqueId()}]",
                    $type === 'NULL' ? 'scalar' : $type,
                    is_object($params[$name]) ? get_class($params[$name]) : gettype($params[$name])
                ));
            }
            if ($params[$name] === $meta['def'] || ($meta['def'] === null && $params[$name] === '')) {
                $params[$name] = null; // value transmit is unnecessary
            }
        }
    }

    public function getReflection()
    {
        return new WidgetReflection($this->getCreator());
    }

    /**
     * Generates URL to route, it can replace persisten params with right key
     * @param  string $name
     * @param  array $parameters
     * @param  bool $absolute
     * @return string
     */
    public function routeUrl($name, $parameters = [], $absolute = true)
    {
        return $this->getRoot()->getUrlGenerator()
            ->setCurrentWidget($this)
            ->route($name, $parameters, $absolute);
    }

    /**
     * Generates URL to action, it can replace persisten params with right key
     * @param  string $action
     * @param  array $parameters
     * @param  bool $absolute
     * @return string
     */
    public function actionUrl($action, $parameters = [], $absolute = true)
    {
        if (is_array($action)) {
            $absolute = $parameters;
            $parameters = $action;
            $action = $this->getRoot()->getAction();
        }
        if (strpos($action, '@') === false) {
            $action = $this->getRoot()->getController() . '@' . $action;
        }

        return $this->getRoot()->getUrlGenerator()
            ->setCurrentWidget($this)
            ->action($action, $parameters, $absolute);
    }

    public function currentUrl($params = [], $absolute = true)
    {
        return $this->getRoot()->getUrlGenerator()
            ->setCurrentWidget($this)
            ->action($this->getRoot()->getAction(), array_merge($this->getCurrentParams(), $params), $absolute);
    }

    public function signalUrl($signal, $args = [], $absolute = true)
    {
        $widget = $this;
        if (strpos(':', $signal) !== false) {
            list($widget, $signal) = explode(':', $signal, 2);
            $widget = $this->getWidget($widget);
        }
        $widget->canHandleSignal($signal, $args);
        $signal = $widget->getParameterId($signal);
        $params = array_merge($this->getCurrentParams(), [
            WidgetsController::SIGNAL_KEY      => $signal,
            WidgetsController::SIGNAL_ARGS_KEY => $args,
        ]);

        return $this->getRoot()->getUrlGenerator()
            ->setCurrentWidget($this)
            ->action($this->getRoot()->getAction(), $params, $absolute);
    }

    protected function getCurrentParams()
    {
        return $this->getRoot()->getRequest()->route()->parameters();
    }
}

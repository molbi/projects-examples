<?php

namespace Mundo\Kernel\Widget\Illuminate\Routing;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Routing\Controller;
use InvalidArgumentException;
use Mundo\Kernel\Widget\Illuminate\Reflection\PersistentParamsByAnnotation;
use Mundo\Kernel\Widget\Illuminate\Reflection\WidgetReflection;
use Mundo\Kernel\Widget\Illuminate\Signalling\BadSignalException;
use Mundo\Kernel\Widget\Illuminate\Signalling\ISignalReceiver;
use Mundo\Kernel\Widget\Illuminate\RootWidget;
use Mundo\Kernel\Widget\IWidget;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

abstract class WidgetsController extends Controller implements \ArrayAccess
{
    use PersistentParamsByAnnotation;

    const SIGNAL_KEY = 'do';
    const SIGNAL_ARGS_KEY = 'doargs';
    private $rootWidget;

    public function __construct()
    {
        $this->getRootWidget();
    }

    /**
     * Root widget container
     *
     * @return RootWidget
     */
    protected function getRootWidget()
    {
        if ($this->rootWidget === null) {
            $this->rootWidget = app(RootWidget::class);
        }

        return $this->rootWidget;
    }

    /**
     * Overload action call and register widgets (lazy creation)
     * @param $method
     * @param $parameters
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function callAction($method, $parameters)
    {
        $this->action = $method;
        $this->getRootWidget()->setController($this, $method);
        $this->getRootWidget()->initGlobalParameters();

        $this->startup();

        $this->getRootWidget()->autoloadWidgets();
        $signalResult = $this->processSignal();
        // If the signal return response, then stop execution
        if ($signalResult instanceof Response) {
            return $signalResult;
        }

        $result = parent::callAction($method, $parameters);

//        if (($canonicalize = $this->getRootWidget()->canonicalize()) instanceof Response) {
//            return $canonicalize;
//        }

        $this->beforeRender();
        // Render content if it is renderable
        if ($result instanceof Renderable) {
            $result = $result->render();
        }

        $this->afterRender();


        $this->getRootWidget()->saveGlobalState();

        return $result;
    }

    /**
     * Add a widget to the container
     *
     * @param  IWidget $widget
     * @param  string $name
     * @return self
     */
    public function addWidget(IWidget $widget, $name)
    {
        $this->getRootWidget()->addWidget($widget, $name);

        return $this;
    }

    /**
     * Remove a widget instance from the container
     *
     * @param  IWidget $widget
     * @return self
     */
    public function removeWidget(IWidget $widget)
    {
        $this->getRootWidget()->removeWidget($widget);

        return $this;
    }

    /**
     * Return widget by it's name
     *
     * @param  string $name
     * @return IWidget|null
     */
    public function getWidget($name)
    {
        return $this->getRootWidget()->getWidget($name);
    }

    /**
     * Iterates over widgets
     *
     * @param  bool $deep Should be recursive
     * @param  string $filter Filter type
     * @return \Iterator
     */
    public function getWidgets($deep = false, $filter = null)
    {
        return $this->getRootWidget()->getWidgets($deep, $filter);
    }

    /**
     * Process signal
     *
     * @return mixed
     */
    protected function processSignal()
    {
        if ($signal = $this->getSignalFromRequest()) {
            if ($receiver = $this->getSignalReceiver($signal)) {
                list($signal, $path) = $signal;
                if (!$receiver instanceof ISignalReceiver) {
                    throw new BadSignalException("The signal receiver widget '$path' is not ISignalReceiver.");
                }

                return $receiver->signalReceived($signal, $this->getSignalArgsFromRequest());
            }
            $path = $signal[1];
            $receiver = $path ? $path : get_class($this);
            throw new BadSignalException("The signal receiver widget [$receiver] is not found.", isset($e) ? $e : null);
        }
    }

    /**
     * Extract arguments for a signal
     *
     * @return array
     */
    protected function getSignalArgsFromRequest()
    {
        return $this->getRouter()->getCurrentRequest()->get(self::SIGNAL_ARGS_KEY, []);
    }

    /**
     * Extract signal and its path, return false if there is no requested signal
     *
     * @return array|bool
     */
    protected function getSignalFromRequest()
    {
        $request = $this->getRouter()->getCurrentRequest();
        if ($request->has(self::SIGNAL_KEY)) {
            return array_pad(
                array_map(
                    'strrev',
                    explode(
                        IWidget::NAME_SEPARATOR,
                        strrev(trim($request->get(self::SIGNAL_KEY), IWidget::NAME_SEPARATOR)),
                        2
                    )),
                2,
                null
            );
        }

        return false;
    }

    /**
     * Get signal receiver
     *
     * @param  array $signal
     * @return bool|ISignalReceiver
     */
    protected function getSignalReceiver(array $signal)
    {
        $path = $signal[1];
        try {
            return $path === null ? $this->getRootWidget() : $this->getRootWidget()->getWidget($path);
        } catch (InvalidArgumentException $e) {
            return false;
        }
    }


    protected function beforeRender()
    {
    }

    protected function afterRender()
    {
    }

    protected function startup()
    {
    }

    /**
     * Syntactic sugar, enables check of widget existence by `is_set($widget['foo'])`
     *
     * @param mixed $name
     * @return bool
     */
    public function offsetExists($name)
    {
        return $this->getRootWidget()->getWidget($name, false) !== null;
    }

    /**
     * Syntactic sugar, enables to obtain widget by `$widget['foo']` it is same like `$widget->getWidget('foo')`
     *
     * @param mixed $name
     * @return bool
     */
    public function offsetGet($name)
    {
        return $this->getRootWidget()->getWidget($name, true);
    }

    /**
     * Syntactic sugar, enables to add widget by `$widget['foo'] = new Foo;` it is same
     * like `$widget->addWidget(new Foo, 'foo')`
     *
     * @param mixed $name
     * @return bool
     */
    public function offsetSet($name, $widget)
    {
        return $this->getRootWidget()->addWidget($widget, $name);
    }

    /**
     * Syntactic sugar, enables to remove widget by `unset($widget['foo'])` it is same
     * like `$widget->removeWidget($widget->getWidget('foo'))`
     *
     * @param mixed $name
     * @return bool
     */
    public function offsetUnset($name)
    {
        if (($widget = $this->getRootWidget()->getWidget($name, false)) !== null) {
            $this->getRootWidget()->removeWidget($widget);
        }
    }

    /**
     * Returns array of persistent widgets.
     * This default implementation detects widgets by class-level annotation @persistent(widget1, widget2).
     * @return array
     */
    public static function getPersistentWidgets()
    {
        return (array)WidgetReflection::parseAnnotation(new \ReflectionClass(get_called_class()), 'persistent');
    }
}

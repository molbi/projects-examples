<?php

namespace Mundo\Kernel\Widget\Illuminate\Routing;

use Faker\Provider\Base;
use Illuminate\Http\Request;
use Illuminate\Routing\Exceptions\UrlGenerationException;
use Illuminate\Routing\Route;
use Illuminate\Routing\RouteCollection;
use Illuminate\Routing\UrlGenerator as BaseUrlGenerator;
use Illuminate\Support\Arr;
use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Kernel\Widget\Illuminate\RootWidget;

class UrlGenerator extends BaseUrlGenerator
{
    /** @var \Mundo\Kernel\Widget\Illuminate\RootWidget */
    protected $widget;

    /** @var  BaseWidget */
    protected $currentWidget;

    /**
     * Sets widget for current generation
     * @internal
     * @param \Mundo\Kernel\Widget\Illuminate\BaseWidget $widget
     * @return self
     */
    public function setCurrentWidget(BaseWidget $widget)
    {
        $this->currentWidget = $widget;

        return $this;
    }

    /**
     * Determines if this url generation should use widget
     * @return bool
     */
    protected function hasCurrentWidget()
    {
        return $this->currentWidget !== null;
    }

    /**
     * Pops widget for url generation
     * @return \Mundo\Kernel\Widget\Illuminate\BaseWidget
     */
    protected function popCurrentWidget()
    {
        $widget = $this->currentWidget;
        $this->currentWidget = null;

        return $widget;
    }

    /**
     * Create a new URL Generator instance.
     *
     * @param  \Illuminate\Routing\RouteCollection $routes
     * @param  \Illuminate\Http\Request $request
     * @param \Mundo\Kernel\Widget\Illuminate\RootWidget $widget
     */
    public function __construct(RouteCollection $routes, Request $request, RootWidget $widget)
    {
        parent::__construct($routes, $request);
        $this->widget = $widget;
    }

    /**
     * Returns controller class for route
     *
     * @param \Illuminate\Routing\Route $route
     * @return false|string
     */
    protected function getControllerForRoute(Route $route)
    {
        if (is_string($route->getAction()['uses'])) {
            return explode('@', $route->getAction()['uses'])[0];
        }

        return false;
    }

    /**
     * What controller is used now
     *
     * @return false|string
     */
    protected function getCurrentController()
    {
        return $this->getControllerForRoute($this->getRequest()->route());
    }

    /**
     * Get the URL for a given route instance.
     *
     * @param  \Illuminate\Routing\Route $route
     * @param  mixed $parameters
     * @param  bool $absolute
     * @return string
     * @throws \Illuminate\Routing\Exceptions\UrlGenerationException
     */
    protected function toRoute($route, $parameters, $absolute)
    {
        if ($this->hasCurrentWidget()) {
            $widget = $this->popCurrentWidget();
            $this->addPrefixesToParams($widget, $parameters);
            $this->addPersistentState($this->getControllerForRoute($route), $parameters);
        }

        return parent::toRoute($route, $parameters, $absolute);
    }

    /**
     * Auto-prefix for persistent parameters of widget from where was route called
     *
     * @param BaseWidget $widget
     * @param array $params
     */
    protected function addPrefixesToParams(BaseWidget $widget, array &$params)
    {
        $persistentParams = $widget->getReflection()->getPersistentParams();
        foreach ($params as $key => $value) {
            if (is_string($key) && array_search($key, array_keys($persistentParams)) !== false) {
                $params[$widget->getParameterId($key)] = $value != $persistentParams[$key]['def'] ? $value : null;
                unset($params[$key]);
            }
        }
    }

    /**
     * Adds persistent state to params for a controller
     * @param string $controller
     * @param array $params
     */
    protected function addPersistentState($controller, array &$params)
    {
        if ($controller === $this->getCurrentController()) {
            $controller = null;
        }
        $state = $this->withoutNulls($this->widget->getGlobalState($controller));
        foreach ($state as $key => $value) {
            if (!array_key_exists($key, $params)) {
                $params[$key] = $value;
            }
        }
        $params = $this->withoutNulls($params);
    }

    /**
     * Remove nulls from array
     *
     * @param  array $array
     * @return array
     */
    protected function withoutNulls(array $array)
    {
        return array_filter($array, function ($item) {
            return $item !== null;
        });
    }

    /**
     * Is current request served by controller?
     * @param $controller
     * @return bool
     */
    public function isCurrentServedByController($controller)
    {
        if ($this->rootNamespace && !(strpos($controller, '\\') === 0)) {
            $controller = $this->rootNamespace . '\\' . $controller;
        } else {
            $controller = trim($controller, '\\');
        }

        return $this->getCurrentController() === $controller;
    }
}

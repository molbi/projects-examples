<?php
namespace Mundo\Kernel\Widget\Illuminate\Signalling;

interface ISignalReceiver
{
    /**
     * @param $signal
     * @param array $params
     * @return mixed
     */
    public function signalReceived($signal, array $params);
}

<?php

namespace Mundo\Kernel\Widget\Illuminate\Reflection;

trait PersistentParamsByAnnotation
{
    /**
     * Returns array of classes persistent parameters. They have public visibility and are non-static.
     * This default implementation detects persistent parameters by annotation @persistent.
     * @return array
     */
    public static function getPersistentParams()
    {
        $rc = new \ReflectionClass(get_called_class());
        $params = [];
        foreach ($rc->getProperties(\ReflectionProperty::IS_PUBLIC) as $rp) {
            if (!$rp->isStatic() && WidgetReflection::parseAnnotation($rp, 'persistent')) {
                $params[] = $rp->getName();
            }
        }

        return $params;
    }
}

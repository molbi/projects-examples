<?php

namespace Mundo\Kernel\Widget\Illuminate\Reflection;

use Mundo\Kernel\Widget\Illuminate\BaseWidget;
use Mundo\Kernel\Widget\Illuminate\Routing\WidgetsController;
use Reflector;

/**
 * Helper class for persistence parameters
 * @package Mundo\Kernel\Widget\Illuminate
 */
class WidgetReflection extends \ReflectionClass
{
    /**
     * Cache for persistence params
     * @var array
     */
    private static $persistentParams = [];

    /** @var array */
    private static $persistentWidgets = [];

    /**
     * Extracts persistent params (annotated by @persistent)
     *
     * @return array
     */
    public function getPersistentParams($class = null)
    {
        $class = $class === null ? $this->getName() : $class;

        if (($params = &self::$persistentParams[$class]) !== null) {
            return $params;
        }
        $params = [];

        if ($this->hasGetPersistentParamsMethod($class)) {
            $defaults = get_class_vars($class);

            foreach ($properties = $class::getPersistentParams() as $name => $default) {
                if (is_int($name)) {
                    $name = $default;
                    $default = isset($defaults[$name]) ? $defaults[$name] : null;
                }
                $params[$name] = [
                    'def'   => $default,
                    'since' => $class,
                ];
            }
            foreach ($this->getPersistentParams(get_parent_class($class)) as $name => $param) {
                if (isset($params[$name])) {
                    $params[$name]['since'] = $param['since'];
                    continue;
                }
                $params[$name] = $param;
            }
        }


        return $params;
    }

    protected static function hasGetPersistentParamsMethod($class)
    {
        try {
            $rf = new \ReflectionMethod($class, 'getPersistentParams');
        } catch (\ReflectionException $ex) {
            return false;
        }

        return $rf->isPublic() && $rf->isStatic();
    }

    /**
     * Returns an annotation value.
     * @param Reflector $ref
     * @param $name
     * @return array|false
     */
    public static function parseAnnotation(Reflector $ref, $name)
    {
        if (!preg_match_all("#[\\s*]@$name(?:\\(\\s*([^)]*)\\s*\\))?#", $ref->getDocComment(), $m)) {
            return false;
        }
        static $tokens = ['true' => true, 'false' => false, 'null' => null];
        $res = [];
        foreach ($m[1] as $s) {
            foreach (preg_split('#\s*,\s*#', $s, -1, PREG_SPLIT_NO_EMPTY) ?: ['true'] as $item) {
                $res[] = array_key_exists($tmp = strtolower($item), $tokens) ? $tokens[$tmp] : $item;
            }
        }

        return $res;
    }

    /**
     * Non data-loss type conversion.
     * @param  mixed $val
     * @param  string $type
     * @return bool $isClass
     */
    public static function convertType(& $val, $type, $isClass = false)
    {
        if ($isClass) {
            return $val instanceof $type;
        } elseif ($type === 'callable') {
            return false;
        } elseif ($type === 'NULL') { // means 'not array'
            return !is_array($val);
        } elseif ($type === 'array') {
            return is_array($val);
        } elseif (!is_scalar($val)) { // array, resource, NULL, etc.
            return false;
        } else {
            $old = $tmp = ($val === false ? '0' : (string)$val);
            settype($tmp, $type);
            if ($old !== ($tmp === false ? '0' : (string)$tmp)) {
                return false; // data-loss occurs
            }
            $val = $tmp;
        }

        return true;
    }

    /**
     * @param  string|NULL
     * @return array of persistent components.
     */
    public function getPersistentWidgets($class = null)
    {
        $class = $class === null ? $this->getName() : $class;
        $widgets = &self::$persistentWidgets[$class];
        if ($widgets !== null) {
            return $widgets;
        }
        $widgets = [];
        if (is_subclass_of($class, WidgetsController::class)) {
            foreach ($class::getPersistentWidgets() as $name => $meta) {
                if (is_string($meta)) {
                    $name = $meta;
                }
                $widgets[$name] = ['since' => $class];
            }
            $widgets = $this->getPersistentWidgets(get_parent_class($class)) + $widgets;
        }

        return $widgets;
    }
}

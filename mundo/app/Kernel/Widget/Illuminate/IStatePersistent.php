<?php

namespace Mundo\Kernel\Widget\Illuminate;

interface IStatePersistent
{
    /**
     * Loads state informations.
     * @return void
     */
    public function loadState(array $params);

    /**
     * Saves state informations for next request.
     * @return void
     */
    public function saveState(array & $params);
}

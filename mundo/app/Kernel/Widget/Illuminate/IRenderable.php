<?php

namespace Mundo\Kernel\Widget\Illuminate;

interface IRenderable
{
    public function render();
}

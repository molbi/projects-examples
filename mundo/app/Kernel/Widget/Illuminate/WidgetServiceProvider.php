<?php

namespace Mundo\Kernel\Widget\Illuminate;

use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Mundo\Kernel\Widget\Illuminate\Routing\UrlGenerator;
use Mundo\Kernel\Widget\Illuminate\View\Environment;

class WidgetServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $env = $this->app[Environment::class];
        $view = $this->app['view'];
        $env->extend($view->getEngineResolver()->resolve('blade')->getCompiler());
        $view->share('__widgetEnv', $env);
        $view->share('widget', $this->app[RootWidget::class]);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerRootWidget();
        $this->registerViewEnvironment();
        $this->registerUrlGenerator();
    }

    protected function registerUrlGenerator()
    {
        $this->app['url'] = $this->app->share(function ($app) {
            $routes = $app['router']->getRoutes();

            // The URL generator needs the route collection that exists on the router.
            // Keep in mind this is an object, so we're passing by references here
            // and all the registered routes will be available to the generator.
            $app->instance('routes', $routes);

            $url = new UrlGenerator($routes, $app->rebinding('request', function ($app, $request) {
                $app['url']->setRequest($request);
            }), $app[RootWidget::class]);

            $url->setSessionResolver(function () {
                return $this->app['session'];
            });

            // If the route collection is "rebound", for example, when the routes stay
            // cached for the application, we will need to rebind the routes on the
            // URL generator instance so it has the latest version of the routes.
            $app->rebinding('routes', function ($app, $routes) {
                $app['url']->setRoutes($routes);
            });

            return $url;
        });
    }

    protected function registerRootWidget()
    {
        $this->app->singleton(RootWidget::class, function (Application $app) {
            $w = new RootWidget($app);
            $w->setName('root');

            return $w;
        });
    }

    protected function registerViewEnvironment()
    {
        $this->app->singleton(Environment::class, function (Application $app) {
            return new Environment($app['view'], $app[RootWidget::class]);
        });
    }
}

<?php

namespace Mundo\Kernel\Widget\Illuminate\View;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\View\Compilers\BladeCompiler;
use Illuminate\View\Factory;
use Mundo\Kernel\Widget\Illuminate\RootWidget;
use Mundo\Kernel\Widget\IWidget;

class Environment
{
    /** @var Factory */
    protected $view;
    /** @var  RootWidget */
    protected $widget;

    public function __construct(Factory $view, RootWidget $widget)
    {
        $this->view = $view;
        $this->widget = $widget;
    }

    public function extend(BladeCompiler $compiler)
    {
        $compiler->extend(function ($value, $compiler) {
            return $this->compileStatements($value, $compiler);
        });
    }

    public function partial($path, $view, array $data, array $mergeData = [])
    {
        return $this->view->file($this->getPartialFile($path, $view), $data, $mergeData);
    }

    public function signal($id, $signal, $args = [])
    {
        $widget = $id === null ? $this->widget : $this->widget->getWidget($id);

        return $widget->signallUrl($signal, $args);
    }

    public function renderEachPartial($path, $view, $data, $iterator, $empty = 'raw|')
    {
        $result = '';

        // If is actually data in the array, we will loop through the data and append
        // an instance of the partial view to the final result HTML passing in the
        // iterated value of this data array, allowing the views to access them.
        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                $data = ['key' => $key, $iterator => $value];

                $result .= $this->view->file($this->getPartialFile($path, $view), $data)->render();
            }
        }

        // If there is no data in the array, we will render the contents of the empty
        // view. Alternatively, the "empty view" could be a raw string that begins
        // with "raw|" for convenience and to let this know that it is a string.
        else {
            if (Str::startsWith($empty, 'raw|')) {
                $result = substr($empty, 4);
            } else {
                $result = $this->view->file($this->getPartialFile($path, $empty))->render();
            }
        }

        return $result;
    }

    protected function getPartialFile($path, $view, $strict = true)
    {
        $file = false;
        foreach ($this->getPartialPossibilities($path, $view) as $possibility) {
            if (($file = realpath($possibility)) && !is_dir($file)) {
                break;
            }
        }
        if ($strict && !$file) {
            throw new \InvalidArgumentException("The view partial [$view] was not found in dir [$path].");
        }

        return $file ? $file : null;
    }


    protected function getPartialPossibilities($path, $view)
    {
        $view = $path . '/' . $view;
        $possibilities = [
            $view,
        ];
        foreach ($this->view->getExtensions() as $extension => $type) {
            $possibilities[] = $view . '.' . $extension;
        }

        return $possibilities;
    }

    /**
     * Strip the parentheses from the given expression.
     *
     * @param  string $expression
     * @return string
     */
    protected function stripParentheses($expression)
    {
        if (Str::startsWith($expression, '(')) {
            $expression = substr($expression, 1, -1);
        }

        return $expression;
    }

    protected function compilePartial($expression, $compiler)
    {
        $expression = $this->stripParentheses($expression);
        $path = dirname($compiler->getPath());

        return "<?php echo \$__widgetEnv->partial('$path', $expression, array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>";
    }

    protected function compileSignal($expression)
    {
        return "<?php echo \$widget->signalUrl{$expression}; ?>";
    }

    protected function compileEachPartial($expression, $compiler)
    {
        $expression = $this->stripParentheses($expression);
        $path = dirname($compiler->getPath());

        return "<?php echo \$__widgetEnv->renderEachPartial('$path', $expression); ?>";
    }

    protected function compileWidget($expression)
    {
        $expression = $this->stripParentheses($expression);

        return "<?php echo \$widget->getWidget($expression)->render(); ?>";
    }

    protected function compileRoute($expression)
    {
        return "<?php echo \$widget->routeUrl{$expression}; ?>";
    }

    protected function compileCurrent($expression)
    {
        return "<?php echo \$widget->currentUrl{$expression}; ?>";
    }

    protected function compileAction($expression)
    {
        return "<?php echo \$widget->actionUrl{$expression}; ?>";
    }

    protected function compileStatements($value, $compiler)
    {
        $callback = function ($match) use ($compiler) {
            if (method_exists($this, $method = 'compile' . ucfirst($match[1]))) {
                $match[0] = $this->$method(Arr::get($match, 3), $compiler);
            }

            return isset($match[3]) ? $match[0] : $match[0] . $match[2];
        };

        return preg_replace_callback('/\B@(@?\w+)([ \t]*)(\( ( (?>[^()]+) | (?3) )* \))?/x', $callback, $value);
    }
}

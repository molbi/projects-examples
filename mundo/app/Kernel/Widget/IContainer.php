<?php

namespace Mundo\Kernel\Widget;

interface IContainer extends IWidget
{

    /**
     * Add a widget to the container
     *
     * @param  IWidget $widget
     * @param  string $name
     * @return void
     */
    public function addWidget(IWidget $widget, $name);

    /**
     * Remove a widget instance from the container
     *
     * @param  IWidget $widget
     * @return void
     */
    public function removeWidget(IWidget $widget);

    /**
     * Return widget by it's name
     *
     * @param  string $name
     * @return IWidget|null
     */
    public function getWidget($name);

    /**
     * Iterates over widgets
     *
     * @param  bool $deep Should be recursive
     * @param  string $filter Filter type
     * @return \Iterator
     */
    public function getWidgets($deep = false, $filter = null);
}

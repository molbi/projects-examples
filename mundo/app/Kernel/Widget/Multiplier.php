<?php

namespace Mundo\Kernel\Widget;

use Mundo\Kernel\Widget\Illuminate\BaseWidget;

class Multiplier extends Container
{
    /** @var callable */
    protected $factory;

    public function __construct(callable $factory)
    {
        $this->factory = $factory;
    }

    protected function createWidget($name)
    {
        return call_user_func($this->factory, $name, $this);
    }
}

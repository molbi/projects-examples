<?php

namespace Mundo\Kernel\Widget;

use LogicException;

abstract class Widget implements IWidget
{
    /**
     * The parent container
     *
     * @var IContainer
     */
    private $parent;
    /**
     * Name of the widget
     *
     * @var String
     */
    private $name;

    /**
     * Cache for monitors
     *
     * @var array of [type => [obj, depth, path, is_monitored?]]
     */
    private $monitors = [];

    /**
     * Get the name of the widget
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Return parent if any
     *
     * @return IContainer|NULL
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set widget name
     *
     * @param  string $name
     * @return Widget
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Set the widget's parent
     *
     * @param  IContainer $parent
     * @return Widget
     */
    public function setParent(IContainer $parent = null)
    {
        if ($parent !== null && $this->parent !== null) {
            throw new LogicException("Widget [$this->name] already has parent.");
        }

        if ($parent !== null) {
            $this->validateParent($parent);
            $this->parent = $parent;
            $tmp = [];
            $this->refreshMonitors(0, $tmp);
        } else {
            $this->refreshMonitors(0);
            $this->parent = null;
        }


        return $this;
    }

    /**
     * Starts monitoring.
     * @param  string class/interface type
     * @return void
     */
    public function monitor($type)
    {
        if (empty($this->monitors[$type][3])) {
            if ($obj = $this->lookup($type, false)) {
                $this->attached($obj);
            }
            $this->monitors[$type][3] = true; // mark as monitored
        }
    }

    /**
     * Stops monitoring.
     * @param  string class/interface type
     * @return void
     */
    public function unmonitor($type)
    {
        unset($this->monitors[$type]);
    }

    /**
     * Search through hierarchy for widget specified by a class name or interface
     *
     * @param  string $type class or interface type
     * @param  bool $strict should throw exception if type is not found?
     * @return IWidget|null
     */
    public function lookup($type, $strict = false)
    {
        if (!isset($this->monitors[$type])) {
            $obj = $this->getParent();
            $path = self::NAME_SEPARATOR . $this->getName();
            $depth = 1;
            while ($obj !== null) {
                $parent = $obj->getParent();
                if ($type ? $obj instanceof $type : $parent === null) {
                    break;
                }
                $path = self::NAME_SEPARATOR . $obj->getName() . $path;
                $depth++;
                $obj = $parent;
                if ($obj === $this) {
                    $obj = NULL; // prevent cycling
                }
            }
            if ($obj) {
                $this->monitors[$type] = [$obj, $depth, substr($path, 1), false];
            } else {
                $this->monitors[$type] = [null, null, null, false];
            }
        }

        if ($strict && $this->monitors[$type][0] === null) {
            throw new LogicException("Widget [$this->name] is not attached to [$type].");
        }

        return $this->monitors[$type][0];
    }

    /**
     * Search through hierarchy for widget's path specified by a class name or interface
     *
     * @param  string $type class or interface type
     * @param  bool $strict
     * @return string|null
     */
    public function lookupPath($type = null, $strict = false)
    {
        $this->lookup($type, $strict);

        return $this->monitors[$type][2];
    }

    /**
     * Validate parent, this method can be used to validate parent.
     *
     * @param IContainer $parent
     * @throws LogicException
     * @return void
     */
    protected function validateParent(IContainer $parent)
    {
    }

    /**
     * This method is called when widget (or widget's parents) are attached.
     *
     * @param IContainer $container
     * @return void
     */
    protected function attached(IContainer $container)
    {
    }

    /**
     * This method is called when widget (or widget's parents) are detached.
     *
     * @param IContainer $container
     */
    protected function detached(IContainer $container)
    {
    }

    /**
     * Refreshes monitors.
     * @param  int
     * @param  array|NULL (array = attaching, NULL = detaching)
     * @param  array
     * @return void
     */
    private function refreshMonitors($depth, & $missing = null, & $listeners = [])
    {
        if ($this instanceof IContainer) {
            foreach ($this->getWidgets() as $component) {
                if ($component instanceof self) {
                    $component->refreshMonitors($depth + 1, $missing, $listeners);
                }
            }
        }
        if ($missing === null) { // detaching
            foreach ($this->monitors as $type => $rec) {
                if (isset($rec[1]) && $rec[1] > $depth) {
                    if ($rec[3]) { // monitored
                        $this->monitors[$type] = [null, null, null, true];
                        $listeners[] = [$this, $rec[0]];
                    } else { // not monitored, just randomly cached
                        unset($this->monitors[$type]);
                    }
                }
            }
        } else { // attaching
            foreach ($this->monitors as $type => $rec) {
                if (isset($rec[0])) { // is in cache yet
                    continue;
                } elseif (!$rec[3]) { // not monitored, just randomly cached
                    unset($this->monitors[$type]);
                } elseif (isset($missing[$type])) { // known from previous lookup
                    $this->monitors[$type] = [null, null, null, true];
                } else {
                    $this->monitors[$type] = null; // forces re-lookup
                    if ($obj = $this->lookup($type, false)) {
                        $listeners[] = [$this, $obj];
                    } else {
                        $missing[$type] = true;
                    }
                    $this->monitors[$type][3] = true; // mark as monitored
                }
            }
        }
        if ($depth === 0) { // call listeners
            $method = $missing === null ? 'detached' : 'attached';
            $prev = [];
            foreach ($listeners as $item) {
                if (!in_array($item, $prev, true)) {
                    $item[0]->$method($item[1]);
                    $prev[] = $item;
                }
            }
        }
    }

    /**
     * Object cloning.
     */
    public function __clone()
    {
        if ($this->parent === null) {
            return;
        } elseif ($this->parent instanceof Container) {
            $this->parent = $this->parent->_isCloning();
            if ($this->parent === null) { // not cloning
                $this->refreshMonitors(0);
            }
        } else {
            $this->parent = null;
            $this->refreshMonitors(0);
        }
    }

    /**
     * Prevents serialization.
     */
    public function __sleep()
    {
        throw new \Exception('Object serialization is not supported by class [' . get_class($this) . ']');
    }

    /**
     * Prevents unserialization.
     */
    public function __wakeup()
    {
        throw new \Exception('Object unserialization is not supported by class [' . get_class($this) . ']');
    }
}

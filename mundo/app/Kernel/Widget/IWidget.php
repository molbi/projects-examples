<?php

namespace Mundo\Kernel\Widget;

interface IWidget
{
    /**
     * Separator for widget names (nesting)
     */
    const NAME_SEPARATOR = '-';

    /**
     * Get the name of the widget
     *
     * @return string
     */
    public function getName();

    /**
     * Return parent if any
     *
     * @return IContainer|NULL
     */
    public function getParent();

    /**
     * Set widget name
     *
     * @param  string $name
     * @return void
     */
    public function setName($name);

    /**
     * Set the widget's parent
     *
     * @param  IContainer $parent
     * @return void
     */
    public function setParent(IContainer $parent);
}

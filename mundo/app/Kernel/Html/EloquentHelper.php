<?php

namespace Mundo\Kernel\Html;

class EloquentHelper
{
    /**
     * The DOM id convention is to use the singular form of an object or class with the id following an underscore.
     * If no id is found, prefix with “create_” instead.
     *
     * @param  object|\Illuminate\Database\Eloquent\Model $record
     * @param  string $prefix
     * @param  string $fallbackPrefix By default it's 'create'
     *
     * @return string
     */
    public static function domId($record, $prefix = null, $fallbackPrefix = 'create')
    {
        if ($recordId = static::recordKeyForDomId($record)) {
            return static::domClass($record, $prefix) . '_' . $recordId;
        } else {
            $prefix = $prefix ?: $fallbackPrefix;

            return static::domClass($record, $prefix);
        }
    }

    /**
     * The Form id convention is to use the singular form of an object or class with the id following an underscore.
     * If id is found, prefix with “edit_”.
     * If no id is found, prefix with “create_” instead.
     *
     * @param  object|\Illuminate\Database\Eloquent\Model $record
     * @param  string $fallbackPrefix By default it's 'create'
     *
     * @return string
     */
    public static function formId($record, $fallbackPrefix = 'create')
    {
        if ($recordId = static::recordKeyForDomId($record)) {
            return static::domClass($record, 'edit') . '_' . $recordId;
        } else {
            return static::domClass($record, $fallbackPrefix);
        }
    }

    /**
     * @param  object|\Illuminate\Database\Eloquent\Model $record
     *
     * @return string
     */
    public static function recordKeyForDomId($record)
    {
        $key = is_a($record, '\Illuminate\Database\Eloquent\Model') ? $record->getKey() : null;

        return $key ? implode('_', (array)$key) : $key;
    }

    /**
     * The DOM class convention is to use the singular form of an object or class.
     *
     * @param  string|object|\Illuminate\Database\Eloquent\Model $recordOrClass
     * @param  string $prefix
     *
     * @return string
     */
    public static function domClass($recordOrClass, $prefix = null)
    {
        $singular = snake_case(camel_case(preg_replace('/\\\\/', ' ',
            static::modelNameFromRecordOrClassname($recordOrClass))));

        return $prefix ? $prefix . '_' . $singular : $singular;
    }

    /**
     * @param  string|object|\Illuminate\Database\Eloquent\Model $recordOrClass
     *
     * @return string
     */
    protected static function modelNameFromRecordOrClassname($recordOrClass)
    {
        return is_string($recordOrClass) ? $recordOrClass : get_class($recordOrClass);
    }
}

<?php

namespace Mundo\Kernel\Html;

use B2M\Laravel\BootstrapForm\FormBuilder as BaseBuilder;

class FormBuilder extends BaseBuilder
{
    /**
     * Vytvoří input pole
     *
     * @param $type
     * @param $name
     * @param null $value
     * @param array $options
     *
     * @return string
     */
    public function plainInput($type, $name, $value = null, $options = [])
    {
        $input = parent::plainInput($type, $name, $value, $options);
        $name = last($this->groupStack);

        return $this->toHtmlString($input . $this->formatError($name));
    }

    /**
     * Vytvoří textarea pole
     *
     * @param $name
     * @param null $value
     * @param array $options
     *
     * @return string
     */
    public function plainTextarea($name, $value = null, $options = [])
    {
        $input = parent::plainTextarea($name, $value, $options);
        $name = last($this->groupStack);

        return $this->toHtmlString($input . $this->formatError($name));
    }

    /**
     * Vytvoří select pole
     *
     * @param $name
     * @param array $list
     * @param null $selected
     * @param array $options
     *
     * @return string
     */
    public function plainSelect($name, $list = [], $selected = null, $options = [])
    {
        $input = parent::plainSelect($name, $list, $selected, $options);
        $name = last($this->groupStack);

        return $this->toHtmlString($input . $this->formatError($name));
    }

    /**
     * Zavře skupinu pro bootstrap
     *
     * @return string
     */
    public function groupClose()
    {
        $name = array_pop($this->groupStack);

        return $this->toHtmlString('</div>');
    }

    /**
     * Naformátuje první chybu
     *
     * @param $name Název inputu
     *
     * @return string
     */
    protected function formatError($name)
    {
        if ($this->hasError($name)) {
            $errors = $this->session->get('errors');

            return $errors->first($this->transformKey($name), '<p class="help-block error">:message</p>');
        } else {
            return '';
        }
    }

    /**
     * Zjistí, zda dané pole má chybu
     *
     * @param $name
     *
     * @return bool
     */
    protected function hasError($name)
    {
        if (!isset($this->session) || !$this->session->has('errors')) {
            return false;
        }

        $errors = $this->session->get('errors');

        return $errors->has($this->transformKey($name));
    }
}

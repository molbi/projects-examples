<?php

namespace Mundo\Kernel\Validators;

/**
 * Class ServicesRule
 * @package Mundo\Kernel\Validators
 */
class ServicesRule
{
    /**
     *
     */
    const RULE = 'services';

    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     * @param $validator
     *
     * @return bool
     */
    public function validate($attribute, $value, $parameters, $validator)
    {
        foreach ($value as $type) {
            if ($this->isSelected($type)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $type
     *
     * @return bool
     */
    private function isSelected($type)
    {
        foreach ($type as $service) {
            if (!empty($service)) {
                return true;
            }
        }

        return false;
    }
}
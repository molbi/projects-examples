<?php

Route::group(['prefix' => 'tm', 'namespace' => 'TM'], function () {
    Route::resource('records', 'RecordsController', ['only' => ['show', 'index']]);
    Route::resource('records.additional-phone-numbers', 'AdditionalPhoneNumbersController', ['only' => ['create', 'store', 'destroy']]);
    Route::resource('records.offered-services', 'OfferedServicesController', ['only' => ['store']]);

    Route::get('records/state/{records}/accepted', ['uses' => 'RecordsController@accepted']);
    Route::put('records/state/{records}/accepted', ['uses' => 'RecordsController@saveAccepted']);

    Route::get('records/state/{records}/{state}', ['uses' => 'RecordsController@editState']);
    Route::put('records/state/{records}', ['uses' => 'RecordsController@updateState']);

    Route::resource('reminders', 'RemindersController', ['only' => ['show', 'index']]);
    Route::get('reminders/state/{reminders}/{state}', ['uses' => 'RemindersController@editState']);
    Route::put('reminders/state/{reminders}', ['uses' => 'RemindersController@updateState']);

    Route::group(['prefix' => 'management', 'namespace' => 'Management'], function () {
        Route::get('/', 'DashboardController@index')->name('tm.management.index');
        Route::resource('records', 'RecordsController', ['only' => 'index']);

        Route::resource('move-tm-records', 'MoveRecordsController', ['only' => ['index', 'store']]);
        Route::get('move-tm-records/records', 'MoveRecordsController@records')->name('tm.management.move-tm-records.records');

        Route::resource('fill-campaigns', 'FillCampaignsController', ['only' => ['index', 'show', 'update']]);

        Route::resource('new-records', 'NewRecordsController', ['only' => ['index', 'edit', 'update']]);
    });

    //Settings
    Route::group(['prefix' => 'settings', 'namespace' => 'Settings'], function () {
        Route::get('/', function () {
            return redirect()->action('TM\Settings\CampaignsController@index');
        })->name('tm.settings.index');
        Route::resource('campaign-service-definitions', 'CampaignServiceDefinitionsController', ['except' => 'show']);
        Route::resource('campaigns', 'CampaignsController', ['except' => ['show', 'destroy']]);
    });

    //Statistics
    Route::group(['prefix' => 'statistics', 'namespace' => 'Statistics'], function () {
        Route::get('/', 'DashboardController@index')->name('tm.statistics.index');

        Route::resource('lgc-statistics', 'LgcStatisticsController', ['only' => ['index']]);
        Route::resource('offered-services', 'OfferedServicesController', ['only' => ['index', 'store']]);


        Route::resource('global', 'GlobalController', ['only' => ['index']]);
        Route::resource('inhouse-morality', 'InhouseMoralityController', ['only' => ['index']]);
        Route::resource('leads-splitter', 'LeadsSplitterController', ['only' => ['index']]);


        Route::resource('scampaigns', 'CampaignsController', ['only' => ['index', 'show']]);
        Route::resource('scampaigns.day-detail', 'Campaigns\DayDetailController', ['only' => ['show']]);

        Route::resource('inhouse-leads', 'InhouseLeadsController', ['only' => ['index', 'show']]);
        Route::resource('inhouse-leads.day-detail', 'InhouseLeads\DayDetailController', ['only' => ['show']]);
    });

    Route::resource('forwarding', 'ForwardingController', ['only' => ['show', 'index', 'update']]);

    Route::get('find-clients/inquiries', ['uses' => 'FindClientsController@inquiries']);
    Route::post('find-clients/find', ['uses' => 'FindClientsController@find']);
    Route::resource('find-clients', 'FindClientsController', ['only' => ['store', 'index']]);

    Route::group(['prefix' => 'leads-forwarding', 'namespace' => 'LeadsForwarding'], function () {
        Route::resource('with-campaigns', 'WithCampaignsController', ['only' => ['show', 'index', 'update']]);
        Route::resource('without-campaigns', 'WithoutCampaignsController', ['only' => ['index', 'store']]);
    });
});

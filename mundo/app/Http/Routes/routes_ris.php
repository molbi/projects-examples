<?php

Route::group(['prefix' => 'ris', 'namespace' => 'Ris'], function() {
    Route::get('/', function() {
        return redirect()->action('Ris\RecordsController@index');
    });
    Route::resource('ris_records', 'RecordsController', ['only' => ['show', 'index']]);
    Route::get('ris_records/state/{ris_records}/accepted', ['uses' => 'RecordsController@accepted']);
    Route::put('ris_records/state/{ris_records}/accepted', ['uses' => 'RecordsController@saveAccepted']);
    Route::get('ris_records/state/{ris_records}/next-selling', ['uses' => 'RecordsController@nextSelling']);
    Route::put('ris_records/state/{ris_records}/next-selling', ['uses' => 'RecordsController@saveNextSelling']);
    Route::get('ris_records/state/{ris_records}/{state}', ['uses' => 'RecordsController@editState']);
    Route::put('ris_records/state/{ris_records}', ['uses' => 'RecordsController@updateState']);

    Route::group(['prefix' => 'settings', 'namespace' => 'Settings'], function () {
        Route::get('/', function() {
            return redirect()->action('Ris\Settings\ServiceDefinitionsController@index');
        })->name('ris.settings.index');
        Route::resource('service-definitions', 'ServiceDefinitionsController', ['except' => 'show']);
    });

    Route::group(['prefix' => 'management', 'namespace' => 'Management'], function () {
        Route::get('/', function() {
            return redirect()->action('Ris\Management\CreditRecordsController@index');
        })->name('ris.management.index');

        Route::resource('credit-records', 'CreditRecordsController', ['except' => 'show']);
    });
});

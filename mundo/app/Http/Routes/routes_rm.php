<?php

Route::group(['prefix' => 'rm', 'namespace' => 'RM'], function() {
    Route::get('/', function() {
        return redirect()->action('RM\RecordsController@index');
    });
    Route::resource('rm_records', 'RecordsController', ['only' => ['show', 'index', 'store', 'create']]);
    Route::get('rm_records/state/{rm_records}/blocked', ['uses' => 'RecordsController@blocked']);
    Route::get('rm_records/state/{rm_records}/{state}', ['uses' => 'RecordsController@editState']);
    Route::put('rm_records/state/{rm_records}', ['uses' => 'RecordsController@updateState']);
});

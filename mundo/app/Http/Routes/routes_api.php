<?php

Route::group(['prefix' => 'api', 'namespace' => 'API', 'middleware' => 'api'], function () {
    Route::group(['prefix' => 'v1'], function () {
//        Route::get('test', function () {
//            return ['sucess' => true];
//        });
//        Route::resource('lead', 'LeadsController', ['only' => 'store']);
        Route::get('payments-morals/{user?}', ['uses' => 'PublicController@paymentMorals']);
    });
});

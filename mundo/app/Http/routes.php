<?php

use Mundo\Models\DbTM\Firm;
use Mundo\Models\Log\PSCRegion;

require app_path('Http/Routes/routes_api.php');

Route::group(['middleware' => 'web'], function () {

    Route::get('/', 'RedirectController@index');

    Route::get('login', 'Auth\AuthController@showLoginForm');
    Route::post('login', 'Auth\AuthController@login');
    Route::get('logout', 'Auth\AuthController@logout');

    Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
    Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\PasswordController@reset');


    Route::group(['middleware' => 'auth'], function () {

        // Searching
        Route::get('search', ['uses' => 'SearchController@search', 'as' => 'search',]);
        Route::get('search/advanced', ['uses' => 'SearchController@advanced']);

        // Email logs
        Route::resource('email-logs', 'EmailLogsController', ['only' => 'show']);

        // Routes for email templating
        Route::get('/email-template/back/{step}', ['uses' => 'EmailTemplatesController@back']);
        Route::get('/email-template/select/{target_type}/{target_id}', ['uses' => 'EmailTemplatesController@select', 'as' => 'send-mail']);
        Route::get('/email-template/variables/{target_type}/{target_id}', ['uses' => 'EmailTemplatesController@variables']);
        Route::get('/email-template/preview/{target_type}/{target_id}', ['uses' => 'EmailTemplatesController@preview']);
        Route::post('/email-template/send/{target_type}/{target_id}', ['uses' => 'EmailTemplatesController@send']);
        // --END -- Routes for email templating

        //SendRequest - požadavek na ZP
        Route::get('send-request/{id}/{model}', ['uses' => 'SendRequestController@create', 'as' => 'send-request.create'])->middleware(['send-request']);
        Route::post('send-request/{id}/{model}', ['uses' => 'SendRequestController@store', 'as' => 'send-request.store'])->middleware(['send-request']);

        //REMINDERING ROUTES
        require app_path('Http/Routes/routes_rm.php');
        //RIS ROUTES
        require app_path('Http/Routes/routes_ris.php');
        //TM ROUTES
        require app_path('Http/Routes/routes_tm.php');

        // System settings - tools for managing all mundo modules (users, roles, permissions, emails templates, user params, user widgets)
        Route::group(['prefix' => 'system-settings', 'namespace' => 'SystemSettings'], function () {
            Route::get('/', function () {
                return redirect()->action('SystemSettings\UsersController@index');
            })->name('system-settings.index');
            Route::resource('user-param-definitions', 'UserParamDefinitionsController', ['except' => 'show']);
            Route::resource('user-widget-definitions', 'UserWidgetDefinitionsController', ['except' => 'show']);
            Route::resource('email-templates', 'EmailTemplatesController', ['except' => ['show']]);
            Route::resource('users', 'UsersController', ['except' => 'show']);
            Route::get('users/sync', 'UsersController@sync');
            Route::get('users/{user}/change-state', 'UsersController@changeState')->name('system-settings.users.change-state');
        });


    });
});

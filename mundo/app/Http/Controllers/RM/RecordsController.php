<?php

namespace Mundo\Http\Controllers\RM;

use Illuminate\Http\Request;
use Mundo\Http\Controllers\Controller;
use Mundo\Jobs\RM\AssignRecordJob;
use Mundo\Kernel\Constants;
use Mundo\Models\Module;
use Mundo\Models\RM\Record;
use Mundo\Modules\TM\BusinessService;
use Mundo\Presenters\RM\RecordsPresenter;
use Mundo\Widgets\RM\RecordWidgets\RecordWidgets;
use Notify;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class RecordsController
 * @package Mundo\Http\Controllers\RM
 */
class RecordsController extends Controller
{

    /**
     * @var
     */
    protected $record;
    /**
     * @var Module
     */
    protected $module;

    /**
     * RecordsController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->module = Module::forName(Module::MODULE_RM);
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function index()
    {
        $presenter = new RecordsPresenter($this->user());

        if (!$presenter->hasRecords()) {
            return view('rm.records.empty');
        } else {
            return redirect($this->getRootWidget()->actionUrl('show', [$presenter->recordsList()->first()->id]));
        }
    }

    /**
     * @param Record $record
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function show(Record $record)
    {
        $presenter = new RecordsPresenter($this->user(), $record, $this->module);
        $this->record = $record;

        if (!$presenter->hasRecords() && !$this->user()->isAdmin()) {
            return view('rm.records.empty');
        } else {
            return response()->view('rm.records.show', compact('presenter'));
        }
    }

    public function create()
    {
        return response()
            ->view('rm.records.create')
            ->ajaxModal('modal-content');
    }

    public function store(Request $request)
    {
        $this->dispatch(new AssignRecordJob($this->user(), $request->only('from', 'to')));

        return redirect()
            ->action('RM\RecordsController@index')
            ->ajaxCloseModal()
            ->ajaxRedirect();
    }

    /**
     * Edit record's state
     *
     * @param Record $record
     * @param  string $state
     *
     * @return mixed
     */
    public function editState(Record $record, $state)
    {
        if (!in_array($state, Record::STATES)) {
            throw new NotFoundHttpException("State [$state] does not exist");
        }

        return response()
            ->view('rm.records.edit-state', compact('record', 'state'))
            ->ajaxModal('modal-content');
    }

    /**
     * Edit record's state
     *
     * @param Record $record
     *
     * @return mixed
     *
     */
    public function blocked(Record $record)
    {
        $record->load([
            'orders' => function ($query) {
                $query->unfinished();
            }
        ]);

        return response()
            ->view('rm.records.blocked', compact('record'))
            ->ajaxModal('modal-content');
    }

    /**
     * Change the record state
     *
     * @param Record $record
     * @param Request $request
     *
     * @param BusinessService $service
     *
     * @return
     */
    public function updateState(Record $record, Request $request, BusinessService $service)
    {
        $notableStates = [Record::STATE_REACHED, Record::STATE_NOT_REACHED, Record::STATE_BLOCKED];
        $scheduableState = [Record::STATE_REACHED, Record::STATE_NOT_REACHED, Record::STATE_BLOCKED];
        $callableStates = [Record::STATE_BLOCKED];

        //validace
        $rules = ['state' => 'required|in:' . implode(',', Record::STATES)];
        if (in_array($request->input('state'), $notableStates)) {
            $rules['note'] = 'required';
        }
        if (in_array($request->input('state'), $scheduableState)) {
            $rules['next_call'] = 'required|date';
        }

        $this->validate($request, $rules);

        //nastavení příštího volání
        if ($nextCall = $request->get('next_call', null)) {
            $record->next_call = $nextCall . ':00';
        }

        //zmena stavu
        $record->setState($request->input('state'), $request->input('note', null), $this->user());

        //ulozeni pozanmky do fobisu
        if ($request->input('save_in_fobis', false)) {
            $service->createNote($record->getRemoteId(), $request->input('note'), $this->user());
        }

        //zavoalni funkce zmeny stavu, pokud existuje
        if (in_array($state = $request->input('state'), $callableStates)) {
            $method = sprintf('setState%s', ucfirst($state));
            $record->$method($request);
        }

        Notify::success('Stav změněn');

        return redirect()
            ->action('RM\RecordsController@index')
            ->ajaxCloseModal()
            ->ajaxRedirect();
    }

    /**
     * @return RecordWidgets
     */
    public function createWidgetRecordWidgets()
    {
        return new RecordWidgets($this->record, $this->module);
    }
}

<?php

namespace Mundo\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailer;
use Illuminate\Mail\Message;
use Mundo\Kernel\Eloquent\TablessModel;
use Mundo\Models\EmailLog;
use Mundo\Models\RIS\RenewalFirm;
use Mundo\Models\TM\Record;
use Mundo\Models\TM\Reminder;

/**
 * Class SendRequestController
 * @package Mundo\Http\Controllers
 */
class SendRequestController extends Controller
{
    /**
     * @var Mailer
     */
    private $mailer;
    /**
     * @var Request
     */
    private $request;
    private $record;

    /**
     * SendRequestController constructor.
     *
     * @param Mailer  $mailer
     * @param Request $request
     */
    public function __construct(Mailer $mailer, Request $request)
    {
        parent::__construct();

        $this->mailer = $mailer;
        $this->request = $request;
    }

    /**
     *
     * @return mixed
     */
    public function create()
    {
        $record = $this->request->route()->getParameter('record');

        return response()
            ->view('send-request.create', compact('record'))
            ->ajaxModal('modal-content');
    }

    /**
     * @return mixed
     *
     */
    public function store()
    {
        $this->record = $this->request->route()->getParameter('record');

        $this->validate($this->request, [
            'request' => 'required|min:9'
        ]);

        $this->sendEmail()
            ->saveHistory();

        return $this->redirectToAction();
    }

    /**
     * @return $this
     *
     */
    private function sendEmail()
    {
        $this->mailer->raw('', function (Message $message) {
            $message->from($this->user()->email, $this->user()->name)
                ->subject($this->emailData()->subject)
                ->to($this->emailData()->to);

            $message->setBody($this->bodyHtml(), 'text/html')
                ->addPart($this->bodyPlain(), 'text/plain');
        });

        return $this;
    }

    /**
     *
     * @return $this
     */
    private function saveHistory()
    {
        EmailLog::log(
            $this->user(),
            $this->record,
            $this->emailData()->subject,
            $this->emailData()->to,
            $this->user()->name,
            $this->user()->email,
            $this->bodyPlain(),
            $this->bodyHtml(),
            []
        );

        return $this;
    }

    private function bodyHtml()
    {
        return
            "Požadavek z TM systému<br /><br />"
            . "Firma: " . record_title($this->record) . " (http://admin.b2m.cz/fobis/firms/detail/id/" . $this->getFirmId() . ")<br />"
            . "Zadal: " . $this->user()->name . "<br /><br />"
            . $this->request->get('request');
    }

    private function bodyPlain()
    {
        return
            "Požadavek z TM systému\n\n\n"
            . "Firma: " . record_title($this->record) . " (http://admin.b2m.cz/fobis/firms/detail/id/" . $this->getFirmId() . ")\n\n"
            . "Zadal: " . $this->user()->name . "\n\n\n"
            . $this->request->get('request');
    }

    private function emailData()
    {
        return (object)[
            'subject' => 'Požadavek - TM systém',
            'to'      => 'callcentrum@b2m.cz'
        ];
    }

    private function redirectToAction()
    {
        return redirect()
            ->action($this->getActionForRecord(), $this->getParametrForAction())
            ->ajaxCloseModal()
            ->ajaxReload();
    }

    private function getFirmId()
    {
        switch (get_class($this->record)) {
            case RenewalFirm::class:
                return $this->record->firm_id;
                break;
            case Record::class:
                return $this->record->remote_id;
                break;
            case Reminder::class:
                return $this->record->subject_id;
                break;
        }
    }

    private function getActionForRecord()
    {
        switch (get_class($this->record)) {
            case RenewalFirm::class:
                return 'Ris\RecordsController@show';
                break;
            case Record::class:
                return 'TM\RecordsController@show';
                break;
            case Reminder::class:
                return 'TM\RemindersController@show';
                break;
        }
    }

    private function getParametrForAction()
    {
        switch(get_class($this->record)) {
            case Model::class:
                return [$this->record];
                break;
            case TablessModel::class:
                return [$this->record->getKey()];
                break;
        }
    }
}

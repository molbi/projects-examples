<?php

namespace Mundo\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Mundo\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Notify;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    protected $redirectPath = '/';

    public function __construct()
    {
        $this->middleware($this->guestMiddleware());
    }

    protected function getResetSuccessResponse($response)
    {
        Notify::success(trans($response));

        return redirect($this->redirectPath());
    }
}

<?php

namespace Mundo\Http\Controllers\TM;

use Illuminate\Http\Request;
use Mundo\Http\Controllers\Controller;
use Mundo\Kernel\Http\Middleware\BeforeChangeState;
use Mundo\Models\TM\Record;
use Mundo\Modules\TM\BusinessService;
use Mundo\Presenters\TM\RecordsPresenter;
use Mundo\Widgets\TM\RecordWidgets\RecordWidgets;
use Notify;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class RecordsController
 * @package Mundo\Http\Controllers
 */
class RecordsController extends Controller
{

    protected $record;

    public function __construct()
    {
        $this->middleware('validate-state:accepted', ['only' => ['accepted']]);

        parent::__construct();
    }


    public function index()
    {
        $presenter = new RecordsPresenter($this->user());
        if (!$presenter->hasRecords()) {
            return view('tm.records.empty');
        } else {
            return redirect($this->getRootWidget()->actionUrl('show', [$presenter->recordsList()->first()->id]));
        }
    }

    public function show(Record $record)
    {
        $presenter = new RecordsPresenter($this->user(), $record);
        $this->record = $record;

        if (!$presenter->hasRecords() && !$this->user()->isAdmin()) {
            return view('tm.records.empty');
        } else {
            return response()->view('tm.records.show', compact('presenter'));
        }
    }

    /**
     * Edit record's state
     *
     * @param  Record $record
     * @param  string $state
     *
     * @return mixed
     */
    public function editState(Record $record, $state)
    {
        if (!in_array($state, Record::STATES)) {
            throw new NotFoundHttpException("State [$state] does not exist");
        }

        return response()
            ->view('tm.records.edit-state', compact('record', 'state'))
            ->ajaxModal('modal-content');
    }

    /**
     * Order + registration form
     *
     * @param BusinessService $service
     * @param Record          $record
     *
     * @return mixed
     */
    public function accepted(BusinessService $service, Record $record)
    {
        $serviceTitles = $service->loadServiceTitles($record->campaign->getServices());
        $categories = $service->loadCategories($record->remoteData->lang);

        return response()
            ->view('tm.records.accepted', compact('record', 'serviceTitles', 'categories'))
            ->ajaxModal('modal-content');
    }

    public function saveAccepted(Request $request, BusinessService $service, Record $record)
    {
        $this->validate($request, [
            'contact_person' => 'required',
            'email'          => 'required|email',
            'phone_number'   => 'required',
            'company'        => 'required',
            'street'         => 'required',
            'city'           => 'required',
            'postal_code'    => 'required',
            'id_number'      => 'required',
            'categories.0'   => 'required',
            'services'       => 'services'
        ]);

        if ($service->isFromBusiness($record)) {
            $firm = $service->updateFirm(array_merge($request->input(), ['firm_id' => $record->remote_id]));
        } else {
            $firm = $service->insertFirm(array_merge($request->input(), ['lang' => $record->remoteData->lang]));
        }

        $source = isset($record->campaign->sync_id)
            ? 'tm_campaign_' . $record->campaign->sync_id
            : 'tm_campaign_new_' . $record->campaign->id;
        foreach ($request->get('services') as $group => $serviceIds) {
            $serviceIds = array_filter($serviceIds, function ($id) {
                return is_numeric($id);
            });
            if (!empty($serviceIds)) {
                $garantion = (bool)$request->input('garantion.' . $group, false);
                $garantion_type = ($garantion && $type = $request->input('garantion-type.' . $group, false)) ? $type : null;

                $orderId = $service->createOrder(
                    $firm->firm_id, $source, $this->user()->id, $serviceIds, $request->input('note'), $garantion, $garantion_type
                );
                Notify::success("Objednávka #$orderId vložena.");
            }
        }

        $record->setState($record::STATE_ACCEPTED, null, $this->user());

        return redirect()
            ->action('TM\RecordsController@index')
            ->ajaxCloseModal()
            ->ajaxRedirect();

        return redirect($this->getRootWidget()->actionUrl('accepted', [$record]));
    }

    /**
     * Change the record state
     *
     * @param Record  $record
     * @param Request $request
     *
     * @return
     */
    public function updateState(Record $record, Request $request)
    {
        $notableStates = [Record::STATE_DECLINED, Record::STATE_REACHED, Record::STATE_REMOVED];
        $scheduableState = [Record::STATE_REACHED, Record::STATE_NOT_REACHED];

        $rules = ['state' => 'required|in:' . implode(',', Record::STATES)];
        if (in_array($request->input('state'), $notableStates)) {
            $rules['note'] = 'required';
        }
        if (in_array($request->input('state'), $scheduableState)) {
            $rules['next_call'] = 'required|date';
        }
        $this->validate($request, $rules);

        if ($nextCall = $request->get('next_call', null)) {
            $record->next_call = $nextCall . ':00';
        }
        $record->setState($request->input('state'), $request->input('note', null), $this->user());
        Notify::success('Stav změněn');

        return redirect()
            ->action('TM\RecordsController@index')
            ->ajaxCloseModal()
            ->ajaxRedirect();
    }


    public function createWidgetRecordWidgets()
    {
        return new RecordWidgets($this->record);
    }
}

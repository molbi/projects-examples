<?php

namespace Mundo\Http\Controllers\TM;

use Illuminate\Http\Request;
use Mundo\Http\Controllers\Controller;
use Mundo\Models\ServiceLog;
use Mundo\Models\TM\Record;

class OfferedServicesController extends Controller
{
    public function store(Record $record, Request $request)
    {
        $this->validate($request, [
            'service' => 'required'
        ]);

        ServiceLog::log($request->user(), $record, $request->get('service'));

        return redirect()->route('tm.records.show', $record)->ajaxReload();
    }


}

<?php

namespace Mundo\Http\Controllers\TM;

use Illuminate\Http\Request;
use Mundo\Http\Controllers\Controller;
use Mundo\Models\TM\AdditionalPhoneNumber;
use Mundo\Models\TM\Record;

class AdditionalPhoneNumbersController extends Controller
{
    public function create(Record $record)
    {
        return response()
            ->view('tm.additional-phone-numbers.create', compact('record'))
            ->ajaxModal('modal-content');
    }

    public function destroy(Record $record, AdditionalPhoneNumber $additionalPhoneNumber) {
        $additionalPhoneNumber->delete();
        $record->searchable();

        return redirect()
            ->action('TM\RecordsController@show', [$record])
            ->ajaxReload();
    }

    public function store(Record $record, Request $request)
    {
        $this->validate($request, [
            'phone_number' => 'required|min:9'
        ]);

        AdditionalPhoneNumber::create([
            'remote_id' => $record->remote_id,
            'source' => $record->campaign->source,
            'phone_number' => $request->input('phone_number'),
            'note' => !empty(trim($request->input('note', ''))) ? $request->input('note', '') : null,
        ]);

        $record->searchable();

        return redirect()
            ->action('TM\RecordsController@show', [$record])
            ->ajaxCloseModal()
            ->ajaxReload();
    }
}

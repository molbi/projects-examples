<?php

namespace Mundo\Http\Controllers\TM;

use Illuminate\Database\Connection;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Http\Request;

use Mundo\Http\Controllers\Controller;
use Mundo\Jobs\TM\FindClients\AssignJob;
use Mundo\Kernel\Cache\PopularInquiries;
use Mundo\Models\DbTM\Firm;
use Mundo\Models\Business\Firm as BusinessFirm;
use Mundo\Models\DbTM\FirmEdb;
use Mundo\Models\Log\PSCRegion;

/**
 * Class FindClientsController
 * @package Mundo\Http\Controllers\TM
 */
class FindClientsController extends Controller
{
    /**
     * @var Request
     */
    private $request;
    /**
     * @var PopularInquiries
     */
    private $cache;
    /**
     * @var Connection
     */
    private $db;

    /**
     * FindClientsController constructor.
     * @param Request $request
     * @param PopularInquiries $cache
     * @param Connection $db
     */
    public function __construct(Request $request, PopularInquiries $cache, Connection $db)
    {
        $this->request = $request;
        $this->cache = $cache;
        $this->db = $db;
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $lang = $this->request->get('lang', 'cs');
        $type = $this->request->get('type', 'cs');
        return response()->view('tm.find-clients.index', compact('lang', 'type'))->ajaxModal('modal-content');
    }

    /**
     * @return mixed
     */
    public function store()
    {
        $this->validate($this->request, [
            'inquiry.*' => 'required',
            'firms'   => 'required_without_all'
        ]);

        $this->dispatch(new AssignJob(
            auth()->user(),
            $this->request->only('inquiry', 'firms'),
            $this->request->get('lang', 'cs'),
            $this->request->get('type', 'cs')
        ));

        return redirect()->route('tm.records.index')->ajaxCloseModal()->ajaxReload();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function inquiries()
    {
        $lang = $this->request->get('lang', 'cs');
        $inquiries = $this->getInquiries($lang);

        return response()->json($inquiries);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function find()
    {
        $type = $this->request->get('type', 'cs');

        switch($type) {
            case 'cs':
                $results = Firm::search($this->prepareQuery())
                    ->take(50)
                    ->asEsResult();
                break;
            case 'sk':
                $results = BusinessFirm::search($this->prepareQuery())
                    ->take(50)
                    ->asEsResult();
                break;
            case 'sk_edb_2017':
               $results = FirmEdb::search($this->prepareQuery())
                ->take(50)
                ->asEsResult();
                break;
        }



        return response()->json($results->toArray());
    }

    /**
     * @return array
     */
    private function prepareQuery()
    {
        $region_id = $this->request->get('region_id', null);
        $lang = $this->request->get('lang', null);
        $q = $this->request->get('q', null);

        $query = [
            'query' => [
                'filtered' => [
                    'query' => [
                        'multi_match' => [
                            'query'  => $q,
                            'fields' => ['description^0.6', 'categories^0.3', 'title^0.1']
                        ],
                    ]
                ]
            ]
        ];

        if ($region_id && $lang == 'cs') {
            $query = array_add($query, 'query.filtered.filter', [
                'terms' => ['postalCode' => PSCRegion::get($region_id)]
            ]);
        }

        return $query;
    }

    /**
     * @param string $lang
     * @return array|static[]
     */
    private function getInquiries($lang = 'cs')
    {
        return $this->db->table('business.Inquiry AS I')
            ->select([
                'I.inquiry_id',
                'I.publishedOn',
                'IL.title',
                'IL.titleSanitized',
                'IL.body',
                'RL.title AS region',
                'CL.title AS category',
                'BL.title AS budget'
            ])
            ->join('business.InquiryLang AS IL', 'IL.inquiry_id', '=', 'I.inquiry_id')
            ->leftJoin('business.CategoryLang AS CL', function (JoinClause $j) {
                $j->on('CL.category_id', '=', 'I.category_id')
                    ->on('CL.lang', '=', 'I.lang');
            })
            ->leftJoin('business.RegionLang AS RL', function (JoinClause $j) {
                $j->on('RL.region_id', '=', 'I.region_id')
                    ->on('RL.lang', '=', 'I.lang');
            })
            ->leftJoin('business.BudgetLang AS BL', function (JoinClause $j) {
                $j->on('BL.budget_id', '=', 'I.budget_id')
                    ->on('BL.lang', '=', 'I.lang');
            })
            ->whereIn('I.inquiry_id', $ids = array_pluck($this->cache->getForLang($lang), 'inquiry_id'))
            ->orderByRaw(sprintf('FIELD(I.inquiry_id, %s)', implode(',', $ids)))
            ->get();
    }

}

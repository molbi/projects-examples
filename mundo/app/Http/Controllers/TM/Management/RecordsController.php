<?php

namespace Mundo\Http\Controllers\TM\Management;

use Mundo\Filters\RecordFilters;
use Mundo\Presenters\TM\Management\RecordsPresenter;

class RecordsController extends ManagementController
{
    /**
     * @param RecordFilters $filters
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     */
    public function index(RecordFilters $filters)
    {
        $presenter = new RecordsPresenter($this->user(), $filters);
        
        return view('tm.management.records.index', compact('presenter'));
    }
}

<?php

namespace Mundo\Http\Controllers\TM\Management;

use Mundo\Http\Controllers\Controller;
use View;

abstract class ManagementController extends Controller
{
    protected function getMenu()
    {
        return [
            [
                'title'       => 'Záznamy operátorů',
                'description' => 'Správa přiřazených záznamů operátora.',
                'icon'        => 'clone',
                'url'         => $this->getRootWidget()->actionUrl('TM\Management\RecordsController@index'),
            ],
            [
                'title'       => 'Přesunutí TM záznamů',
                'description' => 'Přesun TM záznamů z operátora na operátora',
                'icon'        => 'users',
                'url'         => $this->getRootWidget()->actionUrl('TM\Management\MoveRecordsController@index')
            ],
            [
                'title'       => 'Doplnění kontaktů',
                'description' => 'Doplnění kontaktů do kampaní. Zjištění počtu možnách záznamů k doplnění',
                'icon'        => 'pages',
                'url'         => $this->getRootWidget()->actionUrl('TM\Management\FillCampaignsController@index')
            ],
            [
                'title'       => 'Nové záznamy',
                'description' => 'Seznam nových záznamů ze zvolených kampaní',
                'icon'        => 'pages',
                'url'         => $this->getRootWidget()->actionUrl('TM\Management\NewRecordsController@index')
            ],
        ];
    }

    protected function beforeRender()
    {
        View::share('menu', $this->getMenu());
        View::share('menuFromController', true);
        View::share('noLeftBarTopNav', true);
    }
}

<?php

namespace Mundo\Http\Controllers\TM\Management;

class DashboardController extends ManagementController
{
    public function index()
    {
        return view('dashboard');
    }
}

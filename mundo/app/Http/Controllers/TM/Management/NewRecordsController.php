<?php

namespace Mundo\Http\Controllers\TM\Management;

use Illuminate\Http\Request;

use Mundo\Models\TM\Record;
use Mundo\Models\TM\RecordHistory;
use Mundo\Presenters\TM\Management\NewRecordsPresenter;

/**
 * Class NewRecordsController
 * @package Mundo\Http\Controllers\TM\Management
 */
class NewRecordsController extends ManagementController
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $presenter = new NewRecordsPresenter($this->user(), $request);

        return view('tm.management.new-records.index', compact('presenter'));
    }

    /**
     * @param Record $record
     */
    public function edit($record)
    {
        $record = Record::where('id', $record)->first();
        $history = $record->histories->first();

        return response()->view('tm.management.new-records.edit', compact('history'))->ajaxModal('modal-content');
    }

    /**
     * @param Request $request
     * @param $history
     */
    public function update(Request $request, $history)
    {
        $history = RecordHistory::where('id', $history)->first();
        $history->note = $request->get('note');
        $history->save();

        return redirect()->route('tm.management.new-records.index')->ajaxCloseModal();
    }
}

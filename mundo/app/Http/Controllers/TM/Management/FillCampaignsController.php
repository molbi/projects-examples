<?php

namespace Mundo\Http\Controllers\TM\Management;

use Illuminate\Http\Request;
use Mundo\Jobs\TM\Management\FillCampaignsJob;
use Mundo\Models\TM\Campaign;
use Mundo\Models\TM\TargetGroup;

class FillCampaignsController extends ManagementController
{
    /**
     *
     */
    public function index()
    {
        $campaigns = Campaign::with(['targetGroup'])->has('targetGroup')->get();

        return view('tm.management.fill-campaigns.index', compact('campaigns'));
    }

    public function show(Campaign $campaign)
    {
        $count = TargetGroup::getInstance($campaign)->count();

        return response()->json([
            'count' => $count
        ]);
    }

    public function update(Campaign $campaign, Request $request)
    {
        //call old job
        $this->dispatch(new FillCampaignsJob($campaign, $request->get('count', 0)));
        ///call new job when ready
//        $this->dispatch(new NewFillCampaignsJob($campaign, $request->get('count', 0)));
    }
}

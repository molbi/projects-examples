<?php

namespace Mundo\Http\Controllers\TM\Management;

use Illuminate\Http\Request;
use Mundo\Jobs\TM\Management\MoveRecordsJob;
use Mundo\Models\TM\Record;
use Mundo\Models\User;
use Mundo\Presenters\TM\Management\MoveRecordsPresenter;

class MoveRecordsController extends ManagementController
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     */
    public function index()
    {
        $presenter = new MoveRecordsPresenter($this->user());

        return view('tm.management.move-tm-records.index', compact('presenter'));
    }

    public function store(Request $request)
    {
        $this->dispatch(new MoveRecordsJob(
            $request->input('user', null),
            $request->input('records', [])
        ));

        return response([], 200);
    }

    public function records(Request $request)
    {
        $user = User::where('id', $request->get('user'))->first();
        $records = Record::ownedByUser($user)
            ->with('campaign', 'histories', 'params')
            ->unfinished()
            ->ordered()
            ->get()
            ->withRemote()
            ->toJson();

        return response($records);
    }
}

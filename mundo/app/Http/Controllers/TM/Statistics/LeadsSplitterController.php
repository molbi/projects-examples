<?php

namespace Mundo\Http\Controllers\TM\Statistics;

use Illuminate\Http\Request;
use Mundo\Presenters\TM\Statistics\InhouseLeadsPresenter;
use Mundo\Presenters\TM\Statistics\LeadsSplitterPresenter;

/**
 * Class LeadsSplitterController
 * @package Mundo\Http\Controllers\Statistics
 */
class LeadsSplitterController extends StatisticsController
{


    public function index(Request $request)
    {
        $presenter = new LeadsSplitterPresenter($request);

        return view('tm.statistics.leads-splitter.index', compact('presenter'));
    }
}

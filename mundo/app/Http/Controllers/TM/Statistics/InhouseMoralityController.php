<?php

namespace Mundo\Http\Controllers\TM\Statistics;

use Illuminate\Http\Request;
use Mundo\Presenters\TM\Statistics\GlobalPresenter;
use Mundo\Presenters\TM\Statistics\InhouseMoralityPresenter;

/**
 * Class InhouseMoralityController
 * @package Mundo\Http\Controllers\Management
 */
class InhouseMoralityController extends StatisticsController
{
    public function index(Request $request)
    {
        $presenter = new InhouseMoralityPresenter($request);

        return view('tm.statistics.inhouse-morality.index', compact('presenter'));
    }
}

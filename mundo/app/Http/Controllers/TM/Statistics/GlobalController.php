<?php

namespace Mundo\Http\Controllers\TM\Statistics;

use Illuminate\Http\Request;
use Mundo\Presenters\TM\Statistics\GlobalPresenter;

/**
 * Class GlobalController
 * @package Mundo\Http\Controllers\Management
 */
class GlobalController extends StatisticsController
{

    public function index(Request $request)
    {
        $presenter = new GlobalPresenter($request);

        return view('tm.statistics.global.index', compact('presenter'));
    }
}

<?php

namespace Mundo\Http\Controllers\TM\Statistics;

use Illuminate\Http\Request;
use Mundo\Models\User;
use Mundo\Presenters\TM\Statistics\InhouseLeadsPresenter;

/**
 * Class InhouseLeadsController
 * @package Mundo\Http\Controllers\Statistics
 */
class InhouseLeadsController extends StatisticsController
{


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $presenter = new InhouseLeadsPresenter($request);

        return view('tm.statistics.inhouse-leads.index', compact('presenter'));
    }

    /**
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, User $user)
    {
        $presenter = new InhouseLeadsPresenter($request);

        return view('tm.statistics.inhouse-leads.show', compact('presenter', 'user'));
    }
}

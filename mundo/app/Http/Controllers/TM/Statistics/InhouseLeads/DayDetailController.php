<?php

namespace Mundo\Http\Controllers\TM\Statistics\InhouseLeads;

use Mundo\Http\Controllers\TM\Statistics\StatisticsController;
use Mundo\Presenters\TM\Statistics\InhouseLeads\DayDetailPresenter;

class DayDetailController extends StatisticsController
{
    public function show(DayDetailPresenter $presenter)
    {
        return view('tm.statistics.inhouse-leads.day-detail.show', compact('presenter'));
    }
}

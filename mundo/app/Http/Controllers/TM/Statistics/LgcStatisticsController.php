<?php

namespace Mundo\Http\Controllers\TM\Statistics;

use Illuminate\Http\Request;
use Mundo\Presenters\TM\Statistics\LgcStatisticsPresenter;

/**
 * Class LgcStatisticsController
 * @package Mundo\Http\Controllers\Management
 */
class LgcStatisticsController extends StatisticsController
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     */
    public function index(Request $request)
    {
        $presenter = new LgcStatisticsPresenter($this->user(), $request);

        $records = $presenter->usersRecords();
        $orders = $presenter->usersOrders();
        $operators = $presenter->operators();

        return view('tm.statistics.lgc-statistics.index', compact('records', 'orders', 'operators'));
    }
}

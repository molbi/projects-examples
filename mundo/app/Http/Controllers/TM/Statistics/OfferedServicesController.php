<?php

namespace Mundo\Http\Controllers\TM\Statistics;

use Maatwebsite\Excel\Classes\LaravelExcelWorksheet;
use Maatwebsite\Excel\Excel;
use Maatwebsite\Excel\Writers\LaravelExcelWriter;
use Mundo\Filters\BaseFilters;
use Mundo\Presenters\TM\Statistics\OfferedServicesPresenter;

/**
 * Class OfferedServicesController
 * @package Mundo\Http\Controllers\Management
 */
class OfferedServicesController extends StatisticsController
{
    /**
     * @param BaseFilters $filters
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(BaseFilters $filters)
    {
        $presenter = new OfferedServicesPresenter($this->user(), $filters);

        return view('tm.statistics.offered-services.index', compact('presenter', 'chart'));
    }

    public function store(BaseFilters $filters, Excel $excel)
    {
        $presenter = new OfferedServicesPresenter($this->user(), $filters);

        $excel->create('offered-services', function(LaravelExcelWriter $ex) use ($presenter) {
            $ex->sheet('Page1', function(LaravelExcelWorksheet $sheet) use ($presenter) {
                $data = $presenter->records();
                $sheet->loadView('tm.statistics.offered-services.excel', compact('data'));
            });
        })->download('xlsx');
    }
}

<?php

namespace Mundo\Http\Controllers\TM\Statistics;

class DashboardController extends StatisticsController
{
    public function index()
    {
        return view('dashboard');
    }
}

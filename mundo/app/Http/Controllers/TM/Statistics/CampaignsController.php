<?php

namespace Mundo\Http\Controllers\TM\Statistics;

use Mundo\Models\Telemarketing\Campaign;
use Mundo\Presenters\TM\Statistics\CampaignsPresenter;

/**
 * Class CampaignsController
 * @package Mundo\Http\Controllers\Statistics
 */
class CampaignsController extends StatisticsController
{
    /**
     * @param CampaignsPresenter $presenter
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(CampaignsPresenter $presenter)
    {
        return view('tm.statistics.scampaigns.index', compact('presenter'));
    }

    /**
     * @param CampaignsPresenter $presenter
     * @param Campaign $campaign
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(CampaignsPresenter $presenter, Campaign $campaign)
    {
        return view('tm.statistics.scampaigns.show', compact('presenter', 'campaign'));
    }
}

<?php

namespace Mundo\Http\Controllers\TM\Statistics;

use Mundo\Http\Controllers\Controller;
use View;

abstract class StatisticsController extends Controller
{
    protected function getMenu()
    {
        return [
            [
                'title'       => 'Nabízené služby',
                'description' => 'Náhled a export nabízených služeb / operátor',
                'icon'        => 'cog',
                'url'         => $this->getRootWidget()->actionUrl('TM\Statistics\OfferedServicesController@index'),
                'pjax'        => false
            ],

            [
                'title'       => 'Logicall statistics',
                'description' => 'Statistiky operatoru',
                'icon'        => 'cog',
                'url'         => $this->getRootWidget()->actionUrl('TM\Statistics\LgcStatisticsController@index'),
                'pjax'        => false,
                'allow_tags'  => ['logicall']
            ],

            [
                'title'       => 'Globální statistika',
                'description' => 'Globální pohled na cílování a plnění operátorů',
                'icon'        => 'users',
                'url'         => $this->getRootWidget()->actionUrl('TM\Statistics\GlobalController@index'),
                'pjax'        => false,
            ],
            [
                'title'       => 'Denní přírůstky plateb',
                'description' => 'Přehled zaplacených objednávek po dnech',
                'icon'        => 'users',
                'url'         => $this->getRootWidget()->actionUrl('TM\Statistics\InhouseMoralityController@index'),
                'pjax'        => false,
            ],
            [
                'title'       => ' Leady - statistika kampaní',
                'description' => 'Podrobná statistika lead kampaní',
                'icon'        => 'bar-chart',
                'url'         => $this->getRootWidget()->actionUrl('TM\Statistics\CampaignsController@index'),
                'pjax'        => false,
            ],
            [
                'title'       => 'Leady - statistika operátorů',
                'description' => 'Podrobná statistika operátorů na lead kampaních',
                'icon'        => 'users',
                'url'         => $this->getRootWidget()->actionUrl('TM\Statistics\InhouseLeadsController@index'),
                'pjax'        => false,
            ],
            [
                'title'       => 'Leady - rozdělení',
                'description' => 'Pohled na počty rozdělených leadů dle teamů',
                'icon'        => 'users',
                'url'         => $this->getRootWidget()->actionUrl('TM\Statistics\LeadsSplitterController@index'),
                'pjax'        => false,
            ],
        ];
    }

    protected function beforeRender()
    {
        View::share('menu', $this->getMenu());
        View::share('menuFromController', true);
        View::share('noLeftBarTopNav', true);
    }
}

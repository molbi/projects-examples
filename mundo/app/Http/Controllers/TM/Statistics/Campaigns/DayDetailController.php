<?php

namespace Mundo\Http\Controllers\TM\Statistics\Campaigns;

use Mundo\Http\Controllers\TM\Statistics\StatisticsController;
use Mundo\Presenters\TM\Statistics\Campaigns\DayDetailPresenter;

class DayDetailController extends StatisticsController
{
    public function show(DayDetailPresenter $presenter)
    {
        return view('tm.statistics.scampaigns.day-detail.show', compact('presenter'));
    }
}

<?php
namespace Mundo\Http\Controllers\TM\LeadsForwarding;

use Illuminate\Http\Request;

use Mundo\Http\Controllers\Controller;
use Mundo\Jobs\TM\Forwarding\LeadsForwardRecordJob;
use Mundo\Models\TM\Campaign;
use Mundo\Presenters\TM\LeadsForwarding\WithCampaignsPresenter;

class WithCampaignsController extends Controller
{
    public function index()
    {
        $presenter = new WithCampaignsPresenter($this->user());

        if (!$presenter->hasCampaigns()) {
            return redirect()->route('tm.records.index');
        }

        return redirect($this->getRootWidget()->actionUrl('show', [
            $presenter->campaignsList()->first()->id
        ]));
    }

    public function show(Campaign $campaign)
    {
        $presenter = new WithCampaignsPresenter($this->user(), $campaign);

        return response()->view('tm.leads-forwarding.with-campaigns.show', compact('presenter'));
    }

    public function update(Request $request, Campaign $campaign)
    {
        $this->dispatch(new LeadsForwardRecordJob(
            $request->get('records', null),
            $request->get('user_id', null)
        ));

        return redirect()->route('tm.leads-forwarding.with-campaigns.show', $campaign);
    }
}

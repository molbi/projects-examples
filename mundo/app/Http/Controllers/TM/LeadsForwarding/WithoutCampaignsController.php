<?php
namespace Mundo\Http\Controllers\TM\LeadsForwarding;

use Illuminate\Http\Request;

use Mundo\Http\Controllers\Controller;
use Mundo\Jobs\TM\Forwarding\LeadsForwardRecordJob;

use Mundo\Presenters\TM\LeadsForwarding\WithoutCampaignsPresenter;

class WithoutCampaignsController extends Controller
{
    public function index(Request $request)
    {
        $presenter = new WithoutCampaignsPresenter($this->user(), $request);

        return response()->view('tm.leads-forwarding.without-campaigns.index', compact('presenter'));
    }

    public function store(Request $request)
    {
        $this->dispatch(new LeadsForwardRecordJob(
            $request->get('records', null),
            $request->get('user_id', null)
        ));

        return redirect()->back();
    }
}

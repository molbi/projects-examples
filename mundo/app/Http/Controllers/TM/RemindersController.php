<?php

namespace Mundo\Http\Controllers\TM;

use Illuminate\Http\Request;
use Mundo\Http\Controllers\Controller;
use Mundo\Models\TM\Reminder;
use Mundo\Models\TM\ReminderHistory;
use Notify;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RemindersController extends Controller
{
    public function index()
    {
        $reminders = Reminder::allForUser($this->user())
            ->withHistories()
            ->sorted();

        if ($reminders->isEmpty()) {
            return view('tm.reminders.empty');
        } else {
            return redirect($this->getRootWidget()->actionUrl('show', [$reminders->first()->getKey()]));
        }
    }

    public function show($id)
    {

        $reminders = Reminder::allForUser($this->user())
            ->withUsers()
            ->withHistories()
            ->sorted();
        $reminder = $reminders->findOrFail($id);

        return view('tm.reminders.show', compact('reminders', 'reminder'));
    }

    public function editState($id, $state)
    {
        if (!in_array($state, ['reached', 'not-reached'])) {
            throw new NotFoundHttpException("State [$state] does not exist");
        }

        return response()
            ->view('tm.reminders.edit-state', compact('id', 'state'))
            ->ajaxModal('modal-content');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return mixed
     */
    public function updateState(Request $request, $id)
    {
        list($id, $type) = explode('-', $id);
        $this->validate($request, [
            'state'     => 'required|in:reached,not-reached',
            'note'      => 'required',
            'next_call' => 'required',
        ]);

        ReminderHistory::create([
            'remote_id' => $id,
            'type'      => $type,
            'action'    => $request->input('state'),
            'next_call' => $request->input('next_call') . ':00',
            'note'      => $request->input('note'),
            'user_id'   => $this->user()->id,
        ]);

        Notify::success('Stav změněn');

        return redirect()
            ->action('TM\RemindersController@index')
            ->ajaxCloseModal()
            ->ajaxRedirect();
    }
}

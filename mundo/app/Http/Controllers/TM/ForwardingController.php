<?php

namespace Mundo\Http\Controllers\TM;

use Illuminate\Http\Request;
use Mundo\Http\Controllers\Controller;
use Mundo\Jobs\TM\Forwarding\ForwardRecordJob;
use Mundo\Models\User;
use Mundo\Presenters\TM\ForwardingPresenter;

class ForwardingController extends Controller
{
    public function index()
    {
        $presenter = new ForwardingPresenter($this->user());

        if (!$presenter->hasOperators()) {
            return redirect()->route('tm.records.index');
        }

        return redirect($this->getRootWidget()->actionUrl('show', [
            $presenter->operatorsList()->first()->id
        ]));
    }

    public function show(User $operator)
    {
        $presenter = new ForwardingPresenter($this->user(), $operator);

        return response()->view('tm.forwarding.show', compact('presenter'));
    }

    public function update(Request $request, User $operator)
    {
        $this->dispatch(new ForwardRecordJob($operator, $request->only('count', 'campaign')));

        return redirect()->route('tm.forwarding.show', $operator)->ajaxReload();
    }
}

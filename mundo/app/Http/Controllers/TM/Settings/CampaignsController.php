<?php

namespace Mundo\Http\Controllers\TM\Settings;

use Illuminate\Http\Request;
use Mundo\Models\TM\Campaign;
use Notify;

class CampaignsController extends SettingsController
{
    public function index()
    {
        return view('tm.settings.campaigns.index', [
            'campaigns' => Campaign::orderBy('name')->with('tags')->get(),
        ]);
    }

    public function create(Campaign $campaign)
    {
        return view('tm.settings.campaigns.create', compact('campaign'));
    }

    public function edit(Campaign $campaign)
    {
        return view('tm.settings.campaigns.edit', compact('campaign'));
    }

    public function store(Campaign $campaign, Request $request)
    {
        $this->saveCampaign($campaign, $request)
            ->saveTags($campaign, $request)
            ->saveTemplates($campaign, $request);

        Notify::info('Kampaň přidána.');

        return redirect($this->getRootWidget()->actionUrl('index'));
    }

    public function update(Campaign $campaign, Request $request)
    {
        $this->saveCampaign($campaign, $request)
            ->saveTags($campaign, $request)
            ->saveTemplates($campaign, $request);

        Notify::info('Kampaň upravena.');

        return redirect($this->getRootWidget()->actionUrl('index'));
    }

    private function saveCampaign(Campaign $campaign, Request $request)
    {
        $this->validate($request, [
            'sync_id'  => 'integer|unique:tm_campaigns,sync_id,' . $campaign->id . ',id',
            'name'     => 'required',
            'source'   => 'required',
            'widgets'  => 'json',
            'services' => 'json',
        ]);

        $campaign->fill($request->only('name', 'description', 'source', 'sync_id', 'widgets', 'services', 'services_id'));
        if (empty($campaign->services_id)) {
            $campaign->services_id = null;
        }
        $campaign->save();

        return $this;
    }

    private function saveTags(Campaign $campaign, Request $request)
    {
        $campaign->retag($request->get('tags'));

        return $this;
    }

    private function saveTemplates(Campaign $campaign, Request $request)
    {
        $campaign->emailTemplates()->detach();
        foreach ($request->get('email_templates', []) as $id) {
            $campaign->emailTemplates()->attach(['email_template_id' => $id]);
        }

        return $this;
    }
}

<?php

namespace Mundo\Http\Controllers\TM\Settings;

use Illuminate\Http\Request;
use Mundo\Models\TM\CampaignServiceDefinition;
use Notify;

class CampaignServiceDefinitionsController extends SettingsController
{
    protected $rules = [
        'name'       => 'required|max:255',
        'definition' => 'required|json',
        'parent_id'  => 'integer',
    ];

    public function index()
    {
        return view('tm.settings.campaign-service-definitions.index', [
            'definitions' => CampaignServiceDefinition::orderBy('name')->get()
        ]);
    }

    public function create(CampaignServiceDefinition$definition)
    {
        return view('tm.settings.campaign-service-definitions.create', compact('definition'));
    }

    public function edit(CampaignServiceDefinition $definition)
    {
        return view('tm.settings.campaign-service-definitions.edit', compact('definition'));
    }

    public function store(Request $request, CampaignServiceDefinition $definition)
    {
        $this->saveDefinition($definition, $request);
        Notify::success('Definice uložena.');

        return redirect($this->getRootWidget()->actionUrl('index'));
    }

    public function update(Request $request, CampaignServiceDefinition $definition)
    {
        $this->saveDefinition($definition, $request);
        Notify::success('Definice upravena.');

        return redirect($this->getRootWidget()->actionUrl('index'));
    }


    public function destroy(CampaignServiceDefinition $definition)
    {
        $definition->delete();
        Notify::success('Definice smazána.');

        return redirect($this->getRootWidget()->actionUrl('index'));
    }

    protected function saveDefinition(CampaignServiceDefinition $definition, Request $request)
    {
        $this->validate($request, $this->rules);
        $attributes = $request->only('name', 'parent_id', 'definition');
        if (empty($attributes['parent_id'])) {
            $attributes['parent_id'] = null;
        }
        $definition->fill($attributes);

        return $definition->save();
    }
}

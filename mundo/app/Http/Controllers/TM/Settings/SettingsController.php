<?php

namespace Mundo\Http\Controllers\TM\Settings;

use Mundo\Http\Controllers\Controller;
use View;

abstract class SettingsController extends Controller
{
    protected function getMenu()
    {
        return [
            [
                'title'       => 'Kampaně',
                'description' => 'Správa a zakldání nových kampaní.',
                'icon'        => 'phone',
                'url'         => $this->getRootWidget()->actionUrl('TM\Settings\CampaignsController@index'),
            ],
            [
                'title'       => 'Sestavy služeb',
                'description' => 'Předpřipravené sestavy služby, které je možné později přiřadit kampaním',
                'icon'        => 'newspaper-o',
                'url'         => $this->getRootWidget()->actionUrl('TM\Settings\CampaignServiceDefinitionsController@index'),
            ],
        ];
    }

    protected function beforeRender()
    {
        View::share('menu', $this->getMenu());
        View::share('menuFromController', true);
        View::share('noLeftBarTopNav', true);
    }
}

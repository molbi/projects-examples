<?php

namespace Mundo\Http\Controllers;

use Mundo\Models\EmailLog;

class EmailLogsController extends Controller
{
    public function show(EmailLog $log)
    {
        return response()
            ->view('email-logs.show', compact('log'))
            ->ajaxModal('modal-content');
    }
}

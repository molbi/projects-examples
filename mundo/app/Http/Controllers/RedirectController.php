<?php

namespace Mundo\Http\Controllers;

use Illuminate\Http\Request;
use Mundo\Models\User;

class RedirectController extends Controller
{
    public function index(Request $request)
    {
        /** @var User $user */
        $user = $request->user();
        if ($user !== null && ($route = $user->param('system_default_root_route')) !== null ) {
            return redirect()->action($route);
        }
        return redirect()->action('TM\RecordsController@index');
    }
}

<?php

namespace Mundo\Http\Controllers\Ris;

use Illuminate\Http\Request;
use Mundo\Http\Controllers\Controller;
use Mundo\Kernel\Http\Middleware\BeforeChangeState;
use Mundo\Models\RIS\RenewalFirm;
use Mundo\Models\TM\Record;
use Mundo\Modules\TM\BusinessService;
use Mundo\Presenters\Ris\RecordsPresenter;
use Mundo\Widgets\Ris\RecordWidgets\RecordWidgets;
use Notify;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class RecordsController
 * @package Mundo\Http\Controllers
 */
class RecordsController extends Controller
{

    protected $record;

    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
        $presenter = new RecordsPresenter($this->user());

        if (!$presenter->hasRecords()) {
            return view('ris.records.empty');
        } else {
            return redirect($this->getRootWidget()->actionUrl('show', [$presenter->recordsList()->first()->renewalFirm_id]));
        }
    }

    public function show(RenewalFirm $record)
    {
        $presenter = new RecordsPresenter($this->user(), $record);
        $this->record = $record;

        if (!$presenter->hasRecords() && !$this->user()->isAdmin()) {
            return view('records.empty');
        } else {
            return response()->view('ris.records.show', compact('presenter'));
        }
    }

    /**
     * Edit record's state
     *
     * @param RenewalFirm $record
     * @param  string            $state
     *
     * @return mixed
     */
    public function editState(RenewalFirm $record, $state)
    {
        if (!in_array($state, RenewalFirm::ACTIONS)) {
            throw new NotFoundHttpException("State [$state] does not exist");
        }

        return response()
            ->view('ris.records.edit-state', compact('record', 'state'))
            ->ajaxModal('modal-content');
    }

    /**
     * Order + registration form
     *
     * @param BusinessService $service
     * @param RenewalFirm          $record
     *
     * @return mixed
     */
    public function accepted(BusinessService $service, RenewalFirm $record)
    {
        $serviceTitles = $service->loadServiceTitles($record->getServices());
        
        return response()
            ->view('ris.records.accepted', compact('record', 'serviceTitles'))
            ->ajaxModal('modal-content');
    }

    public function saveAccepted(Request $request, RenewalFirm $record)
    {
        $this->validate($request, [
            'services'       => 'services'
        ]);

        foreach ($request->get('services') as $group => $serviceIds) {
            $serviceIds = array_filter($serviceIds, function ($id) {
                return is_numeric($id);
            });
            if (!empty($serviceIds)) {
                $record->createPreOrder($serviceIds[0], $request->input('note'), $this->user());
                Notify::success("Předobjednávka vložena.");
            }
        }

        $record->setState($record::ACTION_RENEWAL, $request->input('note'), $this->user());

        return redirect()
            ->action('Ris\RecordsController@index')
            ->ajaxCloseModal()
            ->ajaxRedirect();
    }

    public function nextSelling(BusinessService $service, RenewalFirm $record) {
        $serviceTitles = $service->loadServiceTitles($record->getServices());

        return response()
            ->view('ris.records.next-selling', compact('record', 'serviceTitles'))
            ->ajaxModal('modal-content');
    }

    public function saveNextSelling(Request $request, BusinessService $service, RenewalFirm $record)
    {
        $this->validate($request, [
            'services'       => 'services'
        ]);

        foreach ($request->get('services') as $group => $serviceIds) {
            $serviceIds = array_filter($serviceIds, function ($id) {
                return is_numeric($id);
            });
            if (!empty($serviceIds)) {
                $garantion = (bool)$request->input('garantion.' . $group, false);
                $garantion_type = ($garantion && $type = $request->input('garantion-type.' . $group, false)) ? $type : null;

                $service->createOrder(
                    $record->firm_id, 'ris-manual', $this->user()->id, $serviceIds, $request->input('note'), $garantion, $garantion_type
                );
                Notify::success("Objednávka uložena.");
            }
        }

        return redirect()
            ->action('Ris\RecordsController@index')
            ->ajaxCloseModal()
            ->ajaxRedirect();
    }

    /**
     * Change the record state
     *
     * @param RenewalFirm $record
     * @param Request            $request
     *
     * @return
     */
    public function updateState(RenewalFirm $record, Request $request)
    {
        $notableStates = [RenewalFirm::ACTION_REACHED, RenewalFirm::ACTION_UNREACHABLE, RenewalFirm::ACTION_DECLINED, RenewalFirm::ACTION_TO_REMOVE];
        $callableStates = [RenewalFirm::ACTION_REMOVED, RenewalFirm::ACTION_DECLINED];
        $scheduableState = [RenewalFirm::ACTION_REACHED, RenewalFirm::ACTION_UNREACHABLE];

        $rules = ['state' => 'required|in:' . implode(',', RenewalFirm::ACTIONS)];
        if (in_array($request->input('state'), $notableStates)) {
            $rules['note'] = 'required';
        }
        if (in_array($request->input('state'), $scheduableState)) {
            $rules['next_call'] = 'required|date';
        }

        $this->validate($request, $rules);

        if ($nextCall = $request->get('next_call', null)) {
            $record->callDate = $nextCall . ':00';
        }
        $record->setState($request->input('state'), $request->input('note', null), $this->user());

        if (in_array($state = $request->input('state'), $callableStates)) {
            $method = sprintf('setState%s', ucfirst($state));
            $record->$method($request);
        }

        Notify::success('Stav změněn');

        return redirect()
            ->action('Ris\RecordsController@index')
            ->ajaxCloseModal()
            ->ajaxRedirect();
    }

    public function createWidgetRisRecordWidgets()
    {
        return new RecordWidgets($this->record);
    }
}

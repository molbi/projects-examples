<?php

namespace Mundo\Http\Controllers\Ris\Settings;

use Mundo\Http\Controllers\Controller;
use View;

abstract class SettingsController extends Controller
{
    protected function getMenu()
    {
        return [
            [
                'title'       => 'Sestavy služeb',
                'description' => 'Předpřipravené sestavy služby',
                'icon'        => 'phone',
                'url'         => $this->getRootWidget()->actionUrl('Ris\Settings\ServiceDefinitionsController@index'),
            ],
        ];
    }

    protected function beforeRender()
    {
        View::share('menu', $this->getMenu());
        View::share('menuFromController', true);
        View::share('noLeftBarTopNav', true);
    }
}

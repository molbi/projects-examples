<?php

namespace Mundo\Http\Controllers\Ris\Settings;

use Illuminate\Http\Request;
use Mundo\Models\RIS\RisServiceDefinition;
use Notify;

class ServiceDefinitionsController extends SettingsController
{
    protected $rules = [
        'name'       => 'required|max:255',
        'definition' => 'required|json',
        'parent_id'  => 'integer',
    ];

    public function index()
    {
        return view('ris.settings.service-definitions.index', [
            'definitions' => RisServiceDefinition::orderBy('name')->get()
        ]);
    }

    public function edit(RisServiceDefinition $definition)
    {
        return view('ris.settings.service-definitions.edit', compact('definition'));
    }

    public function update(Request $request, RisServiceDefinition $definition)
    {
        $this->saveDefinition($definition, $request);
        Notify::success('Definice upravena.');

        return redirect($this->getRootWidget()->actionUrl('index'));
    }


    public function destroy(RisServiceDefinition $definition)
    {
        $definition->delete();
        Notify::success('Definice smazána.');

        return redirect($this->getRootWidget()->actionUrl('index'));
    }

    protected function saveDefinition(RisServiceDefinition $definition, Request $request)
    {
        $this->validate($request, $this->rules);
        $attributes = $request->only('name', 'parent_id', 'definition');
        if (empty($attributes['parent_id'])) {
            $attributes['parent_id'] = null;
        }
        $definition->fill($attributes);

        return $definition->save();
    }
}

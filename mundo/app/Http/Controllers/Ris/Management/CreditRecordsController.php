<?php

namespace Mundo\Http\Controllers\Ris\Management;

use Illuminate\Http\Request;
use Mundo\Jobs\TM\Forwarding\LeadsForwardRecordJob;
use Mundo\Presenters\Ris\Management\CreditRecordsPresenter;

class CreditRecordsController extends ManagementController
{
    public function index(Request $request)
    {
        $presenter = new CreditRecordsPresenter($this->user(), $request);

        return view('ris.management.credit-records.index', compact('presenter'));
    }

    public function store(Request $request)
    {
        $this->dispatch(new LeadsForwardRecordJob(
            $request->get('records', null),
            $request->get('user_id', null)
        ));

        return redirect()->back();
    }
}

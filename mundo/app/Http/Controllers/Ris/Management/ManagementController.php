<?php

namespace Mundo\Http\Controllers\Ris\Management;

use Mundo\Http\Controllers\Controller;
use View;

abstract class ManagementController extends Controller
{
    protected function getMenu()
    {
        return [
            [
                'title'       => 'Kreditové záznamy',
                'description' => 'Předání kreditových záznamů',
                'icon'        => 'phone',
                'url'         => $this->getRootWidget()->actionUrl('Ris\Management\CreditRecordsController@index'),
            ],
        ];
    }

    protected function beforeRender()
    {
        View::share('menu', $this->getMenu());
        View::share('menuFromController', true);
        View::share('noLeftBarTopNav', true);
    }
}

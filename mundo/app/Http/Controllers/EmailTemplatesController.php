<?php

namespace Mundo\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Mail\Mailer;
use Illuminate\Mail\Message;
use Mundo\Kernel\EmailTemplate\Template;
use Mundo\Models\EmailLog;
use Mundo\Models\EmailTemplate;
use Mundo\Modules\TM\Sources\Eloquent\Collection;
use Notify;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class EmailTemplatesController
 *
 * Universal mail sender
 *
 * @package Mundo\Http\Controllers
 */
class EmailTemplatesController extends Controller
{
    /** steps in rigt order */
    const STEPS = ['select', 'variables', 'preview', 'send'];
    /** @var string */
    protected $uuid;

    /**
     * EmailTemplatesController constructor.
     *
     * Stores current's action url and input that comes from previous action
     * This is later used to go back and keep input and URL
     *
     * @param \Illuminate\Http\Request $request
     */
    public function __construct(Request $request)
    {
        $uuid = $this->getRequestUuid($request);
        if (!is_null($request->route())) {
            $action = last(explode('@', $request->route()->getActionName()));
            if (in_array($action, self::STEPS)) {
                $index = array_search($action, self::STEPS);

                // save input to prev req
                if ($index > 0) {
                    $prevAction = self::STEPS[$index - 1];
                    $request->session()->put("email-send-wizzard.$uuid.$prevAction.input", $request->input());
                }
                // save url
                $request->session()->put("email-send-wizzard.$uuid.$action.url", $request->fullUrl());
                //$request->session()->forget("email-send-wizzard");
            }
        }
        parent::__construct();
    }

    /**
     * Selection of the template
     *
     * If 'suggestions' query variable is set (it can be an array), is taken and preloaded to recommended options.
     *
     * It is also possible to set 'allowSelection' query param which indicates to offer other templates with same
     * target type to be selected.
     *
     * This action automatically redirects (preselect) to template when:
     * 1) allowSelection is set to false and suggestions contains only 1 template
     * 2) allowSelection is set to true but after all loading we still have only 1 template
     *
     * @param  Request $request
     * @param  string $targetType
     * @param  int|string $targetId
     * @return Response
     */
    public function select(Request $request, $targetType, $targetId)
    {
        $target = $this->loadTarget($targetType, $targetId);
        $suggestionIds = $request->get('suggestions', []);
        $allowSelection = empty($suggestionIds) || (bool)$request->get('allowSelection', true);
        $uuid = $this->getRequestUuid($request);

        if (!$allowSelection && count($suggestionIds) === 1) {
            return redirect($this->getRootWidget()->actionUrl(
                'variables',
                [$targetType, $targetId, 'template_id' => $suggestionIds[0], 'uuid' => $uuid]
            ));
        }

        $suggestions = EmailTemplate::whereIn('id', $suggestionIds)->get();
        if ($allowSelection) {
            $templates = EmailTemplate::whereNotIn('id', $suggestionIds)->where('target', '=', $targetType)->get();
        } else {
            $templates = new Collection([]);
        }

        if (count($suggestions) + count($templates) === 1) {
            return redirect($this->getRootWidget()->actionUrl('variables', [
                    $targetType,
                    $targetId,
                    'template_id' => $suggestions->merge($templates)->first()->id,
                    'uuid'        => $uuid,
                ]
            ));
        }

        return response()
            ->view('email-templates.select', compact('suggestions', 'templates', 'target', 'uuid'))
            ->ajaxModal('modal-content');
    }

    /**
     * User input of template's variables (if any)
     *
     * Template can have multiple variables and this action offers form to fill them. When there is no variables,
     * action automatically redirects to the next step.
     *
     * @param  Request $request
     * @param  string $targetType
     * @param  string|integer $targetId
     * @return Response
     */
    public function variables(Request $request, $targetType, $targetId)
    {
        $uuid = $this->getRequestUuid($request);

        $target = $this->loadTarget($targetType, $targetId);
        $template = $this->loadTemplate($request, $target);

        if (empty($template->getVariables())) {
            return redirect($this->getRootWidget()->actionUrl('preview', [
                    $targetType,
                    $targetId,
                    'template_id' => $template->id,
                    'uuid'        => $uuid,
                ]
            ));
        }

        $templateRenderer = new Template($template, $this->user(), $target);


        return response()
            ->view('email-templates.variables', [
                'target'    => $target,
                'template'  => $template,
                'variables' => $template->getVariables(),
                'defaults'  => $templateRenderer->getDefaults(),
                'uuid'      => $uuid,
            ])
            ->ajaxModal('modal-content');
    }

    /**
     * Preview and WYSIWIG editor
     *
     * This action is last step of mail sending. User is can change HTML and plain text content. As well as subject
     * and destination mail.
     *
     * @param  Request $request
     * @param  string $targetType
     * @param  string|integer $targetId
     * @return Response
     */
    public function preview(Request $request, $targetType, $targetId)
    {
        $uuid = $this->getRequestUuid($request);

        $target = $this->loadTarget($targetType, $targetId);
        $template = $this->loadTemplate($request, $target);
        $templateRenderer = new Template($template, $this->user(), $target);
        $this->validate($request, $this->getTemplateVariableRules($template));

        return response()
            ->view('email-templates.preview', [
                'target'   => $target,
                'template' => $template,
                'render'   => $templateRenderer->render($request->get('variables', [])),
                'uuid'     => $uuid,
            ])
            ->ajaxModal('modal-content');
    }

    /**
     * Send the mail!
     *
     * @param Mailer $mailer
     * @param Request $request
     * @param string $targetType
     * @param string $targetId
     */
    public function send(Mailer $mailer, Request $request, $targetType, $targetId)
    {
        $target = $this->loadTarget($targetType, $targetId);
        $template = $this->loadTemplate($request, $target);

        $this->validate($request, [
            'email'                     => 'required|email',
            'fromName'                  => 'required',
            'fromEmail'                 => 'required|email',
            'subject'                   => 'required|string',
            'html'                      => 'required',
            'plain'                     => 'required',
            'attachments.*.name'        => 'required',
            'attachments.*.url'         => 'required|url',
            'attachments.*.contentType' => 'required',
        ]);

        $mailer->raw('', function (Message $message) use ($request) {
            $message->from($request->get('fromEmail'), $request->get('fromName'))
                ->subject($request->get('subject'))
                ->to($request->get('email'));

            $message->setBody($request->get('html'), 'text/html')
                ->addPart($request->get('plain'), 'text/plain');

            foreach ($request->get('attachments', []) as $attachment) {
                $message->attach($attachment['url'], [
                    'mime' => $attachment['contentType'],
                    'as'   => $attachment['name'],
                ]);
            }

        });

        EmailLog::log(
            $this->user(),
            $target,
            $request->get('subject'),
            $request->get('email'),
            $this->user()->name,
            $this->user()->email,
            $request->get('plain'),
            $request->get('html'),
            $request->get('attachments'),
            $template
        );

        Notify::success('E-mail odeslán');

        return redirect('/')
            ->ajaxCloseModal()
            ->ajaxReload();
    }

    /**
     * Loads the action's request from sessions and redirects back
     *
     * @param  Request $request
     * @param  string $action
     * @return Response
     */
    public function back(Request $request, $action)
    {
        $uuid = $this->getRequestUuid($request);
        $oldRequest = $request->session()->get("email-send-wizzard.$uuid.$action", null);
        if (is_null($oldRequest)) {
            throw new NotFoundHttpException('Stored request cannot be found');
        }

        return redirect()
            ->to($oldRequest['url'])
            ->withInput(isset($oldRequest['input']) ? $oldRequest['input'] : []);
    }


    /**
     * Load target by type (model class) and id
     *
     * @param  string $targetType
     * @param  string|integer $targetId
     * @return Model
     * @throws ModelNotFoundException when the record does not exist
     */
    protected function loadTarget($targetType, $targetId)
    {
        $target = call_user_func([$targetType, 'findOrFail'], $targetId);

        return $target;
    }

    /**
     * @param  Request $request
     * @param  Model $target
     * @return EmailTemplate
     * @throws \LogicException Target mismatch
     */
    protected function loadTemplate(Request $request, $target)
    {
        $template = EmailTemplate::findOrFail($request->get('template_id', 0));
        if (!empty($template->$target) && !is_a($target, $template->target)) {
            $targetType = get_class($target);
            throw new \LogicException("Template with target [{$template->target}] cannot be used to [{$targetType}]");
        }

        return $template;
    }

    /**
     * Extract validator rules from model (used to validate)
     *
     * @param  EmailTemplate $template
     * @return array
     */
    protected function getTemplateVariableRules(EmailTemplate $template)
    {
        $rules = [];

        foreach ($template->getVariables() as $name => $definition) {
            $rules['variables.' . $name] = isset($definition->validator) ? $definition->validator : 'string';
        }

        return $rules;
    }

    /**
     * Load the current's request uuid
     *
     * @param  Request $request
     * @return string
     */
    protected function getRequestUuid(Request $request)
    {
        if (!isset($this->uuid)) {
            $this->uuid = $request->get('uuid', uuid());
        }

        return $this->uuid;
    }
}

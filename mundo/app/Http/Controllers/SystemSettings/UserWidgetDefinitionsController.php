<?php

namespace Mundo\Http\Controllers\SystemSettings;

use Illuminate\Http\Request;
use Mundo\Models\UserWidgetDefinition;
use Notify;

class UserWidgetDefinitionsController extends SystemSettingsController
{
    protected $rules = [
        'name'       => 'required|max:255',
        'definition' => 'required|json',
        'parent_id'  => 'integer',
    ];

    public function index()
    {
        $definitions = UserWidgetDefinition::orderBy('name')->get();

        return view('system-settings.user-widget-definitions.index', compact('definitions'));
    }

    public function create(UserWidgetDefinition $definition)
    {
        return view('system-settings.user-widget-definitions.create', compact('definition'));
    }

    public function edit(UserWidgetDefinition $definition)
    {
        return view('system-settings.user-widget-definitions.edit', compact('definition'));
    }

    public function store(Request $request, UserWidgetDefinition $definition)
    {
        $this->saveDefinition($definition, $request);
        Notify::success('Definice uložena.');

        return redirect($this->getRootWidget()->actionUrl('index'));
    }

    public function update(Request $request, UserWidgetDefinition $definition)
    {
        $this->saveDefinition($definition, $request);
        Notify::success('Definice upravena.');

        return redirect($this->getRootWidget()->actionUrl('index'));
    }


    public function destroy(UserWidgetDefinition $definition)
    {
        $definition->delete();
        Notify::success('Definice smazána.');

        return redirect($this->getRootWidget()->actionUrl('index'));
    }

    protected function saveDefinition(UserWidgetDefinition $definition, Request $request)
    {
        $this->validate($request, $this->rules);
        $attributes = $request->only('name', 'description', 'parent_id');
        if (empty($attributes['parent_id'])) {
            $attributes['parent_id'] = null;
        }
        $definition->fill($attributes);
        $definition->definition = json_decode($request->get('definition'));

        return $definition->save();
    }
}

<?php

namespace Mundo\Http\Controllers\SystemSettings;

use Illuminate\Http\Request;
use Mundo\Models\EmailTemplate;
use Notify;

class EmailTemplatesController extends SystemSettingsController
{
    /**
     * List of templates
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $collection = EmailTemplate::all();

        return view('system-settings.email-templates.index', compact('collection'));
    }

    /**
     * Form for new template
     * @param \Mundo\Models\EmailTemplate $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(EmailTemplate $model)
    {
        return view('system-settings.email-templates.create', compact('model'));
    }

    /**
     * Form for edit the template
     * @param \Mundo\Models\EmailTemplate $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(EmailTemplate $model)
    {
        return view('system-settings.email-templates.edit', compact('model'));
    }

    /**
     * Deletes the template
     *
     * @param \Mundo\Models\EmailTemplate $model
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(EmailTemplate $model)
    {
        $model->delete();
        Notify::success('Šablona smazána.');

        return redirect($this->getRootWidget()->actionUrl('index'));
    }

    /**
     * Store new template
     * @param \Illuminate\Http\Request $request
     * @param \Mundo\Models\EmailTemplate $model
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request, EmailTemplate $model)
    {
        return $this->save($request, $model, 'Šablona přidána.');
    }

    /**
     * Update template
     * @param \Illuminate\Http\Request $request
     * @param \Mundo\Models\EmailTemplate $model
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, EmailTemplate $model)
    {
        return $this->save($request, $model, 'Šablona upravena.');
    }

    /**
     * Save the tamplate
     * @param \Illuminate\Http\Request $request
     * @param \Mundo\Models\EmailTemplate $model
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    protected function save(Request $request, EmailTemplate $model, $notify = false)
    {
        $this->saveTemplate($model, $request)
            ->saveTags($model, $request);

        if (!is_null($notify)) {
            Notify::info($notify);
        }

        return redirect($this->getRootWidget()->actionUrl('index'));
    }

    /**
     * Store/Update template in db
     *
     * @param \Mundo\Models\EmailTemplate $template
     * @param \Illuminate\Http\Request $request
     * @return $this
     */
    protected function saveTemplate(EmailTemplate $template, Request $request)
    {
        $this->validate($request, [
            'name'        => 'required',
            'subject'     => 'required',
            'html'        => 'required',
            'plain'       => 'required',
            'email'       => 'required',
            'attachments' => 'required|json',
            'variables'   => 'required|json',
        ]);
        $template->fill($request->only(
            'name', 'html', 'plain', 'description', 'subject', 'target', 'email', 'attachments', 'variables'
        ))->save();

        return $this;
    }

    /**
     * Save tags for teample
     *
     * @param \Mundo\Models\EmailTemplate $template
     * @param \Illuminate\Http\Request $request
     * @return $this;
     */
    protected function saveTags(EmailTemplate $template, Request $request)
    {
        $template->retag($request->tags);

        return $this;
    }
}

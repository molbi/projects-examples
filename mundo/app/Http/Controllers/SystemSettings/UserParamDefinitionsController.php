<?php

namespace Mundo\Http\Controllers\SystemSettings;

use Illuminate\Http\Request;
use Mundo\Models\UserParamDefinition;
use Notify;

class UserParamDefinitionsController extends SystemSettingsController
{
    protected $rules = [
        'name' => 'required|max:255',
        'key'  => 'required|max:100|unique:user_param_definitions',
    ];

    public function index()
    {
        $definitions = UserParamDefinition::orderBy('name')->get();

        return view('system-settings.user-param-definitions.index', compact('definitions'));
    }

    public function create()
    {
        $definition = new UserParamDefinition;

        return view('system-settings.user-param-definitions.create', compact('definition'));
    }

    public function store(Request $request)
    {
        $rules = $this->rules;
        if (($validator = $request->get('validator')) !== null) {
            $rules['default'] = $validator;
        }
        $this->validate($request, $rules);
        $definition = new UserParamDefinition(
            $request->only('name', 'description', 'key', 'type', 'validator', 'default')
        );
        $definition->save();

        Notify::success('Uživatelský parametr byl přidán.');

        return redirect($this->getRootWidget()->actionUrl('index'));
    }

    public function edit(UserParamDefinition $definition)
    {
        return view('system-settings.user-param-definitions.edit', compact('definition'));
    }

    public function update(Request $request, UserParamDefinition $definition)
    {
        $rules = $this->rules;
        if (($validator = $request->get('validator')) !== null) {
            $rules['default'] = $validator;
        }
        $rules['key'] .= ',key,' . $definition->id;
        $this->validate($request, $rules);
        $definition->fill($request->only('name', 'description', 'key', 'type', 'validator', 'default'))->save();

        Notify::success('Uživatelský parametr byl upraven.');

        return redirect($this->getRootWidget()->actionUrl('index'));
    }

    public function destroy(UserParamDefinition $definition)
    {
        $definition->delete();

        Notify::success('Uživatelský parametr byl smazán.');

        return redirect($this->getRootWidget()->actionUrl('index'));
    }
}

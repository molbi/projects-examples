<?php

namespace Mundo\Http\Controllers\SystemSettings;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Mundo\Console\Kernel;
use Mundo\Models\User;
use Mundo\Models\UserParamDefinition;
use Notify;

/**
 * Class UsersController
 * @package Mundo\Http\Controllers\SystemSettings
 */
class UsersController extends SystemSettingsController
{
    /**
     * @var array
     */
    protected $rules = [
        'email'              => 'required|email|unique:users,email',
        'name'               => 'required',
        'id'                 => 'required|integer|unique:users,id',
        'widgets_id'         => 'integer',
        'widgets_definition' => 'json',
    ];

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $users = User::orderBy('name')
            ->withoutGlobalScopes()
            ->with('tags', 'params')
            ->get();

        return view('system-settings.users.index', compact('users'));
    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(User $user)
    {
        return view('system-settings.users.create', compact('user'));
    }

    /**
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, User $user)
    {
        $this->save($user, $request);

        Notify::success('Uživatel upraven.');

        return redirect($this->getRootWidget()->actionUrl('index'));
    }

    /**
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request, User $user)
    {
        $this->save($user, $request);

        Notify::success('Uživatel založen.');

        return redirect($this->getRootWidget()->actionUrl('index'));
    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(User $user)
    {
        return view('system-settings.users.edit', compact('user'));
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(User $user)
    {
        $user->delete();

        Notify::success('Uživatel smazán.');

        return redirect($this->getRootWidget()->actionUrl('index'));
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changeState($user) {
        $user = User::withoutGlobalScopes()
            ->where('id', $user)->first();

        $user->update([
            'active' => !$user->active
        ]);

        Notify::success('Uživateli byl změněn stav.');

        return redirect($this->getRootWidget()->actionUrl('index'));
    }

    /**
     * @param Kernel $artisan
     * @return mixed
     */
    public function sync(Kernel $artisan)
    {
        $artisan->call('mundo:import-users');

        return redirect($this->getRootWidget()->actionUrl('index'))->ajaxReload();
    }

    /**
     * @param User $user
     * @param Request $request
     */
    protected function save(User $user, Request $request)
    {
        $this->saveTags($user, $request)
            ->saveUser($user, $request)
            ->saveParams($user, $request)
            ->saveLeadership($user, $request);
    }

    /**
     * @param \Mundo\Models\User $user
     * @param \Illuminate\Http\Request $request
     * @return $this
     */
    protected function saveTags(User $user, Request $request)
    {
        $user->retag($request->tags);

        return $this;
    }


    /**
     * @param \Illuminate\Http\Request $request
     * @param \Mundo\Models\User $user
     * @return $this
     */
    protected function saveUser(User $user, Request $request)
    {
        $rules = $this->rules;
        if ($user->exists) {
            $rules['email'] .= ',' . $user->id;
            $rules['id'] .= ',' . $user->id;
        }

        $this->validate($request, $rules);

        $attributes = $request->only('id', 'name', 'email', 'widgets_id');
        if (empty($attributes['widgets_id'])) {
            $attributes['widgets_id'] = null;
        }

        if ($request->has('password')) {
            $user->password = Hash::make($request->get('password'));
        }

        $user->fill($attributes);
        $user->widgets_definition = json_decode($request->get('widgets_definition'));
        $user->save();

        return $this;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \Mundo\Models\User $user
     * @return $this
     */
    protected function saveParams(User $user, Request $request)
    {
        $user->params()->delete();
        foreach (array_keys($request->input('params_enabled', [])) as $param) {
            $this->validate($request, ["param.$param" => UserParamDefinition::findByKeyOrId($param)->validator]);
            $user->param($param, $request->input("param.$param", null));
        }

        return $this;
    }

    /**
     * Save all leadership relations
     *
     * @param  \Mundo\Models\User $user
     * @param  \Illuminate\Http\Request $request
     * @return $this
     */
    protected function saveLeadership(User $user, Request $request)
    {
        $map = [
            'leadsUsersDirectly'     => ['users', []],
            'leadsUserTags'          => ['userTags', ['type' => 'user-tag']],
            'leadsCampaignsDirectly' => ['campaigns', []],
            'leadsCampaignTags'      => ['campaignTags', ['type' => 'campaign-tag']],
        ];

        foreach ($map as $relation => $item) {
            list($key, $params) = $item;
            $sync = array_fill_keys(array_get($request->leadership, $key, []), $params);
            $user->$relation()->sync($sync);
        }

        return $this;
    }
}

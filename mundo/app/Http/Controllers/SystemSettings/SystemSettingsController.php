<?php

namespace Mundo\Http\Controllers\SystemSettings;

use Mundo\Http\Controllers\Controller;
use View;

abstract class SystemSettingsController extends Controller
{
    protected function getMenu()
    {
        return [
            [
                'title'       => 'Uživatelé',
                'description' => 'Uživatelé, jejich oprávnění, zařazení a další nastavení.',
                'icon'        => 'users',
                'url'         => $this->getRootWidget()->actionUrl('SystemSettings\UsersController@index'),
            ],
            [
                'title'       => 'Uživatelské parametry',
                'description' => 'Umožňuje definovat různé parametry pro uživatele, ty se pak mohou využívat ve widgetech, statistikách, atp.',
                'icon'        => 'tags',
                'url'         => $this->getRootWidget()->actionUrl('SystemSettings\UserParamDefinitionsController@index'),
            ],
            [
                'title'       => 'Uživatelské sestavy widgetů',
                'description' => 'Předpřipravené sety widgetů, které mohou být přiřazeny uživatelům.',
                'icon'        => 'newspaper-o',
                'url'         => $this->getRootWidget()->actionUrl('SystemSettings\UserWidgetDefinitionsController@index'),
            ],
            [
                'title'       => 'Šablony emailů',
                'description' => 'Šablony lze používat pro followupy a další moduly aplikace.',
                'icon'        => 'envelope-o',
                'url'         => $this->getRootWidget()->actionUrl('SystemSettings\EmailTemplatesController@index'),
            ],
            [
                'title'       => 'Logy',
                'description' => 'Chybové notifikace aplikace',
                'icon'        => 'history',
                'url'         => route('log-viewer::dashboard'),
            ]
        ];
    }

    protected function beforeRender()
    {
        View::share('menu', $this->getMenu());
        View::share('menuFromController', true);
        View::share('noLeftBarTopNav', true);
    }
}

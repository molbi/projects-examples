<?php

namespace Mundo\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mundo\Models\RIS\RenewalFirm;
use Mundo\Models\RM\Record as RMRecord;
use Mundo\Models\TM\Record;
use Symfony\Component\DomCrawler\Crawler;

class SearchController extends Controller
{
    /** @var Results */
    protected $results = null;


    public function search(Request $request)
    {
        if (!empty($q = trim($request->get('q')))) {
            $results = $this->getResults($q);

            if ($request->isXmlHttpRequest()) {
                $results = $results->take(30);
            }

            $view = view('search.search', compact('results'));

            if ($request->isXmlHttpRequest()) {
                return response()
                    ->make('')
                    ->addPayload('search', with(new Crawler($view->render()))->filter('#search-result')->html());
            }


            return $view;
        } else {
            return view('search.empty');
        }
    }

    private function getResults($q)
    {
        $forUser = !$this->user()->hasAnyRole('admin');

        $results = [
            $this->getTmRecords($q, $forUser),
            $this->getRisRecords($q, $forUser),
            $this->getRMRecords($q, $forUser)
        ];

        return collect(array_flatten($results));
    }

    private function getTmRecords($q, $forUser = true)
    {
        $query = Record::search($q);

        if ($forUser) {
            $query->asUser($this->user());
        }

        return $query->asQuery()
            ->select([
                'tm_records.*',
                DB::raw('"tm" AS type')
            ])
            ->with('campaign', 'user', 'params')
            ->get()
            ->withRemote();
    }

    private function getRisRecords($q, $forUser = true)
    {
        $query = RenewalFirm::search($q);

        if ($forUser) {
            $query->asUser($this->user());
        }

        return $query->asQuery()
            ->select([
                'RenewalFirm.*',
                DB::raw('"ris" AS type')
            ])
            ->with('histories', 'firm', 'user')
            ->get();
    }

    private function getRMRecords($q, $forUser = true)
    {
        $query = RMRecord::search($q);

        if ($forUser) {
            $query->asUser($this->user());
        }
        return $query->asUser($this->user())
            ->asQuery()
            ->select([
                'rm_records.*',
                DB::raw('"rm" AS type')
            ])
            ->with('histories', 'firm', 'user')
            ->get();
    }
}

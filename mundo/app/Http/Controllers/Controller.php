<?php

namespace Mundo\Http\Controllers;

use Auth;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Mundo\Kernel\Widget\Illuminate\Routing\WidgetsController;
use Mundo\Widgets\UserWidgets\UserWidgets;

/**
 * Class Controller
 * @persistent(userWidgets)
 */
class Controller extends WidgetsController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    public function createWidgetUserWidgets()
    {
        return new UserWidgets(Auth::user());
    }

    protected function user()
    {
        return Auth::getUser();
    }
}

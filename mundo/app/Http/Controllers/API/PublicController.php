<?php

namespace Mundo\Http\Controllers\API;

use Illuminate\Http\Request;
use Mundo\Http\Controllers\Controller;
use Mundo\Kernel\Cache\PaymentsMorals;
use Mundo\Models\User;

class PublicController extends Controller
{
    public function paymentMorals(PaymentsMorals $cache, User $user, Request $request)
    {
        if ($user->exists) {
            return $cache->getForUser($user);
        }

        return $cache->get();
    }
}

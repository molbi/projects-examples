<?php

namespace Mundo\Http\Middleware;

use Closure;
use Illuminate\Database\Eloquent\Model;
use Mundo\Models\TM\Reminder;
use Mundo\Models\User;

/**
 * Class SendRequest
 * @package Mundo\Http\Middleware
 */
class SendRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $model = $request->route()->getParameter('model');
        $id = $request->route()->getParameter('id');

        switch ($model) {
            case Reminder::class:
                $request->route()->setParameter('record', $this->getReminder($model, $id, $request->user()));
                break;
            default:
                $request->route()->setParameter('record', $this->getRecord($model, $id));
                break;
        }


        return $next($request);
    }

    /**
     * @param $model
     * @param $id
     *
     * @return mixed
     */
    private function getRecord($model, $id)
    {
        return $model::where($this->getPrimaryKey($model), $id)->first();
    }


    /**
     * @param $model
     *
     * @return mixed
     */
    private function getPrimaryKey($model)
    {
        /** @var Model $obj */
        $obj = new $model;

        return $obj->getKeyName();
    }

    /**
     * @param      $model
     * @param      $id
     * @param User $user
     *
     * @return mixed
     */
    private function getReminder($model, $id, User $user)
    {
        return $model::findOfFail($user, $id);
    }
}

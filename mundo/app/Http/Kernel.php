<?php

namespace Mundo\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;
use Mundo\Http\Middleware\SendRequest;
use Mundo\Kernel\Http\Middleware\ValidateState;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \Mundo\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \Mundo\Http\Middleware\VerifyCsrfToken::class,
            \Mundo\Kernel\Ajax\Http\Middleware\Pjax::class,
        ],

        'api' => [
            'throttle:60,1',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth'           => \Mundo\Http\Middleware\Authenticate::class,
        'auth.basic'     => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'can'            => \Illuminate\Foundation\Http\Middleware\Authorize::class,
        'guest'          => \Mundo\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle'       => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'validate-state' => ValidateState::class,
        'send-request'   => SendRequest::class
    ];
}

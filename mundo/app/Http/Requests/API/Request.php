<?php
namespace Mundo\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;
use Response;

class Request extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the proper failed validation response for the request.
     *
     * @param array $errors
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        return Response::json([
            'success' => false,
            'message' => $errors,
        ], 400);
    }
}

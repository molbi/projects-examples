<?php

namespace Mundo\Http\Requests\API\Lead;

use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Mundo\Http\Requests\API\Request;

class StoreRequest extends Request
{
    /** @var PhoneNumberUtil */
    protected $numberUtil;

    public function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();
        // Validace a převod telefonního čísla
        $validator->after(function () use ($validator) {
            if ($validator->getMessageBag()->isEmpty()) {
                $number = $this->get('number');
                $country = $this->get('country');
                $phoneNumber = $this->numberUtil->parse($number, $country);
                if ($this->numberUtil->isValidNumber($phoneNumber)) {
                    $this->merge([
                        'number' => $this->numberUtil->format($phoneNumber, PhoneNumberFormat::INTERNATIONAL),
                    ]);
                } else {
                    $validator->getMessageBag()->add('number', 'This is not valid phone number');
                }
            }
        });

        return $validator;
    }

    /**
     * @param \libphonenumber\PhoneNumberUtil $numberUtil
     *
     * @return array
     */
    public function rules(PhoneNumberUtil $numberUtil)
    {
        $this->numberUtil = $numberUtil;

        return [
            'number'  => 'required|max:100',
            'country' => 'required|regex:~^[A-Z]{2}$~',
        ];
    }
}

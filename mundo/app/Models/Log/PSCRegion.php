<?php

namespace Mundo\Models\Log;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Mundo\Models\User;

/**
 * Class PSCRegion
 * @package Mundo\Models\Log
 */
class PSCRegion extends Model
{
    /**
     * @var string
     */
    protected $table = 'log.region_postal_code';

    /**
     * @var string
     */
    protected $primaryKey = 'region_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @param null $region_id
     * @return mixed
     */
    public static function get($region_id = null)
    {
        if (is_null($region_id)) {
            return (new static)->all()->pluck('postal_code')->toArray();
        }

        return (new static)->where('region_id', $region_id)->get()->pluck('postal_code')->toArray();
    }
}
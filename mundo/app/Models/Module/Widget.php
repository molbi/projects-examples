<?php

namespace Mundo\Models\Module;

use Illuminate\Database\Eloquent\Model;

class Widget extends Model
{
    protected $table = 'modules_widgets';

    protected $fillable = [
        'module_id',
        'widgets'
    ];

    /**
     * Returns array of widgets in right order
     *
     * @return array
     */
    public function getWidgets()
    {
        $widgets = (empty($this->widgets)) ? '[]' : $this->widgets;

        return $this->fromJson($widgets);
    }
}

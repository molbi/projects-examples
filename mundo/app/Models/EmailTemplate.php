<?php

namespace Mundo\Models;

use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
    use Taggable;

    /** @var string[] */
    protected $fillable = [
        'name',
        'html',
        'plain',
        'description',
        'subject',
        'target',
        'email',
        'variables',
        'attachments',
    ];

    /**
     * Default values
     * @var array
     */
    protected $attributes = [
        'variables'   => '{}',
        'attachments' => '{}',
    ];

    /**
     * Settings as json
     * @return array
     */
    public function getVariables()
    {
        return (array)$this->fromJson($this->variables ?: '{}', true);
    }

    /**
     * Settings as json
     * @return array
     */
    public function getAttachments()
    {
        return (array)$this->fromJson($this->attachments ?: '{}', true);
    }

    public static function options($target = null)
    {
        $query = static::query()
            ->orderBy('name');
        if (!is_null($target)) {
            $query->where('target', '=', $target);
        }

        return $query->lists('name', 'id');
    }
}

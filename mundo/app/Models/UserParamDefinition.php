<?php

namespace Mundo\Models;

use Illuminate\Database\Eloquent\Model;
use Mundo\Kernel\Eloquent\CastableParams;

class UserParamDefinition extends Model
{
    use CastableParams;
    
    const TYPES = ['integer', 'real', 'float', 'double', 'string', 'boolean', 'object', 'array', 'date', 'datetime'];

    protected $fillable = ['name', 'description', 'key', 'type', 'validator', 'default'];

    protected static $all;

    /**
     * All definition (cached)
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function chachedAll()
    {
        if (self::$all === null) {
            self::$all = self::orderBy('name')->get();
        }

        return self::$all;
    }

    /**
     * Converts key to id
     *
     * @param  string $key
     * @return integer|null
     */
    public static function keyToId($key)
    {
        $index = self::chachedAll()->search(function (UserParamDefinition $param) use ($key) {
            return $key === $param->key;
        });

        return $index !== false ? self::chachedAll()[$index]->id : null;
    }

    /**
     * Finds by id in cached definitions
     *
     * @param  int $id
     * @return UserParamDefinition|null
     */
    public static function findByKeyOrId($id)
    {
        $index = self::chachedAll()->search(function (UserParamDefinition $param) use ($id) {
            return self::normalizeId($id) === $param->id;
        });

        return $index !== false ? self::chachedAll()[$index] : null;
    }

    /**
     * Normalizes id (key or id)
     * @param $id
     * @return int|null
     */
    public static function normalizeId($id)
    {
        if (is_string($id)) {
            return self::keyToId($id);
        }
        if (!is_int($id)) {
            throw new \InvalidArgumentException('ID have to be string or integer, [' . gettype($id) . '] given');
        }

        return $id;
    }

    public function getDefaultAttribute()
    {
        return $this->castParam(isset($this->attributes['default']) ? $this->attributes['default'] : null);
    }

    public function setDefaultAttribute($value)
    {
        $this->attributes['default'] = empty(trim($value)) ? null : $this->serializeParam($value);
    }
}

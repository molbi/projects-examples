<?php

namespace Mundo\Models\Business;

use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;
use Mundo\Kernel\Search\Searchable;

trait FirmSearchable
{
    use Searchable;

    /**
     * What's the model's type name
     *
     * @return string
     */
    public function searchableAs()
    {
        return config('search.prefix') . 'business_firm';
    }

    /**
     * Overloaded for eager loading
     *
     * @return mixed
     */
    public static function makeAllSearchable()
    {
        return (new static)->newQuery()
            ->with([
                'categories' => function ($q) {
                    $q->where('lang', 'sk');
                }, 'orders'
            ])
            ->select([
                'Firm.*'
            ])
            ->leftJoin('tm.Filter_Params AS FP', function (JoinClause $j) {
                $j->on('FP.param_id', '=', 'Firm.firm_id');
                $j->whereIn('FP.filter_id', [49, 221]);
            })
            ->whereNull('FP.filter_id')
            ->leftJoin('obis.Order AS O3', 'O3.firm_id', '=', 'Firm.firm_id')
            ->leftJoin('obis.OrderExactor AS OE', 'OE.order_id', '=', 'O3.order_id')
            ->whereNull('OE.order_id')
            ->where('Firm.state', 'visible')
            ->whereNotIn('Firm.firm_id', self::firmWithActiveService())
            ->where('Firm.lang', 'sk')
            ->whereNotNull('Firm.ipAddress')
            ->groupBy('Firm.firm_id')
            ->searchable();
    }

    /**
     * @return array
     */
    private static function firmWithActiveService()
    {
        $firmService = DB::table('business.Firm_Service AS FS')
            ->select(['FS.firm_id', 'FS.endsOn'])
            ->whereNotIn('FS.service', ['cskontakt-micro']);

        $firmCreditService = DB::table('business.Firm_CreditService AS FCS')
            ->select([
                'firm_id',
                DB::raw('DATE(NOW() + INTERVAL 1 YEAR) AS endsOn')
            ])
            ->whereNotIn('FCS.service', ['cskontakt-micro'])
            ->whereRaw('YEAR(FCS.beginsOn) >= 2016');

        return DB::table(DB::raw(sprintf('(%s) AS results', get_sql($firmService->unionAll($firmCreditService)))))
            ->groupBy('firm_id')
            ->pluck('firm_id');

    }

    /**
     * Create search builder
     *
     * @param  string $query
     * @return array
     */
    protected static function makeSearchQuery($query)
    {
        if (is_array($query)) {

            return $query;
        }

        if (empty($query)) {
            return [
                'match_all' => []
            ];
        }

        return [
            'simple_query_string' => [
                'query'            => $query,
                'analyzer'         => 'default',
                'fields'           => ['_all'],
                'default_operator' => 'and',
            ],
        ];

    }

    /**
     * Data to be indexed
     *
     * @return array
     */
    public function toSearchableArray()
    {
        try {
            return [
                'title'       => $this->title,
                'web'         => $this->web,
                'street'      => $this->street,
                'city'        => $this->city,
                'postalCode'  => $this->postalCode,
                'description' => $this->description,
                'contacts'    => $this->contactPerson,
                'emails'      => $this->email,
                'phones'      => $this->phoneNumber,
                'categories'  => $this->categories->pluck('title')->toArray(),
                'orders'      => $this->orders
            ];
        } catch (\Exception $e) {
            dd($this);
        }
    }
}

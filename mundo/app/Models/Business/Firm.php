<?php

namespace Mundo\Models\Business;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Mundo\Models\User;

/**
 * Class Firm
 * @package Mundo\Models\Business
 */
class Firm extends Model
{
    use FirmSearchable;

    /**
     * @var string
     */
    protected $table = 'business.Firm';

    /**
     * @var string
     */
    protected $primaryKey = 'firm_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'business.Firm_Category', 'firm_id', 'ID_BCFirmRegion');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(Order::class, 'firm_id', 'firm_id');
    }
}
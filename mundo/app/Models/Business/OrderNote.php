<?php

namespace Mundo\Models\Business;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Mundo\Models\User;

/**
 * Class Order
 * @package Mundo\Models\Business
 */
class OrderNote extends Model
{
    const TYPE_DODGER = 'dodger';

    /**
     * @var string
     */
    protected $table = 'obis.OrderNote';

    /**
     * @var string
     */
    protected $primaryKey = 'orderNote_id';

    /**
     * @var array
     */
    protected $dates = [
        'validTo'
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'validTo',
        'content',
        'type',
        'employee_id'
    ];

    /**
     * @var bool
     */
    public $timestamps = false;
}
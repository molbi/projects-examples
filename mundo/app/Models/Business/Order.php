<?php

namespace Mundo\Models\Business;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Mundo\Models\User;

/**
 * Class Order
 * @package Mundo\Models\Business
 */
class Order extends Model
{
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    const STATE_RECEIVED = 'received',
        STATE_PROCESSED = 'processed',
        STATE_TOSPECIFY = 'toSpecify',
        STATE_PAID = 'paid',
        STATE_CANCELLED = 'cancelled',
        STATE_BLOCKED = 'blocked';

    /**
     * @var string
     */
    protected $table = 'obis.Order';

    /**
     * @var string
     */
    protected $primaryKey = 'order_id';

    /**
     * @var array
     */
    protected $dates = [
        'beginsAt'
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'blockedInterval',
        'state'
    ];

    /**
     * @return mixed
     */
    public function getBlockedToAttribute()
    {
        return $this->beginsAt->addDays($this->blockedInterval)->setTime(0, 0, 0);
    }

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @param $query
     * @return mixed
     */
    public function scopeUnfinished($query)
    {
        return $query->whereNotIn('state', [Order::STATE_PAID, Order::STATE_CANCELLED, Order::STATE_RECEIVED]);
    }

    /**
     *
     */
    public function notes()
    {
        return $this->hasMany(OrderNote::class, 'order_id', 'order_id');
    }


}
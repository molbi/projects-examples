<?php

namespace Mundo\Models\Business;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Mundo\Models\User;

/**
 * Class ItParam
 * @package Mundo\Models\Business
 */
class ItParam extends Model
{
    /**
     * @var string
     */
    protected $table = 'log.it_params';

    /**
     * @var array
     */
    protected $casts = [
        'value' => 'json'
    ];

    /**
     * @param $key
     *
     * @return mixed|null
     */
    public static function getByKey($key)
    {
        /** @var ItParam $param */
        $param = static::where('key', $key)->first();

        if (is_null($param)) {
            return null;
        }

        return $param->value;
    }

    /**
     * @param array $exclude
     *
     * @return array
     */
    public static function getCrossCampaignIds(array $exclude = [])
    {
        return collect(static::getByKey('cross_campaigns_sources'))
            ->map(function ($source) {
                return (int)str_replace('tm_campaign_', '', $source);
            })
            ->reject(function ($campaign_id) use ($exclude) {
                return in_array($campaign_id, $exclude);
            })
            ->toArray();
    }

    public static function getInhouseLeadsCampaignIds(array $exclude = [])
    {
        return collect(static::getByKey('inhouse_leads_sources'))
            ->map(function ($source) {
                return (int)str_replace('tm_campaign_', '', $source);
            })
            ->reject(function ($campaign_id) use ($exclude) {
                return in_array($campaign_id, $exclude);
            })
            ->toArray();
    }
}
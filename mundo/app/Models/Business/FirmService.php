<?php

namespace Mundo\Models\Business;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Mundo\Models\User;

class FirmService extends Model
{
    protected $table = 'business.Firm_Service';

    /**
     * @var bool
     */
    public $timestamps = false;

    protected $dates = [
        'endsOn',
        'beginsOn'
    ];
}
<?php

namespace Mundo\Models;

use Illuminate\Database\Eloquent\Model;
use Mundo\Filters\QueryFilter;

class ServiceLog extends Model
{
    protected $fillable = [
        'target_type',
        'target_id',
        'service',
        'user_id'
    ];

    public static function log(User $user, Model $target, $service)
    {
        return static::create([
            'target_type' => get_class($target),
            'target_id'   => $target->getKey(),
            'service'     => $service,
            'user_id'     => $user->id
        ]);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function target()
    {
        return $this->morphTo();
    }

    /**
     * @param             $query
     * @param QueryFilter $filter
     *
     * @return mixed
     */
    public function scopeFilter($query, QueryFilter $filter)
    {
        return $filter->apply($query);
    }
}

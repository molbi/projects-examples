<?php

namespace Mundo\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Mundo\Modules\TM\Sources\Eloquent\Collection;

/**
 * Taggable behavior for model
 *
 * @package Mundo\Models
 */
trait Taggable
{
    /**
     * Init trait
     */
    public static function bootTaggable()
    {
        $model = new static;
        Tag::addTaggable(camel_case($model->getTable()), get_called_class());
    }

    /**
     * Tags relation
     *
     * @return MorphToMany
     */
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable', 'taggables');
    }

    /**
     * Add tags
     *
     * @param  string|array $tags
     *
     * @return $this
     */
    public function tag($tags)
    {
        foreach (Tag::castToArray($tags) as $tag) {
            $this->addOneTag($tag);
        }

        return $this->load('tags');
    }

    /**
     * Remove tags
     *
     * @param  string|array $tags
     *
     * @return $this
     */
    public function untag($tags)
    {
        foreach (Tag::castToArray($tags) as $tag) {
            $this->removeOneTag($tag);
        }

        return $this->load('tags');
    }

    /**
     * Replace tags
     *
     * @param  string|array $tags
     *
     * @return $this
     */
    public function retag($tags)
    {
        return $this->detag()->tag($tags);
    }

    /**
     * Remove all tags
     *
     * @return $this
     */
    public function detag()
    {
        $this->tags()->sync([]);

        return $this->load('tags');
    }

    /**
     * @param Tag $tag
     *
     * @return mixed
     */
    public function hasTag(Tag $tag)
    {
        return $this->tags->contains($tag->getKey());
    }

    /**
     * Add one tag to the model.
     *
     * @param  string $tag
     *
     * @return $this
     */
    protected function addOneTag($tag)
    {
        if (empty(trim($tag))) {
            return $this->load('tags');
        }

        $tag = Tag::findOrCreate($tag);

        if (!$this->tags->contains($tag->getKey())) {
            $this->tags()->attach($tag->getKey());
        }

        return $this->load('tags');
    }

    /**
     * Remove one tag from the model
     *
     * @param string $tag
     */
    protected function removeOneTag($tag)
    {
        if ($tag = Tag::findByTag($tag)) {
            $this->tags()->detach($tag);

            $tagUsage = $tag->getConnection()->table('taggables')
                ->where('tag_id', $tag->id)
                ->count();

            if ($tagUsage == 0) {
                $tag->delete();
            }
        }

        return $this->load('tags');
    }

    /**
     * Scope for a Model that has any of the given tags.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param array                                 $tags
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithAnyTags(Builder $query, ...$tags)
    {
        $tags = Tag::castToNormalizedArray(!empty($tags) && is_array($tags[0]) ? $tags[0] : $tags);

        if (empty($tags)) {
            return $query->has('tags');
        }

        return $query->has('tags', '>', 0, 'and', function (Builder $builder) use ($tags) {
            return $builder->whereIn('normalized', $tags);
        });
    }

    /**
     * Scope for a Model that has all of the given tags.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param array|string                          $tags
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithAllTags(Builder $query, ...$tags)
    {
        $tags = Tag::castToNormalizedArray(is_array($tags[0]) ? $tags[0] : $tags);

        return $query->has('tags', '=', count($tags), 'and', function (Builder $builder) use ($tags) {
            return $builder->whereIn('normalized', $tags);
        });
    }

    /**
     * Scope for a Model that doesn't have any tags.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithoutTags(Builder $query)
    {
        return $query->has('tags', '=', 0);
    }

    /**
     * Get all tags in array
     *
     * @return Collection
     */
    public function tagsLists()
    {
        return $this->tags->lists('name', 'id');
    }
}

<?php

namespace Mundo\Models;

use Illuminate\Database\Eloquent\Model;

class UserParam extends Model
{
    protected $fillable = ['value'];

    public function definition()
    {
        return $this->belongsTo(UserParamDefinition::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getValueAttribute()
    {
        return $this->cachedDefinition()->castParam($this->attributes['value']);
    }

    public function setValueAttribute($value)
    {
        return $this->attributes['value'] = $this->cachedDefinition()->serializeParam($value);
    }

    public function cachedDefinition()
    {
        return UserParamDefinition::findByKeyOrId($this->user_param_definition_id);
    }

    public static function forUser(User $user, UserParamDefinition $definition, $value)
    {
        $self = new self;
        $self->user_param_definition_id = $definition->id;
        $self->user_id = $user->id;
        $self->value = $value;

        return $self;
    }
}

<?php

namespace Mundo\Models;

use Illuminate\Database\Eloquent\Model;

class UserWidgetDefinition extends Model
{
    protected $fillable = ['name', 'description', 'parent_id'];

    protected $casts = [
        'definition' => 'object',
    ];

    public function parent()
    {
        return $this->belongsTo(UserWidgetDefinition::class, 'parent_id');
    }

    public function getMergedDefinitionAttribute()
    {
        $merged = $this->parent === null ? [] : (array)$this->parent->mergedDefinition;

        return (object)array_merge_recursive((array)$this->definition, $merged);
    }
    
    
}

<?php

namespace Mundo\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Mundo\Models\TM\Campaign;

class Tag extends Model
{
    /**
     * Fillable attributes
     * @var string[]
     */
    protected $fillable = ['name'];

    /**
     * Associations
     * @var string[]
     */
    protected static $taggables = [];

    /**
     * Cast string to array of normalized tags by delimiter
     *
     * @param  string|array $tags
     * @return array
     */
    public static function castToNormalizedArray($tags)
    {
        return collect(static::castToArray($tags))
            ->map([get_called_class(), 'normalize'])
            ->toArray();
    }

    /**
     * Cast string to array of tags by delimiter
     *
     * @param  string|array $tags
     * @return array
     */
    public static function castToArray($tags)
    {
        if (is_array($tags)) {
            return $tags;
        }

        if (is_string($tags)) {
            return preg_split('|, *|', $tags);
        }

        return [];
    }

    /**
     * Generate normalized tag
     *
     * @param  string $name
     */
    public function setNameAttribute($name)
    {
        $this->attributes['name'] = $name;
        $this->attributes['normalized'] = static::normalize($name);
    }

    /**
     * Find or create tag
     *
     * @param  string $tag
     * @return static
     */
    public static function findOrCreate($tag)
    {
        if (!$res = self::findByTag($tag)) {
            $res = static::create(['name' => $tag]);
        }

        return $res;
    }

    /**
     * Find tag by name
     *
     * @param  string $tag
     * @return null|static
     */
    public static function findByTag($tag)
    {
        return static::where('normalized', static::normalize($tag))->first();
    }

    /**
     * Add new taggabler relation
     *
     * @param  string $name
     * @param  string $class
     * @return void
     */
    public static function addTaggable($name, $class)
    {
        static::$taggables[$name] = $class;
    }

    /**
     * Options array for select
     *
     * @return array
     */
    public static function optionsForSelect()
    {
        return self::orderBy('name')->pluck('name');
    }

    /**
     * Get a relationship.
     *
     * Overwritten to load relation from static array
     *
     * @param  string $key
     * @return mixed
     */
    public function getRelationValue($key)
    {
        if (!$rel = parent::getRelationValue($key)) {
            if (isset(static::$taggables[$key])) {
                $results = $this->$key()->getResults();
                $this->setRelation($key, $results);

                return $results;
            }
        }

        return $rel;
    }

    /**
     * Return relation
     *
     * @param  string $method
     * @param  array $parameters
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany|mixed
     */
    public function __call($method, $parameters)
    {
        if (isset(static::$taggables[$method])) {
            if (is_string($class = static::$taggables[$method])) {
                return $this->morphedByMany($class, 'taggable', 'taggables');
            } elseif (static::$taggables[$method] instanceof \Closure) {
                $closure = static::$taggables[$method];
                $res = $closure($this);
                if ($res instanceof Relation) {
                    return $res;
                }
            }

            return static::$taggables[$method];
        }

        return parent::__call($method, $parameters);
    }

    /**
     * Check if relation with given name is present
     *
     * @param  string $key
     * @return bool
     */
    public function __isset($key)
    {
        return parent::__isset($key) || isset(static::$taggables[$key]);
    }

    /**
     * Normalize tag
     *
     * @param  string $tag
     * @return string
     */
    protected static function normalize($tag)
    {
        return str_slug($tag);
    }
}

<?php

namespace Mundo\Models;

use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Mundo\Kernel\Search\Searchable;
use Mundo\Models\TM\LeadsUsers;
use Mundo\Models\TM\Record;
use Spatie\Permission\Traits\HasRoles;

/**
 * Class User
 * @package Mundo\Models
 */
class User extends Authenticatable
{
    use Searchable;
    use LeadsUsers;
    use Taggable;
    use HasRoles;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('active', function(Builder $builder) {
            $builder->where('active', 1);
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'email', 'phone', 'login', 'password', 'widgets_id', 'widgets_definition', 'active'];

    /**
     * @var array
     */
    protected $casts = [
        'widgets_definition' => 'object',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    //    public function scopeFilter(Builder $query, UserFilter $filter)
    //    {
    //        $filter->apply($query);
    //    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    protected function userWidgetDefinition()
    {
        return $this->belongsTo(UserWidgetDefinition::class, 'widgets_id', 'id');
    }

    /**
     * @return object
     */
    public function widgets()
    {
        $merged = $this->userWidgetDefinition === null ? [] : (array)$this->userWidgetDefinition->mergedDefinition;

        return (object)array_merge_recursive((array)$this->widgets_definition, $merged);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function params()
    {
        return $this->hasMany(UserParam::class);
    }

    /**
     * Has user param?
     *
     * @param $id
     *
     * @return bool
     */
    public function hasParam($id)
    {
        return $this->getParamById($id) !== null;
    }

    /**
     * Set or get value of param
     *
     * @param  int|string $id
     *
     * @return \Illuminate\Database\Eloquent\Model|int|mixed
     */
    public function param($id)
    {
        $args = func_get_args();
        if (count($args) === 2) {
            $value = $args[1];
        }

        if ($param = $this->getParamById($id)) {
            if (isset($value)) {
                $param->value = $value;
                $param->save();
            }

            return $param->value;
        } elseif ($param = UserParamDefinition::findByKeyOrId($id)) {
            if (isset($value)) {
                $p = UserParam::forUser($this, $param, $value);
                $p->save();

                return $p->value;
            }

            return $param->default;
        }

        throw new \InvalidArgumentException('There is no param defined with [' . $id . '] key or id.');
    }

    /**
     * Resets param
     *
     * @param $id
     */
    public function resetParam($id)
    {
        if ($param = $this->getParamById($id)) {
            $param->delete();
        }
    }

    /**
     * @param $id
     *
     * @return int|null
     */
    protected function getParamById($id)
    {
        if (is_string($id)) {
            $id = UserParamDefinition::keyToId($id);
        }
        if (!is_int($id)) {
            throw new \InvalidArgumentException('ID have to be string or integer, [' . gettype($id) . '] given');
        }

        foreach ($this->params as $param) {
            if ($param->user_param_definition_id === $id) {
                return $param;
            }
        }

        return null;
    }

    /**
     * @return mixed
     */
    public function isAdmin()
    {
        return $this->hasTag(Tag::findByTag('admin'));
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tm_records()
    {
        return $this->hasMany(Record::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tm_records_count()
    {
        return $this->tm_records()
            ->select([
                'user_id',
                'state',
                DB::raw('COUNT(state) AS count')
            ])
            ->whereNotIn('state', Record::TERMINAL_STATES)
            ->groupBy('state')
            ->groupBy('user_id');
    }

}

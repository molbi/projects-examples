<?php

namespace Mundo\Models\RM;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Mundo\Filters\QueryFilter;
use Mundo\Models\Business\Firm;
use Mundo\Models\Business\Order;
use Mundo\Models\Business\OrderNote;
use Mundo\Models\EmailLog;
use Mundo\Models\User;

/**
 * Class Record
 * @package Mundo\Models\RM
 */
class Record extends Model
{
    use RecordSearchable;

    /**
     *
     */
    const STATE_NEW = 'new',
        STATE_SOLVED = 'solved',
        STATE_BLOCKED = 'blocked',
        STATE_REACHED = 'reached',
        STATE_NOT_REACHED = 'not-reached';

    const STATES = [
        self::STATE_NEW,
        self::STATE_SOLVED,
        self::STATE_BLOCKED,
        self::STATE_REACHED,
        self::STATE_NOT_REACHED,
    ];

    const TERMINAL_STATES = [
        self::STATE_SOLVED
    ];

    /**
     * @var string
     */
    protected $table = 'rm_records';

    /**
     * @var array
     */
    protected $dates = ['next_call'];

    /**
     * @var array
     */
    protected $fillable = ['firm_id', 'user_id', 'state', 'next_call', 'created_at'];

    public function getMonthAttribute()
    {
        return $this->created_at->month;
    }

    /**
     * @param             $query
     * @param QueryFilter $filter
     *
     * @return mixed
     */
    public function scopeFilter($query, QueryFilter $filter)
    {
        return $filter->apply($query);
    }

    /**
     * Records owned by particular user
     *
     * @param Builder $query
     * @param User $user
     *
     * @return Builder
     */
    public function scopeOwnedByUser(Builder $query, User $user)
    {
        return $query->where('user_id', '=', $user->id);
    }

    /**
     * Ordered records by next planned call
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeOrdered(Builder $query)
    {
        $raw = $this->getConnection()->raw(
            'CASE WHEN next_call IS NULL THEN NOW() ELSE next_call END'
        );

        return $query->orderBy($raw);
    }

    /**
     * Records, that are not in terminal states
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeUnfinished(Builder $query)
    {
        return $query->whereNotIn('state', self::TERMINAL_STATES);
    }

    /**
     * Sent emails
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function emailLogs()
    {
        return $this->morphMany(EmailLog::class, 'target')->orderBy('created_at', 'DESC');;
    }

    /**
     * All changes in this record
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function histories()
    {
        return $this
            ->hasMany(RecordHistory::class, 'record_id', 'id')
            ->orderBy('created_at', 'DESC');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function firm()
    {
        return $this->belongsTo(Firm::class, 'firm_id', 'firm_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(Order::class, 'firm_id', 'firm_id');
    }

    /**
     * @return null
     */
    public function lastHistoryNote()
    {
        $history = $this->histories->filter(function (RecordHistory $history) {
            return $history->note !== null;
        })->first();

        if ($history !== null && $history->note) {
            return $history->note;
        }

        return null;
    }

    /**
     * @return bool
     */
    public function isNew()
    {
        $state = $this->state;
        $hist = $this->histories->filter(function ($history) {
            return in_array($history->column, ['state', 'user_id']);
        })->first();
        if ($hist) {
            $state = $hist->column === 'state' ? $hist->next : 'new';
        }

        return $state === 'new';
    }

    /**
     * @return mixed
     */
    public function getRemoteId()
    {
        return $this->firm_id;
    }

    /**
     * @return mixed
     */
    public function getRemoteLang()
    {
        return $this->firm->lang;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        if (isset($this->firm->title)) {
            return sprintf('%s (%s)', $this->firm->title, $this->firm->contactPerson);
        } else {
            return $this->firm->contactPerson;
        }
    }

    /**
     * Change the state a save the log
     *
     * @param                    $state
     * @param                    $note
     * @param \Mundo\Models\User $user
     *
     * @return bool
     */
    public function setState($state, $note, User $user)
    {
        $old = $this->attributes['state'];
        $this->attributes['state'] = $state;

        $this->histories()->create([
            'column'   => 'state',
            'previous' => $old,
            'next'     => $state,
            'user_id'  => $user->id,
            'note'     => $note,
        ]);

        return $this->save();
    }

    /**
     * Nastaví objednávkám klienta blokování
     *
     * @param Request $request
     */
    public function setStateBlocked(Request $request)
    {
        if (is_array($orders = $request->input('orders'))) {
            foreach ($orders as $order_id => $blocked_to) {
                if(empty($blocked_to)) {
                    return;
                }

                $blocked_to = Carbon::createFromTimestamp(strtotime($blocked_to));
                /** @var Order $o */
                $o = Order::where('order_id', $order_id)->first();

                if (!is_null($o)) {
                    $o->notes()->create([
                        'validTo'     => $blocked_to,
                        'content'     => sprintf('blokace do %s', $blocked_to->format('j. n. Y')),
                        'type'        => OrderNote::TYPE_DODGER,
                        'employee_id' => $request->user()->id
                    ]);
                }
            }
        }
    }
}

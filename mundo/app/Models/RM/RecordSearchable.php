<?php

namespace Mundo\Models\RM;

use Illuminate\Support\Collection as BaseCollection;
use Mundo\Kernel\Search\Builder;
use Mundo\Kernel\Search\Searchable;
use Mundo\Models\User;

trait RecordSearchable
{
    use Searchable;

    /**
     * Overloaded for eager loading
     *
     * @return mixed
     */
    public static function makeAllSearchable()
    {
        return (new static)->newQuery()
            ->with('histories', 'firm')
            ->searchable();
    }

    /**
     * Overloaded to preload remote data
     *
     * @param  \Illuminate\Support\Collection $models
     *
     * @return mixed
     */
    public function makeSearchable(BaseCollection $models)
    {
        if (!$models->isEmpty()) {
            return $models->first()->searchableUsing()->update($models);
        }
    }

    /**
     * Redefine basic ES search query
     *
     * @param  $query
     *
     * @return array
     */
    protected static function makeSearchQuery($query)
    {
        return [
            'query' => [
                'simple_query_string' => [
                    'query'            => $query,
                    'fields'           => ['_all', 'phone'],
                    'default_operator' => 'and',
                    'analyzer'         => 'default',
                ],
            ],
        ];
    }

    public function searchMapping()
    {
        return [
            '_source'    => [
                'enabled' => true,
            ],
            'properties' => [
                'created_at' => [
                    'type'   => 'date',
                    'index'  => 'not_analyzed',
                    'format' => 'epoch_second',
                ],
                'user_id'    => [
                    'type'  => 'integer',
                    'index' => 'not_analyzed',
                ],
                'state'      => [
                    'type'  => 'string',
                    'index' => 'not_analyzed',
                ],
                'phone'      => [
                    'type'            => 'string',
                    'analyzer'        => 'phone',
                    'term_vector'     => 'yes',
                    'search_analyzer' => 'standard',
                    'include_in_all'  => true,
                ],
                'notes'      => [
                    'type' => 'string',
                ]
            ],
        ];
    }


    /**
     * Data to be indexed
     *
     * @return array
     */
    public function toSearchableArray()
    {
        return [
            'created_at' => $this->created_at->timestamp,
            'next_call'  => $this->next_call->timestamp,
            'user_id'    => $this->user_id,
            'phone'      => $this->firm->phoneNumber,
            'state'      => $this->state,
            'notes'      => $this->histories->pluck('note')->toArray(),
            'firm'       => $this->firm,
        ];
    }

    /**
     * Limits search only to user's records
     *
     * @param  Builder $query
     * @param  User $user
     *
     * @return Builder
     */
    public function searchScopeAsUser(Builder $query, User $user)
    {
        return $query->where('user_id', $user->id);
    }

    /**
     * What's the model's type name
     *
     * @return string
     */
    public function searchableAs()
    {
        return config('search.prefix') . 'rm_firm';
    }
}

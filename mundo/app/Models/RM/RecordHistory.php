<?php

namespace Mundo\Models\RM;

use Illuminate\Database\Eloquent\Model;
use Mundo\Models\User;

class RecordHistory extends Model
{
    protected $table = 'rm_record_histories';

    protected $fillable = ['column', 'previous', 'next', 'user_id', 'note', 'created_at', 'updated_at'];

    /**
     * Record
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function record()
    {
        return $this->belongsTo(Record::class, 'record_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}

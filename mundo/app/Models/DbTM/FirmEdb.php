<?php

namespace Mundo\Models\DbTM;

use Illuminate\Database\Eloquent\Model;
use Mundo\Models\DbTM\Firm\Contact;
use Mundo\Models\DbTM\Firm\Email;
use Mundo\Models\DbTM\Firm\PhoneNumber;

/**
 * Class Record
 * @package Mundo\Models\TM
 */
class FirmEdb extends Model
{
    use FirmEdbSearchable;

    /**
     * @var string
     */
    protected $table = 'tm.Firm';

    /**
     * @var string
     */
    protected $primaryKey = 'firm_id';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contacts()
    {
        return $this->hasMany(Contact::class, 'firm_id', 'firm_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function emails()
    {
        return $this->hasMany(Email::class, 'firm_id', 'firm_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function numbers()
    {
        return $this->hasMany(PhoneNumber::class, 'firm_id', 'firm_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'tm.Firm_Category', 'firm_id', 'category_id');
    }


}

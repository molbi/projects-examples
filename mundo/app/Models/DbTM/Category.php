<?php

namespace Mundo\Models\DbTM;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Mundo\Filters\QueryFilter;
use Mundo\Kernel\Eloquent\UuidPrimaryKey;
use Mundo\Models\DbTM\Firm\Contact;
use Mundo\Models\DbTM\Firm\Email;
use Mundo\Models\DbTM\Firm\PhoneNumber;
use Mundo\Models\EmailLog;
use Mundo\Models\ServiceLog;
use Mundo\Models\User;
use Mundo\Modules\TM\ActiveServices\UseActiveServices;
use Mundo\Modules\TM\Sources\Eloquent\Collection;
use Mundo\Modules\TM\Sources\UseSources;

/**
 * Class Record
 * @package Mundo\Models\TM
 */
class Category extends Model
{
    /**
     * @var string
     */
    protected $table = 'tm.CategoryLang';

    /**
     * @var string
     */
    protected $primaryKey = 'category_id';
}

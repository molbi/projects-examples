<?php

namespace Mundo\Models\DbTM\Firm;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Mundo\Filters\QueryFilter;
use Mundo\Kernel\Eloquent\UuidPrimaryKey;
use Mundo\Models\EmailLog;
use Mundo\Models\ServiceLog;
use Mundo\Models\User;
use Mundo\Modules\TM\ActiveServices\UseActiveServices;
use Mundo\Modules\TM\Sources\Eloquent\Collection;
use Mundo\Modules\TM\Sources\UseSources;

/**
 * Class Record
 * @package Mundo\Models\TM
 */
class PhoneNumber extends Model
{
    /**
     * @var string
     */
    protected $table = 'tm.Firm_PhoneNumber';

    protected $primaryKey = 'firm_id';
}

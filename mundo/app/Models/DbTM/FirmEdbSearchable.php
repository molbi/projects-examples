<?php

namespace Mundo\Models\DbTM;

use Illuminate\Database\Query\JoinClause;
use Mundo\Kernel\Search\Searchable;

trait FirmEdbSearchable
{
    use Searchable;

    /**
     * What's the model's type name
     *
     * @return string
     */
    public function searchableAs()
    {
        return config('search.prefix') . 'edb_2017';
    }

    /**
     * Overloaded for eager loading
     *
     * @return mixed
     */
    public static function makeAllSearchable()
    {
        return (new static)->newQuery()
            ->select([
                'Firm.*'
            ])
            ->with('contacts', 'emails', 'numbers', 'categories')
            ->leftJoin('tm.Filter_Params AS FP', function (JoinClause $j) {
                $j->on('FP.param_id', '=', 'Firm.firm_id');
                $j->whereIn('FP.filter_id', [235]);
            })
            ->whereNull('FP.filter_id')
            ->where('source', 'edb_sk_2017')
            ->searchable();
    }

    /**
     * Create search builder
     *
     * @param  string $query

     * @return array
     */
    protected static function makeSearchQuery($query)
    {
        if(is_array($query)) {

            return $query;
        }

        if (empty($query)) {
            return [
                'match_all' => []
            ];
        }

        return [
            'simple_query_string' => [
                'query'            => $query,
                'analyzer'         => 'default',
                'fields'           => ['_all'],
                'default_operator' => 'and',
            ],
        ];

    }

    /**
     * Data to be indexed
     *
     * @return array
     */
    public function toSearchableArray()
    {
        try {
            return [
                'title'       => $this->title,
                'web'         => $this->web,
                'street'      => $this->street,
                'city'        => $this->city,
                'postalCode'  => $this->postalCode,
                'description' => $this->description,
                'contacts'    => $this->contacts->pluck('contactPerson')->toArray(),
                'emails'      => $this->emails->pluck('email')->toArray(),
                'phones'      => $this->numbers->pluck('phoneNumber')->toArray(),
                'categories'  => $this->categories->pluck('title')->toArray()
            ];
        } catch (\Exception $e) {
            dd($this);
        }
    }
}

<?php

namespace Mundo\Models\Telemarketing;


use Illuminate\Database\Eloquent\Model;
use Internal\Models\Employee;
use Mundo\Models\User;

/**
 * Class Stack
 * @package Packages\PisTools\Models
 */
class Stack extends Model
{
    /**
     *
     */
    const CREATED_AT = 'createdAt';
    /**
     *
     */
    const UPDATED_AT = null;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'telemarketing.Stack';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'stack_id';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function employee()
    {
        return $this->hasOne(User::class, 'employee_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function histories()
    {
        return $this->hasMany(History::class, 'stack_id', 'stack_id');
    }

}

<?php

namespace Mundo\Models\Telemarketing;


use Illuminate\Database\Eloquent\Model;
use Mundo\Models\User;

/**
 * Class Stack
 * @package Packages\PisTools\Models
 */
class History extends Model
{
    /**
     *
     */
    const CREATED_AT = 'createdAt';
    /**
     *
     */
    const UPDATED_AT = null;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'telemarketing.History';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'history_id';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function employee()
    {
        return $this->hasOne(User::class, 'employee_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function stack()
    {
        return $this->belongsTo(Stack::class, 'stack_id', 'stack_id');
    }

}

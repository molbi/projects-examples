<?php

namespace Mundo\Models\Telemarketing;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Mundo\Filters\QueryFilter;
use Mundo\Kernel\Eloquent\UuidPrimaryKey;
use Mundo\Models\DbTM\Firm\Contact;
use Mundo\Models\DbTM\Firm\Email;
use Mundo\Models\DbTM\Firm\PhoneNumber;
use Mundo\Models\EmailLog;
use Mundo\Models\ServiceLog;
use Mundo\Models\User;
use Mundo\Modules\TM\ActiveServices\UseActiveServices;
use Mundo\Modules\TM\Sources\Eloquent\Collection;
use Mundo\Modules\TM\Sources\UseSources;

/**
 * Class Record
 * @package Mundo\Models\TM
 */
class Campaign extends Model
{
    /**
     * @var string
     */
    protected $table = 'telemarketing.Campaign';

    /**
     * @var string
     */
    protected $primaryKey = 'campaign_id';
}

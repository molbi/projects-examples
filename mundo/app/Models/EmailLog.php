<?php

namespace Mundo\Models;

use Illuminate\Database\Eloquent\Model;

class EmailLog extends Model
{
    protected $fillable = [
        'target_type',
        'target_id',
        'subject',
        'plain',
        'html',
        'attachments',
        'user_id',
        'to',
        'from_name',
        'from_email',
        'email_template_id'
    ];

    public static function log(
        User $user,
        $target,
        $subject,
        $to,
        $fromName,
        $fromEmail,
        $plain,
        $html,
        $attachemnts,
        EmailTemplate $template = null
    )
    {
        return static::create([
            'email_template_id' => $template === null ? null : $template->id,
            'target_type'       => get_class($target),
            'target_id'         => $target->getKey(),
            'to'                => $to,
            'subject'           => $subject,
            'plain'             => $plain,
            'html'              => $html,
            'from_name'         => $fromName,
            'from_email'        => $fromEmail,
            'user_id'           => $user->id,
            'attachments'       => json_encode(is_null($attachemnts) ? [] : (array)$attachemnts),
        ]);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function attachmentsArray()
    {
        return array_map(function ($record) {
            return (object)$record;
        }, $this->fromJson($this->attachments, false));
    }

    public function target()
    {
        return $this->morphTo();
    }
}

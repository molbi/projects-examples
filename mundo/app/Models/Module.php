<?php

namespace Mundo\Models;

use Illuminate\Database\Eloquent\Model;
use Mundo\Models\Module\Widget;

/**
 * Class Module
 * @package Mundo\Models
 */
class Module extends Model
{
    const MODULE_TM = 'tm',
        MODULE_RIS = 'ris',
        MODULE_RM = 'rm';

    /**
     * @param $name
     *
     * @return self
     */
    public static function forName($name)
    {
        return static::where('name', $name)->with('widgets', 'emailTemplates')->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function emailTemplates()
    {
        return $this->belongsToMany(EmailTemplate::class, 'modules_email_templates');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function widgets()
    {
        return $this->hasOne(Widget::class);
    }

    /**
     * @return []
     */
    public function getWidgets()
    {
        return $this->widgets->getWidgets();
    }

}

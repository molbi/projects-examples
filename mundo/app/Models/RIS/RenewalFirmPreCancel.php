<?php

namespace Mundo\Models\RIS;

use Illuminate\Database\Eloquent\Model;

class RenewalFirmPreCancel extends Model
{
    protected $table = 'obis.RenewalFirmPreCancel';

    /**
     * @var string
     */
    protected $primaryKey = 'renewalFirmPreCancel_id';

    /**
     * @var array
     */
    protected $fillable = ['renewalFirm_id', 'createdAt', 'employee_id', 'state', 'reason'];

    /**
     * @var bool
     */
    public $timestamps = false;
}
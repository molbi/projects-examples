<?php

namespace Mundo\Models\RIS;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Http\Request;
use Mundo\Models\Business\Firm;
use Mundo\Models\Business\FirmService;
use Mundo\Models\EmailLog;
use Mundo\Models\User;

/**
 * Class RenewalFirm
 * @package Mundo\Models\RIS
 */
class RenewalFirm extends Model
{

    use RenewalFirmSearchable;

    /**
     *
     */
    const ACTION_DECLINED = 'declined',
        ACTION_TO_REMOVE = 'to-remove',
        ACTION_RENEWAL = 'renewal',
        ACTION_UNREACHABLE = 'unreachable',
        ACTION_REACHED = 'reached',
        ACTION_PENDING = 'pending',
        ACTION_REMOVED = 'removed',
        ACTION_RETURNED = 'returned',
        ACTION_FORWARDED = 'forwarded';


    /**
     *
     */
    const ACTIONS = [
        self::ACTION_DECLINED,
        self::ACTION_TO_REMOVE,
        self::ACTION_RENEWAL,
        self::ACTION_UNREACHABLE,
        self::ACTION_REACHED,
        self::ACTION_PENDING,
        self::ACTION_REMOVED,
        self::ACTION_RETURNED,
        self::ACTION_FORWARDED
    ];

    /**
     *
     */
    const TERMINAL_STATES = [
        self::ACTION_DECLINED,
        self::ACTION_RENEWAL,
        self::ACTION_TO_REMOVE,
        self::ACTION_REMOVED
    ];

    /**
     * @var string
     */
    protected $table = 'obis.RenewalFirm';

    /**
     * @var string
     */
    protected $primaryKey = 'renewalFirm_id';

    /**
     * @var array
     */
    protected $fillable = ['firm_id', 'employee_id', 'state', 'service', 'lang'];

    /**
     * @var array
     */
    protected $dates = [
        'createdAt',
        'callDate',
        'order_by'
    ];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * All changes in this record
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function histories()
    {
        return $this
            ->hasMany(RenewalFirmHistory::class, 'renewalFirm_id', 'renewalFirm_id')
            ->orderBy('createdAt', 'DESC');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function preCancel()
    {
        return $this->hasOne(RenewalFirmPreCancel::class, 'renewalFirm_id', 'renewalFirm_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'employee_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function firm()
    {
        return $this->hasOne(Firm::class, 'firm_id', 'firm_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function preOrder()
    {
        return $this->hasOne(RenewalFirmPreOrder::class, 'renewalFirm_id', 'renewalFirm_id');
    }

    /**
     * Sent emails
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function emailLogs()
    {
        return $this->morphMany(EmailLog::class, 'target')->orderBy('created_at', 'DESC');;
    }

    /**
     * @return mixed
     */
    public function activeServices()
    {
        return $this->hasMany(FirmService::class, 'firm_id', 'firm_id');
    }

    /**
     * Records, that are not in terminal states
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeUnfinished(Builder $query)
    {
        return $query->whereNotIn('state', self::TERMINAL_STATES);
    }

    /**
     * Records owned by particular user
     *
     * @param Builder $query
     * @param User    $user
     *
     * @return Builder
     */
    public function scopeOwnedByUser(Builder $query, User $user)
    {
        return $query->where('employee_id', '=', $user->id);
    }

    /**
     * Ordered records by next planned call
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeOrdered(Builder $query)
    {
        $raw = $this->getConnection()->raw('IF(callDate != createdAt, callDate, IF(FS.endsOn IS NULL, NOW(), FS.endsOn) ) as order_by');


        $query->select('RenewalFirm.*', $raw)
            ->leftJoin('business.Firm_Service AS FS', function (JoinClause $j) {
                $j->on('FS.firm_id', '=', 'RenewalFirm.firm_id')
                    ->on('FS.service', '=', 'RenewalFirm.service')
                    ->on('FS.lang', '=', 'RenewalFirm.lang');
            })->orderBy('order_by');

        return $query;
    }

    /**
     * @return bool
     */
    public function isNew()
    {
        return false;
    }

    /**
     * @return null
     */
    public function lastHistoryNote()
    {
        $history = $this->histories->filter(function (RenewalFirmHistory $history) {
            return $history->note !== null;
        })->first();

        if ($history !== null && $history->note) {
            return $history->note;
        }

        return null;
    }

    /**
     * Change the state a save the log
     *
     * @param                    $state
     * @param                    $note
     * @param \Mundo\Models\User $user
     *
     * @return bool
     */
    public function setState($state, $note, User $user)
    {
        $this->attributes['state'] = $state;

        $this->histories()->create([
            'action'      => $state,
            'note'        => $note,
            'employee_id' => $user->id
        ]);

        return $this->save();
    }

    /**
     * @param Request $request
     */
    public function setStateRemoved(Request $request)
    {
        $this->preCancel()->create([
            'employee_id' => $request->user()->id,
            'state'       => 'pending',
            'reason'      => $request->input('reason', null)
        ]);
    }

    /**
     * @param Request $request
     */
    public function setStateDeclined(Request $request)
    {
        $this->preCancel()->create([
            'employee_id' => $request->user()->id,
            'state'       => 'pending',
            'reason'      => $request->input('reason', null)
        ]);
    }

    /**
     * @return mixed
     */
    public function getRemoteId()
    {
        return $this->firm_id;
    }

    /**
     * @return mixed
     */
    public function getRemoteLang()
    {
        return $this->firm->lang;
    }

    /**
     * @return mixed
     */
    public function getRemote()
    {
        return $this->firm;
    }

    /**
     * Services object
     *
     * @return mixed
     */
    public function getServices()
    {
        $definition = RisServiceDefinition::where('lang', $this->lang)->first();

        return collect($definition->mergedDefinition)->sortBy('order')->toArray();
    }

    /**
     *
     */
    public function hasActiveService()
    {
        if (($service = $this->activeService()) !== null) {
            return $service->endsOn->gt(Carbon::now());
        }

        return false;
    }

    public function activeService()
    {
        return $this->activeServices
            ->where('service', $this->service)
            ->where('lang', $this->lang)
            ->first();
    }

    /**
     * @param      $serviceId
     * @param      $note
     * @param User $user
     */
    public function createPreOrder($serviceId, $note, User $user)
    {
        /** @var RenewalFirmPreOrder $preOrder */
        $preOrder = $this->preOrder()->create([
            'service_id'  => $serviceId,
            'employee_id' => $user->id,
            'state'       => 'pending'
        ]);

        $preOrder->note()->create([
            'note'        => $note,
            'employee_id' => $user->id
        ]);
    }
}
<?php

namespace Mundo\Models\RIS;

use Illuminate\Database\Eloquent\Model;

class RenewalFirmPreManual extends Model
{
    protected $table = 'obis.RenewalFirmPreManual';

    /**
     * @var string
     */
    protected $primaryKey = 'renewalFirmPreManual_id';

    /**
     * @var array
     */
    protected $fillable = ['renewalFirm_id', 'service_id', 'createdAt', 'employee_id'];

    /**
     * @var bool
     */
    public $timestamps = false;
}
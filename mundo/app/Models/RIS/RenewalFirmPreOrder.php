<?php

namespace Mundo\Models\RIS;

use Illuminate\Database\Eloquent\Model;

class RenewalFirmPreOrder extends Model
{
    protected $table = 'obis.RenewalFirmPreOrder';

    /**
     * @var string
     */
    protected $primaryKey = 'renewalFirmPreOrder_id';

    /**
     * @var array
     */
    protected $fillable = ['renewalFirm_id', 'service_id', 'employee_id', 'state'];

    /**
     * @var bool
     */
    public $timestamps = false;

    public function note()
    {
        return $this->hasOne(RenewalFirmPreOrderNote::class, 'renewalFirm_id', 'renewalFirm_id');
    }
}
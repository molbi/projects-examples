<?php

namespace Mundo\Models\RIS;

use Illuminate\Database\Eloquent\Model;
use Mundo\Models\User;

class RenewalFirmHistory extends Model
{
    const ACTION_DECLINED = 'declined',
        ACTION_FORWARDED = 'forwarded',
        ACTION_FOUND_PHONE = 'found-phone',
        ACTION_LAST_APPEAL = 'last-appeal',
        ACTION_NOT_FOUND_PHONE = 'not-found-phone',
        ACTION_ORDER_CALL_REACHED = 'order-call-reached',
        ACTION_ORDER_CALL_UNREACHED = 'order-call-unreachable',
        ACTION_PENDING = 'pending',
        ACTION_REACHED = 'reached',
        ACTION_REMOVED = 'removed',
        ACTION_RENEWAL = 'renewal',
        ACTION_RETURNED = 'returned',
        ACTION_TO_REMOVE = 'to-remove',
        ACTION_UNREACHABLE = 'unreachable';

    const ACTIONS = [
        self::ACTION_DECLINED,
        self::ACTION_FORWARDED,
        self::ACTION_FOUND_PHONE,
        self::ACTION_LAST_APPEAL,
        self::ACTION_NOT_FOUND_PHONE,
        self::ACTION_ORDER_CALL_REACHED,
        self::ACTION_ORDER_CALL_UNREACHED,
        self::ACTION_PENDING,
        self::ACTION_REACHED,
        self::ACTION_REMOVED,
        self::ACTION_RENEWAL,
        self::ACTION_RETURNED,
        self::ACTION_TO_REMOVE,
        self::ACTION_UNREACHABLE
    ];

    protected $table = 'obis.RenewalFirmHistory';

    /**
     * @var string
     */
    protected $primaryKey = 'renewalFirmHistory_id';

    /**
     * @var array
     */
    protected $fillable = ['renewalFirm_id', 'action', 'note', 'employee_id', 'emailSent'];

    protected $dates = [
        'createdAt'
    ];

    /**
     * @var bool
     */
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class, 'employee_id', 'id');
    }
}
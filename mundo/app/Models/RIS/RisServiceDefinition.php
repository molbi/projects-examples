<?php

namespace Mundo\Models\RIS;

use Illuminate\Database\Eloquent\Model;

class RisServiceDefinition extends Model
{
    protected $fillable = ['name', 'parent_id', 'definition'];

    public function parent()
    {
        return $this->belongsTo(RisServiceDefinition::class, 'parent_id');
    }

    public function getMergedDefinitionAttribute()
    {
        $merged = $this->parent === null ? [] : $this->parent->mergedDefinition;

        return array_merge_recursive($this->getDefinition(), $merged);
    }


    public function getDefinition()
    {
        return $this->fromJson($this->definition);
    }
}

<?php

namespace Mundo\Models\RIS;

use Illuminate\Database\Eloquent\Model;

class RenewalFirmPreOrderNote extends Model
{
    protected $table = 'obis.RenewalFirmPreOrderNote';

    /**
     * @var string
     */
    protected $primaryKey = 'renewalFirmPreOrderNote_id';

    /**
     * @var array
     */
    protected $fillable = ['renewalFirm_id', 'note', 'employee_id'];

    /**
     * @var bool
     */
    public $timestamps = false;
}
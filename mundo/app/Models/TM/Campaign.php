<?php

namespace Mundo\Models\TM;

use Illuminate\Database\Eloquent\Model;
use Mundo\Models\EmailTemplate;
use Mundo\Models\Taggable;
use Mundo\Widgets\TM as TMWidgets;

class Campaign extends Model
{
    use Taggable;

    const DEFAULT_WIDGETS = [
    ];

    protected $table = 'tm_campaigns';

    protected $attributes = [
        'widgets'  => '[]',
        'services' => '[]',
    ];

    protected $fillable = ['name', 'description', 'sync_id', 'source', 'widgets', 'services', 'services_id'];

    public function emailTemplates()
    {
        return $this->belongsToMany(EmailTemplate::class, 'tm_campaigns_email_templates');
    }

    public static function sources()
    {
        return self::groupBy('source')->orderBy('source')->pluck('source');
    }

    public function targetGroup()
    {
        return $this->belongsTo(TargetGroup::class, 'target_group_id');
    }

    public function records()
    {
        return $this->hasMany(Record::class, 'campaign_id', 'id');
    }

    public function forwardableRecords()
    {
        return $this->records()
            ->where('state', 'new')
            ->whereNull('user_id')
            ->orderBy('created_at');
    }

    public function forwardableRecordsCount()
    {
        return $this->forwardableRecords()
            ->selectRaw('campaign_id, count(*) as aggregate')
            ->groupBy('campaign_id');
    }

    public function forwardableLeadsRecords()
    {
        return $this->records()
            ->where('state', 'new');
    }

    public function forwardableLeadsRecordsCount()
    {
        return $this->forwardableLeadsRecords()
            ->selectRaw('campaign_id, count(*) as aggregate')
            ->groupBy('campaign_id');
    }

    public function getServiceTemplate()
    {
        return $this->belongsTo(CampaignServiceDefinition::class, 'services_id');
    }

    public function getForwardableCountAttribute()
    {
        if ($res = $this->forwardableRecordsCount->first()) {
            return $res->aggregate;
        }

        return 0;
    }

    public function getForwardableLeadsCountAttribute()
    {
        if ($res = $this->forwardableLeadsRecordsCount->first()) {
            return $res->aggregate;
        }

        return 0;
    }

    public function setSyncIdAttribute($value)
    {
        $this->attributes['sync_id'] = empty(trim($value)) ? null : $value;
    }

    /**
     * Returns array of widgets in right order
     *
     * @return array
     */
    public function getWidgets()
    {
        $widgets = (empty($this->widgets)) ? '[]' : $this->widgets;

        return array_merge(self::DEFAULT_WIDGETS, $this->fromJson($widgets));
    }

    public function getWidgetButtons()
    {
        $result = [];
        foreach ($this->getWidgets() as $widget) {
            if (isset($widget['buttons'])) {
                $result[] = $widget['buttons'];
            }
        }

        return array_flatten($result);
    }

    /**
     * Services object
     *
     * @return mixed
     */
    public function getServices()
    {
        $services = $this->fromJson($this->services);
        if ($this->getServiceTemplate) {
            return array_merge_recursive($this->getServiceTemplate->mergedDefinition, $services);
        }

        return $services;
    }
}

<?php

namespace Mundo\Models\TM;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Mundo\Models\User;

class ReminderCollection extends Collection
{
    /**
     * Eager load users
     *
     * @return self
     */
    public function withUsers()
    {
        /** @var \Mundo\Modules\TM\Sources\Eloquent\Collection $users */
        $users = User::whereIn('id', $this->pluck('user_id'))->get();
        foreach ($this->all() as $item) {
            $item->user = $users->find($item->user_id);
        }

        return $this;
    }

    public function findOrFail($key)
    {
        list($id, $type) = explode('-', $key);
        $item = $this->whereLoose('id', $id)->whereLoose('type', $type)->first();
        if (!$item) {
            throw new ModelNotFoundException;
        }

        return $item;
    }

    /**
     * @return static
     */
    public function sorted() {
        return $this->sortBy(function($item) {
            return $item->due_days;
        }, SORT_REGULAR, true);
    }

    /**
     * Eager load all histories
     *
     * @return $this
     */
    public function withHistories()
    {
        if($this->isEmpty()) {
            return $this;
        }

        $ids = [];
        /** @var  Builder $histories */
        $histories = with(new ReminderHistory)->newQuery()->orderBy('created_at', 'desc');
        foreach ($this as $item) {
            $ids[$item->type][] = $item->id;
        }

        foreach ($ids as $type => $in) {
            $histories->orWhere(function (Builder $q) use ($type, $in) {
                $q->where('type', $type)->whereIn('remote_id', $in);
            });
        }
        $histories = $histories->get();

        foreach ($this as $item) {
            $item->setHistories(
                $histories->whereLoose('remote_id', $item->id)->whereLoose('type', $item->type)
            );
        }

        return $this;
    }
}

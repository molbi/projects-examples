<?php

namespace Mundo\Models\TM;

use Illuminate\Database\Eloquent\Model;

class AdditionalPhoneNumber extends Model {
    protected $table = 'tm_additional_phone_numbers';

    protected $fillable = ['source', 'remote_id', 'note', 'phone_number'];
}
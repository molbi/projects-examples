<?php

namespace Mundo\Models\TM;

use Illuminate\Database\Eloquent\Model;

class IndexEntry extends Model {
    protected $table = 'tm_index_entries';
}
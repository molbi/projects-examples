<?php

namespace Mundo\Models\TM;

use Carbon\Carbon;
use Doctrine\DBAL\Query\QueryBuilder;
use Illuminate\Database\Connection;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Collection;
use Mundo\Kernel\Eloquent\TablessModel;
use Mundo\Models\EmailLog;
use Mundo\Models\User;

/**
 * Class Reminder
 * @package Mundo\Models\TM
 */
class Reminder extends TablessModel
{
    /**
     *
     */
    const KARTUS_COLUMNS = [
        'id'                   => 'invoices.id',
        'subject_id'           => 'users.id',
        'lang'                 => 'users.lang',
        'created_at'           => 'invoices.created_at',
        'maturity'             => 'maturity',
        'due_at'               => 'DATE(invoices.created_at) + INTERVAL invoices.maturity DAY',
        'due_days'             => 'DATEDIFF(NOW(), DATE(invoices.created_at) + INTERVAL invoices.maturity DAY)',
        'subject_company'      => 'contacts.title',
        'subject_person'       => 'contacts.name',
        'subject_phone_number' => 'contacts.phone',
        'subject_email'        => 'contacts.email',
        'service_title'        => '"CSKontakt"',
        'service_price'        => 'invoices.price',
        'service_currency'     => 'invoices.currency',
        'service_garantion'    => 'invoices.garantion',
        'user_id'              => 'invoices.employee_id',
        'type'                 => '"kartus"',
    ];

    /**
     *
     */
    const B2M_COLUMNS = [
        'id'                   => 'Order.order_id',
        'subject_id'           => 'Firm.firm_id',
        'lang'                 => 'Firm.lang',
        'created_at'           => 'Invoice.createdOn',
        'maturity'             => 'Invoice.maturity',
        'due_at'               => 'DATE(Invoice.createdOn) + INTERVAL Invoice.maturity DAY',
        'due_days'             => 'DATEDIFF(NOW(), DATE(Invoice.createdOn) + INTERVAL Invoice.maturity DAY)',
        'subject_company'      => 'Firm.title',
        'subject_person'       => 'Firm.contactPerson',
        'subject_phone_number' => 'Firm.phoneNumber',
        'subject_email'        => 'Firm.email',
        'service_title'        => 'GROUP_CONCAT(DISTINCT Service.title SEPARATOR ";")',
        'service_price'        => 'GROUP_CONCAT(DISTINCT Service.price SEPARATOR ";")',
        'service_currency'     => '"€"',
        'service_garantion'    => 'Order.garantion',
        'user_id'              => 'Order.employee_id',
        'type'                 => '"b2m"',
    ];

    /**
     *
     */
    const SOURCES = [
        'b2m'
    ];

    /** @var  Connection */
    protected static $connection;
    /** @var  ReminderCollection */
    protected static $reminders;
    /** @var  User */
    protected $user;
    /** @var  Collection */
    protected $histories;
    /** @var array */
    protected $casts = [
        'created_at'        => 'datetime',
        'due_at'            => 'date',
        'service_garantion' => 'boolean',
    ];

    /**
     * Set connection to be used
     *
     * @param \Illuminate\Database\Connection $connection
     */
    public static function setConnection(Connection $connection)
    {
        static::$connection = $connection;
    }

    /**
     * @return mixed
     */
    public function nextCall()
    {
        $history = $this->histories->filter(function ($history) {
            return isset($history->next_call);
        })->first();

        return $history ? $history->next_call : with(clone $this->created_at)->addDay(3)->setTime(15, 0, 0);
    }

    /**
     * @return null
     */
    public function lastNote()
    {
        $history = $this->histories->filter(function ($history) {
            return isset($history->note);
        })->first();

        return $history ? $history->note : null;
    }

    /**
     * Connetction to be used
     *
     * @return \Illuminate\Database\Connection
     */
    public static function getConnection()
    {
        return static::$connection;
    }

    /**
     * @return mixed
     */
    public function additionalPhoneNumbers()
    {
        return AdditionalPhoneNumber::where('source', $this->type)
            ->where('remote_id', $this->subject_id);
    }

    /**
     * Overloaded for user getter
     *
     * @param string $key
     *
     * @return mixed|\Mundo\Models\User
     */
    public function __get($key)
    {
        if ($key === 'user') {
            if (!isset($this->user)) {
                $this->user = $this->user();
            }

            return $this->user;
        }

        if ($key === 'histories') {
            if (!isset($this->histories)) {
                $this->histories = $this->histories()->get();
            }

            return $this->histories;
        }

        return parent::__get($key);
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param \Illuminate\Support\Collection $histories
     */
    public function setHistories(Collection $histories)
    {
        $this->histories = $histories;
    }

    /**
     * User that made this order
     *
     * @return User
     */
    public function user()
    {
        return User::findOrFail($this->user_id);
    }

    /**
     * All saved histories
     * @return QueryBuilder
     */
    public function histories()
    {
        return ReminderHistory::where('type', $this->type)
            ->where('remote_id', $this->id)
            ->orderBy('created_at', 'desc');
    }

    /**
     * @return mixed
     */
    public function emailLogs()
    {
        return EmailLog::where('target_type', Reminder::class)
            ->where('target_id', sprintf('%s-%s', $this->id, $this->type))
            ->orderBy('created_at', 'desc')
            ->get();
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return implode('-', [$this->id, $this->type]);
    }

    /**
     * Get reminders collection for all SOURCES
     *
     * @param User $user
     *
     * @return ReminderCollection
     */
    public static function allForUser(User $user)
    {
        if (is_null(static::$reminders)) {
            static::$reminders = new ReminderCollection();
        }

        foreach (self::SOURCES as $source) {
            static::remindersForUser($user, $source);
        }

        return static::$reminders;
    }

    /**
     * @param User $user
     * @param      $key
     *
     * @return mixed
     */
    public static function findOfFail(User $user, $key)
    {
        return static::allForUser($user)->findOrFail($key);
    }

    /**
     * Get B2M reminders date
     *
     * @param User $user
     * @param      $source
     */
    protected static function remindersForUser(User $user, $source)
    {
        $connection = static::getConnection();

        try {
            $const = constant(sprintf('self::%s_COLUMNS', strtoupper($source)));

            // Build all column to project
            $columns = array_map(function ($as, $column) use ($connection, $const) {
                return $connection->raw($column . ' as ' . $as);
            }, array_keys($const), $const);

            $method = sprintf('get%sData', ucfirst($source));
            if (method_exists(get_class(), $method)) {
                array_map(function ($reminder) use ($user, $columns, $connection) {
                    static::$reminders->push(new static ((array)$reminder));
                }, static::$method($user, $columns, $connection));
            }
        } catch (\Exception $e) {
        }
    }

    /**
     * @param User       $user
     * @param            $columns
     * @param Connection $connection
     *
     * @return array|static[]
     */
    private static function getKartusData(User $user, $columns, Connection $connection)
    {
        return $connection->table('cs_contact.invoices')
            ->select($columns)
            ->where('invoices.state', 'processed')
            ->leftJoin('obis.CancellRequest', function (JoinClause $clause) {
                $clause->on('CancellRequest.subject_id', '=', 'invoices.id')
                    ->where('CancellRequest.subject_type', '=', 'cs_contact');
            })
            ->join('cs_contact.users', 'invoices.supplier_id', '=', 'users.id')
            ->join('cs_contact.contacts', function (JoinClause $clause) {
                $clause->on('contacts.contactable_id', '=', 'users.id')
                    ->where('contacts.contactable_type', '=', 'Kartus\Model\User');
            })
            ->whereNull('CancellRequest.id')
            ->whereRaw('DATE(invoices.created_at) + INTERVAL ' . static::getUserRemindersFromSetting($user) . ' DAY <= NOW()')
            ->where('invoices.employee_id', $user->id)
            ->groupBy('invoices.id')
            ->orderBy('due_days', 'DESC')
            ->get();
    }

    /**
     * @param User       $user
     * @param            $columns
     * @param Connection $connection
     *
     * @return array|static[]
     */
    private static function getB2mData(User $user, $columns, Connection $connection)
    {
        return $connection->table('obis.Order')
            ->select($columns)
            ->whereNotIn('Order.state', ['cancelled', 'toSpecify', 'paid'])
            ->leftJoin('obis.CancellRequest', function (JoinClause $clause) {
                $clause->on('CancellRequest.subject_id', '=', 'Order.order_id')
                    ->where('CancellRequest.subject_type', '=', 'obis');
            })
            ->leftJoin('obis.OrderExactor', function (JoinClause $clause) {
                $clause->on('OrderExactor.order_id', '=', 'Order.order_id');
            })
            ->join('business.Firm', 'Firm.firm_id', '=', 'Order.firm_id')
            ->join('obis.Invoice', 'Invoice.order_id', '=', 'Order.order_id')
            ->join('obis.Order_Service', 'Order_Service.order_id', '=', 'Order.order_id')
            ->join('obis.Service', 'Service.service_id', '=', 'Order_Service.service_id')
            ->whereNull('CancellRequest.id')
            ->whereNull('OrderExactor.order_id')
            ->whereRaw('DATE(Invoice.createdOn) + INTERVAL ' . static::getUserRemindersFromSetting($user) . ' DAY <= NOW()')
            ->whereRaw('DATE(Invoice.createdOn) + INTERVAL 42 DAY >= NOW()')
            ->where('Order.employee_id', $user->id)
            ->groupBy('Order.order_id')
            ->orderBy('due_days', 'DESC')
            ->get();
    }

    /**
     * @param User $user
     *
     * @return \Illuminate\Database\Eloquent\Model|int
     */
    private static function getUserRemindersFromSetting(User $user)
    {
        return $user->hasParam('reminders_days_from') ? $user->param('reminders_days_from') : 0;
    }
}

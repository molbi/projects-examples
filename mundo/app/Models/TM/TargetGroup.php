<?php

namespace Mundo\Models\TM;

use Illuminate\Database\Eloquent\Model;
use Mundo\Modules\TM\TargetGroups\ITargetGroup;
use Mundo\Modules\TM\TargetGroups\TargetGroup as AbstractTargetGroup;
use Symfony\Component\Debug\Exception\ClassNotFoundException;

/**
 * Class TargetGroup
 * @package Mundo\Models\TM
 */
class TargetGroup extends Model
{
    /**
     * @var string
     */
    protected $table = 'tm_target_groups';

    /**
     * @param Campaign $campaign
     *
     * @return ITargetGroup
     * @throws ClassNotFoundException
     */
    public static function getInstance(Campaign $campaign)
    {
        /** @var AbstractTargetGroup $klass */
        $klass = $campaign->targetGroup->shortname;
        if (class_exists($klass)) {
            return new $klass($campaign, app('tm.sources'));
        }

        throw new ClassNotFoundException("Class {$klass} not found", new \ErrorException(""));
    }
}

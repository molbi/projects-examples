<?php

namespace Mundo\Models\TM;

use Illuminate\Database\Eloquent\Model;

class CampaignServiceDefinition extends Model
{
    protected $table = 'tm_campaign_service_definitions';
    protected $fillable = ['name', 'parent_id', 'definition'];

    public function parent()
    {
        return $this->belongsTo(CampaignServiceDefinition::class, 'parent_id');
    }

    public function getMergedDefinitionAttribute()
    {
        $merged = $this->parent === null ? [] : $this->parent->mergedDefinition;

        return array_merge_recursive($this->getDefinition(), $merged);
    }


    public function getDefinition()
    {
        return $this->fromJson($this->definition);
    }
}

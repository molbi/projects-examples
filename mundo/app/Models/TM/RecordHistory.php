<?php

namespace Mundo\Models\TM;

use Illuminate\Database\Eloquent\Model;
use Mundo\Kernel\Eloquent\UuidPrimaryKey;
use Mundo\Models\User;

class RecordHistory extends Model
{
    use UuidPrimaryKey;

    protected $table = 'tm_record_histories';

    protected $fillable = ['column', 'previous', 'next', 'user_id', 'note', 'created_at', 'updated_at'];

    /**
     * Record
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function record()
    {
        return $this->belongsTo(Record::class, 'record_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}

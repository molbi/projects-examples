<?php

namespace Mundo\Models\TM;

use Illuminate\Database\Eloquent\Builder;
use Mundo\Models\Tag;
use Mundo\Models\User;

/**
 * Leadership taxonomy
 *
 * This trait should be used in User model only
 *
 * User is able to lead:
 * 1) user
 * 2) tag wchich contains users
 * 3) campaign
 * 4) tag which contains campaigns
 *
 * @package Mundo\Models\TM
 */
trait LeadsUsers
{
    /**
     * Add relation "leaders" to tag model
     */
    public static function bootLeadsUsers()
    {
        Tag::addTaggable('leaders', function(Tag $tag) {
            return $tag->morphToMany(User::class, 'leaderable', 'tm_leaderships')->where('type', 'user-tag');
        });
    }

    /**
     * Preload all relations for relationships taxonomy
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePreloadLeaderships(Builder $query)
    {
        return $query->with(
            'directLeaders',
            'tags', 'tags.leaders',
            'leadsUsersDirectly', 'leadsUserTags', 'leadsUserTags.users',
            'leadsCampaignsDirectly', 'leadsCampaignTags', 'leadsCampaignTags.tmCampaigns'
        );
    }

    /**
     * Direct leaders of this leaders
     *
     * @return mixed
     */
    public function directLeaders()
    {
        return $this->morphToMany(User::class, 'leaderable', 'tm_leaderships');
    }

    /**
     * All leaders of this user (from direct leadership and tag leaderships)
     *
     * @return mixed
     */
    public function getLeadersAttribute()
    {
        return $this->directLeaders->merge(
            $this->tags->load('leaders')->pluck('leaders')->flatten()
        )->unique();
    }

    /**
     * Users leaded by this user directly
     *
     * @return mixed
     */
    public function leadsUsersDirectly()
    {
        return $this->morphedByMany(User::class, 'leaderable', 'tm_leaderships');
    }

    /**
     * Tags beeing leaded by this user
     *
     * @return mixed
     */
    public function leadsUserTags()
    {
        return $this->morphedByMany(Tag::class, 'leaderable', 'tm_leaderships')->wherePivot('type', 'user-tag');
    }

    /**
     * Campaigns beeing leaded by this user directly
     *
     * @return mixed
     */
    public function leadsCampaignsDirectly()
    {
        return $this->morphedByMany(Campaign::class, 'leaderable', 'tm_leaderships');
    }

    /**
     * Campaign tags beeing leaded by this user directly
     *
     * @return mixed
     */
    public function leadsCampaignTags()
    {
        return $this->morphedByMany(Tag::class, 'leaderable', 'tm_leaderships')->wherePivot('type', 'campaign-tag');
    }

    /**
     * All users beeing leaded by this user
     *
     * @return mixed
     */
    public function getLeadsUsersAttribute()
    {
        return $this->leadsUsersDirectly->merge(
            $this->leadsUserTags->load('users')->pluck('users')->flatten()
        )->unique();
    }

    /**
     * All campaigns beeing leaded by this user
     *
     * @return mixed
     */
    public function getLeadsCampaignsAttribute()
    {
        return $this->leadsCampaignsDirectly->merge(
            $this->leadsCampaignTags->load('tmCampaigns')->pluck('tmCampaigns')->flatten()
        )->unique();
    }
}

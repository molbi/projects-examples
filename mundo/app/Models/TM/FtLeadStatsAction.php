<?php

namespace Mundo\Models\TM;

use Illuminate\Database\Eloquent\Model;
use Mundo\Models\Telemarketing\Campaign as OldCampaign;

/**
 * Class FtLeadStatsAction
 * @package Mundo\Models\TM
 */
class FtLeadStatsAction extends Model
{
    /**
     * @var string
     */
    protected $table = 'mundo.ft_lead_stats_actions';
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function new_campaign()
    {
        return $this->belongsTo(Campaign::class, 'campaign_id', 'sync_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function campaign()
    {
        return $this->belongsTo(OldCampaign::class, 'campaign_id', 'campaign_id');
    }
}

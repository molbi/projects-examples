<?php

namespace Mundo\Models\TM;

use Illuminate\Support\Collection as BaseCollection;
use Mundo\Kernel\Search\Searchable;
use Mundo\Models\User;

trait RecordSearchable
{
    use Searchable;

    /**
     * Overloaded for eager loading
     *
     * @return mixed
     */
    public static function makeAllSearchable()
    {
        return (new static)->newQuery()
            ->with('histories', 'campaign', 'params')
            ->searchable();
    }

    /**
     * Overloaded to preload remote data
     *
     * @param  \Illuminate\Support\Collection $models
     * @return mixed
     */
    public function makeSearchable(BaseCollection $models)
    {
        if (!$models->isEmpty()) {
            return $models->first()->searchableUsing()->update(
                $models->withRemote()
            );
        }
    }

    /**
     * Redefine basic ES search query
     * @param  $query
     * @return array
     */
    protected static function makeSearchQuery($query)
    {
        return [
            'query' => [
                'simple_query_string' => [
                    'query'            => $query,
                    'fields'           => ['_all', 'phone'],
                    'default_operator' => 'and',
                    'analyzer'         => 'default',
                ],
            ],
        ];
    }

    public function searchMapping()
    {
        return [
            '_source'    => [
                'enabled' => true,
            ],
            'properties' => [
                'created_at'  => [
                    'type'   => 'date',
                    'index'  => 'not_analyzed',
                    'format' => 'epoch_second',
                ],
                'updated_at'  => [
                    'type'   => 'date',
                    'index'  => 'not_analyzed',
                    'format' => 'epoch_second',
                ],
                'user_id'     => [
                    'type'  => 'integer',
                    'index' => 'not_analyzed',
                ],
                'uuid'        => [
                    'type'  => 'string',
                    'index' => 'not_analyzed',
                ],
                'state'       => [
                    'type'  => 'string',
                    'index' => 'not_analyzed',
                ],
                'phone'       => [
                    'type'            => 'string',
                    'analyzer'        => 'phone',
                    'term_vector'     => 'yes',
                    'search_analyzer' => 'standard',
                    'include_in_all'  => true,
                ],
                'campaign_id' => [
                    'type'  => 'integer',
                    'index' => 'not_analyzed',
                ],
                'notes'       => [
                    'type' => 'string',
                ],
            ],
        ];
    }


    /**
     * Data to be indexed
     *
     * @return array
     */
    public function toSearchableArray()
    {
        try {
            return [
                'created_at'  => !is_null($this->created_at) ? $this->created_at->timestamp : null,
                'updated_at'  => !is_null($this->updated_at) ? $this->updated_at->timestamp : null,
                'user_id'     => $this->user_id,
                'phone'       => $this->searchablePhoneNumber(),
                'uuid'        => $this->id,
                'state'       => $this->state,
                'campaign_id' => $this->campaign_id,
                'notes'       => $this->histories->pluck('note')->toArray(),
                'remoteData'  => (array)$this->remoteData,
            ];
        } catch (\Exception $e) {
            dd($this);
        }
    }

    protected function searchablePhoneNumber()
    {
        $phone = [];

        isset($this->remoteData->phone) && $phone[] = $this->remoteData->phone;
        isset($this->remoteData->phone_number) && $phone[] = $this->remoteData->phone_number;

        if(!empty($phones = $this->additionalPhoneNumbers()->pluck('phone_number')->toArray())) {
            $phone = array_merge($phone, $phones);
        }

        $phone = array_map(function ($phone) {
            return preg_replace('|[^0-9]|', '', $phone);
        }, $phone);

        return $phone;
    }

    /**
     * Limits search only to user's records
     *
     * @param  \Mundo\Kernel\Search\Builder $query
     * @param  User $user
     * @return \Mundo\Kernel\Search\Builder
     */
    public function searchScopeAsUser(\Mundo\Kernel\Search\Builder $query, User $user)
    {
        return $query->where('user_id', $user->id);
    }
}

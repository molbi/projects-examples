<?php

namespace Mundo\Models\TM;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Mundo\Filters\QueryFilter;
use Mundo\Kernel\Eloquent\UuidPrimaryKey;
use Mundo\Models\EmailLog;
use Mundo\Models\ServiceLog;
use Mundo\Models\User;
use Mundo\Modules\TM\ActiveServices\UseActiveServices;
use Mundo\Modules\TM\Sources\Eloquent\Collection;
use Mundo\Modules\TM\Sources\UseSources;

/**
 * Class Record
 * @package Mundo\Models\TM
 */
class Record extends Model
{
    use UuidPrimaryKey, UseSources, RecordSearchable, UseActiveServices;

    const STATE_NEW = 'new',
        STATE_ACCEPTED = 'accepted',
        STATE_DECLINED = 'declined',
        STATE_REMOVED = 'removed',
        STATE_REACHED = 'reached',
        STATE_NOT_REACHED = 'not-reached';

    const STATES = [
        self::STATE_NEW,
        self::STATE_ACCEPTED,
        self::STATE_DECLINED,
        self::STATE_REMOVED,
        self::STATE_REACHED,
        self::STATE_NOT_REACHED,
    ];

    const TERMINAL_STATES = [
        self::STATE_ACCEPTED,
        self::STATE_DECLINED,
        self::STATE_REMOVED,
    ];

    /**
     * @var string
     */
    protected $table = 'tm_records';

    /**
     * @var array
     */
    protected $dates = ['next_call'];

    /**
     * @var array
     */
    protected $fillable = ['remote_id', 'user_id'];

    /** @var  object */
    public $remoteDataHolder;

    /** @var string */
    protected $note = null;

    protected $appends = [
        'record_label'
    ];

    /**
     * @return string
     */
    public function __toString()
    {
        if (isset($this->remoteData->company)) {
            return sprintf('%s (%s)', $this->remoteData->company, $this->remoteData->contact_person);
        } else {
            return $this->remoteData->contact_person;
        }
    }

    /**
     *
     */
    public static function boot()
    {
        parent::boot();

        self::updated(function (Record $record) {
            $note = $record->popNote();
            foreach ($record->getDirty() as $column => $value) {
                if (!in_array($column, ['created_at', 'updated_at', 'state'])) {
                    $history = new RecordHistory([
                        'column'   => $column,
                        'previous' => $record->getOriginal($column),
                        'next'     => $value,
                        'user_id'  => ($user = \Auth::getUser()) ? $user->id : null,
                        'note'     => $note,
                    ]);
                    $history->record_id = $record->id;
                    $history->created_at = $record->updated_at;
                    $history->updated_at = $record->updated_at;
                    $history->save();
                }
            }
        });
    }

    /**
     * Set the remote data
     *
     * @param $data
     */
    public function setRemoteData($data)
    {
        $this->remoteDataHolder = $data;
    }

    /**
     * @return mixed
     */
    public function additionalPhoneNumbers()
    {
        return AdditionalPhoneNumber::where('source', $this->campaign->source)
            ->where('remote_id', $this->remote_id)
            ->get();
    }

    /**
     * Loading remote data on demand
     *
     * @param string $key
     *
     * @return mixed|object
     */
    public function __get($key)
    {
        if ($key === 'remoteData') {
            if (is_null($this->remoteDataHolder)) {
                $this->withRemote();
            }

            return $this->remoteDataHolder;
        }

        return parent::__get($key);
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function __isset($key)
    {
        if ($key === 'remoteData') {
            return true;
        }

        return parent::__isset($key);
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return array_merge(parent::jsonSerialize(), ['remoteData' => $this->remoteData]);
    }

    /**
     * Sent emails
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function emailLogs()
    {
        return $this->morphMany(EmailLog::class, 'target')->orderBy('created_at', 'DESC');;
    }

    /**
     * Offered services
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function serviceLogs()
    {
        return $this->morphMany(ServiceLog::class, 'target')->orderBy('created_at', 'DESC');;
    }

    /**
     * Ordered records by next planned call
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeOrdered(Builder $query)
    {
        $raw = $this->getConnection()->raw(
            'CASE WHEN next_call IS NULL THEN NOW() ELSE next_call END'
        );

        return $query->orderBy($raw);
    }

    /**
     * Only records for a month ahead
     *
     * @param Builder $query
     * @return Builder|static
     */
    public function scopeMonthAhead(Builder $query)
    {
        return $query->where(function ($query) {
            $query->whereRaw('DATE(next_call) <= DATE(NOW() + INTERVAL 1 MONTH)')
                ->orWhereNull('next_call');
        });
    }

    /**
     * Only records with given source
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param                                       $source
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSourceIs(Builder $query, $source)
    {
        return $query->with('campaign')
            ->whereHas('campaign', function (Builder $query) use ($source) {
                return $query->where('source', $source);
            });
    }

    /**
     * @param Builder $query
     *
     * @return $this
     */
    public function scopeForwardableLeads(Builder $query)
    {
        return $query->where('state', 'new');
    }

    /**
     * @return mixed
     */
    public function displayableHistories()
    {
        return $this->histories()->whereIn('column', ['state', 'user_id'])->with('user');
    }

    /**
     * Records owned by particular user
     *
     * @param Builder $query
     * @param User $user
     *
     * @return Builder
     */
    public function scopeOwnedByUser(Builder $query, User $user)
    {
        return $query->where('user_id', '=', $user->id);
    }

    /**
     * Records owned by list of users
     *
     * @param Builder $query
     * @param array $users
     *
     * @return Builder
     *
     */
    public function scopeOwnedByUsers(Builder $query, array $users)
    {
        return $query->whereIn('user_id', $users);
    }

    /**
     * @param             $query
     * @param QueryFilter $filter
     *
     * @return mixed
     */
    public function scopeFilter($query, QueryFilter $filter)
    {
        return $filter->apply($query);
    }

    /**
     * Records, that are not in terminal states
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeUnfinished(Builder $query)
    {
        return $query->whereNotIn('state', self::TERMINAL_STATES);
    }

    /**
     * Creates new collection of models
     *
     * @param array $models
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function newCollection(array $models = [])
    {
        return $this->searchableCollection(new Collection($models));
    }

    /**
     * Loads remote data
     */
    public function withRemote()
    {
        $this->getSourceManager()->get($this->campaign->source)->fill($this);
    }

    /**
     * User to whom is record assigned
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Is this record assigned?
     * @return bool
     */
    public function hasUser()
    {
        return $this->user !== null;
    }

    /**
     * Assigned campaign
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }

    /**
     * Is this record terminal?
     * @return bool
     */
    public function isTerminal()
    {
        return in_array($this->state, self::TERMINAL_STATES);
    }

    /**
     * Set the note for history entries
     *
     * @param string $note
     */
    public function setNote($note)
    {
        $this->note = $note;
    }

    /**
     * @return mixed|object
     */
    public function getRemoteId()
    {
        return $this->remote_id;
    }

    /**
     * @return mixed
     */
    public function getRemoteLang()
    {
        return $this->remoteData->lang;
    }

    /**
     * @return mixed|object
     */
    public function getRemote()
    {
        return $this->remoteData;
    }

    /**
     * Get and reset the note
     *
     * @return string
     */
    protected function popNote()
    {
        $note = $this->note;
        $this->note = null;

        return $note;
    }

    /**
     * All changes in this record
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function histories()
    {
        return $this
            ->hasMany(RecordHistory::class, 'record_id', 'id')
            ->orderBy('created_at', 'DESC');
    }

    /**
     * All params
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function params()
    {
        return $this->hasMany(RecordParam::class, 'record_id', 'id');
    }

    /**
     * @return mixed
     */
    public function sentMailsCount()
    {
        return $this->emailLogs->count();
    }

    /**
     * @param        $key
     * @param        $value
     * @param string $type
     * @param bool $overwrite
     */
    public function addParam($key, $value, $type = 'string', $overwrite = false)
    {
        if ($overwrite) {
            $this->params()->where('key', '=', $key)->delete();
        }

        $param = new RecordParam();
        $param->record_id = $this->id;
        $param->type = $type;
        $param->value = $value;
        $param->key = $key;
        $param->save();
    }

    /**
     * @param      $key
     * @param null $default
     *
     * @return mixed|null
     */
    public function getParam($key, $default = null)
    {
        return (($value = $this->paramsArray()[$key]) !== null) ? $value : $default;
    }

    /**
     * Change the state a save the log
     *
     * @param                    $state
     * @param                    $note
     * @param \Mundo\Models\User $user
     */
    public function setState($state, $note, User $user)
    {
        $old = $this->attributes['state'];
        $this->attributes['state'] = $state;

        $this->histories()->create([
            'column'   => 'state',
            'previous' => $old,
            'next'     => $state,
            'user_id'  => $user->id,
            'note'     => $note,
        ]);

        return $this->save();
    }

    /**
     * Params as associative array
     * @return array
     */
    public function paramsArray()
    {
        return $this->params->lists('value', 'key')->toArray();
    }

    /**
     * @param $state
     */
    public function setStateAttribute($state)
    {
        if (!in_array($state, self::STATES)) {
            throw new \InvalidArgumentException('This is not valid state');
        }
        $this->attributes['state'] = $state;
    }

    /**
     * @return null
     */
    public function lastHistoryNote()
    {
        $history = $this->histories->filter(function (RecordHistory $history) {
            return $history->note !== null;
        })->first();

        if ($history !== null && $history->note) {
            return $history->note;
        }

        return null;
    }

    /**
     * @return null
     */
    public function lastServiceLog()
    {
        $service_log = $this->serviceLogs()->latest()->first();

        if (!is_null($service_log)) {
            return $service_log;
        }

        return null;
    }

    /**
     * @return bool
     */
    public function isNew()
    {
        $state = $this->state;
        $hist = $this->histories->filter(function ($history) {
            return in_array($history->column, ['state', 'user_id']);
        })->first();
        if ($hist) {
            $state = $hist->column === 'state' ? $hist->next : 'forwarded';
        }

        return $state === 'forwarded';
    }

    /**
     * Params as object
     * @return object
     */
    public function paramsObject()
    {
        return (object)$this->paramsArray();
    }

    /**
     * Is record owned by the user?
     *
     * @param \Mundo\Models\User $user
     *
     * @return bool
     */
    public function isOwnedBy(User $user)
    {
        return $this->user_id === $user->id;
    }

    /**
     * @return string
     */
    public function getRecordLabelAttribute()
    {
        return record_label($this)->toHtml();
    }
}

<?php

namespace Mundo\Models\TM;

use Illuminate\Database\Eloquent\Model;
use Mundo\Kernel\Eloquent\CastableParams;

class RecordParam extends Model
{
    use CastableParams;

    protected $table = 'tm_record_params';

    protected $fillable = ['key', 'value', 'type'];

    public function record()
    {
        return $this->belongsTo(Record::class, 'record_id', 'id');
    }

    public function getValueAttribute()
    {
        return $this->castParam(isset($this->attributes['value']) ? $this->attributes['value'] : null);
    }

    public function setValueAttribute($value)
    {
        $this->attributes['value'] = empty(trim($value)) ? null : $this->serializeParam($value);
    }
}

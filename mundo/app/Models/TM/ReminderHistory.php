<?php

namespace Mundo\Models\TM;

use Illuminate\Database\Eloquent\Model;
use Mundo\Kernel\Eloquent\UuidPrimaryKey;
use Mundo\Models\User;

class ReminderHistory extends Model
{
    use UuidPrimaryKey;

    protected $table = 'tm_reminder_histories';

    protected $fillable = ['type', 'remote_id', 'action', 'next_call', 'user_id', 'note', 'synced'];

    protected $casts = [
        'next_call' => 'datetime',
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }
}

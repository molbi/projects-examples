<?php

namespace Mundo\Providers;

use Blade;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Mundo\Kernel\Sync\RecordHistoryObserver;
use Mundo\Kernel\Sync\ReminderHistoryObserver;
use Mundo\Kernel\Validators\ServicesRule;
use Mundo\Models\EmailTemplate;
use Mundo\Models\Tag;
use Mundo\Models\TM\Campaign;
use Mundo\Models\TM\RecordHistory;
use Mundo\Models\TM\ReminderHistory;
use Mundo\Models\User;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        setlocale(LC_TIME, config('app.locale_full'));
        Carbon::setLocale(config('app.locale'));

        RecordHistory::observe($this->app->make(RecordHistoryObserver::class));
        ReminderHistory::observe($this->app->make(ReminderHistoryObserver::class));
        $this->extendBlade();

        Tag::addTaggable('users', User::class);
        Tag::addTaggable('emailTemplates', EmailTemplate::class);
        Tag::addTaggable('tmCampaigns', Campaign::class);

        $this->bootValidatorRules();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    public function extendBlade()
    {
        Blade::directive('datetime', function ($expression) {
            $expression = trim($expression, '()');

            return "<?php echo with(is_string($expression) ? Carbon\\Carbon::parse($expression) : $expression)->format('d. m. Y H:i'); ?>";
        });

        Blade::extend(function ($value) {
            return preg_replace('/\{\?(.+)\?\}/', '<?php ${1} ?>', $value);
        });
    }

    private function bootValidatorRules()
    {
        Validator::extend(ServicesRule::RULE, sprintf('%s@%s', ServicesRule::class, 'validate'));
    }
}

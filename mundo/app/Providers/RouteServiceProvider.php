<?php

namespace Mundo\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;
use Mundo\Models\EmailLog;
use Mundo\Models\EmailTemplate;
use Mundo\Models\RIS\RenewalFirm;
use Mundo\Models\RIS\RisServiceDefinition;
use Mundo\Models\RM\Record as RMRecord;
use Mundo\Models\TM\AdditionalPhoneNumber;
use Mundo\Models\TM\Campaign;
use Mundo\Models\TM\CampaignServiceDefinition;
use Mundo\Models\TM\Record;
use Mundo\Models\User;
use Mundo\Models\UserParamDefinition;
use Mundo\Models\UserWidgetDefinition;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'Mundo\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router $router
     *
     * @return void
     */
    public function boot(Router $router)
    {
        parent::boot($router);
        $router->model('user_param_definitions', UserParamDefinition::class);
        $router->model('users', User::class);
        $router->model('user_widget_definitions', UserWidgetDefinition::class);
        $router->model('campaign_service_definitions', CampaignServiceDefinition::class);
        $router->model('service_definitions', RisServiceDefinition::class);
        $router->model('additional_phone_numbers', AdditionalPhoneNumber::class);
        $router->model('campaigns', Campaign::class);
        $router->model('scampaigns', \Mundo\Models\Telemarketing\Campaign::class);
        $router->model('records', Record::class);
        $router->model('ris_records', RenewalFirm::class);
        $router->model('email_templates', EmailTemplate::class);
        $router->model('email_logs', EmailLog::class);
        $router->model('forwarding', User::class);
        $router->model('with_campaigns', Campaign::class);
        $router->model('without_campaigns', Campaign::class);
        $router->model('fill_campaigns', Campaign::class);
        $router->model('inhouse_leads', User::class);

        $router->model('rm_records', RMRecord::class);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router $router
     *
     * @return void
     */
    public function map(Router $router)
    {
        $this->mapWebRoutes($router);

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @param  \Illuminate\Routing\Router $router
     *
     * @return void
     */
    protected function mapWebRoutes(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}

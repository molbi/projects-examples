<?php

namespace Mundo\Providers;

use Illuminate\Support\ServiceProvider;
use libphonenumber\PhoneNumberUtil;

class PhoneNumberLibProvider extends ServiceProvider
{

    public function provides()
    {
        return [PhoneNumberUtil::class, 'phonenumberlib'];
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(PhoneNumberUtil::class, function () {
            return PhoneNumberUtil::getInstance();
        });
        $this->app->alias(PhoneNumberUtil::class, 'phonenumberlib');
    }
}

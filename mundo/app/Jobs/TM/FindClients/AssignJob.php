<?php

namespace Mundo\Jobs\TM\FindClients;

use Illuminate\Database\Connection;
use Illuminate\Database\Eloquent\Collection;
use Mundo\Jobs\Job;
use Mundo\Models\DbTM\Firm;
use Mundo\Models\Business\Firm as BusinessFirm;
use Mundo\Models\User;
use Mundo\Modules\TM\Sources\Support\SourceManager;

/**
 * Class AssignJob
 * @package Mundo\Jobs\TM\FindClients
 */
class AssignJob extends Job
{
    /**
     * @var User
     */
    private $operator;
    /**
     * @var array
     */
    private $attributes;
    /**
     * @var Connection
     */
    private $sources;
    /**
     * @var string
     */
    private $lang;

    private $settings = [
        'cs' => [
            'campaign_id' => 735,
            'inquiry_url' => 'http://poptavky.epoptavka.cz/poptavka',
            'filter_id' => 219
        ],
        'sk' => [
            'campaign_id' => 737,
            'inquiry_url' => 'http://dopyty.123dopyt.sk/dopyt',
            'filter_id' => 221
        ],
        'sk_edb_2017' => [
            'campaign_id' => 749,
            'inquiry_url' => 'http://dopyty.123dopyt.sk/dopyt',
            'filter_id' => 235
        ]
    ];
    /**
     * @var string
     */
    private $type;

    /**
     * ForwardRecordJob constructor.
     *
     * @param User $operator
     * @param array $attributes
     * @param string $lang
     * @param string $type
     */
    public function __construct(User $operator, array $attributes, $lang = 'cs', $type = 'cs')
    {
        $this->operator = $operator;
        $this->attributes = $attributes;
        $this->lang = $lang;
        $this->type = $type;
    }

    /**
     * @param SourceManager $sources
     */
    public function handle(SourceManager $sources)
    {
        $this->sources = $sources;
        foreach ($firms = $this->attributes['firms'] as $firm) {
            if (!$this->inStack($firm)) {
                $stack_id = $this->insertStack($firm);
                $this->insertNote($stack_id);
                $this->insertToTm($firm);
            }
        }

        $this->removeFromIndex($firms);
    }

    /**
     * @param $firm
     * @return string
     */
    private function insertStack($firm)
    {
        $this->db()->table('telemarketing.Stack')->insert([
            'targetSubject_id' => $firm,
            'campaign_id'      => $this->settings[$this->type]['campaign_id'],
            'employee_id'      => $this->operator->id,
            'state'            => 'pending'
        ]);

        return $this->db()->getPdo()->lastInsertId();
    }

    /**
     * @param $stack_id
     */
    private function insertNote($stack_id)
    {
        $inquiry = $this->attributes['inquiry'];
        $this->db()->table('telemarketing.History')->insert([
            'stack_id' => $stack_id,
            'action'   => 'pending',
            'note'     => sprintf('%s, <a target="_blank" href="%s/%s-%s">link</a>',
                $inquiry['title'],
                $this->settings[$this->type]['inquiry_url'],
                $inquiry['inquiry_id'],
                $inquiry['titleSanitized'])
        ]);
    }

    /**
     * @return Connection
     */
    protected function db()
    {
        return $this->sources->get('b2m')->getConnection();
    }

    private function removeFromIndex($firms)
    {
        /** @var Collection $firms */
        switch($this->type) {
            case 'cs':
                $firms = Firm::whereIn('firm_id', $firms)->get();
                break;
            case 'sk':
                $firms = BusinessFirm::whereIn('firm_id', $firms)->get();
                break;
            case 'sk_edb_2017':
                $firms = Firm::whereIn('firm_id', $firms)->get();
                break;
        }

        $firms->each(function ($firm) {
            $firm->unsearchable();
        });
    }

    private function inStack($firm)
    {
        return $this->db()->table('telemarketing.Stack')
            ->where('targetSubject_id', $firm)
            ->where('campaign_id', $this->settings[$this->type]['campaign_id'])
            ->whereRaw('DATE(createdAt) > DATE(NOW() - INTERVAL 31 DAY) ')
            ->first();
    }

    private function insertToTm($firm)
    {
        $this->db()->insert('INSERT IGNORE INTO tm.Filter_Params (filter_id, param_id) VALUES(:filter, :firm)', [
            'filter' => $this->settings[$this->type]['filter_id'],
            'firm' => $firm
        ]);
    }

}

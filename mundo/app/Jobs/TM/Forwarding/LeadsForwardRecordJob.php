<?php

namespace Mundo\Jobs\TM\Forwarding;

use Mundo\Jobs\Job;
use Mundo\Models\TM\Campaign;
use Mundo\Models\TM\Record;
use Mundo\Models\User;
use Mundo\Modules\TM\Sources\Eloquent\Collection;

class LeadsForwardRecordJob extends Job
{
    /**
     * @var array
     */
    private $records_ids;
    /**
     * @var int
     */
    private $user_id;

    /**
     * LeadsForwardRecordJob constructor.
     *
     * @param string $records_ids
     * @param int    $user_id
     *
     * @internal param array $attributes
     */
    public function __construct($records_ids, $user_id)
    {
        $this->records_ids = $records_ids;
        $this->user_id = $user_id;
    }

    public function handle()
    {
        if (is_null($this->records_ids) || is_null($this->user_id)) {
            return;
        }

        $this->records()->each(function (Record $record) {
            $record->update(['user_id' => $this->user_id]);
        });
    }

    private function recordsIds()
    {
        return explode(',', $this->records_ids);
    }

    private function records()
    {
        return Record::whereIn('id', $this->recordsIds())->get();
    }
}

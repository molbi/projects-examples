<?php

namespace Mundo\Jobs\TM\Forwarding;

use Mundo\Jobs\Job;
use Mundo\Models\TM\Campaign;
use Mundo\Models\TM\Record;
use Mundo\Models\User;
use Mundo\Modules\TM\Sources\Eloquent\Collection;

class LogicallForwardRecordJob extends Job
{
    /**
     * @var User
     */
    private $operator;
    /**
     * @var array
     */
    private $attributes;

    /**
     * ForwardRecordJob constructor.
     *
     * @param User $operator
     * @param array $attributes
     */
    public function __construct(User $operator, array $attributes)
    {
        $this->operator = $operator;
        $this->attributes = $attributes;
    }

    public function handle()
    {
        /** @var Campaign $campaign */
        $campaign = Campaign::where('id', $this->attributes['campaign'])->firstOrFail();

        /** @var Collection $records */
        $sql = sprintf('(SELECT IF(state = "accepted", NULL, user_id) AS user_id FROM mundo.tm_records WHERE campaign_id = %s AND user_id IS NOT NULL AND remote_id = tm_records.remote_id ORDER BY created_at DESC LIMIT 1) != %s', $this->attributes['campaign'], $this->operator->id);
        $records = $campaign->forwardableRecords()
            ->whereRaw($sql)
            ->limit($this->attributes['count'])
            ->get();

        $records->each(function (Record $record) {
            $record->update(['user_id' => $this->operator->id]);
        });
    }
}

<?php

namespace Mundo\Jobs\TM\Forwarding;

use Mundo\Jobs\Job;
use Mundo\Models\TM\Campaign;
use Mundo\Models\TM\Record;
use Mundo\Models\User;
use Mundo\Modules\TM\Sources\Eloquent\Collection;

class ForwardRecordJob extends Job
{
    /**
     * @var User
     */
    private $operator;
    /**
     * @var array
     */
    private $attributes;

    /**
     * ForwardRecordJob constructor.
     *
     * @param User  $operator
     * @param array $attributes
     */
    public function __construct(User $operator, array $attributes)
    {
        $this->operator = $operator;
        $this->attributes = $attributes;
    }

    public function handle()
    {
        /** @var Campaign $campaign */
        $campaign = Campaign::where('id', $this->attributes['campaign'])->firstOrFail();

        /** @var Collection $records */
        $records = $campaign->forwardableRecords()
            ->limit($this->attributes['count'])
            ->get();

        $records->each(function (Record $record) {
            $record->update(['user_id' => $this->operator->id]);
        });
    }
}

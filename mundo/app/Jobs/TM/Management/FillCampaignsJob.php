<?php

namespace Mundo\Jobs\TM\Management;

use Illuminate\Support\Collection;
use Mundo\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mundo\Models\TM\Campaign;
use Mundo\Models\TM\Record;
use Mundo\Models\TM\TargetGroup;
use Mundo\Modules\TM\BusinessService;

/**
 * Class FillCampaignsJob
 * @package Mundo\Jobs\TM\Management
 */
class FillCampaignsJob
{

    /**
     * @var Campaign
     */
    private $campaign;
    /**
     * @var int
     */
    private $count;

    /**
     * FillCampaignsJob constructor.
     *
     * @param Campaign $campaign
     * @param int      $count
     */
    public function __construct(Campaign $campaign, int $count)
    {
        $this->campaign = $campaign;
        $this->count = $count;
    }

    /**
     * @param BusinessService $businessService
     */
    public function handle(BusinessService $businessService)
    {
        $records = TargetGroup::getInstance($this->campaign)->get($this->count);
        $campaign_id = $this->campaign->sync_id ? $this->campaign->sync_id : $this->campaign->id;

        foreach ($records as $record) {
            $businessService->insertToStack($record->targetSubject_identificator, $campaign_id);
        }

    }
}

<?php

namespace Mundo\Jobs\TM\Management;

use Illuminate\Support\Collection;
use Mundo\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mundo\Models\TM\Record;

/**
 * Class MoveRecordsJob
 * @package Mundo\Jobs\TM\Management
 */
class MoveRecordsJob
{

    /**
     * @var
     */
    private $user;
    /**
     * @var array
     */
    private $records;

    /**
     * MoveRecordsJob constructor.
     *
     * @param       $user
     * @param array $records
     */
    public function __construct($user, array $records)
    {
        $this->user = $user;
        $this->records = $records;
    }

    /**
     *
     */
    public function handle()
    {
        if ($this->user > 0) {
            $this->getRecords()->each(function (Record $record) {
                $record->update(['user_id' => $this->user]);
            });
        }
    }

    /**
     * @return Collection
     */
    private function getRecords()
    {
        return Record::whereIn('id', $this->records)->get();
    }
}

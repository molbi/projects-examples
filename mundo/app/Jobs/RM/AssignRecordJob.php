<?php

namespace Mundo\Jobs\RM;

use Carbon\Carbon;
use Illuminate\Database\Connection;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;
use Mundo\Jobs\Job;
use Mundo\Models\Business\ItParam;
use Mundo\Models\RM\Record;
use Mundo\Models\User;
use Mundo\Modules\TM\Sources\Support\SourceManager;

/**
 * Class AssignRecordJob
 * @package Mundo\Jobs\RM
 */
class AssignRecordJob extends Job
{
    /**
     * @var User
     */
    private $operator;
    /**
     * @var SourceManager
     */
    private $source;
    /**
     * @var
     */
    private $attributes;

    /**
     * ForwardRecordJob constructor.
     *
     * @param User $operator
     * @param $attributes
     */
    public function __construct(User $operator, $attributes)
    {
        $this->operator = $operator;
        $this->attributes = $attributes;
    }

    /**
     * @param SourceManager $source
     */
    public function handle(SourceManager $source)
    {
        $this->source = $source;

        $records = $this->records();

        $records->each(function ($record) {
            Record::create([
                'created_at' => $record->createdOn,
                'firm_id'    => $record->firm_id,
                'user_id'    => $this->operator->id,
                'state'      => Record::STATE_NEW,
                'next_call'  => $record->createdOn
            ]);
        });
    }

    /**
     *
     * @return Connection
     */
    private function db()
    {
        return $this->source->get('b2m')->getConnection();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    private function records()
    {
        return collect($this->db()->table('obis.Order AS O')
            ->select(['O.firm_id', 'O.createdOn'])
            ->leftJoin('obis.OrderExactor AS OE', 'OE.order_id', '=', 'O.order_id')
            ->leftJoin('obis.CancellRequest AS CR', 'CR.subject_id', '=', 'O.order_id')
            ->leftJoin('mundo.rm_records AS R', function (JoinClause $j) {
                $j->on('R.firm_id', '=', 'O.firm_id')
                    ->on(DB::raw('DATE(R.created_at)'), '>=', DB::raw('DATE(NOW() - INTERVAL 1 MONTH)'));
            })->whereIn('O.state', ['processed', 'blocked'])
            ->whereBetween(DB::raw('DATE(O.createdOn)'), [
                $this->getDate('from')->format('Y-m-d'),
                $this->getDate('to')->format('Y-m-d')
            ])
            ->whereNull('OE.order_id')
            ->whereNull('CR.id')
            ->whereNull('R.id')
            ->whereNotIn('O.source', ['UniCall', 'ris-renewal', 'ris-manual', 'amax', 'amax_hodonin', 'AMAX-reobnova', 'tm_campaign_671', 'tm_campaign_673'])
            ->whereNotIn('O.source', ItParam::getByKey('logicall_sources'))
            ->orderBy('O.createdOn')
            ->groupBy('O.firm_id')
            ->get());
    }

    /**
     * @param $string
     * @return Carbon
     */
    private function getDate($string)
    {
        return Carbon::createFromTimestamp(strtotime($this->attributes[$string]));
    }
}

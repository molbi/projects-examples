<?php

namespace Mundo\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Mundo\Console\Commands\Stats\LeadsStats;
use Mundo\Console\Commands\Stats\LeadsStatsChanges;
use Mundo\Console\Commands\Stats\LeadsStatsHistory;
use Mundo\Console\Commands\Stats\LeadsStatsHistoryChanges;
use Mundo\Kernel\Cache\PaidOrders;
use Mundo\Kernel\Cache\PaymentsMorals;
use Mundo\Kernel\Cache\PopularInquiries;
use Mundo\Widgets\CallLimits;
use Mundo\Widgets\CarCompetition;
use Mundo\Widgets\KorejskeLeto;
use Mundo\Widgets\Logicall;
use Mundo\Widgets\Nafr;
use Mundo\Widgets\Targeting;
use Mundo\Widgets\TM\B2M\Services\Types\ActivesellingCache;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\ImportUsers::class,

        \Mundo\Kernel\Sync\Command::class,
        \Mundo\Kernel\Cron\Command::class,

        LeadsStats::class,
        LeadsStatsChanges::class,

        LeadsStatsHistory::class,
        LeadsStatsHistoryChanges::class,

        //        Commands\Index\Source\Init::class,
        //        Commands\Index\Source\All::class,
        //        Commands\Index\Source\Run::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('mundo:sync')->withoutOverlapping()->cron('*/3 * * * *');
        $schedule->command('mundo:search:import-all')->daily();
        $schedule->command('mundo:search:import-changes')->everyMinute();

        $schedule->command('stats:leads-stats:generate')->daily()->withoutOverlapping();
        #$schedule->command('stats:leads-stats-history:generate')->daily()->withoutOverlapping();

        $this->sheduleCronable($schedule, CallLimits\Cache::class, ['phonny'])->cron('*/3 * * * *');
        $this->sheduleCronable($schedule, Targeting\Cache::class, ['b2m'])->cron('*/3 * * * *');
        $this->sheduleCronable($schedule, CarCompetition\Cache::class, ['b2m'])->cron('*/3 * * * *');
        $this->sheduleCronable($schedule, KorejskeLeto\Cache::class, ['b2m'])->cron('*/3 * * * *');
        $this->sheduleCronable($schedule, PaidOrders::class, ['b2m'])->cron('*/3 * * * *');
        $this->sheduleCronable($schedule, PaymentsMorals::class, ['b2m'])->cron('*/3 * * * *');
        $this->sheduleCronable($schedule, Nafr\Cache::class, ['b2m'])->cron('*/3 * * * *');
        $this->sheduleCronable($schedule, ActivesellingCache::class, ['b2m'])->cron('0 6 * * * *')->withoutOverlapping();
        $this->sheduleCronable($schedule, PopularInquiries::class, ['b2m'])->cron('0 6 * * * *')->withoutOverlapping();

        $this->sheduleCronable($schedule, Logicall\Targeting\Cache::class, ['b2m'])->cron('*/3 * * * *');
    }

    protected function sheduleCronable(Schedule $schedule, $class, array $params = [])
    {
        return $schedule->command('mundo:run-scheduled', array_merge([addslashes($class)], $params));
    }
}

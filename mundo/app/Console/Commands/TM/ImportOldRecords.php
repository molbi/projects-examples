<?php

namespace Mundo\Console\Commands\TM;

use Illuminate\Console\Command;
use Illuminate\Database\DatabaseManager;
use Mundo\Kernel\Sync\Importer;
use Mundo\Models\TM\Campaign;

class ImportOldRecords extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tm:import-old-records';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';


    /** @var  DatabaseManager */
    private $manager;

    /**
     * Campaign constructor.
     *
     * @param \Illuminate\Database\DatabaseManager $manager
     *
     * @internal param \Illuminate\Filesystem\FilesystemManager $fs
     */
    public function __construct(DatabaseManager $manager)
    {
        $this->manager = $manager;
        parent::__construct();
    }

    /**
     * Execute the console command.
     * @return mixed
     */
    public function handle()
    {
        $stacks = collect($this->b2mDb()->table('telemarketing.Stack AS S')
            ->join('cs_contact.users AS U', 'U.id', '=', 'S.targetSubject_id')
            ->whereIn('S.state', ['pending','forwarded'])
            ->whereDate('createdAt', '<', '2016-11-20')
            ->where('S.employee_id', 1559)
            ->get());


        foreach($stacks->groupBy('campaign_id') as $campaign_id => $groups) {
            $campaign = Campaign::where('sync_id', $campaign_id)->first();

            $worker = new Importer($this->manager, $campaign);
            $worker->work(function() {}, $groups->pluck('stack_id')->toArray());
        }
    }

    /**
     * Connection to destination DB
     * @return \Illuminate\Database\Connection
     */
    protected function db()
    {
        return $this->manager->connection();
    }

    /**
     * Connection to source DB
     *
     * @return \Illuminate\Database\Connection
     */
    protected function b2mDb()
    {
        return $this->manager->connection('b2m');
    }
}

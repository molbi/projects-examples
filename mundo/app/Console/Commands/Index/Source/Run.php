<?php

namespace Mundo\Console\Commands\Index\Source;

use Cviebrock\LaravelElasticsearch\Manager;
use Elasticsearch\Client;
use Illuminate\Console\Command;
use Mundo\Kernel\Source\Contract\Indexer;
use Mundo\Kernel\Source\IndexManager;

class Run extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'index:source:run
                            {index             : Index of this process}
                            {count             : Number of parallel processes}
                            {bulk              : Size of bulk}
                            {indexName=sources : Name of the ES index}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Indexuje dokumenty (worker)';

    /** @var IndexManager */
    private $indexManager;
    /** @var Client */
    private $es;

    public function __construct(IndexManager $indexManager, Manager $esManager)
    {
        $this->indexManager = $indexManager;
        $this->es = $esManager->connection();
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bulk = (int)$this->argument('bulk');
        $count = (int)$this->argument('count');
        /** @var Indexer $indexer */
        foreach ($this->indexManager->items() as $indexer) {
            $indexer->prepare();
            $currentIndex = (int)$this->argument('index');
            do {
                $chunk = $indexer->chunk($bulk, $currentIndex);
                $this->output->writeln(count($chunk));
                if (count($chunk) == 0) {
                    break;
                }
                $this->es->bulk($this->getIndexParams($indexer, $chunk));
                $currentIndex += $count;
            } while (true);
        }
    }

    private function getIndexParams(Indexer $indexer, array $chunk)
    {
        $params = ['body' => []];
        foreach ($chunk as $data) {
            $params['body'][] = [
                'index' => [
                    '_index' => $this->argument('indexName'),
                    '_type'  => $indexer->name(),
                    '_id'    => $data->_id,
                ],
            ];
            $params['body'][] = $indexer->properties($data);
        }

        return $params;
    }
}

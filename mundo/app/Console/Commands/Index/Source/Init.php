<?php

namespace Mundo\Console\Commands\Index\Source;

use Cviebrock\LaravelElasticsearch\Manager as ESManager;
use Elasticsearch\Client;
use Illuminate\Console\Command;
use Mundo\Kernel\Source\Contract\Indexer;
use Mundo\Kernel\Source\Impl;
use Mundo\Kernel\Source\IndexManager;

class Init extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'index:source:init
                            {name=sources : Name of the ES index}
                            {shards=5     : Number of shard}
                            {replicas=0   : Number of replicas}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Inicializuje source index';

    /** @var IndexManager */
    private $indexManager;
    /** @var  Client */
    private $es;

    /**
     * Index constructor.
     *
     * @param \Mundo\Kernel\Source\IndexManager $indexManager
     * @param \Cviebrock\LaravelElasticsearch\Manager $esManager
     *
     */
    public function __construct(IndexManager $indexManager, ESManager $esManager)
    {
        $this->indexManager = $indexManager;
        $this->es = $esManager->connection();
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->output->title('Initializing index');
        $this->initIndex();
        $this->output->success('Initialized');
    }

    /**
     * Vytvoří index
     */
    private function initIndex()
    {
        $params = $this->getIndexParams();
        if ($this->es->indices()->exists($index = array_only($params, ['index']))) {
            $this->es->indices()->delete($index);
        }

        /** @var Indexer $indexer */
        foreach ($this->indexManager->items() as $indexer) {
            $params['body']['mappings'][$indexer->name()] = [
                'properties' => $indexer->mapping(),
            ];
        }

        $this->es->indices()->create($params);
    }

    /**
     * @return array
     */
    private function getIndexParams()
    {
        return [
            'index' => $this->argument('name'),
            'body'  => [
                'mappings' => [],
                'settings' => [
                    'number_of_shards'   => (int)$this->argument('shards'),
                    'number_of_replicas' => (int)$this->argument('replicas'),
                ],
            ],
        ];
    }
}

<?php

namespace Mundo\Console\Commands\Index\Source;

use Cviebrock\LaravelElasticsearch\Manager;
use Elasticsearch\Client;
use Elasticsearch\Common\Exceptions\Missing404Exception;
use Illuminate\Console\Command;
use Mundo\Kernel\Source\Impl;
use Mundo\Kernel\Source\IndexManager;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Process\ProcessBuilder;

class All extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'index:source:all
                            {count             : Number of parallel processes}
                            {bulk              : Size of bulk}                            
                            {indexName=sources : Name of the ES index}
                            {shards=5          : Number of shard}
                            {replicas=0        : Number of replicas}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Indexuje dokumenty';
    /** @var string */
    private $indexName;
    /** @var IndexManager */
    private $indexManager;
    /** @var Client */
    private $es;

    public function __construct(IndexManager $indexManager, Manager $esManager)
    {
        $this->indexManager = $indexManager;
        $this->es = $esManager->connection();
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->initIndex();
        $this->output->title('Indexing all sources');
        $processes = [];
        for ($i = 0; $i < $this->argument('count'); $i++) {
            $processes[] = $p = $this->getProcess($i);
        }
        $bar = $this->output->createProgressBar($this->totalCount());
        $bar->setFormat(' %current%/%max% [%bar%] %percent:3s%% Time: %elapsed:6s%/%remaining:6s% Memory: %memory:6s%');
        $this->startProcesses($processes);
        while ($this->areRunning($processes)) {
            $this->reportProgress($bar, $processes);
        }
        $exitCode = $this->gatherExitCodes($processes);
        if ($exitCode !== 0) {
            $this->output->error('Something goes wrong!');
        } else {
            $this->finishIndex();
            $bar->finish();
            $this->output->newLine(2);
            $this->output->success('Indexed.');
        }
    }

    /**
     * Gather the cumulative exit code for the processes.
     *
     * @param  array $processes
     *
     * @return int
     */
    private function gatherExitCodes(array $processes)
    {
        $code = 0;
        foreach ($processes as $process) {
            $code = $code + $process->getExitCode();
        }

        return $code;
    }

    private function reportProgress(ProgressBar $bar, array $processes)
    {
        foreach ($processes as $process) {
            $output = $process->getIncrementalOutput();
            if (!empty($output)) {
                $bar->advance((int)$output);
            }
        }
    }

    private function startProcesses($processes)
    {
        foreach ($processes as $process) {
            $process->start();
        }
    }

    private function areRunning($processes)
    {
        foreach ($processes as $process) {
            if ($process->isRunning()) {
                return true;
            }
        }

        return false;
    }

    private function getProcess($index)
    {
        return ProcessBuilder::create([
            PHP_BINARY,
            base_path('artisan'),
            preg_replace('/[^:]+$/', 'run', $this->getName()),
            $index,
            $this->argument('count'),
            $this->argument('bulk'),
            $this->indexName(),
        ])->getProcess();
    }

    private function totalCount()
    {
        $totalCount = 0;
        foreach ($this->indexManager->items() as $indexer) {
            $totalCount += $indexer->count();
        }

        return $totalCount;
    }

    private function initIndex()
    {
        $this->call('index:source:init', [
            'name'     => $this->indexName(),
            'replicas' => $this->argument('replicas'),
            'shards'   => $this->argument('shards'),
        ]);
    }

    private function indexName()
    {
        if (!isset($this->indexName)) {
            $this->indexName = implode('_', [$this->argument('indexName'), md5(random_int(0, 1000) . time())]);
        }

        return $this->indexName;
    }

    private function finishIndex()
    {
        // old index name
        try {
            $alias = $this->es->indices()->getAlias(['name' => $this->argument('indexName')]);
            foreach (array_keys($alias) as $index) {
                $this->es->indices()->delete(compact('index'));
            }
        } catch (Missing404Exception $ex) {
        }
        // Create alias
        $this->es->indices()->updateAliases([
            'body' => [
                'actions' => [
                    [
                        'add' => [
                            'index' => $this->indexName(),
                            'alias' => $this->argument('indexName'),
                        ],
                    ],
                ],
            ],
        ]);
    }
}

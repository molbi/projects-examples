<?php

namespace Mundo\Console\Commands\Stats;

use Illuminate\Cache\CacheManager;
use Illuminate\Console\Command;
use Illuminate\Database\Connection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Mundo\Models\Telemarketing\History;
use Mundo\Models\Telemarketing\Stack;
use Symfony\Component\Console\Helper\ProgressBar;


/**
 * Class LeadsStatsHistoryChanges
 * @package Mundo\Console\Commands\Stats
 */
class LeadsStatsHistoryChanges extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stats:leads-stats-history:changes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Hlídání změn a přírůstků na akcích operátora';
    /**
     * @var Connection
     */
    private $db;
    /**
     * @var array
     */
    private $bulk = [];
    /**
     * @var integer
     */
    private $total;
    /**
     * @var int
     */
    private $take = 2000;
    /**
     * @var int
     */
    private $skip = 0;
    /**
     * @var int
     */
    private $actual = 1;
    /**
     * @var ProgressBar
     */
    private $bar;
    /**
     * @var CacheManager
     */
    private $cache;

    /**
     * @var
     */
    private $dates;

    /**
     * Create a new command instance.
     * @param Connection $db
     * @param CacheManager $cache
     */
    public function __construct(Connection $db, CacheManager $cache)
    {
        parent::__construct();
        $this->db = $db;
        $this->cache = $cache;
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        Log::info('LeadStats - changes');

        $this->output->title('Build lead stats statistics for specific dates (ft_lead_stats_actions)');

        $this->setDatesForRebuild();
        $this->truncateSpecificDates();
        $this->setTotalCount();
        $this->storeLastHistoryId();

        while ($this->actual <= ceil($this->total / $this->take)) {
            $this->processData();
            $this->storeBulk();
            $this->actual++;
        }
    }

    /**
     * @param $day
     * @param $campaign
     * @param $user
     * @param $state
     * @param $count
     */
    private function addToBulk($day, $campaign, $user, $state, $count)
    {
        array_push($this->bulk, [
            'date_id'     => $day,
            'campaign_id' => empty($campaign) ? 0 : $campaign,
            'user_id'     => empty($user) ? 0 : $user,
            'state'       => $state,
            'amount'      => $count
        ]);
    }

    /**
     *
     */
    private function storeBulk()
    {
        $this->db->table('mundo.ft_lead_stats_actions')->insert($this->bulk);
        $this->bar->advance($this->take);
        $this->skip = $this->actual * $this->take;
        $this->bulk = [];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    private function getData()
    {
        return collect(History::with('stack')
            ->whereIn(DB::raw('DATE(createdAt)'), $this->dates->toArray())
            ->orderBy('history_id')
            ->take($this->take)
            ->skip($this->skip)
            ->get());
    }

    /**
     *
     */
    private function setTotalCount()
    {
        $this->info('Setting total count');
        $this->total = History::whereIn(DB::raw('DATE(createdAt)'), $this->dates->toArray())->count();
        $this->bar = $this->output->createProgressBar($this->total);
    }

    /**
     *
     */
    private function processData()
    {
        foreach ($this->getDays() as $day => $items) {
            foreach ($items->groupBy('campaign_id') as $campaign => $items2) {
                foreach ($items2->groupBy('employee_id') as $user => $items3) {
                    foreach ($items3->groupBy('actions') as $state => $items4) {
                        $this->addToBulk($day, $campaign, $user, $state, $items4->count());
                    }
                }
            }
        }
    }

    /**
     *
     */
    private function getDays()
    {
        return $this->getData()->groupBy(function ($item, $key) {
            return $item->createdAt->format('Ymd');
        });
    }

    /**
     * @return mixed
     */
    private function setDatesForRebuild()
    {
        $this->info('Setting dates for rebuild');

        $history_id = $this->cache->driver('file')->get('ft_lead_stats_actions_history_id');

        $this->dates = collect(DB::table('telemarketing.History AS H')
            ->select([
                DB::raw('DATE(H.createdAt) AS date')
            ])
            ->where('H.history_id', '>', $history_id)
            ->groupBy(DB::raw('DATE(H.createdAt)'))
            ->get())
            ->pluck('date');
    }

    /**
     *
     */
    private function storeLastHistoryId()
    {
        $this->info('Storing last history ID');

        $id = History::latest('createdAt')->first()->history_id;

        $this->cache->driver('file')->forever('ft_lead_stats_history_id', $id);
    }

    /**
     *
     */
    private function truncateSpecificDates()
    {
        $this->info('Truncating specific dates from table');
        $date_ids = $this->dates->map(function ($item) {
            return str_replace('-', '', $item);
        });
        $this->db->table('mundo.ft_lead_stats_actions')->whereIn('date_id', $date_ids)->delete();
    }

}

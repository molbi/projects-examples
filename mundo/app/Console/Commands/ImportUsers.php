<?php

namespace Mundo\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\DatabaseManager;
use Illuminate\Foundation\Inspiring;
use Mundo\Models\User;

class ImportUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mundo:import-users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import all users';

    /** @var  DatabaseManager */
    protected $manager;

    public function __construct(DatabaseManager $manager)
    {
        $this->manager = $manager;
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $db = $this->manager->connection('b2m');

        $users = $db->query()
            ->select([
                'login',
                'realname as name',
                'employee_id as id',
                'email',
                'phone'
            ])
            ->from('zis.Employee')
            ->get();

        foreach ($users as $user) {
            $u = User::withoutGlobalScopes()->find($user->id);
            if ($u) {
                $u->fill((array)$user);
                $u->save();
            } else {
                $u = new User((array)$user);
                $u->password = bcrypt('heslo');
                $u->widgets_definition = [];
                $u->widgets_id = null;
                $u->save();
            }
        }
    }
}

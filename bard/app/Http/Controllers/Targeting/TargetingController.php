<?php

namespace App\Http\Controllers\Targeting;

use App\Console\Commands\Contracts\BuildIndex;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\CsContact\Inquiry as CsInquiry;
use App\Models\FailedInquiry;
use App\Models\Firm;
use App\Models\Inquiry;
use App\Models\RegFirm;
use App\Models\Region;
use App\Models\Targeting\Targeting;
use Elasticquent\ElasticquentCollection;
use Elasticquent\ElasticquentResultCollection;
use Illuminate\Http\Request;

/**
 * Class TargetingController
 * @package App\Http\Controllers
 */
class TargetingController extends Controller
{
    /**
     * @param $lang
     * @param $contract_type
     * @param null $contract
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function index($lang, $contract_type, $contract = null)
    {
        /** @var Inquiry|CsInquiry $model */
        $model = BuildIndex::TYPES[$contract_type];

        if (is_null($contract)) {

            if(is_null($inquiry = $model::loadInquiry($lang))) {
                return response()->view('targeting.index', compact('inquiry'));
            }

            return redirect()->route('targeting', [
                'lang'     => $lang,
                'type'     => $contract_type,
                'contract' => $model::loadInquiry($lang)
            ]);
        }

        $inquiry = $model::loadForTargeting($contract);

        return response()->view('targeting.index', compact('inquiry'));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function resources()
    {
        $regions = Region::whereIn('lang', ['cs', 'sk'])->whereIn('parentNode', [11, 26])->with('langRecord')->orderBy('lang')->get();
        $categories = Category::where('type', 'firm')->with('langRecord')->get();

        return response()->json([
            'regions'    => $regions->map(function ($reg) {
                return ['value' => $reg->region_id, 'label' => $reg->langRecord->title];
            })->toArray(),
            'categories' => $categories->map(function ($cat) {
                return ['value' => $cat->category_id, 'label' => $cat->langRecord->title];
            })->toArray()
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {
        $filters = $request->only(['q', 'categories', 'regions', 'inquiry', 'lang']);
        $categories = collect($filters['categories'])->pluck('value');
        $regions = collect($filters['regions'])->pluck('value');
        $inquiry = $filters['inquiry'];


        $esFilters = [];
        if (!$categories->isEmpty()) {
            $esFilters[] = ['terms' => ['categories' => $categories->toArray()]];
        }

        if (!$regions->isEmpty()) {
            $esFilters[] = ['terms' => ['regions' => $regions->toArray()]];
        }

        $esFilters[] = ['term' => ['lang' => $filters['lang']]];
        $esFilters[] = ['range' => ['last_inquiry' => ['lt' => 'now-3d/d']]];

        $query = [
            'bool' => [
                'must'   => is_null($filters['q']) ? ['match_all' => (object)[]] : [
                    'multi_match' => [
                        'query'  => $filters['q'],
                        "type"   => "best_fields",
                        'fields' => ['description_lemma', 'description_2_lemma' ,'title']
                    ]
                ],
                'filter' => [
                    'bool' => [
                        'must'     => $esFilters,
                        'must_not' => [
                            'terms' => ['inquiries' => [$inquiry]]
                        ]
                    ]
                ]
            ]
        ];

        $firms = Firm::searchByQuery($query, null, null, 50, null, ['_score' => 'desc', 'inquiries_count' => 'asc']);
//        $regFirms = RegFirm::searchByQuery($query, null, null, 50, null, ['_score' => 'desc', 'inquiries_count' => 'asc']);

//        dd($regFirms);

//        /** @var ElasticquentResultCollection $collection */
//        $collection = $firms->merge($regFirms);

//        dd($collection);

        if ($firms->isEmpty()) {
            return response()->json([]);
        }

        return response()->json([
            'total' => $firms->totalHits(),
            'items' => $firms->fresh('region.langRecord', 'cats.langRecord', 'addition_description')
        ]);
    }

    /**
     * @param Request $request
     * @param $type
     */
    public function store(Request $request, $type)
    {
        /** @var Inquiry|CsInquiry $model */
        $model = BuildIndex::TYPES[$type];

        $ids = Targeting::removeAlreadyTargetedToday($request->get('suppliers', []));

        foreach ($ids as $supplier) {
            if (!Targeting::store($request->get('inquiry'), $model::CONTRACT_TYPE, $supplier, 'business_firm', $request->user()->employee_id)) {
                unset($ids[$supplier]);
            }
        }

        $firms = Firm::whereIn('firm_id', $ids)
            ->with(['inquiries', 'region', 'cats', 'addition_description'])
            ->withCount('inquiries')
            ->get();

        $firms->each(function (Firm $firm) {
            $firm->updateIndex();
        });

    }

    /**
     * @param Request $request
     */
    public function notForTargeting(Request $request)
    {
        /** @var Inquiry|CsInquiry $model */
        $model = BuildIndex::TYPES[$request->get('type')];
        $inquiry = $model::loadForTargeting($request->get('inquiry'));

        if (!is_null($inquiry)) {
            FailedInquiry::create([
                'subject_id'   => $inquiry->getKey(),
                'subject_type' => $inquiry->getMorphClass(),
                'employee_id'  => $request->user()->employee_id
            ]);
        }
    }

    /**
     * @param $lang
     * @param $type
     * @return array
     */
    public function numbers($lang, $type)
    {
        /** @var Inquiry|CsInquiry $model */
        $model = BuildIndex::TYPES[$type];

        return [
            'inquiries' => $model::countForTargeting($lang),
            'targets'   => $model::countForToday($lang, $type)
        ];
    }
}
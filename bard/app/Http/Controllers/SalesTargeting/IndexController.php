<?php

namespace App\Http\Controllers\SalesTargeting;

use App\Console\Commands\Contracts\BuildIndex;
use App\Http\Controllers\Controller;
use App\Models\SalesTargeting\Record;
use App\Models\SalesTargeting\RecordTarget;
use Carbon\Carbon;
use Elasticquent\ElasticquentTrait;
use Illuminate\Http\Request;
use Prophecy\Exception\Doubler\MethodNotFoundException;

/**
 * Class IndexController
 * @package App\Http\Controllers\SalesTargeting
 */
class IndexController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->view('sales-targeting.index');
    }

    /**
     * @param Request $request
     * @param $type
     * @return \Illuminate\Http\JsonResponse
     */
    public function load(Request $request, $type)
    {
        if (!method_exists($this, $method = sprintf('load%s', studly_case($type)))) {
            throw new MethodNotFoundException("Method {$method} for ajax call is not allowed or missing", self::class, $method);
        }

        $data = call_user_func([$this, $method], $request);

        return response()->json($data);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function loadRecords(Request $request)
    {
        return [
            'assigned' => Record::forUser(auth()->user())->forList()->with('supplier.cats.langRecord', 'supplier.region')->get(),
            'toAssign' => Record::forAssign()->count()
        ];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function loadAssignRecords(Request $request)
    {
        Record::assignToUser(auth()->user());

        return $this->loadRecords($request);
    }

    /**
     * @param Request $request
     * @return array|bool
     */
    public function loadStore(Request $request)
    {
        /** @var Record $record */
        $record = Record::where('id', $request->get('record'))->first();
        if (is_null($record)) {
            return false;
        }

        foreach ($request->get('items') as $contract) {
            try {
                $record->contracts()->create([
                    'contract_id'   => $contract,
                    'contract_type' => $request->get('type'),
                    'for_state'     => $record->state
                ]);
            } catch (\Exception $e) {
            }
        }

        $record->contracts->each(function ($contract) {
            $contract->contract->updateIndex();
        });

        return [
            'targeted' => $record->contracts->count()
        ];
    }

    /**
     * @param Request $request
     * @return array
     */
    public function loadFinish(Request $request)
    {
        $record = Record::where('id', $request->get('record'))->first();
        $record->setAttribute('state', $request->get('state'));

        if(Record::STATE_TARGETED_2 === $request->get('state') && $record->isExpired()) {
            $record->setAttribute('valid_to', Carbon::now()->addDays(3));
            $record->setAttribute('extended', 1);
        }

        $record->save();

        return [];
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function loadSearch(Request $request)
    {
        $filters = $request->only(['q', 'categories', 'regions', 'record', 'lang']);
        $categories = collect($filters['categories'])->pluck('value');
        $regions = collect($filters['regions'])->pluck('value');
        $model = BuildIndex::TYPES[$request->get('type')];

        $esFilters = [];
        if (!$categories->isEmpty()) {
            $esFilters[] = ['terms' => ['cats' => $categories->toArray()]];
        }

        if (!$regions->isEmpty()) {
            $esFilters[] = ['terms' => ['regs' => $regions->toArray()]];
        }

        $esFilters[] = ['term' => ['lang' => $filters['lang']]];

        $query = [
            'bool' => [
                'must'   => is_null($filters['q']) ? ['match_all' => (object)[]] : [
                    'multi_match' => [
                        'query'  => $filters['q'],
                        "type"   => "best_fields",
                        'fields' => ['description_lemma', 'title']
                    ]
                ],
                'filter' => [
                    'bool' => [
                        'must'     => $esFilters,
                        'must_not' => [
                            'terms' => ['records' => [$filters['record']]]
                        ]
                    ]
                ]
            ]
        ];

//        $contracts = $model::searchByQuery($query, null, null, 50, null, ['publishedOn' => 'desc', '_score' => 'desc']);
        $contracts = $model::searchByQuery($query, null, null, 50, null, ['_score' => 'desc']);
        if ($contracts->isEmpty()) {
            return [
                'total' => 0,
                'items' => []
            ];
        }

        return [
            'total' => $contracts->totalHits(),
            'items' => $contracts->fresh('langRecord')
        ];
    }

}

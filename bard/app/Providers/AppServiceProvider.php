<?php

namespace App\Providers;

use App\Models\CsContact\Inquiry as CsInquiry;
use App\Models\Firm;
use App\Models\Inquiry;
use App\Models\Pvis\Inquiry as PsInquiry;
use App\Models\RegFirm;
use App\Models\SalesTargeting\Record;
use App\Observers\RecordObserver;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Record::observe(RecordObserver::class);

        Relation::morphMap([
            'inquiry'       => Inquiry::class,
            'pvs'           => PsInquiry::class,
            'cs_inquiry'    => CsInquiry::class,
            'business_firm' => Firm::class,
            'reg_firm'      => RegFirm::class,
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Firm
 * @property Collection inquiries
 * @package App\Models
 */
class CategoryLang extends Model
{
    /**
     * @var string
     */
    protected $table = 'business.CategoryLang';

    /**
     * @var string
     */
    protected $primaryKey = 'category_id';
}
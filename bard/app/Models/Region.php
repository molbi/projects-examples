<?php

namespace App\Models;

use Elasticquent\ElasticquentTrait;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

/**
 * Class Firm
 * @property Collection inquiries
 * @property mixed region_id
 * @package App\Models
 */
class Region extends Model
{
    /**
     * @var string
     */
    protected $table = 'business.Region';

    /**
     * @var string
     */
    protected $primaryKey = 'region_id';


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function langRecord()
    {
        return $this->hasOne(RegionLang::class, 'region_id', 'region_id')->where('lang', 'cs');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Region::class, 'parentNode', 'region_id');
    }

    /**
     * @param null $region
     * @return array
     */
    public function getParents($region = null)
    {
        if(is_null($this->parent)) {
            return [];
        }

        $ids = [];
        $parent = is_null($region) ? $this->parent : $region->parent;
        $ids[] = $parent->region_id;

        if (!is_null($parent->parentNode)) {
            $ids = array_merge_recursive($ids, $this->getParents($parent));
        }

        return $ids;
    }
}

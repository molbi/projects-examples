<?php

namespace App\Models;

use Elasticquent\ElasticquentTrait;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

/**
 * Class Firm
 * @property Collection inquiries
 * @property mixed region_id
 * @package App\Models
 */
class RegionLang extends Model
{
    /**
     * @var string
     */
    protected $table = 'business.RegionLang';

    /**
     * @var string
     */
    protected $primaryKey = 'region_id';
}

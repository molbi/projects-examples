<?php

namespace App\Models\Targeting;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


/**
 * Class Targeting
 * @package App\Models\Targeting
 */
class Targeting extends Model
{
    /**
     *
     */
    const UPDATED_AT = null;

    /**
     * @var string
     */
    protected $table = 'bard.targeting';

    /**
     * @var array
     */
    protected $fillable = [
        'supplier_id',
        'supplier_type',
        'contract_id',
        'contract_type',
        'employee_id'
    ];

    /**
     * @param $contract_id
     * @param $contract_type
     * @param $supplier_id
     * @param $supplier_type
     * @param $employee_id
     *
     * @return bool
     */
    public static function store($contract_id, $contract_type, $supplier_id, $supplier_type, $employee_id)
    {
        $exists = static::where('supplier_id', $supplier_id)
            ->where('supplier_type', $supplier_type)
            ->where('contract_id', $contract_id)
            ->where('contract_type', $contract_type)
            ->first();

        if (is_null($exists)) {
            static::create([
                'supplier_id'   => $supplier_id,
                'supplier_type' => $supplier_type,
                'contract_id'   => $contract_id,
                'contract_type' => $contract_type,
                'employee_id'   => $employee_id
            ]);

            return true;
        }

        return false;
    }

    /**
     * @param array $ids
     * @return array
     */
    public static function removeAlreadyTargetedToday(array $ids)
    {
        $alreadyTargeted = (new static)->whereIn('supplier_id', $ids)
            ->where('supplier_type', 'business_firm')
            ->whereRaw('DATE(created_at) = DATE(NOW())')
            ->get();

        if ($alreadyTargeted->isEmpty()) {
            return $ids;
        }

        return array_diff($ids, $alreadyTargeted->pluck('supplier_id')->toArray());
    }
}

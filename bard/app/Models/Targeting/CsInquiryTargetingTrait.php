<?php

namespace App\Models\Targeting;

use App\Models\CsContact\Region;
use App\Models\Semaphor;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

/**
 * Trait CsInquiryTargetingTrait
 * @package App\Models\Targeting
 */
trait CsInquiryTargetingTrait
{
    use TargetingTrait;

    /**
     * @param $id
     * @return mixed
     */
    public static function loadForTargeting($id)
    {
        $csinquiry = (new static)->where('id', $id)->first();

        $csinquiry->load('operators', 'categories', 'regions.b2m_region.langRecord');

        return $csinquiry;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function firms()
    {
        return $this->hasMany(Targeting::class, 'contract_id', 'inquiry_id')
            ->where('contract_type', self::CONTRACT_TYPE);
    }

    /**
     *
     */
    public function getFirmsCountAttribute()
    {
        return $this->firms->count();
    }


    /**
     * @return mixed
     */
    public function getTargetingBody()
    {
        return $this->body;
    }

    /**
     * @return mixed
     */
    public function getTargetingOperator()
    {
        return $this->operators->first();
    }

    /**
     * @return mixed
     */
    public function getTargetingLang()
    {
        return $this->locale == 'cs_CZ' ? 'cs' : 'sk';
    }

    /**
     * @return array
     */
    public function getTargetingCategories()
    {
        return $this->categories;
    }

    /**
     * @return array
     */
    public function getTargetingRegions()
    {
        return $this->regions->map(function (Region $region) {
            return $region->b2m_region;
        });
    }

    /**
     * @return mixed
     */
    public function getTargetingPublishedOn()
    {
        return $this->sent_at->toDateTimeString();
    }

    /**
     * @return mixed
     */
    public function getTargetingFirmsCount()
    {
        return $this->firms_count;
    }

    /**
     * @return mixed
     */
    public function getTargetingType()
    {
        return $this->getMorphClass();
    }

    /**
     * @param $lang
     * @return mixed
     */
    public static function loadInquiry($lang)
    {
        //nemá náhodou rozpracovanou poptávku??
        $inquiry = ContractEmployee::select(['contract_employees.*'])
            ->leftJoin('bard.targeting AS T', function (JoinClause $j) {
                $j->on('T.contract_id', '=', 'contract_employees.contract_id')
                    ->where('T.contract_type', '=', static::CONTRACT_TYPE);
            })
            ->leftJoin('bard.failed_inquiries AS FI', function (JoinClause $j) {
                $j->on('FI.subject_id', '=', 'contract_employees.contract_id')
                    ->where('FI.subject_type', '=', static::CONTRACT_TYPE);
            })
            ->join('cs_contact.inquiries AS I', 'I.id', '=', 'contract_employees.contract_id')
            ->where('I.locale', $lang == 'cs' ? 'cs_CZ' : 'sk_SK')
            ->whereNull('T.id')
            ->whereNull('FI.id')
            ->where('contract_employees.contract_type', static::CONTRACT_TYPE)
            ->where('contract_employees.employee_id', auth()->user()->employee_id)
            ->first();

        if (!is_null($inquiry)) {
            return (new static)->where('id', $inquiry->contract_id)->first();
        }

        //Semaphor init
        $semaphore = new Semaphor();
        $semaphore->setSemaphor(2, 262);
        $semaphore->accessTransactionLoop();

        $inquiry = (new static)
            ->select(['inquiries.*'])
            ->leftJoin('bard.targeting AS T', function (JoinClause $j) {
                $j->on('T.contract_id', '=', 'inquiries.id')
                    ->where('T.contract_type', '=', static::CONTRACT_TYPE);
            })
            ->leftJoin('bard.contract_employees AS CE', function (JoinClause $j) {
                $j->on('CE.contract_id', '=', 'inquiries.id')
                    ->where('CE.contract_type', '=', static::CONTRACT_TYPE);
            })
            ->leftJoin('bard.failed_inquiries AS FI', function (JoinClause $j) {
                $j->on('FI.subject_id', '=', 'inquiries.id')
                    ->where('FI.subject_type', '=', static::CONTRACT_TYPE);
            })
            ->whereNull('CE.id')
            ->whereRaw('DATE(inquiries.sent_at) >= DATE(NOW() - INTERVAL 3 DAY)')
            ->where('inquiries.locale', '=', $lang == 'cs' ? 'cs_CZ' : 'sk_SK')
            ->whereNull('FI.id')
            ->whereNull('T.id')
            ->orderBy('inquiries.sent_at')
            ->first();

        if (is_null($inquiry)) {
            $semaphore->quitTransaction();
            return null;
        }

        ContractEmployee::storeForUser($inquiry, auth()->user());

        $semaphore->quitTransaction();

        return $inquiry;
    }

    /**
     * @param $lang
     * @return mixed
     */
    public static function countForTargeting($lang)
    {
        return (new static)
            ->select(['inquiries.*'])
            ->leftJoin('bard.targeting AS T', function (JoinClause $j) {
                $j->on('T.contract_id', '=', 'inquiries.id')
                    ->where('T.contract_type', '=', static::CONTRACT_TYPE);
            })
            ->leftJoin('bard.contract_employees AS CE', function (JoinClause $j) {
                $j->on('CE.contract_id', '=', 'inquiries.id')
                    ->where('CE.contract_type', '=', static::CONTRACT_TYPE);
            })
            ->leftJoin('bard.failed_inquiries AS FI', function (JoinClause $j) {
                $j->on('FI.subject_id', '=', 'inquiries.id')
                    ->where('FI.subject_type', '=', static::CONTRACT_TYPE);
            })
            ->whereNull('CE.id')
            ->whereRaw('DATE(inquiries.sent_at) >= DATE(NOW() - INTERVAL 3 DAY)')
            ->where('inquiries.locale', '=', $lang == 'cs' ? 'cs_CZ' : 'sk_SK')
            ->whereNull('FI.id')
            ->whereNull('T.id')
            ->orderBy('inquiries.sent_at')
            ->count();
    }

    /**
     * @param $lang
     * @return int
     */
    public static function countForToday($lang)
    {
        return Targeting::select(['targeting.*'])
            ->where(DB::raw('DATE(targeting.created_at)'), '=', DB::raw('DATE(NOW())'))
            ->where('contract_type', self::CONTRACT_TYPE)
            ->join('cs_contact.inquiries AS I', 'I.id', '=', 'targeting.contract_id')
            ->where('I.locale', $lang == 'cs' ? 'cs_CZ' : 'sk_SK')
            ->count();
    }

}
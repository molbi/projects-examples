<?php
/**
 * Created by PhpStorm.
 * User: kubahavle
 * Date: 31.07.17
 * Time: 14:09
 */

namespace App\Models\Targeting;

/**
 * Interface TargetingContract
 * @package App\Models\Targeting
 */
/**
 * Interface ITargetingContract
 * @package App\Models\Targeting
 */
interface ITargetingContract
{
    /**
     * @return mixed
     */
    public function getTargetingBody();

    /**
     * @return mixed
     */
    public function getTargetingOperator();

    /**
     * @return mixed
     */
    public function getTargetingLang();

    /**
     * @return mixed
     */
    public function getTargetingCategories();

    /**
     * @return mixed
     */
    public function getTargetingRegions();

    /**
     * @return mixed
     */
    public function getTargetingPublishedOn();

    /**
     * @return mixed
     */
    public function getTargetingFirmsCount();

    /**
     * @return mixed
     */
    public function getTargetingType();
}
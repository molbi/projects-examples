<?php

namespace App\Models\Targeting;

/**
 * Trait TargetingTrait
 * @package App\Models\Targeting
 */
trait TargetingTrait
{
    /**
     * @return array
     */
    public function forTargetingArray()
    {
        return [
            'id'          => $this->getKey(),
            'lang'        => $this->getTargetingLang(),
            'operator'    => $this->getTargetingOperator(),
            'body'        => $this->getTargetingBody(),
            'categories'  => $this->getTargetingCategories(),
            'regions'     => $this->getTargetingRegions(),
            'publishedOn' => $this->getTargetingPublishedOn(),
            'firms_count' => $this->getTargetingFirmsCount(),
            'type'        => $this->getTargetingType()
        ];
    }
}
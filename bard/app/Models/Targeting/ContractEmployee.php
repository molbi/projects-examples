<?php

namespace App\Models\Targeting;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ContractEmployee
 * @package App\Models\Targeting
 */
class ContractEmployee extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'contract_id',
        'contract_type',
        'employee_id'
    ];

    /**
     * @param Model $contract
     * @param Authenticatable $user
     */
    public static function storeForUser(Model $contract, Authenticatable $user)
    {
        try {
            (new static)->create([
                'contract_id'   => $contract->getKey(),
                'contract_type' => $contract->getMorphClass(),
                'employee_id'   => $user->getKey()
            ]);
        } catch (\Exception $e) {
        }
    }
}

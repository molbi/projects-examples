<?php

namespace App\Models\Targeting;

use App\Models\Semaphor;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

/**
 * Trait InquiryTargetingTrait
 * @package App\Models\Targeting
 */
trait InquiryTargetingTrait
{
    use TargetingTrait;

    /**
     * @param $id
     * @return mixed
     */
    public static function loadForTargeting($id)
    {
        $inquiry = (new static)->where('inquiry_id', $id)->first();
        $inquiry->load('langRecord', 'categories', 'regions.langRecord', 'category', 'region', 'operator');

        return $inquiry;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function firms()
    {
        return $this->hasMany(Targeting::class, 'contract_id', 'inquiry_id')
            ->where('contract_type', self::CONTRACT_TYPE);
    }

    /**
     *
     */
    public function getFirmsCountAttribute()
    {
        return $this->firms->count();
    }

    /**
     * @return mixed
     */
    public function getTargetingBody()
    {
        return $this->langRecord->body;
    }

    /**
     * @return mixed
     */
    public function getTargetingOperator()
    {
        return $this->operator;
    }

    /**
     * @return mixed
     */
    public function getTargetingLang()
    {
        return $this->lang;
    }

    /**
     * @return mixed
     */
    public function getTargetingCategories()
    {
        return $this->categories;
    }

    /**
     * @return mixed
     */
    public function getTargetingRegions()
    {
        return $this->regions;
    }

    /**
     * @return mixed
     */
    public function getTargetingPublishedOn()
    {
        return $this->publishedOn->toDateTimeString();
    }

    /**
     * @return mixed
     */
    public function getTargetingFirmsCount()
    {
        return $this->firms_count;
    }

    /**
     * @return mixed
     */
    public function getTargetingType()
    {
        return $this->getMorphClass();
    }

    /**
     * @param $lang
     * @return mixed
     */
    public static function loadInquiry($lang)
    {
        //nemá náhodou rozpracovanou poptávku??
        $inquiry = ContractEmployee::select(['contract_employees.*'])
            ->leftJoin('bard.targeting AS T', function (JoinClause $j) {
                $j->on('T.contract_id', '=', 'contract_employees.contract_id')
                    ->where('T.contract_type', '=', static::CONTRACT_TYPE);
            })
            ->leftJoin('bard.failed_inquiries AS FI', function (JoinClause $j) {
                $j->on('FI.subject_id', '=', 'contract_employees.contract_id')
                    ->where('FI.subject_type', '=', static::CONTRACT_TYPE);
            })
            ->join('business.Inquiry AS I', 'I.inquiry_id', '=', 'contract_employees.contract_id')
            ->where('I.lang', $lang)
            ->whereNull('T.id')
            ->whereNull('FI.id')
            ->where('contract_employees.contract_type', static::CONTRACT_TYPE)
            ->where('contract_employees.employee_id', auth()->user()->employee_id)
            ->first();

        if (!is_null($inquiry)) {
            return (new static)->where('inquiry_id', $inquiry->contract_id)->first();
        }

        //Semaphor init
        $semaphore = new Semaphor();
        $semaphore->setSemaphor(2, 261);
        $semaphore->accessTransactionLoop();

        $inquiry = (new static)
            ->select(['Inquiry.*'])
            ->leftJoin('bard.targeting AS T', function (JoinClause $j) {
                $j->on('T.contract_id', '=', 'Inquiry.inquiry_id')
                    ->where('T.contract_type', '=', static::CONTRACT_TYPE);
            })
            ->leftJoin('bard.contract_employees AS CE', function (JoinClause $j) {
                $j->on('CE.contract_id', '=', 'Inquiry.inquiry_id')
                    ->where('CE.contract_type', '=', static::CONTRACT_TYPE);
            })
            ->leftJoin('bard.failed_inquiries AS FI', function (JoinClause $j) {
                $j->on('FI.subject_id', '=', 'Inquiry.inquiry_id')
                    ->where('FI.subject_type', '=', static::CONTRACT_TYPE);
            })
            ->whereNull('CE.id')
            ->whereRaw('DATE(Inquiry.publishedOn) >= DATE(NOW() - INTERVAL 3 DAY)')
            ->where('Inquiry.lang', '=', $lang)
            ->where('Inquiry.state', '=', 'published')
            ->whereNull('FI.id')
            ->whereNull('T.id')
            ->orderBy('Inquiry.publishedOn')
            ->first();

        if (is_null($inquiry)) {
            $semaphore->quitTransaction();
            return null;
        }

        ContractEmployee::storeForUser($inquiry, auth()->user());

        $semaphore->quitTransaction();

        return $inquiry;
    }

    /**
     * @param $lang
     * @return mixed
     */
    public static function countForTargeting($lang)
    {
        return (new static)
            ->select(['Inquiry.*'])
            ->leftJoin('bard.targeting AS T', function (JoinClause $j) {
                $j->on('T.contract_id', '=', 'Inquiry.inquiry_id')
                    ->where('T.contract_type', '=', static::CONTRACT_TYPE);
            })
            ->leftJoin('bard.contract_employees AS CE', function (JoinClause $j) {
                $j->on('CE.contract_id', '=', 'Inquiry.inquiry_id')
                    ->where('CE.contract_type', '=', static::CONTRACT_TYPE);
            })
            ->leftJoin('bard.failed_inquiries AS FI', function (JoinClause $j) {
                $j->on('FI.subject_id', '=', 'Inquiry.inquiry_id')
                    ->where('FI.subject_type', '=', static::CONTRACT_TYPE);
            })
            ->whereNull('CE.id')
            ->whereRaw('DATE(Inquiry.publishedOn) >= DATE(NOW() - INTERVAL 3 DAY)')
            ->where('Inquiry.lang', '=', $lang)
            ->whereNull('FI.id')
            ->whereNull('T.id')
            ->orderBy('Inquiry.publishedOn')
            ->count();
    }

    /**
     * @param $lang
     * @return int
     */
    public static function countForToday($lang)
    {
        return Targeting::select(['targeting.*'])
            ->where(DB::raw('DATE(targeting.created_at)'), '=', DB::raw('DATE(NOW())'))
            ->where('contract_type', self::CONTRACT_TYPE)
            ->join('business.Inquiry AS I', 'I.inquiry_id', '=', 'targeting.contract_id')
            ->where('I.lang', $lang)
            ->count();
    }

}
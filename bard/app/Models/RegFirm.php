<?php

namespace App\Models;

use App\Models\SalesTargeting\SalesTargetable;
use App\Models\SalesTargeting\SalesTargetingSupplier;
use App\Models\Targeting\Targeting;
use Carbon\Carbon;
use Elasticquent\ElasticquentTrait;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Class RegFirm
 * @package App\Models
 */
class RegFirm extends Model implements SalesTargetingSupplier
{
    use ElasticquentTrait;

    /**
     * @var string
     */
    protected $table = 'log.RegFirm';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * @var array
     */
    protected $appends = [
        'last_inquiry'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'inquiries'
    ];

    /**
     * @return mixed
     */
    public function getLastInquiryAttribute()
    {
        /** @var InquiryTargeting $last */
        $last = $this->inquiries->last();

        return is_null($last) ? null : (string)$last->created_at;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function cats()
    {
        return $this->belongsToMany(Category::class, 'log.RegFirm_Category', 'regFirm_id', 'category_id')
            ->whereNotIn('type', ['profito']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function region()
    {
        return $this->hasOne(Region::class, 'region_id', 'region_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function inquiries()
    {
        return $this->hasMany(Targeting::class, 'supplier_id', 'firm_id')
            ->where('supplier_type', 'reg_firm');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function addition_description()
    {
        return $this->morphOne(Description::class, 'subject');
    }

    /**
     * @return mixed
     */
    public function getSalesTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getSalesDescription()
    {
        return null;
    }

    /**
     * @return mixed
     */
    public function getSalesRegion()
    {
        return null;
    }

    /**
     * @return mixed
     */
    public function getSalesLang()
    {
        return $this->lang;
    }

    /**
     * @return null
     */
    public function getSalesCategories()
    {
        return $this->cats->pluck('langRecord');
    }

    /**
     *
     */
    public function getSalesAddress()
    {
        return [
            'street'      => $this->street,
            'city'        => $this->city,
            'postal_code' => $this->postal_code
        ];
    }

    /** ----------------- Es ------------------ */

    /**
     * @return array
     */
    public function getIndexDocumentData()
    {
        return [
            'firm_id'             => $this->id,
            'lang'                => $this->lang,
            'title'               => $this->title,
            'description'         => null,
            'description_lemma'   => null,
            'description_2'       => !is_null($this->addition_description) ? $this->addition_description->description : null,
            'description_2_lemma' => !is_null($this->addition_description) ? $this->addition_description->description_lemma : null,
            'inquiries'           => $this->inquiries->pluck('contract_id')->toArray(),
            'inquiries_count'     => $this->inquiries_count,
            'last_inquiry'        => is_null($this->last_inquiry) ? Carbon::createFromTimestamp(0)->toDateTimeString() : $this->last_inquiry,
            'regions'             => is_null($this->region) ? [] : array_merge([$this->region_id], $this->region->getParents()),
            'categories'          => $this->cats->pluck('category_id')->toArray()
        ];
    }

    /**
     * @var array
     */
    protected $mappingProperties = [
        'last_inquiry' => [
            'type'   => 'date',
            'format' => 'yyyy-MM-dd HH:mm:ss',
        ]
    ];

    /**
     * @return Model|null|static
     */
    public static function forIndexCount()
    {
        return self::basicQuery()->select([
            DB::raw('COUNT(DISTINCT RegFirm.id) AS pocet')
        ])->first();
    }

    /**
     * @param array $options
     * @return mixed
     */
    public static function forIndex(array $options)
    {
        return self::basicQuery()
            ->select([
                'RegFirm.*'
            ])
            ->with('inquiries', 'region', 'cats', 'addition_description')
            ->withCount('inquiries')
            ->groupBy('RegFirm.id')
            ->limit($options['limit'])
            ->skip($options['offset'])
            ->get();
    }

    /**
     * @return Builder
     */
    private static function basicQuery()
    {
        return (new static)
            //remove logouted firms
            ->leftJoin('logout.records AS R', function (JoinClause $j) {
                $j->on('R.pattern', '=', DB::raw('log.RegFirm.email COLLATE utf8_unicode_ci'))
                    ->where('R.generic', 0);
            })
            ->leftJoin('logout.project_record AS PR', function (JoinClause $j) {
                $j->on('PR.record_id', '=', 'R.id')
                    ->whereIn('PR.project_id', [3, 7]);
            })
            ->whereNull('PR.record_id')
            //remove smtp_bounces
            ->leftJoin('log.SMTP_bounces AS SB', 'SB.mail_to', '=', 'log.RegFirm.email')
            ->whereNull('SB.id')
            ->whereIn('RegFirm.lang', ['cs', 'sk'])
            ->whereIn('log.RegFirm.state', ['solved', 'solved-auto']);
    }

    /**
     * @return string
     */
    public function getIndexName()
    {
        return 'bard_reg_firm';
    }

}

<?php

namespace App\Models;

/**
 * Class PriceMatrix
 * @package App\Models
 */
use App\Models\SalesTargeting\Record;

/**
 * Class PriceMatrix
 * @package App\Models
 */
class PriceMatrix
{
    /**
     * @var array
     */
    private static $matrix = [
        'cs' => [
            '1-1'     => [
                'offered'    => 2990,
                'original'   => 5990,
                'service_id' => 4099
            ],
            '2-3'     => [
                'offered'    => 3490,
                'original'   => 5990,
                'service_id' => 4101
            ],
            '4-5'     => [
                'offered'    => 3990,
                'original'   => 5990,
                'service_id' => 4103
            ],
            '6-7'     => [
                'offered'    => 4990,
                'original'   => 7990,
                'service_id' => 3195
            ],
            '8-10000' => [
                'offered'    => 5990,
                'original'   => 9990,
                'service_id' => 3197
            ]
        ],
        'sk' => [
            '1-1'     => [
                'offered'    => 99,
                'original'   => 199,
                'service_id' => 3257
            ],
            '2-3'     => [
                'offered'    => 149,
                'original'   => 249,
                'service_id' => 3057
            ],
            '4-5'     => [
                'offered'    => 179,
                'original'   => 279,
                'service_id' => 2025
            ],
            '6-7'     => [
                'offered'    => 199,
                'original'   => 319,
                'service_id' => 2023
            ],
            '8-10000' => [
                'offered'    => 249,
                'original'   => 399,
                'service_id' => 2759
            ]
        ]
    ];


    /**
     * Return offered price
     *
     * @param Record $record
     * @param $lang
     * @return int
     */
    public static function getOfferedPrice(Record $record, $lang)
    {
        $prices = findInRange(static::recordCount($record), static::$matrix[$lang]);

        return isset($prices['offered']) ? $prices['offered'] : 0;
    }

    /**
     * Return original price
     *
     * @param Record $record
     * @param $lang
     * @return int
     */
    public static function getOriginalPrice(Record $record, $lang)
    {
        $prices = findInRange(static::recordCount($record), static::$matrix[$lang]);

        return isset($prices['original']) ? $prices['original'] : 0;
    }

    /**
     * Return % discount
     *
     * @param Record $record
     * @param $lang
     * @return float
     */
    public static function getDiscount(Record $record, $lang)
    {
        return round(1 - static::getOfferedPrice($record, $lang) / static::getOriginalPrice($record, $lang), 2) * 100;
    }

    /**
     * @param Record $record
     * @param $lang
     * @return mixed
     */
    public static function getService(Record $record, $lang)
    {
        $prices = findInRange(static::recordCount($record), static::$matrix[$lang]);

        return $prices['service_id'];
    }

    /**
     * @param Record $record
     * @return int
     */
    private static function recordCount(Record $record)
    {
        return $record->contracts()->where('for_state', Record::STATE_NEW)->count();
    }
}
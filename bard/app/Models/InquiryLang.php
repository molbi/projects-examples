<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InquiryLang extends Model
{
    protected $table = 'business.InquiryLang';
}

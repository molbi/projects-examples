<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property Carbon created_at
 */
class InquiryTargeting extends Model
{
    const UPDATED_AT =  null;

    protected $table = 'bard.inquiry_targeting';

    protected $fillable = [
        'firm_id',
        'inquiry_id',
        'employee_id'
    ];
}

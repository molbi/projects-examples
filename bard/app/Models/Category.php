<?php

namespace App\Models;

use Elasticquent\ElasticquentTrait;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

/**
 * Class Firm
 * @property Collection inquiries
 * @package App\Models
 */
class Category extends Model
{
    /**
     * @var string
     */
    protected $table = 'business.Category';

    /**
     * @var string
     */
    protected $primaryKey = 'category_id';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function langRecord()
    {
        return $this->hasOne(CategoryLang::class, 'category_id', 'category_id')->where('lang', 'cs');
    }
}

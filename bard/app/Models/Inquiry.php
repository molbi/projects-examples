<?php

namespace App\Models;

use App\Models\SalesTargeting\InquirySalesTargetingTrait;
use App\Models\SalesTargeting\SalesTargetingContract;
use App\Models\Targeting\InquiryTargetingTrait;
use App\Models\Targeting\ITargetingContract;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Inquiry
 * @package App\Models
 */
class Inquiry extends Model implements SalesTargetingContract, ITargetingContract
{

    use InquirySalesTargetingTrait,
        InquiryTargetingTrait;

    /**
     *
     */
    const CONTRACT_TYPE = 'inquiry';

    /**
     * @var string
     */
    protected $table = 'business.Inquiry';
    /**
     * @var string
     */
    protected $primaryKey = 'inquiry_id';
    /**
     * @var array
     */
    protected $dates = ['publishedOn'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function langRecord()
    {
        return $this->hasOne(InquiryLang::class, 'inquiry_id', 'inquiry_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(CategoryLang::class, 'category_id', 'category_id')
            ->where('lang', '=', 'cs');
    }

    /**
     *
     */
    public function categories()
    {
        return $this->belongsToMany(CategoryLang::class, 'business.Inquiry_Category', 'inquiry_id', 'category_id')
            ->where('lang', '=', 'cs');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region()
    {
        return $this->belongsTo(RegionLang::class, 'region_id', 'region_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function regions()
    {
        return $this->belongsToMany(Region::class, 'business.Inquiry_Region', 'inquiry_id', 'region_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function operator()
    {
        return $this->belongsTo(Employee::class, 'operator_id', 'employee_id');
    }
}

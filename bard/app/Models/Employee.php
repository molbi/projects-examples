<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class Employee
 * @package App
 */
class Employee extends Authenticatable
{
    protected $hidden = [
        'photo'
    ];

    protected $table = 'zis.Employee';

    protected $primaryKey = 'employee_id';

}

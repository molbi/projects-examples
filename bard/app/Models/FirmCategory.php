<?php

namespace App\Models;

use Elasticquent\ElasticquentTrait;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

/**
 * Class Firm
 * @property Region region
 * @property Collection inquiries
 * @package App\Models
 */
class FirmCategory extends Model
{
    /**
     * @var string
     */
    protected $table = 'business.Firm_Category';

    /**
     * @var string
     */
    protected $primaryKey = 'firm_id';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function langRecord()
    {
        return $this->belongsTo(CategoryLang::class, 'ID_BCFirmRegion', 'category_id')
            ->where('lang', 'cs');
    }
}

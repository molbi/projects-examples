<?php

namespace App\Models\Garen;

use Elasticquent\ElasticquentTrait;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

/**
 * Class Firm
 * @property Collection inquiries
 * @package App\Models
 */
class Description extends Model
{
    /**
     * @var string
     */
    protected $table = 'business.Descriptions';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * @var array
     */
    protected $fillable = [
        'subject_id',
        'subject_type',
        'description',
        'description_lemma'
    ];


    /**
     * @param $value
     */
    public function setDescriptionAttribute($value)
    {
        $this->attributes['description'] = $value;

        $client = new Client(); //GuzzleHttp\Client
        try {
            $result = $client->post('http://db2.b2m.cz:8181', [
                'form_params' => [
                    'text' => $this->description,
                    'lang' => $this->lang
                ]
            ]);
            $this->attributes['description_lemma'] = $result->getBody()->getContents();
        } catch (\Exception $e) {
            $this->attributes['description_lemma'] = $value;
        }
    }
}

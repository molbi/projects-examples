<?php

namespace App\Models\Garen;

use App\Models\Firm as BusinessFirm;
use App\Models\RegFirm;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Firm
 * @property Collection inquiries
 * @package App\Models
 */
class Email extends Model
{
    /**
     * @var string
     */
    protected $table = 'garen.emails';

    protected $connection = 'mysql_devel';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function business_firms()
    {
        return $this->hasMany(BusinessFirm::class, 'email', 'email');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reg_firms()
    {
        return $this->hasMany(RegFirm::class, 'email', 'email');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function firm()
    {
        return $this->belongsTo(Firm::class);
    }

}

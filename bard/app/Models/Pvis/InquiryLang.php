<?php

namespace App\Models\Pvis;

use App\Models\SalesTargeting\SalesTargetingContract;
use Illuminate\Database\Eloquent\Model;


/**
 * Class Inquiry
 * @package App\Models
 */
class InquiryLang extends Model
{
    /**
     * @var string
     */
    protected $table = 'pvis.PsInquiryLang';

    protected $primaryKey = 'psInquiry_id';
}

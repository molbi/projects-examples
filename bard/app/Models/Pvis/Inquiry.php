<?php

namespace App\Models\Pvis;

use App\Models\SalesTargeting\SalesTargetingContract;
use Illuminate\Database\Eloquent\Model;


/**
 * Class Inquiry
 * @package App\Models
 */
class Inquiry extends Model implements SalesTargetingContract
{
    /**
     * @var string
     */
    protected $table = 'pvis.PsInquiry';

    /**
     * @var string
     */
    protected $primaryKey = 'psInquiry_id';


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function langRecord()
    {
        return $this->hasOne(InquiryLang::class, 'psInquiry_id', 'psInquiry_id')
            ->where('lang', 'cs');
    }

    /**
     *
     */
    public function getSalesTitle()
    {
        return $this->langRecord->title;
    }

    /**
     *
     */
    public function getSalesBody()
    {
        return $this->langRecord->description;
    }

    /**
     * @return string
     */
    public function getSalesType()
    {
        return 'pvs';
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: kubahavle
 * Date: 31.07.17
 * Time: 14:09
 */

namespace App\Models\SalesTargeting;


/**
 * Interface SalesTargetingSupplier
 * @package App\Models\SalesTargeting
 */
/**
 * Interface SalesTargetingSupplier
 * @package App\Models\SalesTargeting
 */
/**
 * Interface SalesTargetingSupplier
 * @package App\Models\SalesTargeting
 */
interface SalesTargetingSupplier
{
    /**
     * @return mixed
     */
    public function getSalesTitle();

    /**
     * @return mixed
     */
    public function getSalesDescription();

    /**
     * @return mixed
     */
    public function getSalesRegion();

    /**
     * @return mixed
     */
    public function getSalesCategories();

    /**
     * @return mixed
     */
    public function getSalesAddress();

    /**
     * @return mixed
     */
    public function getSalesLang();
}
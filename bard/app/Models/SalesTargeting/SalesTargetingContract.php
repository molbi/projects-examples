<?php
/**
 * Created by PhpStorm.
 * User: kubahavle
 * Date: 31.07.17
 * Time: 14:09
 */

namespace App\Models\SalesTargeting;

/**
 * Interface SalesTargetingContract
 * @package App\Models\SalesTargeting
 */
interface SalesTargetingContract
{
    /**
     * @return mixed
     */
    public function getSalesTitle();

    /**
     * @return mixed
     */
    public function getSalesBody();

    /**
     * @return mixed
     */
    public function getSalesType();

    /**
     * @return mixed
     */
    public function getSalesUrl();
}
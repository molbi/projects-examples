<?php

namespace App\Models\SalesTargeting;

use App\Models\Employee;
use App\Models\PriceMatrix;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;

/**
 * Class Record
 * @package App\Models\SalesTargeting
 */
class Record extends Model
{
    /**
     * Vznik záznamu
     */
    const STATE_NEW = 'new';
    /**
     * Po prvotním nacílení
     */
    const STATE_TARGETED = 'targeted';
    /**
     * Po 2 dvech od prvního nacílení
     */
    const STATE_FIRST_FOLLOWUP = 'first_followup';
    /**
     * Po 7 dnech od prvního nacílení v případě, že není záznam ukončen, objednávkou, či jinak
     */
    const STATE_FOR_TARGETING = 'for_targeting';
    /**
     * Po druhém necílení
     */
    const STATE_TARGETED_2 = 'targeted_2';
    /**
     * Po druhém necílení
     */
    const STATE_SECOND_FOLLOWUP = 'second_followup';
    /**
     *
     */
    const STATE_ORDERED = 'ordered';
    /**
     *
     */
    const STATE_FINISH = 'finish';

    /**
     *
     */
    const STATE_NO_CONTRACTS_2 = 'no-contracts-2';

    /**
     *
     */
    const FOR_LIST = [
        self::STATE_NEW,
        self::STATE_FOR_TARGETING
    ];

    /**
     *
     */
    const STATES = [
        self::STATE_NEW,
        self::STATE_TARGETED,
        self::STATE_FIRST_FOLLOWUP,
        self::STATE_FOR_TARGETING,
        self::STATE_SECOND_FOLLOWUP,
        self::STATE_TARGETED_2,
        self::STATE_ORDERED,
        self::STATE_FINISH
    ];

    /**
     * @var string
     */
    protected $table = 'bard.zed_records';

    /**
     * @var array
     */
    protected $hidden = [
        'supplier_type'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'data' => 'json'
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'state'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'valid_to'
    ];

    /**
     * @param $state
     */
    public function setStateAttribute($state)
    {
        if ($state == Record::STATE_TARGETED) {
            $this->setAttribute('valid_to', Carbon::now()->addDays(10));
        }

        $this->attributes['state'] = $state;
    }

    /**
     * @param Employee|null $user
     */
    public static function assignToUser(Employee $user = null)
    {
        self::whereNull('employee_id')->orderBy(self::CREATED_AT)->limit(5)->update([
            'employee_id' => $user->employee_id
        ]);
    }

    /**
     * @return Builder
     */
    public static function forAssign()
    {
        return self::whereNull('employee_id');
    }

    /**
     * @param Builder $builder
     * @param Employee $employee
     * @return Builder
     */
    public function scopeForUser(Builder $builder, Employee $employee = null)
    {
        return $builder->where('employee_id', $employee->employee_id);
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeForList(Builder $builder)
    {
        return $builder->whereIn('state', self::FOR_LIST);
    }

    /**
     * @return bool
     */
    public function isExpired()
    {
        $updated_at = Carbon::createFromTimestamp(strtotime($this->updated_at))->setTime(0, 0, 0);
        $now = Carbon::now()->setTime(0, 0, 0);

        return $now->ne($updated_at);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function supplier()
    {
        return $this->morphTo('supplier');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contracts()
    {
        return $this->hasMany(RecordTarget::class, 'record_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id', 'employee_id');
    }

    /**
     * @return mixed
     */
    public function getEmployeeName()
    {
        return $this->employee->realname;
    }

    /**
     * @return string
     */
    public function getEmployeeEmail()
    {
        $emailName = str_replace('-', '.', str_slug($this->employee->realname));

        switch ($this->source) {
            case 'hz':
                $domain = 'hlidanizakazek.cz';
                break;
            default:
                $domain = 'epoptavka.cz';
                break;
        }

        return sprintf('%s@%s', $emailName, $domain);
    }

    /**
     * @return string
     */
    public function getOrderUrl()
    {
        $first = $this->contracts->first();

        switch ($first->contract->getMorphClass()) {
            case 'inquiry':
                if ($this->supplier->lang == 'cs') {
                    return sprintf('http://www.epoptavka.cz/%s/%s/%s',
                        $this->supplier_type == 'business_firm' ? 'aktivace-placeneho-pristupu' : 'aktivace-placeneho-pristupu/nova-firma',
                        md5($this->supplier_id),
                        PriceMatrix::getService($this, 'cs'));
                } else {
                    return sprintf('http://www.123dopyt.sk/%s/%s/%s',
                        $this->supplier_type == 'business_firm' ? 'aktivacia-plateneho-pristupu' : 'aktivacia-plateneho-pristupu/nova-firma',
                        md5($this->supplier_id),
                        PriceMatrix::getService($this, 'sk'));
                }
                break;
            case 'pvs':
                break;
            case 'cs_inquiry':
                break;
        }

    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'id'            => $this->getKey(),
            'lang'          => $this->supplier->getSalesLang(),
            'state'         => $this->state,
            'title'         => $this->supplier->getSalesTitle(),
            'description'   => $this->supplier->getSalesDescription(),
            'address'       => $this->supplier->getSalesAddress(),
            'region'        => $this->supplier->getSalesRegion(),
            'categories'    => $this->supplier->getSalesCategories(),
            'supplier_id'   => $this->supplier_id,
            'supplier_type' => $this->supplier_type,
            'form'          => $this->data,
        ];
    }

}

<?php

namespace App\Models\SalesTargeting;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Record
 * @package App\Models\SalesTargeting
 */
class RecordTarget extends Model
{
    /**
     * @var string
     */
    protected $table = 'bard.zed_record_targeting';

    /**
     * @var array
     */
    protected $fillable = [
        'contract_id',
        'contract_type',
        'for_state'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function contract()
    {
        return $this->morphTo('contract');
    }
}

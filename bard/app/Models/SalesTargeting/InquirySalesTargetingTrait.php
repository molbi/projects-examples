<?php

namespace App\Models\SalesTargeting;

use Elasticquent\ElasticquentTrait;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Builder;

/**
 * Trait InquirySalesTargetingTrait
 * @package App\Models\SalesTargeting
 */
trait InquirySalesTargetingTrait
{
    use ElasticquentTrait;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function records()
    {
        return $this->morphMany(RecordTarget::class, 'contract');
    }

    /**
     *
     */
    public function getLemmaDescriptionAttribute()
    {
        $client = new Client(); //GuzzleHttp\Client
        try {
            $result = $client->post('http://db2.b2m.cz:8181', [
                'form_params' => [
                    'text' => $this->getSalesBody(),
                    'lang' => $this->lang
                ]
            ]);

            return $result->getBody()->getContents();

        } catch (\Exception $e) {
            return $this->getSalesBody();
        }
    }

    /**
     * @return mixed
     */
    public function getSalesTitle()
    {
        return $this->langRecord->title;
    }

    /**
     * @return mixed
     */
    public function getSalesBody()
    {
        return $this->langRecord->body;
    }

    /**
     * @return string
     */
    public function getSalesType()
    {
        return 'inquiry';
    }

    /**
     *
     */
    public function getSalesUrl()
    {
        switch ($this->lang) {
            case 'cs':
                return sprintf('http://poptavky.epoptavka.cz/poptavka/%s-%s', $this->getKey(), $this->langRecord->titleSanitized);
                break;
            case 'sk':
                return sprintf('http://dopyty.123dopyt.sk/dopyt/%s-%s', $this->getKey(), $this->langRecord->titleSanitized);
                break;
        }
    }

    /** ----------------- Es ------------------ */

    /**
     * @return array
     */
    public function getIndexDocumentData()
    {
        return [
            'id'                => $this->inquiry_id,
            'lang'              => $this->lang,
            'publishedOn'       => $this->publishedOn->format('Y-m-d H:i:s'),
            'title'             => $this->getSalesTitle(),
            'description'       => $this->getSalesBody(),
            'description_lemma' => $this->lemma_description,
            'regs'              => $this->regions->pluck('region_id')->toArray(),
            'cats'              => $this->categories->pluck('category_id')->toArray(),
            'records'           => $this->records->pluck('record_id')->toArray()
        ];
    }

    /**
     * @var array
     */
    protected $mappingProperties = [
        'publishedOn' => [
            'type'   => 'date',
            'format' => 'yyyy-MM-dd HH:mm:ss',
        ]
    ];

    /**
     * @var array
     */
//    protected $mappingProperties = [];

    /**
     * @return string
     */
    public function getIndexName()
    {
        return 'bard_inquiry';
    }

    /**
     * @return int
     */
    public static function forIndexCount()
    {
        return self::basicQuery()->count();
    }

    /**
     * @param array $options
     * @return mixed
     */
    public static function forIndex(array $options)
    {
        return self::basicQuery()
            ->with('regions', 'categories')
            ->limit($options['limit'])
            ->skip($options['offset'])
            ->get();
    }

    /**
     * @return Builder
     */
    private static function basicQuery()
    {
        return (new static)
            ->where('state', 'published')
            ->whereRaw('DATE(publishedOn) > DATE(NOW() - INTERVAL 180 DAY)');
    }

}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FailedInquiry extends Model
{
    protected $fillable = [
        'subject_id',
        'subject_type',
        'employee_id'
    ];
}

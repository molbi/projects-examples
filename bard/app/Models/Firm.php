<?php

namespace App\Models;

use App\Models\SalesTargeting\SalesTargetable;
use App\Models\SalesTargeting\SalesTargetingSupplier;
use App\Models\Targeting\Targeting;
use Carbon\Carbon;
use Elasticquent\ElasticquentTrait;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Class Firm
 * @property Region region
 * @property Collection categories
 * @property Collection inquiries
 * @property mixed last_inquiry
 * @property mixed description
 * @property mixed inquiries_count
 * @property mixed title
 * @property mixed firm_id
 * @property mixed lang
 * @property mixed lemma_description
 * @property Description addition_description
 * @package App\Models
 */
class Firm extends Model implements SalesTargetingSupplier
{
    use ElasticquentTrait;

    /**
     * @var string
     */
    protected $table = 'business.Firm';

    /**
     * @var string
     */
    protected $primaryKey = 'firm_id';

    /**
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * @var array
     */
    protected $appends = [
        'last_inquiry'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'inquiries'
    ];

    /**
     * @return mixed
     */
    public function getLastInquiryAttribute()
    {
        /** @var InquiryTargeting $last */
        $last = $this->inquiries->last();

        return is_null($last) ? null : (string)$last->created_at;
    }

    /**
     *
     */
    public function getLemmaDescriptionAttribute()
    {
        $client = new Client(); //GuzzleHttp\Client
        try {
            $result = $client->post('http://db2.b2m.cz:8181', [
                'form_params' => [
                    'text' => $this->description,
                    'lang' => $this->lang
                ]
            ]);

            return $result->getBody()->getContents();

        } catch (\Exception $e) {
            return $this->description;
        }
    }

    /**
     * @return mixed
     */
    public function getSalesTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getSalesDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getSalesRegion()
    {
        if (is_null($this->region)) {
            return [];
        }

        $id = array_slice($this->region->getParents(), -3, 1);

        return RegionLang::where('region_id', $id)->first()->toArray();


    }

    /**
     * @return mixed
     */
    public function getSalesCategories()
    {
        return $this->cats->pluck('langRecord');
    }

    /**
     * @return array
     */
    public function getSalesAddress()
    {
        return [
            'street'      => $this->street,
            'city'        => $this->cityPostalCode,
            'postal_code' => $this->postalCode
        ];
    }

    /**
     * @return mixed
     */
    public function getSalesLang()
    {
        return $this->lang;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function inquiries()
    {
        return $this->hasMany(Targeting::class, 'supplier_id', 'firm_id')
            ->where('supplier_type', 'business_firm');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function cats()
    {
        return $this->belongsToMany(Category::class, 'business.Firm_Category', 'firm_id', 'ID_BCFirmRegion')
            ->whereNotIn('type', ['profito']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function region()
    {
        return $this->hasOne(Region::class, 'region_id', 'region_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function addition_description()
    {
        return $this->morphOne(Description::class, 'subject');
    }

    /** ----------------- Es ------------------ */

    /**
     * @return array
     */
    public function getIndexDocumentData()
    {
        return [
            'firm_id'             => $this->firm_id,
            'lang'                => $this->lang,
            'title'               => $this->title,
            'description'         => $this->description,
            'description_lemma'   => $this->lemma_description,
            'description_2'       => !is_null($this->addition_description) ? $this->addition_description->description : null,
            'description_2_lemma' => !is_null($this->addition_description) ? $this->addition_description->description_lemma : null,
            'inquiries'           => $this->inquiries->pluck('contract_id')->toArray(),
            'inquiries_count'     => $this->inquiries_count,
            'last_inquiry'        => is_null($this->last_inquiry) ? Carbon::createFromTimestamp(0)->toDateTimeString() : $this->last_inquiry,
            'regions'             => is_null($this->region) ? [] : array_merge([$this->region_id], $this->region->getParents()),
            'categories'          => $this->cats->pluck('category_id')->toArray()
        ];
    }

    /**
     * @var array
     */
    protected $mappingProperties = [
        'last_inquiry' => [
            'type'   => 'date',
            'format' => 'yyyy-MM-dd HH:mm:ss',
        ]
    ];

    /**
     * @return Model|null|static
     */
    public static function forIndexCount()
    {
        return self::basicQuery()->select([
            DB::raw('COUNT(DISTINCT Firm.firm_id) AS pocet')
        ])->first();
    }

    /**
     * @param array $options
     * @return mixed
     */
    public static function forIndex(array $options)
    {
        return self::basicQuery()
            ->select([
                'Firm.*'
            ])
            ->with('inquiries', 'region', 'cats', 'addition_description')
            ->withCount('inquiries')
            ->groupBy('Firm.firm_id')
            ->limit($options['limit'])
            ->skip($options['offset'])
            ->get();
    }

    /**
     * @return Builder
     */
    private static function basicQuery()
    {
        return (new static)
            //remove IN, IN KW services
            ->leftJoin('business.Firm_Service AS FS', function (JoinClause $j) {
                $j->on('FS.firm_id', '=', 'business.Firm.firm_id')
                    ->whereIn('FS.service', ['inquiries', 'inquiries-keywords']);
            })
            ->whereNull('FS.firm_id')
            //remove IN, IN KW credit services
            ->leftJoin('business.Firm_CreditService AS FCS', function (JoinClause $j) {
                $j->on('FCS.firm_id', '=', 'business.Firm.firm_id')
                    ->whereIn('FCS.service', ['inquiries', 'inquiries-keywords']);
            })
            ->whereNull('FCS.firm_id')
            //remove logouted firms
            ->leftJoin('logout.records AS R', function (JoinClause $j) {
                $j->on('R.pattern', '=', DB::raw('business.Firm.email COLLATE utf8_unicode_ci'))
                    ->where('R.generic', 0);
            })
            ->leftJoin('logout.project_record AS PR', function (JoinClause $j) {
                $j->on('PR.record_id', '=', 'R.id')
                    ->whereIn('PR.project_id', [3, 7]);
            })
            ->whereNull('PR.record_id')
            //remove smtp_bounces
            ->leftJoin('log.SMTP_bounces AS SB', 'SB.mail_to', '=', 'business.Firm.email')
            ->whereNull('SB.id')
            ->whereIn('Firm.lang', ['cs', 'sk'])
            ->where('business.Firm.state', 'visible');
    }
}

<?php

namespace App\Models;

use Elasticquent\ElasticquentTrait;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

/**
 * Class Firm
 * @property Collection inquiries
 * @package App\Models
 */
class Description extends Model
{
    /**
     * @var string
     */
    protected $table = 'business.Descriptions';

    /**
     * @var bool
     */
    public $timestamps = false;


}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

/**
 * Class Semaphor
 * @package App\Models
 */
class Semaphor extends Model
{
    /**
     * @var string
     */
    protected $table = 'log.Semaphor_Transaction';

    /**
     * @var bool
     */
    public $timestamps = false;

    /** @var int Transaction interval */
    private $interval = 10;
    /** @var int Semaphor ID */
    private $id;

    /**
     * @param $interval
     * @param $id
     */
    public function setSemaphor($interval, $id)
    {
        $this->interval = $interval;
        $this->id = $id;
    }

    /**
     *
     */
    public function accessTransactionLoop()
    {
        do {
            try {
                $semaphor = $this->select(DB::RAW('COUNT(*) AS total'))
                    ->where('id', '=', $this->id)
                    ->where('locked', '=', 1)
                    ->whereRaw('created > (NOW() - INTERVAL ' . $this->interval . ' MINUTE)')
                    ->first('total');
                if ($semaphor->total == 0) {
                    $this->where('id', '=', $this->id)->delete();
                    $this->insert(['id' => $this->id]);
                    break;
                }
            } catch (\Exception $e) {

            }
        } while (true);
    }

    /**
     *
     */
    public function quitTransaction()
    {
        $this->where('id', '=', $this->id)->delete();
    }
}
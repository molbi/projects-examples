<?php

namespace App\Models\CsContact;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Models\Region as B2MRegion;

/**
 * Class Firm
 * @property Collection inquiries
 * @property mixed region_id
 * @package App\Models
 */
class Region extends Model
{
    /**
     * @var string
     */
    protected $table = 'cs_contact.regions';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function b2m_region()
    {
        return $this->belongsTo(B2MRegion::class, 'b2mRegion_id', 'region_id');
    }
}

<?php

namespace App\Models\CsContact;

use App\Models\CategoryLang;
use App\Models\Employee;
use App\Models\RegionLang;
use App\Models\SalesTargeting\CsInquirySalesTargetingTrait;
use App\Models\SalesTargeting\SalesTargetingContract;
use App\Models\Targeting\CsInquiryTargetingTrait;
use App\Models\Targeting\ITargetingContract;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Inquiry
 * @package App\Models
 */
class Inquiry extends Model implements SalesTargetingContract, ITargetingContract
{
    use CsInquiryTargetingTrait,
        CsInquirySalesTargetingTrait;

    /**
     *
     */
    const CONTRACT_TYPE = 'cs_inquiry';

    /**
     * @var string
     */
    protected $table = 'cs_contact.inquiries';

    /**
     * @var array
     */
    protected $dates = [
        'sent_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function operators()
    {
        return $this->belongsToMany(Employee::class, 'cs_contact.inquiry_employee', 'inquiry_id', 'employee_id');
    }

    /**
     *
     */
    public function categories()
    {
        return $this->belongsToMany(CategoryLang::class, 'cs_contact.category_inquiry', 'inquiry_id', 'category_id')
            ->where('lang', '=', 'cs');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function regions()
    {
        return $this->belongsToMany(Region::class, 'cs_contact.inquiry_region');
    }
}

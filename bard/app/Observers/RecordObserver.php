<?php

namespace App\Observers;

use App\Mail\SalesTargeting\FollowupMail;
use App\Mail\SalesTargeting\FollowupPlainMail;
use App\Mail\SalesTargeting\Targeted2Mail;
use App\Mail\SalesTargeting\Targeted2PlainMail;
use App\Mail\SalesTargeting\TargetedMail;
use App\Mail\SalesTargeting\TargetedPlainMail;
use App\Models\SalesTargeting\Record;
use Carbon\Carbon;
use Illuminate\Mail\Mailer;

/**
 * Class RecordObserver
 * @package App\Observer
 */
class RecordObserver
{
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * RecordObserver constructor.
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param Record $record
     */
    public function saved(Record $record)
    {
        if ($record->isDirty('state')) {
            switch ($record->getDirty()['state']) {
                case Record::STATE_TARGETED:
                    rand(0, 1) == 1
                        ? $this->mailer->send(new TargetedMail($record))
                        : $this->mailer->send(new TargetedPlainMail($record));
                    break;
                case Record::STATE_TARGETED_2:
                    rand(0, 1) == 1
                        ? $this->mailer->send(new Targeted2Mail($record))
                        : $this->mailer->send(new Targeted2PlainMail($record));
                    break;
                case Record::STATE_FIRST_FOLLOWUP:
                    rand(0, 1) == 1
                        ? $this->mailer->send(new FollowupMail($record, Record::STATE_NEW))
                        : $this->mailer->send(new FollowupPlainMail($record, Record::STATE_NEW));
                    break;
                case Record::STATE_SECOND_FOLLOWUP:
                    rand(0, 1) == 1
                        ? $this->mailer->send(new FollowupMail($record, $record->getOriginal('state') === Record::STATE_TARGETED_2 ? Record::STATE_FOR_TARGETING : Record::STATE_NEW))
                        : $this->mailer->send(new FollowupPlainMail($record, $record->getOriginal('state') === Record::STATE_TARGETED_2 ? Record::STATE_FOR_TARGETING : Record::STATE_NEW));
                    break;
            }
        }
    }
}
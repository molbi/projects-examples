<?php

if (!function_exists('get_sql')) {
    /**
     * @param $builder
     *
     * @return mixed
     */
    function get_sql($builder)
    {
        $sql = $builder->toSql();
        foreach ($builder->getBindings() as $binding) {
            $value = is_numeric($binding) ? $binding : "'" . $binding . "'";
            $sql = preg_replace('/\?/', $value, $sql, 1);
        }

        return $sql;
    }
}

if (!function_exists('findInRange')) {
    /**
     * @param $number
     * @param $array
     * @return mixed
     */
    function findInRange($number, $array)
    {
        foreach ($array as $key => $value) {
            list($min, $max) = explode('-', $key);

            if ($number >= $min && $number <= $max) {
                return $value;
            }
        }

        return null;
    }
}

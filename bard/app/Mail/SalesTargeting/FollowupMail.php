<?php

namespace App\Mail\SalesTargeting;

use App\Models\SalesTargeting\Record;
use Illuminate\Mail\Mailable;


/**
 * Class FollowupMail
 * @package App\Mail\SalesTargeting
 */
class FollowupMail extends Mailable
{
    /**
     * @var Record
     */
    private $record;

    /**
     * Create a new message instance.
     * @param Record $record
     * @param $for_state
     */
    public function __construct(Record $record, $for_state)
    {
        $this->record = $record;
        $this->record->load(['contracts' => function ($builder) use ($record, $for_state) {
            $builder->where('for_state', $for_state)->with('contract');
        }]);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->to($this->record->data['email'])
            ->bcc('eprodej@b2m.cz')
            ->from($this->record->getEmployeeEmail(), $this->record->getEmployeeName())
            ->subject($this->subjectForState())
            ->view(sprintf('mails.sales-targeting.%s.fu-pseudo', $this->record->supplier->lang), [
                'record' => $this->record
            ]);
    }

    /**
     * @return string
     */
    private function subjectForState()
    {
        return $this->record->state == Record::STATE_FIRST_FOLLOWUP
            ? trans('mails.fu_first_follow_up', [], $this->record->supplier->lang)
            : trans('mails.fu_second_follow_up', [], $this->record->supplier->lang);
    }
}

<?php

namespace App\Mail\SalesTargeting;

use App\Models\SalesTargeting\Record;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class TargetedPlainMail
 * @package App\Mail\SalesTargeting
 */
class Targeted2PlainMail extends Mailable
{
    /**
     * @var Record
     */
    private $record;

    /**
     * Create a new message instance.
     * @param Record $record
     */
    public function __construct(Record $record)
    {
        $this->record = $record;
        $this->record->load(['contracts' => function ($builder) {
            $builder->where('for_state', Record::STATE_FOR_TARGETING)->with('contract');
        }]);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->to($this->record->data['email'])
            ->bcc('eprodej@b2m.cz')
            ->from($this->record->getEmployeeEmail(), $this->record->getEmployeeName())
            ->subject(trans('mails.targeted_2', [], $this->record->supplier->lang))
            ->text(sprintf('mails.sales-targeting.%s.targeted_2_plain', $this->record->supplier->lang), [
                'record' => $this->record
            ]);
    }
}

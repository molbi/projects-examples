<?php

namespace App\Mail\SalesTargeting;

use App\Models\SalesTargeting\Record;
use Illuminate\Mail\Mailable;

/**
 * Class TargetedMail
 * @package App\Mail\SalesTargeting
 */
class Targeted2Mail extends Mailable
{
    /**
     * @var Record
     */
    private $record;

    /**
     * Create a new message instance.
     * @param Record $record
     */
    public function __construct(Record $record)
    {
        $this->record = $record;
        $this->record->load(['contracts' => function ($builder) use ($record) {
            $builder->where('for_state', Record::STATE_FOR_TARGETING)->with('contract');
        }]);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->to($this->record->data['email'])
            ->bcc('eprodej@b2m.cz')
            ->from($this->record->getEmployeeEmail(), $this->record->getEmployeeName())
            ->subject(trans('mails.targeted_2', [], $this->record->supplier->lang))
            ->view(sprintf('mails.sales-targeting.%s.targeted_2', $this->record->supplier->lang), [
                'record' => $this->record
            ]);
    }
}

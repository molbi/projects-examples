<?php

namespace App\Console;

use App\Console\Commands\Contracts\BuildIndex as ContractsBuildIndex;
use App\Console\Commands\Firm\BuildIndex as FirmBuildIndex;
use App\Console\Commands\Firm\FillDescription as FirmFillDescription;
use App\Console\Commands\RegFirm\BuildIndex as RegFirmBuildIndex;
use App\Console\Commands\RegFirm\FillDescription as RegFirmFillDescription;
use App\Console\Commands\SalesTargeting\ReturnForTargeting;
use App\Console\Commands\SalesTargeting\SendFirstFollowup;
use App\Console\Commands\SalesTargeting\SendSecondFollowup;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

/**
 * Class Kernel
 * @package App\Console
 */
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        FirmBuildIndex::class,
        RegFirmBuildIndex::class,
        ContractsBuildIndex::class,

        SendFirstFollowup::class,
        SendSecondFollowup::class,
        ReturnForTargeting::class,

        FirmFillDescription::class,
        RegFirmFillDescription::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('sales-targeting:send-first-followup')->everyTenMinutes();
        $schedule->command('sales-targeting:send-second-followup')->everyTenMinutes();
        $schedule->command('sales-targeting:return-for-targeting')->everyTenMinutes();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}

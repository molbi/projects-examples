<?php

namespace App\Console\Commands\Firm;

use App\Models\Firm;
use Exception;
use Illuminate\Console\Command;

/**
 * Class BuildIndex
 * @package App\Console\Commands\Firm
 */
class BuildIndex extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'firm:build-index {--limit=} {--debug=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Vytvoří index firem pro vyhledávání';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        ini_set( 'memory_limit', '2048M' );

        $this->createIndex();
        $this->addFirmsToIndex();
    }

    /**
     *
     */
    private function createIndex()
    {
        try {
            Firm::deleteIndex();
        } catch (Exception $e) {
        }

        Firm::createIndex($shards = null, $replicas = null);
    }

    /**
     *
     */
    private function addFirmsToIndex()
    {
        $limit = (int)$this->option('limit');

        $this->info('Začátek zjištění počtu dodavatelů.');
        $total = Firm::forIndexCount()->pocet;
        $this->info('Počet dodavatelů k indexaxi:' . $total);

        $bar = $this->output->createProgressBar($total);

        for ($index = 0; $index <= floor($total / $limit); $index++) {
            $firms = Firm::forIndex(['limit' => $limit, 'offset' => $index * $limit]);

            $firms->each(function (Firm $firm) use ($bar) {
                $firm->addToIndex();
                $bar->advance();
            });
        }
        $bar->finish();
    }
}

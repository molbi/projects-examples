<?php

namespace App\Console\Commands\Firm;

use App\Models\Garen\Description;
use App\Models\Garen\Email;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class FillDescription
 * @package App\Console\Commands\Firm
 */
class FillDescription extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'firm:fill-description {limit} {segment}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Doplnění popisků firmy podle emailu';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $total = Email::whereHas('firm', function ($q) {
            $q->whereDate('created_at', '<', '2016-11-10');
        })->where(DB::raw('MOD(id, 5)'), $this->argument('segment'))
            ->count();

        $bar = $this->output->createProgressBar($total);

        $i = 0;
        do {
            $emails = Email::with('business_firms', 'firm')
                ->whereHas('firm', function ($q) {
                    $q->whereDate('created_at', '<', '2016-11-10');

                })
                ->where(DB::raw('MOD(id, 5)'), $this->argument('segment'))
                ->take($limit = $this->argument('limit'))
                ->skip($i * $limit)
                ->get();

            foreach ($emails as $email) {
                try {
                    if ($email->business_firms->isEmpty()) {
                        $bar->advance();
                        continue;
                    }

                    /** @var Model $firm */
                    foreach ($email->business_firms as $firm) {
                        Description::create([
                            'subject_id'   => $firm->getKey(),
                            'subject_type' => 'business_firm',
                            'description'  => $email->firm->description
                        ]);
                    }
                } catch (\Exception $e) {
                    $this->error(sprintf('Problem with email: %s', $email->getKey()));
                }

                $bar->advance();
            }

            $i++;
        } while ($emails->count() > 0);

        $bar->finish();
    }
}

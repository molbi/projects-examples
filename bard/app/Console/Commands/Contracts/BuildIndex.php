<?php

namespace App\Console\Commands\Contracts;

use App\Models\CsContact\Inquiry as CsInquiry;
use App\Models\Firm;
use App\Models\Inquiry;
use App\Models\Pvis\Inquiry as PsInquiry;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BuildIndex
 * @package App\Console\Commands\Firm
 */
class BuildIndex extends Command
{

    /**
     *
     */
    const TYPES = [
        'inquiry'    => Inquiry::class,
        'pvs'        => PsInquiry::class,
        'cs_inquiry' => CsInquiry::class
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'contracts:build-index {type} {--limit=1000} {--debug=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Vytvoří index firem pro vyhledávání';

    /**
     * @var Inquiry|PsInquiry|CsInquiry null
     */
    private $model = null;

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        if (!isset(self::TYPES[$this->argument('type')])) {
            throw new Exception(sprintf('Unknown contract types. Only allowed are %s', implode(',', array_keys(self::TYPES))));
        }

        $this->model = self::TYPES[$this->argument('type')];

        $this->createIndex();
        $this->addFirmsToIndex();
    }

    /**
     *
     */
    private function createIndex()
    {
        try {
            ($this->model)::deleteIndex();
        } catch (Exception $e) {
        }

        ($this->model)::createIndex($shards = null, $replicas = null);
    }

    /**
     *
     */
    private function addFirmsToIndex()
    {
        $limit = (int)$this->option('limit');

        $this->info('Začátek zjištění počtu poptávek.');
        $total = ($this->model)::forIndexCount();
        $this->info('Počet poptávek k indexaxi:' . $total);

        $bar = $this->output->createProgressBar($total);

        for ($index = 0; $index <= floor($total / $limit); $index++) {
            $contracts = ($this->model)::forIndex(['limit' => $limit, 'offset' => $index * $limit]);
            $contracts->each(function ($contract) use ($bar) {

                $contract->addToIndex();

                $bar->advance();
            });
        }
        $bar->finish();
    }
}

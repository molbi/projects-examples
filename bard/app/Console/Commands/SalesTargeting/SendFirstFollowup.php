<?php

namespace App\Console\Commands\SalesTargeting;

use App\Models\SalesTargeting\Record;
use Illuminate\Console\Command;

/**
 * Class SendFirstFollowup
 * @package App\Console\Commands\SalesTargeting
 */
class SendFirstFollowup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sales-targeting:send-first-followup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pošle první followup po 2 dnech od prvního nacílení';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->info('Start sending first followup');

        $records = $this->records();
        $records->each(function (Record $record) {
            $record->update([
                'state' => Record::STATE_FIRST_FOLLOWUP
            ]);
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    private function records()
    {
        return Record::where('state', Record::STATE_TARGETED)
            ->whereRaw('updated_at + INTERVAL 2 DAY < NOW()')
            ->get();
    }
}

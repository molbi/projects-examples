<?php

namespace App\Console\Commands\SalesTargeting;

use App\Models\SalesTargeting\Record;
use Illuminate\Console\Command;

/**
 * Class ReturnForTargeting
 * @package App\Console\Commands\SalesTargeting
 */
class ReturnForTargeting extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sales-targeting:return-for-targeting';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Vrátí záznam zpět ke druhému nacílení po 5ti dnech od prvního followup';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->info('Return firm to targeting');

        $records = $this->records();
        $records->each(function (Record $record) {
            $record->update([
                'state' => Record::STATE_FOR_TARGETING
            ]);
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    private function records()
    {
        return Record::where('state', Record::STATE_FIRST_FOLLOWUP)
            ->whereRaw('updated_at + INTERVAL 5 DAY < NOW()')
            ->get();
    }
}

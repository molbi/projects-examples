<?php

namespace App\Console\Commands\SalesTargeting;

use App\Models\SalesTargeting\Record;
use Illuminate\Console\Command;

/**
 * Class SendSecondFollowup
 * @package App\Console\Commands\SalesTargeting
 */
class SendSecondFollowup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sales-targeting:send-second-followup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pošle druhý followup po 2 dnech od druhého nacílení';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->info('Start sending second followup');

        $records = $this->records();
        $records->each(function (Record $record) {
            $record->update([
                'state' => Record::STATE_SECOND_FOLLOWUP
            ]);
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    private function records()
    {
        return Record::whereIn('state', [Record::STATE_TARGETED_2, Record::STATE_NO_CONTRACTS_2])
            ->whereRaw('updated_at + INTERVAL 2 DAY < NOW()')
            ->get();
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();
use App\Models\SalesTargeting\Record;
use Illuminate\Http\Request;

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');

Route::group(['middleware' => 'auth'], function () {

    Route::group(['prefix' => 'sales-targeting', 'namespace' => 'SalesTargeting'], function () {
        Route::get('{firm?}', 'IndexController@index')->name('sales-targeting.index');
        Route::post('load/{type}', 'IndexController@load')->name('sales-targeting.load');
    });

    Route::group(['prefix' => 'targeting', 'namespace' => 'Targeting'], function () {
        Route::get('resources', 'TargetingController@resources');
        Route::post('search', 'TargetingController@search');
        Route::post('store/{type}', 'TargetingController@store');
        Route::post('not-for-targeting', 'TargetingController@notForTargeting');

        Route::get('numbers/{lang}/{type}', 'TargetingController@numbers');
        Route::get('{lang}/{type}/{contract?}', 'TargetingController@index')->name('targeting');
    });

    Route::get('show-records', function () {
        $records = Record::with(['employee', 'supplier'])->whereNotIn('state', [Record::STATE_NEW])->latest('updated_at')->get();

        return response()->view('show-records', compact('records'));
    })->name('sales-targeting.show-records');

    Route::post('show-records/{record}/change-state', function (Request $request, Record $record) {
        $record->state = $request->input('state');
        $record->save();

        return redirect()->route('sales-targeting.show-records');
    })->name('sales-targeting.change-state');

    Route::get('/', 'IndexController@index');

});




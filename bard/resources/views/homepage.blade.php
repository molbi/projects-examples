@extends('layouts.app')
@section('title', 'Cílení | Rozcestník')

@section('content')
    <div class="container homepage">
        <div class="row">
            <div class="col-md-6">
                <a href="{{ route('targeting', ['lang' => 'cs', 'type' => 'inquiry' ]) }}" class="btn btn-block btn-success">CZ poptávky</a>
            </div>
            <div class="col-md-6">
                <a href="{{ route('targeting', ['lang' => 'sk', 'type' => 'inquiry']) }}" class="btn btn-block btn-outline-success">SK poptávky</a>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <a href="{{ route('targeting', ['lang' => 'cs', 'type' => 'cs_inquiry' ]) }}" class="btn btn-block btn-primary"><small>CSK</small> CZ poptávky</a>
            </div>
            <div class="col-md-6">
                <a href="{{ route('targeting', ['lang' => 'sk', 'type' => 'cs_inquiry' ]) }}" class="btn btn-block btn-outline-primary"><small>CSK</small> SK poptávky</a>
            </div>
        </div>
    </div>
@endsection
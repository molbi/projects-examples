Dobrý deň,

prostredníctvom webu sme sa od Vás dozvedeli, že hľadáte:
"{{ $record->data['text']  }}"
Prešiel som všetky dostupné dopyty a posielam tie, ktoré by mali zodpovedať službám, o ktoré máte záujem:

@foreach($record->contracts as $contract)
{{ $contract->contract->getSalesUrl() }}
@endforeach

Podobné zákazky sú na 123dopyt.sk {{ $record->contracts->count() > 5 ? 'stovky' : 'desiatky' }} ročne.
Získajte prístup k týmto a ďalším zákazkam.
Prístup na 12 mesiacov získate do {{ $record->valid_to->format('j. n. Y') }} za zvýhodnenú cenu {{ \App\Models\PriceMatrix::getOfferedPrice($record, 'sk') }} €. Bežná cena je {{ \App\Models\PriceMatrix::getOriginalPrice($record, 'sk') }} € (cenové zvýhodnenie je tak {{ \App\Models\PriceMatrix::getDiscount($record, 'sk') }} %).

V cene prístupu je zahrnuté:
1) Dopyty priamo z vášho oboru podnikania s kontaktom na zákazníka.
2) Okamžitá možnosť kontaktovať zákazníka a získať zákazku!
3) Prístup do najväčšieho dopytového systému v ČR a SR.

Ročne Vám zašleme až stovky dopytov.
Ak máte záujem o aktiváciu platenej služby za zvýhodnenú cenu, prosím, napíšte mi do {{ $record->valid_to->format('j. n. Y') }}.

S pozdravom,
{{ $record->getEmployeeName() }}
@if($record->source == 'hz')
    SledovanieZákazok.sk
@else
    123dopyt.sk
@endif
{{ $record->getEmployeeEmail() }}

Toto obchodné oznámenie Vám bolo zaslané na základe registrácie v katalógu firiem 123dodavatel.sk ({{ $record->supplier->getSalesTitle() }} - {{ $record->supplier->email }}).
Pre odhlásenie prosíme odpovedzte na tento e-mail.


---------- Pôvodná správa ----------
Od:{{ $record->supplier->email }}
Dátum: {{  $record->created_at->format('j. n. Y') }}
Predmet: Dopyty pre Vašu firmu

{{ $record->supplier->getSalesTitle() }}: Zháňam zákazky v oblasti
"{{ $record->data['text'] }}"
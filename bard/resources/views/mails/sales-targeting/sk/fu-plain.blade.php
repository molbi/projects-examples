Dobrý deň,

pred dvoma dňami sme Vám poslali obchodné príležitosti, ktoré by mali zodpovedať Vami hľadanému:
"{{ $record->data['text']  }}"

* Zodpovedajú?
* Máte o dané zákazky záujem?

Poslané dopyty nájdete tu:
@foreach($record->contracts as $contract)
{{ $contract->contract->getSalesUrl() }}
@endforeach

Tieto a podobné zákazky, môžete získať s prístupom na 12 mesiacov za zvýhodnenú cenu {{ \App\Models\PriceMatrix::getOfferedPrice($record, 'sk') }} € (bežná cena {{ \App\Models\PriceMatrix::getOriginalPrice($record, 'sk') }} €).

Cena zahŕňa:
1) Dopyty priamo z Vášho oboru podnikania s kontaktom na zákazníka.
2) Okamžitá možnosť kontaktovať zákazníka a získať zákazku!
3) Najväčší dopytový systém v ČR a SR.

Ak máte záujem, dajte mi vedieť do {{ $record->valid_to->format('j. n. Y') }}

S pozdravom,
{{ $record->getEmployeeName() }}
@if($record->source == 'hz')
    SledovanieZákazok.sk
@else
    123dopyt.sk
@endif
{{ $record->getEmployeeEmail() }}

Toto obchodné oznámenie Vám bolo zaslané na základe registrácie v katalógu firiem 123dodavatel.sk ({{ $record->supplier->getSalesTitle() }} - {{ $record->supplier->email }}).
Pre odhlásenie prosíme odpovedzte na tento e-mail.


---------- Pôvodná správa ----------
Od:{{ $record->supplier->email }}
Dátum: {{  $record->created_at->format('j. n. Y') }}
Predmet: Dopyty pre Vašu firmu

{{ $record->supplier->getSalesTitle() }}: Zháňam zákazky v oblasti
"{{ $record->data['text'] }}"
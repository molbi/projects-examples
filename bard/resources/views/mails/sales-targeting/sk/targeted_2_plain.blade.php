Dobrý deň,

pred týždňom som Vám posielal dopyty, ktoré zodpovedajú tomu, čo hľadáte:
“{{ $record->data['text'] }}”

Skontroloval som, či pre Vás nemáme ešte nejaké nové dopyty.

Posielam čerstvé príležitosti, ktoré pre Vás aktuálne máme:
@foreach($record->contracts as $contract)
{{ $contract->contract->getSalesUrl() }}
@endforeach

Nové zákazky neustále prichádzajú. Získajte prístup na 12 mesiacov k aktuálnym obchodných príležitostiam.


@if($record->extended)
    Predĺžili sme Vám termín na objednanie aktivácie za zvýhodnenú cenu {{ \App\Models\PriceMatrix::getOfferedPrice($record, 'sk') }} €. Bežná  cena je {{ \App\Models\PriceMatrix::getOriginalPrice($record, 'sk') }} € (cenové zvýhodnenie je tak {{ \App\Models\PriceMatrix::getDiscount($record, 'sk') }} %) do {{ $record->valid_to->format('j. n. Y') }}.
@else
    Prístup na 12 mesiacov získate do {{ $record->valid_to->format('j. n. Y') }} za zvýhodnenú cenu {{ \App\Models\PriceMatrix::getOfferedPrice($record, 'sk') }} €. Bežná  cena je {{ \App\Models\PriceMatrix::getOriginalPrice($record, 'sk') }} € (cenové zvýhodnenie je tak {{ \App\Models\PriceMatrix::getDiscount($record, 'sk') }} %).
@endif

V cene prístupu je:
1) Dopyty priamo z Vášho oboru podnikania s kontaktom na zákazníka.
2) Okamžitá možnosť kontaktovať zákazníka a získať zákazku!
3) Prístup do najväčšieho dopytového systému v ČR a SR.


S pozdravom,
{{ $record->getEmployeeName() }}
@if($record->source == 'hz')
    SledovanieZákazok.sk
@else
    123dopyt.sk
@endif
{{ $record->getEmployeeEmail() }}

Toto obchodné oznámenie Vám bolo zaslané na základe registrácie v katalógu firiem 123dodavatel.sk ({{ $record->supplier->getSalesTitle() }} - {{ $record->supplier->email }}).
Pre odhlásenie prosíme odpovedzte na tento e-mail.


---------- Pôvodná správa ----------
Od:{{ $record->supplier->email }}
Dátum: {{  $record->created_at->format('j. n. Y') }}
Predmet: Dopyty pre Vašu firmu

{{ $record->supplier->getSalesTitle() }}: Zháňam zákazky v oblasti
"{{ $record->data['text'] }}"
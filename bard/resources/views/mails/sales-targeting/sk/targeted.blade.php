<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8">
    <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width">
    <!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="x-apple-disable-message-reformatting">
    <!-- Disable auto-scale in iOS 10 Mail entirely -->
    <title>Nabídka</title>
    <!-- The title tag shows in email notifications, like Android 4.4. -->


</head>

<body width="100%" bgcolor="#ffffff" style="margin: 0; mso-line-height-rule: exactly;">
    <center style="width: 100%; background: #ffffff;">
        <table width="100%" bgcolor="#ffffff" style="width: 100%; background: #ffffff;" cellspacing="0" cellpadding="0"
               id="cskontakt-mail">
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td width="25" style="width: 25px;">&nbsp;</td>
                <td align="center">
                    <table width="600" style="width: 600px;" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <table width="600" style="width: 600px;" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="left" valign="middle">
                                            @include('mails.sales-targeting.sk.logo')
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="600" style="width: 600px;" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td colspan="3" height="20" style="height: 20px;"></td>
                                    </tr>
                                    <tr>
                                        <td width="20" style="width: 20px;">&nbsp;</td>
                                        <td>
                                            <font size="2" face="Arial" color="#555555"
                                                  style="font-size: 14px; color: #555555; font-family: Arial;">
                                                Dobrý deň,<br/>
                                                prostredníctvom webu sme sa od Vás dozvedeli, že hľadáte:
                                                <table width="560">
                                                    <tr>
                                                        <td bgcolor="#eeeeee" style="padding: 10px;">
                                                            <font size="2" face="Arial" color="#555555"
                                                                  style="font-size: 14px; color: #555555; font-family: Arial;">
                                                                "{{ $record->data['text']  }}"
                                                            </font>
                                                        </td>
                                                    </tr>
                                                </table>
                                                Prešiel som všetky dostupné dopyty a posielam tie, ktoré by mali zodpovedať službám, o ktoré máte záujem:
                                            </font>
                                        </td>
                                        <td width="20" style="width: 20px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="20" style="width: 20px;">&nbsp;</td>
                                        <td>
                                            <font size="2" face="Arial" color="#555555"
                                                  style="font-size: 14px; color: #555555; font-family: Arial;">
                                                <ul>
                                                    @foreach($record->contracts as $contract)
                                                        <li>
                                                            <a href="{{ $contract->contract->getSalesUrl() }}">{{ $contract->contract->getSalesTitle() }}</a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </font>
                                        </td>
                                        <td width="20" style="width: 20px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="20" style="width: 20px;">&nbsp;</td>
                                        <td>
                                            <font size="2" face="Arial" color="#555555"
                                                  style="font-size: 14px; color: #555555; font-family: Arial;">
                                                Podobné zákazky sú na 123dopyt.sk {{ $record->contracts->count() > 5 ? 'stovky' : 'desiatky' }}
                                                ročne.<br/>
                                                Získajte prístup k týmto a ďalším zákazkam.<br/>
                                                Prístup na 12 mesiacov získate do {{ $record->valid_to->format('j. n. Y') }} za zvýhodnenú
                                                cenu {{ \App\Models\PriceMatrix::getOfferedPrice($record, 'sk') }}
                                                €. Bežná cena
                                                je {{ \App\Models\PriceMatrix::getOriginalPrice($record, 'sk') }}
                                                € (cenové zvýhodnenie je
                                                tak {{ \App\Models\PriceMatrix::getDiscount($record, 'sk') }}
                                                %).
                                            </font>
                                        </td>
                                        <td width="20" style="width: 20px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="20" style="width: 20px;">&nbsp;</td>
                                        <td>
                                            <font size="2" face="Arial" color="#555555"
                                                  style="font-size: 14px; color: #555555; font-family: Arial;">
                                                V cene prístupu je zahrnuté:<br/>
                                                <ol>
                                                    <li>Dopyty priamo z vášho oboru podnikania s kontaktom na zákazníka.</li>
                                                    <li>Okamžitá možnosť kontaktovať zákazníka a získať zákazku!</li>
                                                    <li>Prístup do najväčšieho dopytového systému v ČR a SR.</li>
                                                </ol>
                                            </font>
                                        </td>
                                        <td width="20" style="width: 20px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="20" style="width: 20px;">&nbsp;</td>
                                        <td>
                                            <font size="2" face="Arial" color="#555555"
                                                  style="font-size: 14px; color: #555555; font-family: Arial;">
                                                Ročne Vám zašleme až stovky dopytov.<br/>
                                                Máte záujem o aktiváciu platené služby?
                                            </font>
                                        </td>
                                        <td width="20" style="width: 20px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="20" style="width: 20px;"></td>
                                        <td style="border-radius: 3px 3px 0 0; background: #2980b9; text-align: center;">
                                            <a href="{{ $record->getOrderUrl() }}"
                                               style="background: #2980b9; border: 15px solid #2980b9; color: #ffffff; font-family: sans-serif; font-size: 15px; line-height: 1; text-align: center; text-decoration: none; display: block; border-radius: 3px;">
                                                <b>Aktivovať platenú službu</b>
                                            </a>
                                        </td>
                                        <td width="20" style="width: 20px;"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td width="20" style="width: 20px;">&nbsp;</td>
                                        <td>
                                            <font size="2" face="Arial" color="#888888"
                                                  style="font-size: 14px; color: #888888; font-family: Arial;">
                                                S pozdravom,<br/>
                                                {{ $record->getEmployeeName() }}<br />
                                                @if($record->source == 'hz')
                                                    SledovanieZákazok.sk<br />
                                                @else
                                                    123dopyt.sk<br />
                                                @endif
                                                {{ $record->getEmployeeEmail() }}
                                            </font>
                                        </td>
                                        <td width="20" style="width: 20px;">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <table width="600" style="width: 600px;" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <font size="1" face="Arial" color="#888888"
                                                  style="font-size: 11px; color: #888888; font-family: Arial;">
                                                Toto obchodné oznámenie Vám bolo zaslané na základe registrácie v katalógu firiem 123dodavatel.sk ({{ $record->supplier->getSalesTitle() }} - {{ $record->supplier->email }}).
                                                <br>Pre odhlásenie prosíme odpovedzte na tento e-mail.
                                            </font>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="25" style="width: 25px;">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                    <table width="100%">
                        <tr><td colspan="2">---------- Pôvodná správa ----------</td></tr>
                        <tr>
                            <td width="20" style="border-right: #729fcf 2px solid;"></td>
                            <td style="padding: 20px;">
                                Od:{{ $record->supplier->email }}<br>
                                Dátum: {{  $record->created_at->format('j. n. Y') }}<br>
                                Predmet: Dopyty pre Vašu firmu<br><br>
                                {{ $record->supplier->getSalesTitle() }}: Zháňam zákazky v oblasti<br>
                                "{{ $record->data['text'] }}"
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </center>
</body>
</html>
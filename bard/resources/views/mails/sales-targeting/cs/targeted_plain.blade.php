Dobrý den,

prostřednictvím webu jsme se od Vás dozvěděli, že hledáte:
"{{ $record->data['text']  }}"
Prošel jsem všechny dostupné poptávky a posílám Vám ty, které by měly odpovídat službám, o které máte zájem:

@foreach($record->contracts as $contract)
{{ $contract->contract->getSalesUrl() }}
@endforeach

Podobných zakázek jsou na ePoptávce {{ $record->contracts->count() > 5 ? 'stovky' : 'desítky' }} ročně.
Získejte přístup k těmto a dalším zakázkám.
Přístup na 12 měsíců získáte do {{ $record->valid_to->format('j. n. Y') }} za zvýhodněnou cenu {{ \App\Models\PriceMatrix::getOfferedPrice($record, 'cs') }} Kč. Běžná cena je {{ \App\Models\PriceMatrix::getOriginalPrice($record, 'cs') }} Kč (cenové zvýhodnění je tak {{ \App\Models\PriceMatrix::getDiscount($record, 'cs') }} %).

Cena přístupu zahrnuje:
1) Poptávky přímo z Vašeho oboru podnikání s kontaktem na zákazníka.
2) Okamžitou možnost kontaktovat zákazníka a získat zakázku!
3) Přístup do největšího poptávkového systému v ČR a SR.

Ročně Vám zašleme až stovky poptávek.
Pokud máte zájem o aktivaci placené služby za zvýhodněnou cenu, napište mi prosím do {{ $record->valid_to->format('j. n. Y') }}.

S přátelským pozdravem,
{{ $record->getEmployeeName() }}
@if($record->source == 'hz')
HlídáníZakázek.cz
@else
ePoptávka.cz
@endif
{{ $record->getEmployeeEmail() }}

Tento e-mail Vám byl zaslán na základě registrace v katalogu Hledat.cz ({{ $record->supplier->getSalesTitle() }} - {{ $record->supplier->email }}).
Pro odhlášení prosíme odpovězte na tento e-mail.


---------- Původní zpráva ----------
Od:{{ $record->supplier->email }}
Datum: {{  $record->created_at->format('j. n. Y') }}
Předmět: Poptávky pro Vaši firmu

{{ $record->supplier->getSalesTitle() }}: Sháním zakázky v oblasti
"{{ $record->data['text'] }}"
Dobrý den,

před týdnem jsem Vám posílal poptávky, které odpovídají tomu, co hledáte:
“{{ $record->data['text'] }}”

Zkontroloval jsem, zda pro Vás nenajdu další nové poptávky.

Posílám čerstvé příležitosti, které pro Vás aktuálně máme:
@foreach($record->contracts as $contract)
{{ $contract->contract->getSalesUrl() }}
@endforeach

Nové zakázky neustále přicházejí. Získejte přístup na 12 měsíců k aktuálním obchodním příležitostem.

@if($record->extended)
    Prodloužili jsme Vám termín k objednání aktivace za zvýhodněnou cenu {{ \App\Models\PriceMatrix::getOfferedPrice($record, 'cs') }} Kč. Běžná cena je {{ \App\Models\PriceMatrix::getOriginalPrice($record, 'cs') }} Kč (cenové zvýhodnění je tak {{ \App\Models\PriceMatrix::getDiscount($record, 'cs') }} %) do {{ $record->valid_to->format('j. n. Y') }}.
@else
    Přístup na 12 měsíců získáte do {{ $record->valid_to->format('j. n. Y') }} za zvýhodněnou cenu {{ \App\Models\PriceMatrix::getOfferedPrice($record, 'cs') }} Kč. Běžná cena je {{ \App\Models\PriceMatrix::getOriginalPrice($record, 'cs') }} Kč (cenové zvýhodnění je tak {{ \App\Models\PriceMatrix::getDiscount($record, 'cs') }} %).
@endif

Cena přístupu zahrnuje:
1) Poptávky přímo z Vašeho oboru podnikání s kontaktem na zákazníka.
2) Okamžitou možnost kontaktovat zákazníka a získat zakázku!
3) Přístup do největšího poptávkového systému v ČR a SR.


S přátelským pozdravem,
{{ $record->getEmployeeName() }}
@if($record->source == 'hz')
HlídáníZakázek.cz
@else
ePoptávka.cz
@endif
{{ $record->getEmployeeEmail() }}

Tento e-mail Vám byl zaslán na základě registrace v katalogu Hledat.cz ({{ $record->supplier->getSalesTitle() }} - {{ $record->supplier->email }}).
Pro odhlášení prosíme odpovězte na tento e-mail.


---------- Původní zpráva ----------
Od:{{ $record->supplier->email }}
Datum: {{  $record->created_at->format('j. n. Y') }}
Předmět: Poptávky pro Vaši firmu

{{ $record->supplier->getSalesTitle() }}: Sháním zakázky v oblasti
"{{ $record->data['text'] }}"
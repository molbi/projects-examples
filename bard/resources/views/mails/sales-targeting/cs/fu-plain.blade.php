Dobrý den,

před dvěma dny jsem Vám poslal obchodní příležitosti, které by měly odpovídat Vámi hledanému:
"{{ $record->data['text']  }}"

* Odpovídají?
* Máte o dané zakázky zájem?

Zaslané poptávky najdete zde:
@foreach($record->contracts as $contract)
{{ $contract->contract->getSalesUrl() }}
@endforeach

Tyto a podobné zakázky můžete získat s přístupem na 12 měsíců za zvýhodněnou cenu {{ \App\Models\PriceMatrix::getOfferedPrice($record, 'cs') }} Kč (běžná cena {{ \App\Models\PriceMatrix::getOriginalPrice($record, 'cs') }} Kč).

Cena zahrnuje:
1) Poptávky přímo z Vašeho oboru podnikání s kontaktem na zákazníka.
2) Okamžitou možnost kontaktovat zákazníka a získat zakázku!
3) Největší poptávkový systém v ČR a SR.

Máte-li zájem, dejte mi vědět do {{ $record->valid_to->format('j. n. Y') }}

S přátelským pozdravem,
{{ $record->getEmployeeName() }}
@if($record->source == 'hz')
HlídáníZakázek.cz
@else
ePoptávka.cz
@endif
{{ $record->getEmployeeEmail() }}

Tento e-mail Vám byl zaslán na základě registrace v katalogu Hledat.cz ({{ $record->supplier->getSalesTitle() }} - {{ $record->supplier->email }}).
Pro odhlášení prosíme odpovězte na tento e-mail.


---------- Původní zpráva ----------
Od:{{ $record->supplier->email }}
Datum: {{  $record->created_at->format('j. n. Y') }}
Předmět: Poptávky pro Vaši firmu

{{ $record->supplier->getSalesTitle() }}: Sháním zakázky v oblasti
"{{ $record->data['text'] }}"
@extends('layouts.app')

@section('title', 'Zobrazení záznamů')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <table class="table">
                    <tbody>
                    @foreach($records as $record)
                        <tr>
                            <td>{{ $record->updated_at->format('j. n. Y H:i:s') }}</td>
                            <td>{{ $record->state }}</td>
                            <td>{{ $record->supplier->title }}</td>
                            <td>
                                <form action="{{ route('sales-targeting.change-state', [$record]) }}" method="post">
                                    {!! csrf_field() !!}
                                    <select name="state">
                                        @foreach(\App\Models\SalesTargeting\Record::STATES as $state)
                                            <option @if($state == $record->state) selected
                                                    @endif value="{{ $state }}">{{ $state }}</option>
                                        @endforeach
                                    </select>
                                    <input type="submit" value="Změnit stav"/>
                                </form>
                            </td>
                            <td></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        @unless(is_null($inquiry))
            <targeting :inquiry="{{ json_encode($inquiry->forTargetingArray()) }}"></targeting>
        @else
            <h3>Žádné poptávky k nacílení</h3>
        @endunless
    </div>
@endsection
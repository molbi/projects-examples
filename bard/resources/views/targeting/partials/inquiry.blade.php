<div class="inquiry-box">
    <hr>

    <div class="progress">
        <div class="progress-bar bg-success" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
        <div class="progress-bar" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100">30%</div>
    </div>

    <hr>

    <div class="inquiry">
        <h4>{{ $inquiry->title }}</h4>

        <div class="row">
            <div class="col-4">
                <strong>Datum zadání:</strong>
                {{ $inquiry->publishedOn->format('j. n. Y') }}
            </div>
            <div class="col-4">
                <strong>Místo realizace:</strong>
                {{ $inquiry->region->title }} <a href="#"><i class="fa fa-plus-circle"></i></a>
            </div>
            <div class="col-4">
                <strong>Kategorie:</strong>
                {{ $inquiry->category->title }}
            </div>
        </div>

        <div class="inquiry-body">
            {!! nl2br($inquiry->body) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-7">
        <h5>Filtr</h5>
        <form action="">
            <div class="form-group">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Klíčová slova">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <select name="region" class="form-control">
                    <option value="0">- Kraj -</option>
                </select>
            </div>
            <div class="form-group">
                <select name="categories" class="form-control">
                    <option value="0">- Kategorie -</option>
                </select>
            </div>
        </form>
    </div>
    <div class="col-5 facets-box">
        <h5>Filtrováno:</h5>
        <ul class="list-inline facets-regions">
            <li class="list-inline-item">
                <a href="#" class="btn btn-primary btn-sm">Praha <i class="fa fa-times"></i></a>
            </li>
            <li class="list-inline-item">
                <a href="#" class="btn btn-primary btn-sm">Kraj Vysočina <i class="fa fa-times"></i></a>
            </li>
            <li class="list-inline-item">
                <a href="#" class="btn btn-primary btn-sm">Ústecký Kraj <i class="fa fa-times"></i></a>
            </li>
        </ul>

        <hr>

        <ul class="list-inline facets-categories">
            <li class="list-inline-item">
                <a href="#" class="btn btn-primary btn-sm">Stavebnictví <i class="fa fa-times"></i></a>
            </li>
            <li class="list-inline-item">
                <a href="#" class="btn btn-primary btn-sm">Dřevo <i class="fa fa-times"></i></a>
            </li>
            <li class="list-inline-item ">
                <a href="#" class="btn btn-primary btn-sm">Lodžie <i class="fa fa-times"></i></a>
            </li>
            <li class="list-inline-item">
                <a href="#" class="btn btn-primary btn-sm">Lodžie <i class="fa fa-times"></i></a>
            </li>
            <li class="list-inline-item">
                <a href="#" class="btn btn-primary btn-sm">Lodžie <i class="fa fa-times"></i></a>
            </li>
        </ul>
    </div>
</div>
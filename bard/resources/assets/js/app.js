/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import Nl2br from 'vue-nl2br';
import Multiselect from 'vue-multiselect';
import Spinner from 'vue-simple-spinner';
import VueClipboards from 'vue-clipboards';

Vue.use(VueClipboards);

Vue.component('nl2br', Nl2br);
Vue.component('multiselect', Multiselect);
Vue.component('spinner', Spinner);

Vue.component('sales-targeting', require('./components/sales-targeting.vue'));
Vue.component('targeting', require('./components/targeting.vue'));
Vue.component('copy-button', require('./components/copy-button.vue'));

const moment = require('moment');
require('moment/locale/cs');

Vue.use(require('vue-moment'), {
    moment
});

Vue.directive('hightlight', {
    // When the bound element is inserted into the DOM...
    bind: function (el) {
        let word = $(el).data('q');
        if (word) {
            let instance = new Mark(el);
            instance.mark(word);
        }
    }
});


const app = new Vue({
    el: '#app'
});

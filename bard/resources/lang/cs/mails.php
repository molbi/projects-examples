<?php

return [
    'fu_first_follow_up'  => 'Připomínka RE: Poptávky pro Vaši firmu',
    'fu_second_follow_up' => 'Prosím o odpověď ► RE: Další obchodní příležitosti pro Vaši firmu',
    'targeted'            => 'RE: Poptávky pro Vaši firmu',
    'targeted_2'          => 'RE: Další obchodní příležitosti pro Vaši firmu',
];
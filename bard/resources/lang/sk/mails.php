<?php

return [
    'fu_first_follow_up'  => 'Pripomienka RE: Dopyty pre Vašu firmu',
    'fu_second_follow_up' => 'Prosím o odpoveď ► RE: Ďalšie obchodné príležitosti pre Vašu firmu',
    'targeted'            => 'RE: Dopyty pre Vašu firmu',
    'targeted_2'          => 'RE: Ďalšie obchodné príležitosti pre Vašu firmu',
];
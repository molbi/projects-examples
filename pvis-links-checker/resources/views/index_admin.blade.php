@extends('layout')

@section('content')
    <article class="content dashboard-page">
        <div class="title-block">
            <h1 class="title"> Static Tables </h1>
            <p class="title-description"> When blocks aren't enough </p>
        </div>
        <section class="section">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-block">
                            <section class="example">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Url</th>
                                        <th>Poslední kontrola</th>
                                        <th>Poslední změna</th>
                                        <th>Shoda %</th>
                                        <th>Rozdíl %</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($links as $link)
                                        <tr>
                                            <th scope="row">{{ $link->id }}</th>
                                            <td>{{ $link->url }}</td>
                                            <td>{{ $link->checked_at->format('j. n. Y H:i:s') }}</td>
                                            <td>{{ $link->diffs->last() ? $link->diffs->last()->created_at->format('j. n. Y H:i:s') : 'n/a' }}</td>
                                            <td>{{ $link->diffs->last() ? $link->diffs->last()->similarity : 0 }}</td>
                                            <td>{{ $link->diffs->last() ? $link->diffs->last()->difference : 0 }}</td>
                                            <td>
                                                <a href="{{ route('links.show', $link) }}"><i class="fa fa-search"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </article>
@endsection
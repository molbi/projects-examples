@extends('layout')

@section('content')
    <article class="content dashboard-page">
        <div class="title-block">
            <h1 class="title"> Static Tables </h1>
            <p class="title-description"> When blocks aren't enough </p>
        </div>
        <section class="section">
            @foreach($link->diffs->take(1) as $diff)
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-block">
                                <div class="card-title-block">
                                    <h3 class="title"> shoduje se z {{ round($diff->similarity*100, 3) }}%, rozdílný z {{ round($diff->difference*100, 3) }}%</h3>
                                </div>
                                <section class="example">
                                    {!! base64_decode($diff->diff) !!}
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </section>
    </article>
@endsection
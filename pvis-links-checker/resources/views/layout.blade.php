<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> ModularAdmin - Free Dashboard Theme | HTML Version </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link rel="stylesheet" href="{{ asset('css/vendor.css') }}">
    <link rel="stylesheet" id="theme-style" href="{{ asset('css/app.css') }}">

    <style type="text/css">
        .diff {
            width:100%;
        }

        .diff td{
            padding:0 0.667em;
            vertical-align:top;
            white-space:pre;
            white-space:pre-wrap;
            font-family:Consolas,'Courier New',Courier,monospace;
            font-size:0.75em;
            line-height:1.333;
        }

        .diff span{
            display:block;
            min-height:1.333em;
            margin-top:-1px;
            padding:0 3px;
        }

        * html .diff span{
            height:1.333em;
        }

        .diff span:first-child{
            margin-top:0;
        }

        .diffDeleted span{
            border:1px solid rgb(255,192,192);
            background:rgb(255,224,224);
        }

        .diffInserted span{
            border:1px solid rgb(192,255,192);
            background:rgb(224,255,224);
        }

        #toStringOutput{
            margin:0 2em 2em;
        }

    </style>

</head>

<body>

<div class="main-wrapper">
    <div class="app" id="app">
        @include('layouts.header')
        @include('layouts.left-bar')
        @yield('content')
        @include('layouts.footer')
    </div>
</div>

<script src="{{ asset('js/vendor.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
<?php

namespace App\Console\Commands;

use App\Model\Source;
use Illuminate\Console\Command;
use Illuminate\Database\Connection;
use Illuminate\Database\DatabaseManager;

class ImportLinks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:links';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    /**
     * @var DatabaseManager
     */
    private $manager;

    /**
     * Create a new command instance.
     *
     * @param DatabaseManager $manager
     *
     */
    public function __construct(DatabaseManager $manager)
    {
        parent::__construct();
        $this->manager = $manager;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $links = $this->getLinks();

        $links->each(function($link) {

            $source = new Source([
                'title' => $link->title,
                'lang' => $link->lang,
                'priority' => $link->priority
            ]);
            $source->save();

            $source->links()->create([
                'url' => $link->url
            ]);

            if(!empty($link->urlMemo)) {
                $source->links()->create([
                    'url' => $link->urlMemo
                ]);
            }

            $this->info('Naimportováno: ' . $link->title);
        });
    }

    private function getLinks()
    {
        return $this->db()->table('pvis.PsInquiryLink')->whereIn('priority', [1, 2])->get();
    }

    private function db() {
        return $this->manager->connection('b2m');
    }
}

<?php

namespace App\Console\Commands;

use App\HtmlDiff\HtmlDiff;
use App\Model\Link;
use Carbon\Carbon;
use ForceUTF8\Encoding;
use Html2Text\Html2Text;
use Illuminate\Cache\CacheManager;
use Illuminate\Cache\Repository;
use Illuminate\Console\Command;
use TextAnalysis\Analysis\FreqDist;
use TextAnalysis\Comparisons\CosineSimilarityComparison;
use TextAnalysis\Tokenizers\WhitespaceTokenizer;

/**
 * Class Check
 * @package App\Console\Commands
 */
class Check extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'links:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    /**
     * @var Repository
     */
    private $repository;
    /**
     * @var Html2Text
     */
    private $html2Text;
    /**
     * @var WhitespaceTokenizer
     */
    private $tokenizer;
    /**
     * @var CosineSimilarityComparison
     */
    private $comparison;

    private $similarity;
    private $difference;

    /**
     * Create a new command instance.
     *
     * @param CacheManager               $cacheManager
     * @param Html2Text                  $html2Text
     * @param WhitespaceTokenizer        $tokenizer
     * @param CosineSimilarityComparison $comparison
     */
    public function __construct(
        CacheManager $cacheManager,
        Html2Text $html2Text,
        WhitespaceTokenizer $tokenizer,
        CosineSimilarityComparison $comparison
    )
    {
        parent::__construct();
        $this->repository = $cacheManager->driver('file');
        $this->html2Text = $html2Text;
        $this->tokenizer = $tokenizer;
        $this->comparison = $comparison;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $links = Link::all();

        $links->each(function (Link $link) {
            $contents = $this->loadContents($link);
            $tokens = $this->tokenizeContents($contents);
            $frequencies = $this->getFrequencies($tokens);

            if ($this->compare($frequencies, $link)) {
                $this->saveDiff($link, $contents);
            }

            $link->update([
                'checked_at' => Carbon::now()
            ]);
        });
    }

    /**
     * @param Link $link
     *
     * @return mixed
     */
    private function getNewContent(Link $link)
    {
        $content = $this->grabUrl($link);
        $text = $this->html2text($content);
        $this->saveToCache($link, $text);

        return $text;
    }

    /**
     * @param Link $link
     *
     * @return mixed|string
     */
    private function getOldContent(Link $link)
    {
        if ($content = $this->repository->get($link->getCacheKey())) {
            return $content;
        }

        return '';
    }

    /**
     * @param $link
     * @param $ret
     */
    private function saveToCache(Link $link, $ret)
    {
        $this->repository->forever($link->getCacheKey(), $ret);
    }

    /**
     * @param Link $link
     *
     * @return mixed
     */
    private function grabUrl(Link $link)
    {
        $crl = curl_init();
        $timeout = 5;
        curl_setopt($crl, CURLOPT_URL, $link->url);
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($crl, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($crl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($crl, CURLOPT_TIMEOUT, 60);
        curl_setopt($crl, CURLOPT_HEADER, 0);
        curl_setopt($crl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1');
        curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($crl, CURLOPT_SSL_VERIFYHOST, 2);

        $ret = curl_exec($crl);
        curl_close($crl);

        return Encoding::toUTF8(trim($ret));
    }

    /**
     * @param $content
     *
     * @return string
     */
    private function html2text($content)
    {
        $this->html2Text->setHtml($content);

        return $this->html2Text->getText();
    }

    /**
     * @param      $frequencies
     * @param Link $link
     *
     * @return bool
     */
    private function compare($frequencies, Link $link)
    {
        $this->similarity = $this->comparison->similarity(
            $frequencies->freq_old->getKeys(),
            $frequencies->freq_new->getKeys()
        );

        $this->difference = $this->comparison->distance(
            $frequencies->freq_old->getKeys(),
            $frequencies->freq_new->getKeys()
        );

        if ((string)$this->similarity == '1.0') {
            $this->info(sprintf('Obsah Url %s je bezezměny (%s)', $link->url, $this->similarity));

            return false;
        } elseif ($this->similarity < 1 && $this->similarity > 0.6) {
            $this->info(sprintf('Obsah Url %s je podobný z %s%%', $link->url, $this->similarity));
        } else {
            $this->info(sprintf('Obsah Url %s je podobný z %s%%', $link->url, $this->similarity));
        }

        return true;
    }

    /**
     * @param Link $link
     *
     * @return object
     */
    private function loadContents(Link $link)
    {
        return (object)[
            'old_content' => $this->getOldContent($link),
            'new_content' => $this->getNewContent($link)
        ];
    }

    /**
     * @param $contents
     *
     * @return object
     */
    private function tokenizeContents($contents)
    {
        return (object)[
            'old_tokens' => $this->tokenizer->tokenize($contents->old_content),
            'new_tokens' => $this->tokenizer->tokenize($contents->new_content)
        ];
    }

    /**
     * @param $tokens
     *
     * @return object
     */
    private function getFrequencies($tokens)
    {
        return (object)[
            'freq_old' => new FreqDist($tokens->old_tokens),
            'freq_new' => new FreqDist($tokens->new_tokens),
        ];
    }

    /**
     * @param Link $link
     * @param      $contents
     */
    private function saveDiff(Link $link, $contents)
    {
        Link::saveDiff(
            $link,
            $contents,
            $this->similarity,
            $this->difference
        );
    }
}

<?php

namespace App\Model;

use App\Support\Diff as TextDiff;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Link
 * @package App\Model
 */
class Link extends Model
{
    protected $dates = [
        'checked_at'
    ];

    protected $fillable = [
        'url',
        'content',
        'checked_at'
    ];

    public static function saveDiff(Link $link, $contents, $similarity, $difference)
    {
        $diff = TextDiff::compare($contents->old_content, $contents->new_content);

        $link->diffs()->create([
            'diff'       => base64_encode(TextDiff::toTable($diff)),
            'similarity' => $similarity,
            'difference' => $difference
        ]);
    }

    /**
     * @param Builder $query
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeForCheck(Builder $query)
    {
        return $query->whereDate('checked_at', '<', Carbon::now()->format('Y-m-d'));
    }

    public function diffs()
    {
        return $this->hasMany(Diff::class);
    }

    public function lastDiff()
    {
        return $this->diffs->last();
    }

    /**
     * @return string
     */
    public function getCacheKey()
    {
        return sprintf('pvis_link_%s', $this->id);
    }
}

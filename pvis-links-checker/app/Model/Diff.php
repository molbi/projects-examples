<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Diff extends Model
{
    protected $fillable = [
        'diff',
        'similarity',
        'difference'
    ];
}

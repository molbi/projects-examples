<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Source extends Model
{
    protected $fillable = [
        'title',
        'lang',
        'priority'
    ];

    public function links()
    {
        return $this->hasMany(Link::class);
    }
}

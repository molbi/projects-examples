<?php

namespace App\Providers;

use Html2Text\Html2Text;
use Illuminate\Support\ServiceProvider;

class Html2TextServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Html2Text::class, function () {
            return new Html2Text();
        });
    }
}

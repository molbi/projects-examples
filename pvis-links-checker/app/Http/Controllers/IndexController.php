<?php

namespace App\Http\Controllers;

use App\Model\Link;

/**
 * Class IndexController
 * @package App\Http\Controllers
 */
class IndexController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $links = Link::with('diffs')->get();

        return response()->view('index_admin', compact('links'));
    }

    public function show(Link $link)
    {
        $link->load([
            'diffs' => function ($query) {
                $query->orderBy('created_at', 'desc');
            }
        ]);

        return response()->view('show_admin', compact('link'));
    }
}
